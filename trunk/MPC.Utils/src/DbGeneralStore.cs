// DbGeneralStore.cs
// 
// Copyright (C) 2008 Bengt Thuree
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;
using System.Data;
using Npgsql;

namespace MPC.Utils
{
	
	public class DbItem
	{
		public DbItem ()
		{
			id = System.Int32.MinValue;			
		}
		private int id;
		public int Id {
			set { id = value; }			
			get { return id; }
		}		
	}
	
	public abstract class DbGeneralStore
	{
		
		private IDbConnection dbcon;
		private IDbCommand dbcmd;
		private IDataReader dbreader;
		private string TblName;
	
		public DbGeneralStore (IDbConnection _dbcon, string _TblName)
		{
			dbcon = _dbcon;
			TblName = _TblName;
		}

		public abstract DbItem DecodeReader (IDataReader reader);
		
		private void Setup(string _sql)
		{
			dbcmd = dbcon.CreateCommand();
			dbcmd.CommandText = _sql;			
			MPC.Utils.Log.Debug (String.Format (">>{0}<<", dbcmd.CommandText));
//			System.Console.WriteLine (String.Format (">>{0}<<", dbcmd.CommandText));								
		}
		
		private void CleanUp()
		{
			if (dbreader != null) {
				dbreader.Close();
				dbreader = null;
			}
			dbcmd.Dispose();		
			dbcmd = null;
		}
		
		
		private int OneDbCommandWithIntegerResult (string _sql)
		{
			int result = -1;
			try {
				Setup(_sql);
				result = System.Convert.ToInt32 ( dbcmd.ExecuteScalar() );
				MPC.Utils.Log.Debug (String.Format ("(Result = {0})", result));				
//				System.Console.WriteLine (String.Format ("(Result = {0})", result));				
				CleanUp();
			} catch (Exception ex)
			{
				if (ex.Message.StartsWith("ERROR: 23505:")) {
					MPC.Utils.NotUniqueException my_ex = new MPC.Utils.NotUniqueException();
					throw ( my_ex );
				} else		
				if (ex.Message.StartsWith("ERROR:  duplicate key value violates unique constraint")) {
					MPC.Utils.NotUniqueException my_ex = new MPC.Utils.NotUniqueException();
					throw ( my_ex );
				} else 
				{
					MPC.Utils.Log.Debug (String.Format ("Failed: {0}", _sql));			
					MPC.Utils.Log.Exception (ex);
				}
			}
			return result;
		}
		
		private void OneDbCommandNoResult (string _sql)
		{
			try {
				Setup(_sql);
				dbcmd.ExecuteNonQuery();
				CleanUp();
			} catch (Exception ex)
			{
				if (ex.Message.StartsWith("ERROR: 23505:")) {
					MPC.Utils.NotUniqueException my_ex = new MPC.Utils.NotUniqueException();
					throw ( my_ex );
				} else		
				if (ex.Message.StartsWith("ERROR:  duplicate key value violates unique constraint")) {
					MPC.Utils.NotUniqueException my_ex = new MPC.Utils.NotUniqueException();
					throw ( my_ex );
				} else 
				{
					MPC.Utils.Log.Debug (String.Format ("Failed: {0}", _sql));			
					MPC.Utils.Log.Exception (ex);
				}
			}
		}		
		
		private DbItem GetDbItem (string sql)
		{
			DbItem item = null;
			try {
				Setup(sql);
				dbreader = dbcmd.ExecuteReader();
				while(dbreader.Read()) {
					item = DecodeReader (dbreader);
				}
				CleanUp();
			} catch (Exception ex)
			{
				MPC.Utils.Log.Debug (String.Format ("Getting a row from {0} FAILED", TblName));			
				MPC.Utils.Log.Exception (ex);
				item = null;
			}
			return item;
		}

//FIXME - Not good lock/commit here
		public void BeginTran()
		{		
			OneDbCommandNoResult ("BEGIN WORK" );							
		}
		public void LockTable()
		{		
			BeginTran();							
			OneDbCommandNoResult (String.Format ("LOCK TABLE {0}", TblName ));				
		}
		
		
		public void UnlockTable()
		{
			OneDbCommandNoResult ("COMMIT WORK" );											
		}		
		public void EndTran()
		{
			OneDbCommandNoResult ("COMMIT WORK" );											
		}		
		public void RollbackTran()
		{
			OneDbCommandNoResult ("ROLLBACK WORK" );											
		}				
		
		public int Count ()
		{
			return OneDbCommandWithIntegerResult (String.Format ("SELECT COUNT(*)  FROM {0}", TblName));			
		}

		protected void Add (string sql)
		{
			OneDbCommandNoResult (sql);	
		}

		protected int Add_GetIndex (string sql)
		{
			int lastid = 0;
			try {			
				LockTable();		
				Add (sql);
				sql = String.Format ("SELECT MAX(id) as MAX_ID from {0} ", TblName); 
				Setup(sql);
				dbreader = dbcmd.ExecuteReader();
				while(dbreader.Read()) {
					lastid = dbreader.GetInt32(dbreader.GetOrdinal("max_id"));
				}
				CleanUp();
			} catch (Exception ex)
			{
				MPC.Utils.Log.Debug (String.Format ("Getting max(id) from {0} failed", TblName));			
				MPC.Utils.Log.Exception (ex);
				UnlockTable();				
				throw (ex);
			}
			UnlockTable();
			return lastid;
		}
				
		protected void Del (string sql)
		{
			OneDbCommandNoResult (sql);	
		}
		
		public void Del_All ()
		{
			OneDbCommandNoResult (String.Format ("TRUNCATE {0} CASCADE", TblName));			
		}


		protected void Change (string sql)
		{
			OneDbCommandNoResult (sql);	
		}

		protected DbItem Get (string sql)
		{
			return GetDbItem (sql);			
		}
		
		protected System.Collections.ArrayList GetList (string sql)
		{
			System.Collections.ArrayList c_db = new System.Collections.ArrayList();
			try {
				Setup(sql);
				dbreader = dbcmd.ExecuteReader();
				while(dbreader.Read()) {
					c_db.Add ( DecodeReader (dbreader) );
				}
				CleanUp();
			} catch (Exception ex)
			{
				MPC.Utils.Log.Debug ("Getting a a list of Currency from db FAILED");			
				MPC.Utils.Log.Exception (ex);
				c_db = null;
			}		
			return c_db;
		}
	}	
}
