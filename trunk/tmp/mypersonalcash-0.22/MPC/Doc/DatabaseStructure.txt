Perhaps not needed.
==WorldCurrency
	Country
	Currency Code


BasePool = Fetch exchange rates for every currency marked with Fetch, and convert it to each currency marked as BasePool.
==Currency
	Currency Code		3 chars		Unique
	Currency Name
	Symbol
	Fetch					Yes / No
	BasePool				Yes / No

==Quote Source
	Uri
	Encoding				utf-8
	Name
	Date Format				Y M D, D M Y etc
	Date Row RegExp				Pick out Date row from page
	Date RegExp				Pick out date from date row
	Quote RegExp				Pick out Quote from Symbol row
	Symbol RegExp				Pick out ROW with Symbol

==Security
	Symbol
	Name
	Quote Source
	Currency Code
	
==Security Quotes
	Date
	Symbol
	Quote
	Type		(Manual, Auto, Transaction)

== Currency Rates
	Date
	Symbol
	Base
	Exchange Rate
	
