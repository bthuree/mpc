// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by a tool.
//      Mono Runtime Version: 2.0.50727.42
// 
//      Changes to this file may cause incorrect behavior and will be lost if 
//      the code is regenerated.
//  </autogenerated>
// ------------------------------------------------------------------------------

namespace MPC.Quotes {
    
    
    public partial class CurrencyGui {
        
        private Gtk.Action newAction;
        
        private Gtk.Action deleteAction;
        
        private Gtk.Action DefaultAction;
        
        private Gtk.Toolbar toolbar6;
        
        private Gtk.VBox vbox2;
        
        private Gtk.HBox hbox1;
        
        private Gtk.ScrolledWindow GtkScrolledWindow;
        
        private Gtk.TreeView CurrencyTreeView;
        
        private Gtk.Frame CurrencyDetails_Frame;
        
        private Gtk.Alignment GtkAlignment3;
        
        private Gtk.Table CurrencyDetailsTable;
        
        private Gtk.Entry CurrencyDetailsCountry;
        
        private Gtk.HBox hbox2;
        
        private Gtk.Entry CurrencyDetailsCode;
        
        private Gtk.CheckButton CurrencyDetailsBasePool;
        
        private Gtk.Label label1;
        
        private Gtk.Label label2;
        
        private Gtk.Label GtkLabel5;
        
        private Gtk.Button buttonCancel;
        
        private Gtk.Button buttonOk;
        
        private Gtk.Button buttonSave;
        
        protected virtual void Build() {
            Stetic.Gui.Initialize(this);
            // Widget MPC.Quotes.CurrencyGui
            Gtk.UIManager w1 = new Gtk.UIManager();
            Gtk.ActionGroup w2 = new Gtk.ActionGroup("Default");
            this.newAction = new Gtk.Action("newAction", null, null, "gtk-new");
            w2.Add(this.newAction, null);
            this.deleteAction = new Gtk.Action("deleteAction", null, null, "gtk-delete");
            w2.Add(this.deleteAction, null);
            this.DefaultAction = new Gtk.Action("DefaultAction", Mono.Unix.Catalog.GetString("De_fault"), null, "gtk-add");
            this.DefaultAction.ShortLabel = Mono.Unix.Catalog.GetString("De_fault");
            w2.Add(this.DefaultAction, null);
            w1.InsertActionGroup(w2, 0);
            this.AddAccelGroup(w1.AccelGroup);
            this.Name = "MPC.Quotes.CurrencyGui";
            this.Title = Mono.Unix.Catalog.GetString("Specify currencies");
            this.WindowPosition = ((Gtk.WindowPosition)(4));
            this.HasSeparator = false;
            // Internal child MPC.Quotes.CurrencyGui.VBox
            Gtk.VBox w3 = this.VBox;
            w3.Name = "dialog1_VBox";
            w3.BorderWidth = ((uint)(2));
            // Container child dialog1_VBox.Gtk.Box+BoxChild
            w1.AddUiFromString("<ui><toolbar name='toolbar6'><toolitem action='newAction'/><toolitem action='deleteAction'/><toolitem action='DefaultAction'/></toolbar></ui>");
            this.toolbar6 = ((Gtk.Toolbar)(w1.GetWidget("/toolbar6")));
            this.toolbar6.Name = "toolbar6";
            this.toolbar6.ShowArrow = false;
            this.toolbar6.ToolbarStyle = ((Gtk.ToolbarStyle)(2));
            this.toolbar6.IconSize = ((Gtk.IconSize)(2));
            w3.Add(this.toolbar6);
            Gtk.Box.BoxChild w4 = ((Gtk.Box.BoxChild)(w3[this.toolbar6]));
            w4.Position = 0;
            w4.Expand = false;
            w4.Fill = false;
            // Container child dialog1_VBox.Gtk.Box+BoxChild
            this.vbox2 = new Gtk.VBox();
            this.vbox2.Name = "vbox2";
            this.vbox2.Spacing = 6;
            // Container child vbox2.Gtk.Box+BoxChild
            this.hbox1 = new Gtk.HBox();
            this.hbox1.Name = "hbox1";
            this.hbox1.Spacing = 6;
            // Container child hbox1.Gtk.Box+BoxChild
            this.GtkScrolledWindow = new Gtk.ScrolledWindow();
            this.GtkScrolledWindow.Name = "GtkScrolledWindow";
            this.GtkScrolledWindow.ShadowType = ((Gtk.ShadowType)(1));
            // Container child GtkScrolledWindow.Gtk.Container+ContainerChild
            this.CurrencyTreeView = new Gtk.TreeView();
            this.CurrencyTreeView.CanFocus = true;
            this.CurrencyTreeView.Name = "CurrencyTreeView";
            this.CurrencyTreeView.HeadersClickable = true;
            this.GtkScrolledWindow.Add(this.CurrencyTreeView);
            this.hbox1.Add(this.GtkScrolledWindow);
            Gtk.Box.BoxChild w6 = ((Gtk.Box.BoxChild)(this.hbox1[this.GtkScrolledWindow]));
            w6.Position = 0;
            this.vbox2.Add(this.hbox1);
            Gtk.Box.BoxChild w7 = ((Gtk.Box.BoxChild)(this.vbox2[this.hbox1]));
            w7.Position = 0;
            // Container child vbox2.Gtk.Box+BoxChild
            this.CurrencyDetails_Frame = new Gtk.Frame();
            this.CurrencyDetails_Frame.Name = "CurrencyDetails_Frame";
            this.CurrencyDetails_Frame.ShadowType = ((Gtk.ShadowType)(0));
            // Container child CurrencyDetails_Frame.Gtk.Container+ContainerChild
            this.GtkAlignment3 = new Gtk.Alignment(0F, 0F, 1F, 1F);
            this.GtkAlignment3.Name = "GtkAlignment3";
            this.GtkAlignment3.LeftPadding = ((uint)(12));
            // Container child GtkAlignment3.Gtk.Container+ContainerChild
            this.CurrencyDetailsTable = new Gtk.Table(((uint)(2)), ((uint)(2)), false);
            this.CurrencyDetailsTable.Name = "CurrencyDetailsTable";
            this.CurrencyDetailsTable.RowSpacing = ((uint)(6));
            this.CurrencyDetailsTable.ColumnSpacing = ((uint)(6));
            // Container child CurrencyDetailsTable.Gtk.Table+TableChild
            this.CurrencyDetailsCountry = new Gtk.Entry();
            this.CurrencyDetailsCountry.CanFocus = true;
            this.CurrencyDetailsCountry.Name = "CurrencyDetailsCountry";
            this.CurrencyDetailsCountry.IsEditable = true;
            this.CurrencyDetailsCountry.InvisibleChar = '●';
            this.CurrencyDetailsTable.Add(this.CurrencyDetailsCountry);
            Gtk.Table.TableChild w8 = ((Gtk.Table.TableChild)(this.CurrencyDetailsTable[this.CurrencyDetailsCountry]));
            w8.TopAttach = ((uint)(1));
            w8.BottomAttach = ((uint)(2));
            w8.LeftAttach = ((uint)(1));
            w8.RightAttach = ((uint)(2));
            w8.YOptions = ((Gtk.AttachOptions)(4));
            // Container child CurrencyDetailsTable.Gtk.Table+TableChild
            this.hbox2 = new Gtk.HBox();
            this.hbox2.Name = "hbox2";
            this.hbox2.Spacing = 6;
            // Container child hbox2.Gtk.Box+BoxChild
            this.CurrencyDetailsCode = new Gtk.Entry();
            this.CurrencyDetailsCode.CanFocus = true;
            this.CurrencyDetailsCode.Name = "CurrencyDetailsCode";
            this.CurrencyDetailsCode.IsEditable = true;
            this.CurrencyDetailsCode.InvisibleChar = '●';
            this.hbox2.Add(this.CurrencyDetailsCode);
            Gtk.Box.BoxChild w9 = ((Gtk.Box.BoxChild)(this.hbox2[this.CurrencyDetailsCode]));
            w9.Position = 0;
            // Container child hbox2.Gtk.Box+BoxChild
            this.CurrencyDetailsBasePool = new Gtk.CheckButton();
            this.CurrencyDetailsBasePool.CanFocus = true;
            this.CurrencyDetailsBasePool.Name = "CurrencyDetailsBasePool";
            this.CurrencyDetailsBasePool.Label = Mono.Unix.Catalog.GetString("Belongs to base pool");
            this.CurrencyDetailsBasePool.DrawIndicator = true;
            this.CurrencyDetailsBasePool.UseUnderline = true;
            this.hbox2.Add(this.CurrencyDetailsBasePool);
            Gtk.Box.BoxChild w10 = ((Gtk.Box.BoxChild)(this.hbox2[this.CurrencyDetailsBasePool]));
            w10.Position = 1;
            this.CurrencyDetailsTable.Add(this.hbox2);
            Gtk.Table.TableChild w11 = ((Gtk.Table.TableChild)(this.CurrencyDetailsTable[this.hbox2]));
            w11.LeftAttach = ((uint)(1));
            w11.RightAttach = ((uint)(2));
            w11.YOptions = ((Gtk.AttachOptions)(4));
            // Container child CurrencyDetailsTable.Gtk.Table+TableChild
            this.label1 = new Gtk.Label();
            this.label1.Name = "label1";
            this.label1.Xalign = 1F;
            this.label1.LabelProp = Mono.Unix.Catalog.GetString("Code");
            this.CurrencyDetailsTable.Add(this.label1);
            Gtk.Table.TableChild w12 = ((Gtk.Table.TableChild)(this.CurrencyDetailsTable[this.label1]));
            w12.XOptions = ((Gtk.AttachOptions)(4));
            w12.YOptions = ((Gtk.AttachOptions)(4));
            // Container child CurrencyDetailsTable.Gtk.Table+TableChild
            this.label2 = new Gtk.Label();
            this.label2.Name = "label2";
            this.label2.Xalign = 1F;
            this.label2.LabelProp = Mono.Unix.Catalog.GetString("Country");
            this.CurrencyDetailsTable.Add(this.label2);
            Gtk.Table.TableChild w13 = ((Gtk.Table.TableChild)(this.CurrencyDetailsTable[this.label2]));
            w13.TopAttach = ((uint)(1));
            w13.BottomAttach = ((uint)(2));
            w13.XOptions = ((Gtk.AttachOptions)(4));
            w13.YOptions = ((Gtk.AttachOptions)(4));
            this.GtkAlignment3.Add(this.CurrencyDetailsTable);
            this.CurrencyDetails_Frame.Add(this.GtkAlignment3);
            this.GtkLabel5 = new Gtk.Label();
            this.GtkLabel5.Name = "GtkLabel5";
            this.GtkLabel5.LabelProp = Mono.Unix.Catalog.GetString("<b>Currency details</b>");
            this.GtkLabel5.UseMarkup = true;
            this.CurrencyDetails_Frame.LabelWidget = this.GtkLabel5;
            this.vbox2.Add(this.CurrencyDetails_Frame);
            Gtk.Box.BoxChild w16 = ((Gtk.Box.BoxChild)(this.vbox2[this.CurrencyDetails_Frame]));
            w16.Position = 1;
            w16.Expand = false;
            w16.Fill = false;
            w3.Add(this.vbox2);
            Gtk.Box.BoxChild w17 = ((Gtk.Box.BoxChild)(w3[this.vbox2]));
            w17.Position = 1;
            // Internal child MPC.Quotes.CurrencyGui.ActionArea
            Gtk.HButtonBox w18 = this.ActionArea;
            w18.Name = "dialog1_ActionArea";
            w18.Spacing = 6;
            w18.BorderWidth = ((uint)(5));
            w18.LayoutStyle = ((Gtk.ButtonBoxStyle)(4));
            // Container child dialog1_ActionArea.Gtk.ButtonBox+ButtonBoxChild
            this.buttonCancel = new Gtk.Button();
            this.buttonCancel.CanDefault = true;
            this.buttonCancel.CanFocus = true;
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.UseStock = true;
            this.buttonCancel.UseUnderline = true;
            this.buttonCancel.Label = "gtk-cancel";
            this.AddActionWidget(this.buttonCancel, -6);
            Gtk.ButtonBox.ButtonBoxChild w19 = ((Gtk.ButtonBox.ButtonBoxChild)(w18[this.buttonCancel]));
            w19.Expand = false;
            w19.Fill = false;
            // Container child dialog1_ActionArea.Gtk.ButtonBox+ButtonBoxChild
            this.buttonOk = new Gtk.Button();
            this.buttonOk.CanDefault = true;
            this.buttonOk.CanFocus = true;
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.UseStock = true;
            this.buttonOk.UseUnderline = true;
            this.buttonOk.Label = "gtk-ok";
            this.AddActionWidget(this.buttonOk, -5);
            Gtk.ButtonBox.ButtonBoxChild w20 = ((Gtk.ButtonBox.ButtonBoxChild)(w18[this.buttonOk]));
            w20.Position = 1;
            w20.Expand = false;
            w20.Fill = false;
            // Container child dialog1_ActionArea.Gtk.ButtonBox+ButtonBoxChild
            this.buttonSave = new Gtk.Button();
            this.buttonSave.CanFocus = true;
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.UseStock = true;
            this.buttonSave.UseUnderline = true;
            this.buttonSave.Label = "gtk-save";
            this.AddActionWidget(this.buttonSave, 0);
            Gtk.ButtonBox.ButtonBoxChild w21 = ((Gtk.ButtonBox.ButtonBoxChild)(w18[this.buttonSave]));
            w21.Position = 2;
            w21.Expand = false;
            w21.Fill = false;
            if ((this.Child != null)) {
                this.Child.ShowAll();
            }
            this.DefaultWidth = 408;
            this.DefaultHeight = 324;
            this.Show();
            this.newAction.Activated += new System.EventHandler(this.OnNewActionActivated);
            this.deleteAction.Activated += new System.EventHandler(this.OnDeleteActionActivated);
            this.DefaultAction.Activated += new System.EventHandler(this.OnDefaultActionActivated);
            this.CurrencyTreeView.CursorChanged += new System.EventHandler(this.OnCurrencyTreeViewCursorChanged);
            this.buttonCancel.Clicked += new System.EventHandler(this.OnButtonCancelClicked);
            this.buttonOk.Clicked += new System.EventHandler(this.OnButtonOkClicked);
            this.buttonSave.Clicked += new System.EventHandler(this.OnButtonSaveClicked);
        }
    }
}
