// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by a tool.
//      Mono Runtime Version: 2.0.50727.42
// 
//      Changes to this file may cause incorrect behavior and will be lost if 
//      the code is regenerated.
//  </autogenerated>
// ------------------------------------------------------------------------------

namespace MPC.Accounts {
    
    
    public partial class ContactGui {
        
        private Gtk.Action newAction;
        
        private Gtk.Action deleteAction;
        
        private Gtk.VBox vbox2;
        
        private Gtk.Toolbar toolbar1;
        
        private Gtk.Frame frame1;
        
        private Gtk.Alignment GtkAlignment5;
        
        private Gtk.ScrolledWindow GtkScrolledWindow;
        
        private Gtk.TreeView treeview;
        
        private Gtk.Label GtkLabel5;
        
        private Gtk.Frame frame2;
        
        private Gtk.Alignment GtkAlignment6;
        
        private Gtk.VBox vbox3;
        
        private Gtk.Table table1;
        
        private Gtk.ComboBox details_address;
        
        private Gtk.Entry details_fax;
        
        private Gtk.Entry details_mobile;
        
        private Gtk.Entry details_name;
        
        private Gtk.Entry details_other_mail;
        
        private Gtk.Entry details_other_phone;
        
        private Gtk.Entry details_phone;
        
        private Gtk.Entry details_private_mail;
        
        private Gtk.Entry details_uri;
        
        private Gtk.Entry details_work_mail;
        
        private Gtk.Label label10;
        
        private Gtk.Label label11;
        
        private Gtk.Label label12;
        
        private Gtk.Label label2;
        
        private Gtk.Label label3;
        
        private Gtk.Label label4;
        
        private Gtk.Label label5;
        
        private Gtk.Label label6;
        
        private Gtk.Label label8;
        
        private Gtk.Label label9;
        
        private Gtk.HBox hbox1;
        
        private Gtk.Label label1;
        
        private Gtk.Entry details_notes;
        
        private Gtk.Label GtkLabel6;
        
        private Gtk.Button buttonCancel;
        
        private Gtk.Button buttonOk;
        
        private Gtk.Button buttonSave;
        
        protected virtual void Build() {
            Stetic.Gui.Initialize(this);
            // Widget MPC.Accounts.ContactGui
            Gtk.UIManager w1 = new Gtk.UIManager();
            Gtk.ActionGroup w2 = new Gtk.ActionGroup("Default");
            this.newAction = new Gtk.Action("newAction", null, null, "gtk-new");
            w2.Add(this.newAction, null);
            this.deleteAction = new Gtk.Action("deleteAction", null, null, "gtk-delete");
            w2.Add(this.deleteAction, null);
            w1.InsertActionGroup(w2, 0);
            this.AddAccelGroup(w1.AccelGroup);
            this.Name = "MPC.Accounts.ContactGui";
            this.Title = Mono.Unix.Catalog.GetString("Define contacts");
            this.WindowPosition = ((Gtk.WindowPosition)(4));
            this.HasSeparator = false;
            // Internal child MPC.Accounts.ContactGui.VBox
            Gtk.VBox w3 = this.VBox;
            w3.Name = "dialog1_VBox";
            w3.BorderWidth = ((uint)(2));
            // Container child dialog1_VBox.Gtk.Box+BoxChild
            this.vbox2 = new Gtk.VBox();
            this.vbox2.Name = "vbox2";
            this.vbox2.Spacing = 6;
            // Container child vbox2.Gtk.Box+BoxChild
            w1.AddUiFromString("<ui><toolbar name='toolbar1'><toolitem action='newAction'/><toolitem action='deleteAction'/></toolbar></ui>");
            this.toolbar1 = ((Gtk.Toolbar)(w1.GetWidget("/toolbar1")));
            this.toolbar1.Name = "toolbar1";
            this.toolbar1.ShowArrow = false;
            this.toolbar1.ToolbarStyle = ((Gtk.ToolbarStyle)(0));
            this.vbox2.Add(this.toolbar1);
            Gtk.Box.BoxChild w4 = ((Gtk.Box.BoxChild)(this.vbox2[this.toolbar1]));
            w4.Position = 0;
            w4.Expand = false;
            w4.Fill = false;
            // Container child vbox2.Gtk.Box+BoxChild
            this.frame1 = new Gtk.Frame();
            this.frame1.Name = "frame1";
            this.frame1.ShadowType = ((Gtk.ShadowType)(0));
            // Container child frame1.Gtk.Container+ContainerChild
            this.GtkAlignment5 = new Gtk.Alignment(0F, 0F, 1F, 1F);
            this.GtkAlignment5.Name = "GtkAlignment5";
            this.GtkAlignment5.LeftPadding = ((uint)(12));
            // Container child GtkAlignment5.Gtk.Container+ContainerChild
            this.GtkScrolledWindow = new Gtk.ScrolledWindow();
            this.GtkScrolledWindow.Name = "GtkScrolledWindow";
            this.GtkScrolledWindow.ShadowType = ((Gtk.ShadowType)(1));
            // Container child GtkScrolledWindow.Gtk.Container+ContainerChild
            this.treeview = new Gtk.TreeView();
            this.treeview.CanFocus = true;
            this.treeview.Name = "treeview";
            this.treeview.HeadersClickable = true;
            this.GtkScrolledWindow.Add(this.treeview);
            this.GtkAlignment5.Add(this.GtkScrolledWindow);
            this.frame1.Add(this.GtkAlignment5);
            this.GtkLabel5 = new Gtk.Label();
            this.GtkLabel5.Name = "GtkLabel5";
            this.GtkLabel5.LabelProp = Mono.Unix.Catalog.GetString("<b>Contacts</b>");
            this.GtkLabel5.UseMarkup = true;
            this.frame1.LabelWidget = this.GtkLabel5;
            this.vbox2.Add(this.frame1);
            Gtk.Box.BoxChild w8 = ((Gtk.Box.BoxChild)(this.vbox2[this.frame1]));
            w8.Position = 1;
            // Container child vbox2.Gtk.Box+BoxChild
            this.frame2 = new Gtk.Frame();
            this.frame2.Name = "frame2";
            this.frame2.ShadowType = ((Gtk.ShadowType)(0));
            // Container child frame2.Gtk.Container+ContainerChild
            this.GtkAlignment6 = new Gtk.Alignment(0F, 0F, 1F, 1F);
            this.GtkAlignment6.Name = "GtkAlignment6";
            this.GtkAlignment6.LeftPadding = ((uint)(12));
            // Container child GtkAlignment6.Gtk.Container+ContainerChild
            this.vbox3 = new Gtk.VBox();
            this.vbox3.Name = "vbox3";
            this.vbox3.Spacing = 6;
            // Container child vbox3.Gtk.Box+BoxChild
            this.table1 = new Gtk.Table(((uint)(5)), ((uint)(4)), false);
            this.table1.Name = "table1";
            this.table1.RowSpacing = ((uint)(6));
            this.table1.ColumnSpacing = ((uint)(6));
            // Container child table1.Gtk.Table+TableChild
            this.details_address = Gtk.ComboBox.NewText();
            this.details_address.Name = "details_address";
            this.table1.Add(this.details_address);
            Gtk.Table.TableChild w9 = ((Gtk.Table.TableChild)(this.table1[this.details_address]));
            w9.TopAttach = ((uint)(3));
            w9.BottomAttach = ((uint)(4));
            w9.LeftAttach = ((uint)(3));
            w9.RightAttach = ((uint)(4));
            w9.XOptions = ((Gtk.AttachOptions)(4));
            w9.YOptions = ((Gtk.AttachOptions)(4));
            // Container child table1.Gtk.Table+TableChild
            this.details_fax = new Gtk.Entry();
            this.details_fax.CanFocus = true;
            this.details_fax.Name = "details_fax";
            this.details_fax.IsEditable = true;
            this.details_fax.InvisibleChar = '●';
            this.table1.Add(this.details_fax);
            Gtk.Table.TableChild w10 = ((Gtk.Table.TableChild)(this.table1[this.details_fax]));
            w10.TopAttach = ((uint)(4));
            w10.BottomAttach = ((uint)(5));
            w10.LeftAttach = ((uint)(1));
            w10.RightAttach = ((uint)(2));
            w10.YOptions = ((Gtk.AttachOptions)(4));
            // Container child table1.Gtk.Table+TableChild
            this.details_mobile = new Gtk.Entry();
            this.details_mobile.CanFocus = true;
            this.details_mobile.Name = "details_mobile";
            this.details_mobile.IsEditable = true;
            this.details_mobile.InvisibleChar = '●';
            this.table1.Add(this.details_mobile);
            Gtk.Table.TableChild w11 = ((Gtk.Table.TableChild)(this.table1[this.details_mobile]));
            w11.TopAttach = ((uint)(2));
            w11.BottomAttach = ((uint)(3));
            w11.LeftAttach = ((uint)(1));
            w11.RightAttach = ((uint)(2));
            w11.YOptions = ((Gtk.AttachOptions)(4));
            // Container child table1.Gtk.Table+TableChild
            this.details_name = new Gtk.Entry();
            this.details_name.CanFocus = true;
            this.details_name.Name = "details_name";
            this.details_name.IsEditable = true;
            this.details_name.InvisibleChar = '●';
            this.table1.Add(this.details_name);
            Gtk.Table.TableChild w12 = ((Gtk.Table.TableChild)(this.table1[this.details_name]));
            w12.LeftAttach = ((uint)(1));
            w12.RightAttach = ((uint)(2));
            w12.YOptions = ((Gtk.AttachOptions)(4));
            // Container child table1.Gtk.Table+TableChild
            this.details_other_mail = new Gtk.Entry();
            this.details_other_mail.CanFocus = true;
            this.details_other_mail.Name = "details_other_mail";
            this.details_other_mail.IsEditable = true;
            this.details_other_mail.InvisibleChar = '●';
            this.table1.Add(this.details_other_mail);
            Gtk.Table.TableChild w13 = ((Gtk.Table.TableChild)(this.table1[this.details_other_mail]));
            w13.TopAttach = ((uint)(2));
            w13.BottomAttach = ((uint)(3));
            w13.LeftAttach = ((uint)(3));
            w13.RightAttach = ((uint)(4));
            w13.XOptions = ((Gtk.AttachOptions)(4));
            w13.YOptions = ((Gtk.AttachOptions)(4));
            // Container child table1.Gtk.Table+TableChild
            this.details_other_phone = new Gtk.Entry();
            this.details_other_phone.CanFocus = true;
            this.details_other_phone.Name = "details_other_phone";
            this.details_other_phone.IsEditable = true;
            this.details_other_phone.InvisibleChar = '●';
            this.table1.Add(this.details_other_phone);
            Gtk.Table.TableChild w14 = ((Gtk.Table.TableChild)(this.table1[this.details_other_phone]));
            w14.TopAttach = ((uint)(3));
            w14.BottomAttach = ((uint)(4));
            w14.LeftAttach = ((uint)(1));
            w14.RightAttach = ((uint)(2));
            w14.YOptions = ((Gtk.AttachOptions)(4));
            // Container child table1.Gtk.Table+TableChild
            this.details_phone = new Gtk.Entry();
            this.details_phone.CanFocus = true;
            this.details_phone.Name = "details_phone";
            this.details_phone.IsEditable = true;
            this.details_phone.InvisibleChar = '●';
            this.table1.Add(this.details_phone);
            Gtk.Table.TableChild w15 = ((Gtk.Table.TableChild)(this.table1[this.details_phone]));
            w15.TopAttach = ((uint)(1));
            w15.BottomAttach = ((uint)(2));
            w15.LeftAttach = ((uint)(1));
            w15.RightAttach = ((uint)(2));
            w15.YOptions = ((Gtk.AttachOptions)(4));
            // Container child table1.Gtk.Table+TableChild
            this.details_private_mail = new Gtk.Entry();
            this.details_private_mail.CanFocus = true;
            this.details_private_mail.Name = "details_private_mail";
            this.details_private_mail.IsEditable = true;
            this.details_private_mail.InvisibleChar = '●';
            this.table1.Add(this.details_private_mail);
            Gtk.Table.TableChild w16 = ((Gtk.Table.TableChild)(this.table1[this.details_private_mail]));
            w16.TopAttach = ((uint)(1));
            w16.BottomAttach = ((uint)(2));
            w16.LeftAttach = ((uint)(3));
            w16.RightAttach = ((uint)(4));
            w16.XOptions = ((Gtk.AttachOptions)(4));
            w16.YOptions = ((Gtk.AttachOptions)(4));
            // Container child table1.Gtk.Table+TableChild
            this.details_uri = new Gtk.Entry();
            this.details_uri.CanFocus = true;
            this.details_uri.Name = "details_uri";
            this.details_uri.IsEditable = true;
            this.details_uri.InvisibleChar = '●';
            this.table1.Add(this.details_uri);
            Gtk.Table.TableChild w17 = ((Gtk.Table.TableChild)(this.table1[this.details_uri]));
            w17.TopAttach = ((uint)(4));
            w17.BottomAttach = ((uint)(5));
            w17.LeftAttach = ((uint)(3));
            w17.RightAttach = ((uint)(4));
            w17.XOptions = ((Gtk.AttachOptions)(4));
            w17.YOptions = ((Gtk.AttachOptions)(4));
            // Container child table1.Gtk.Table+TableChild
            this.details_work_mail = new Gtk.Entry();
            this.details_work_mail.CanFocus = true;
            this.details_work_mail.Name = "details_work_mail";
            this.details_work_mail.IsEditable = true;
            this.details_work_mail.InvisibleChar = '●';
            this.table1.Add(this.details_work_mail);
            Gtk.Table.TableChild w18 = ((Gtk.Table.TableChild)(this.table1[this.details_work_mail]));
            w18.LeftAttach = ((uint)(3));
            w18.RightAttach = ((uint)(4));
            w18.XOptions = ((Gtk.AttachOptions)(4));
            w18.YOptions = ((Gtk.AttachOptions)(4));
            // Container child table1.Gtk.Table+TableChild
            this.label10 = new Gtk.Label();
            this.label10.Name = "label10";
            this.label10.Xalign = 1F;
            this.label10.LabelProp = Mono.Unix.Catalog.GetString("Other mail");
            this.table1.Add(this.label10);
            Gtk.Table.TableChild w19 = ((Gtk.Table.TableChild)(this.table1[this.label10]));
            w19.TopAttach = ((uint)(2));
            w19.BottomAttach = ((uint)(3));
            w19.LeftAttach = ((uint)(2));
            w19.RightAttach = ((uint)(3));
            w19.XOptions = ((Gtk.AttachOptions)(4));
            w19.YOptions = ((Gtk.AttachOptions)(4));
            // Container child table1.Gtk.Table+TableChild
            this.label11 = new Gtk.Label();
            this.label11.Name = "label11";
            this.label11.Xalign = 1F;
            this.label11.LabelProp = Mono.Unix.Catalog.GetString("Address");
            this.table1.Add(this.label11);
            Gtk.Table.TableChild w20 = ((Gtk.Table.TableChild)(this.table1[this.label11]));
            w20.TopAttach = ((uint)(3));
            w20.BottomAttach = ((uint)(4));
            w20.LeftAttach = ((uint)(2));
            w20.RightAttach = ((uint)(3));
            w20.XOptions = ((Gtk.AttachOptions)(4));
            w20.YOptions = ((Gtk.AttachOptions)(4));
            // Container child table1.Gtk.Table+TableChild
            this.label12 = new Gtk.Label();
            this.label12.Name = "label12";
            this.label12.Xalign = 1F;
            this.label12.LabelProp = Mono.Unix.Catalog.GetString("Uri");
            this.table1.Add(this.label12);
            Gtk.Table.TableChild w21 = ((Gtk.Table.TableChild)(this.table1[this.label12]));
            w21.TopAttach = ((uint)(4));
            w21.BottomAttach = ((uint)(5));
            w21.LeftAttach = ((uint)(2));
            w21.RightAttach = ((uint)(3));
            w21.XOptions = ((Gtk.AttachOptions)(4));
            w21.YOptions = ((Gtk.AttachOptions)(4));
            // Container child table1.Gtk.Table+TableChild
            this.label2 = new Gtk.Label();
            this.label2.Name = "label2";
            this.label2.Xalign = 1F;
            this.label2.LabelProp = Mono.Unix.Catalog.GetString("Name");
            this.table1.Add(this.label2);
            Gtk.Table.TableChild w22 = ((Gtk.Table.TableChild)(this.table1[this.label2]));
            w22.XOptions = ((Gtk.AttachOptions)(4));
            w22.YOptions = ((Gtk.AttachOptions)(4));
            // Container child table1.Gtk.Table+TableChild
            this.label3 = new Gtk.Label();
            this.label3.Name = "label3";
            this.label3.Xalign = 1F;
            this.label3.LabelProp = Mono.Unix.Catalog.GetString("Phone");
            this.table1.Add(this.label3);
            Gtk.Table.TableChild w23 = ((Gtk.Table.TableChild)(this.table1[this.label3]));
            w23.TopAttach = ((uint)(1));
            w23.BottomAttach = ((uint)(2));
            w23.XOptions = ((Gtk.AttachOptions)(4));
            w23.YOptions = ((Gtk.AttachOptions)(4));
            // Container child table1.Gtk.Table+TableChild
            this.label4 = new Gtk.Label();
            this.label4.Name = "label4";
            this.label4.Xalign = 1F;
            this.label4.LabelProp = Mono.Unix.Catalog.GetString("Mobile");
            this.table1.Add(this.label4);
            Gtk.Table.TableChild w24 = ((Gtk.Table.TableChild)(this.table1[this.label4]));
            w24.TopAttach = ((uint)(2));
            w24.BottomAttach = ((uint)(3));
            w24.XOptions = ((Gtk.AttachOptions)(4));
            w24.YOptions = ((Gtk.AttachOptions)(4));
            // Container child table1.Gtk.Table+TableChild
            this.label5 = new Gtk.Label();
            this.label5.Name = "label5";
            this.label5.Xalign = 1F;
            this.label5.LabelProp = Mono.Unix.Catalog.GetString("Other phone");
            this.table1.Add(this.label5);
            Gtk.Table.TableChild w25 = ((Gtk.Table.TableChild)(this.table1[this.label5]));
            w25.TopAttach = ((uint)(3));
            w25.BottomAttach = ((uint)(4));
            w25.XOptions = ((Gtk.AttachOptions)(4));
            w25.YOptions = ((Gtk.AttachOptions)(4));
            // Container child table1.Gtk.Table+TableChild
            this.label6 = new Gtk.Label();
            this.label6.Name = "label6";
            this.label6.Xalign = 1F;
            this.label6.LabelProp = Mono.Unix.Catalog.GetString("FAX");
            this.table1.Add(this.label6);
            Gtk.Table.TableChild w26 = ((Gtk.Table.TableChild)(this.table1[this.label6]));
            w26.TopAttach = ((uint)(4));
            w26.BottomAttach = ((uint)(5));
            w26.XOptions = ((Gtk.AttachOptions)(4));
            w26.YOptions = ((Gtk.AttachOptions)(4));
            // Container child table1.Gtk.Table+TableChild
            this.label8 = new Gtk.Label();
            this.label8.Name = "label8";
            this.label8.Xalign = 1F;
            this.label8.LabelProp = Mono.Unix.Catalog.GetString("Work mail");
            this.table1.Add(this.label8);
            Gtk.Table.TableChild w27 = ((Gtk.Table.TableChild)(this.table1[this.label8]));
            w27.LeftAttach = ((uint)(2));
            w27.RightAttach = ((uint)(3));
            w27.XOptions = ((Gtk.AttachOptions)(4));
            w27.YOptions = ((Gtk.AttachOptions)(4));
            // Container child table1.Gtk.Table+TableChild
            this.label9 = new Gtk.Label();
            this.label9.Name = "label9";
            this.label9.Xalign = 1F;
            this.label9.LabelProp = Mono.Unix.Catalog.GetString("Private mail");
            this.table1.Add(this.label9);
            Gtk.Table.TableChild w28 = ((Gtk.Table.TableChild)(this.table1[this.label9]));
            w28.TopAttach = ((uint)(1));
            w28.BottomAttach = ((uint)(2));
            w28.LeftAttach = ((uint)(2));
            w28.RightAttach = ((uint)(3));
            w28.XOptions = ((Gtk.AttachOptions)(4));
            w28.YOptions = ((Gtk.AttachOptions)(4));
            this.vbox3.Add(this.table1);
            Gtk.Box.BoxChild w29 = ((Gtk.Box.BoxChild)(this.vbox3[this.table1]));
            w29.Position = 0;
            w29.Expand = false;
            w29.Fill = false;
            // Container child vbox3.Gtk.Box+BoxChild
            this.hbox1 = new Gtk.HBox();
            this.hbox1.Name = "hbox1";
            this.hbox1.Spacing = 6;
            // Container child hbox1.Gtk.Box+BoxChild
            this.label1 = new Gtk.Label();
            this.label1.Name = "label1";
            this.label1.Xalign = 1F;
            this.label1.LabelProp = Mono.Unix.Catalog.GetString("Notes");
            this.hbox1.Add(this.label1);
            Gtk.Box.BoxChild w30 = ((Gtk.Box.BoxChild)(this.hbox1[this.label1]));
            w30.Position = 0;
            w30.Expand = false;
            w30.Fill = false;
            // Container child hbox1.Gtk.Box+BoxChild
            this.details_notes = new Gtk.Entry();
            this.details_notes.CanFocus = true;
            this.details_notes.Name = "details_notes";
            this.details_notes.IsEditable = true;
            this.details_notes.InvisibleChar = '●';
            this.hbox1.Add(this.details_notes);
            Gtk.Box.BoxChild w31 = ((Gtk.Box.BoxChild)(this.hbox1[this.details_notes]));
            w31.Position = 1;
            this.vbox3.Add(this.hbox1);
            Gtk.Box.BoxChild w32 = ((Gtk.Box.BoxChild)(this.vbox3[this.hbox1]));
            w32.Position = 1;
            w32.Expand = false;
            w32.Fill = false;
            this.GtkAlignment6.Add(this.vbox3);
            this.frame2.Add(this.GtkAlignment6);
            this.GtkLabel6 = new Gtk.Label();
            this.GtkLabel6.Name = "GtkLabel6";
            this.GtkLabel6.LabelProp = Mono.Unix.Catalog.GetString("<b>Details</b>");
            this.GtkLabel6.UseMarkup = true;
            this.frame2.LabelWidget = this.GtkLabel6;
            this.vbox2.Add(this.frame2);
            Gtk.Box.BoxChild w35 = ((Gtk.Box.BoxChild)(this.vbox2[this.frame2]));
            w35.Position = 2;
            w35.Expand = false;
            w35.Fill = false;
            w3.Add(this.vbox2);
            Gtk.Box.BoxChild w36 = ((Gtk.Box.BoxChild)(w3[this.vbox2]));
            w36.Position = 0;
            // Internal child MPC.Accounts.ContactGui.ActionArea
            Gtk.HButtonBox w37 = this.ActionArea;
            w37.Name = "dialog1_ActionArea";
            w37.Spacing = 6;
            w37.BorderWidth = ((uint)(5));
            w37.LayoutStyle = ((Gtk.ButtonBoxStyle)(4));
            // Container child dialog1_ActionArea.Gtk.ButtonBox+ButtonBoxChild
            this.buttonCancel = new Gtk.Button();
            this.buttonCancel.CanDefault = true;
            this.buttonCancel.CanFocus = true;
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.UseStock = true;
            this.buttonCancel.UseUnderline = true;
            this.buttonCancel.Label = "gtk-cancel";
            this.AddActionWidget(this.buttonCancel, -6);
            Gtk.ButtonBox.ButtonBoxChild w38 = ((Gtk.ButtonBox.ButtonBoxChild)(w37[this.buttonCancel]));
            w38.Expand = false;
            w38.Fill = false;
            // Container child dialog1_ActionArea.Gtk.ButtonBox+ButtonBoxChild
            this.buttonOk = new Gtk.Button();
            this.buttonOk.CanDefault = true;
            this.buttonOk.CanFocus = true;
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.UseStock = true;
            this.buttonOk.UseUnderline = true;
            this.buttonOk.Label = "gtk-ok";
            this.AddActionWidget(this.buttonOk, -5);
            Gtk.ButtonBox.ButtonBoxChild w39 = ((Gtk.ButtonBox.ButtonBoxChild)(w37[this.buttonOk]));
            w39.Position = 1;
            w39.Expand = false;
            w39.Fill = false;
            // Container child dialog1_ActionArea.Gtk.ButtonBox+ButtonBoxChild
            this.buttonSave = new Gtk.Button();
            this.buttonSave.CanFocus = true;
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.UseStock = true;
            this.buttonSave.UseUnderline = true;
            this.buttonSave.Label = "gtk-save";
            this.AddActionWidget(this.buttonSave, 0);
            Gtk.ButtonBox.ButtonBoxChild w40 = ((Gtk.ButtonBox.ButtonBoxChild)(w37[this.buttonSave]));
            w40.Position = 2;
            w40.Expand = false;
            w40.Fill = false;
            if ((this.Child != null)) {
                this.Child.ShowAll();
            }
            this.DefaultWidth = 521;
            this.DefaultHeight = 493;
            this.Show();
            this.newAction.Activated += new System.EventHandler(this.OnNewActionActivated);
            this.deleteAction.Activated += new System.EventHandler(this.OnDeleteActionActivated);
            this.treeview.CursorChanged += new System.EventHandler(this.OnTreeviewCursorChanged);
            this.details_address.Changed += new System.EventHandler(this.OnDetailsAddressChanged);
            this.buttonCancel.Clicked += new System.EventHandler(this.OnButtonCancelClicked);
            this.buttonOk.Clicked += new System.EventHandler(this.OnButtonOkClicked);
            this.buttonSave.Clicked += new System.EventHandler(this.OnButtonSaveClicked);
        }
    }
}
