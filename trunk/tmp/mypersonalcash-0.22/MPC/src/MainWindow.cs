// MainWindow.cs
// 
// Copyright (C) 2008 Bengt Thuree
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;
using Gtk;
using MPC.Utils;

namespace MPC
{
	
	
	public partial class MainWindow : Gtk.Window
	{
		
		Db db;
		
		public Db Database {
			get { return db; }
		}
		
		public MainWindow (Db db): base (Gtk.WindowType.Toplevel)
		{
			this.db = db;
			Build ();
		}
		
		protected void OnDeleteEvent (object sender, DeleteEventArgs a)
		{
			Application.Quit ();
			a.RetVal = true;
		}

		protected virtual void OnQuitAction (object sender, System.EventArgs e)
		{
			Application.Quit ();
		}

		protected virtual void OnQuoteSourceAction (object sender, System.EventArgs e)
		{
			Log.Information ("Calling Quote Sources...");
			MPC.Quotes.QuoteSourceGui win = new MPC.Quotes.QuoteSourceGui (db.QuoteSources);
			win.Run();			
		}

		protected virtual void OnDefineCurrenciesActionActivated (object sender, System.EventArgs e)
		{
			Log.Information ("Calling Currencies...");
			MPC.Quotes.CurrencyGui win = new MPC.Quotes.CurrencyGui (db.Currencies);
			win.Run();						
		}

		protected virtual void OnDefineSecuritiesActionActivated (object sender, System.EventArgs e)
		{
			Log.Information ("Calling Securities...");
			MPC.Quotes.SecurityGui win = new MPC.Quotes.SecurityGui ();
			win.Run();									
		}

		protected virtual void OnGetOnlineQuotesActionActivated (object sender, System.EventArgs e)
		{
			Log.Information ("Calling Get Online Quotes...");
			MPC.Quotes.OnlineQuotes oq = new MPC.Quotes.OnlineQuotes ();
			oq.GetQuote();
		}

		protected virtual void OnSecurityQuotesActionActivated (object sender, System.EventArgs e)
		{
			Log.Information ("Calling Securities Quotes...");
			MPC.Quotes.SecurityQuoteGui win = new MPC.Quotes.SecurityQuoteGui ();
			win.Run();												
		}

		protected virtual void OnGetExchangeRatesActionActivated (object sender, System.EventArgs e)
		{
			Log.Information ("Calling Get Exchange rates...");
			MPC.Quotes.OnlineExchangeRates oer = new MPC.Quotes.OnlineExchangeRates ();
			oer.GetExchangeRates();							
		}

		protected virtual void OnExchangeRatesActionActivated (object sender, System.EventArgs e)
		{
			Log.Information ("Calling Exchange rates...");
			MPC.Quotes.ExchangeRateGui win = new MPC.Quotes.ExchangeRateGui ();
			win.Run();										
		}

		protected virtual void OnDefineAccountActionActivated (object sender, System.EventArgs e)
		{
			Log.Information ("Calling AccountHeaderGui header...");
			MPC.Accounts.AccountHeaderGui win = new MPC.Accounts.AccountHeaderGui (db.Accounts, db.AddressDetails, db.ContactDetails, db.Currencies);
			win.Run();													
		}

		protected virtual void OnDefineAddressActionActivated (object sender, System.EventArgs e)
		{
			Log.Information ("Calling AddressGui...");
			MPC.Accounts.AddressGui win = new MPC.Accounts.AddressGui (db.AddressDetails);
			win.Run();																
		}

		protected virtual void OnInvestmentTransactionActionActivated (object sender, System.EventArgs e)
		{
			Log.Information ("Calling InvestmentTransactionsGui header...");
			MPC.Accounts.InvestmentTransactionsGui win = new MPC.Accounts.InvestmentTransactionsGui ();
			win.Run();																
		}

		protected virtual void OnDefineContactActionActivated (object sender, System.EventArgs e)
		{
			Log.Information ("Calling ContactGui header...");
			MPC.Accounts.ContactGui win = new MPC.Accounts.ContactGui (db.ContactDetails, db.AddressDetails);
			win.Run();																			
		}

		protected virtual void OnSplitSecurityActionActivated (object sender, System.EventArgs e)
		{
			Log.Information ("Calling Split Security...");
			MPC.Accounts.SplitSecurityGui win = new MPC.Accounts.SplitSecurityGui ();
			win.Run();																						
		}

		protected virtual void OnUpdateAllActionActivated (object sender, System.EventArgs e)
		{
			Log.Information ("Updating security quotes...");
			MPC.Quotes.OnlineQuotes oq = new MPC.Quotes.OnlineQuotes ();
			oq.GetQuote();	
			Log.Information ("Updating exchange rates...");			
			MPC.Quotes.OnlineExchangeRates oer = new MPC.Quotes.OnlineExchangeRates ();
			oer.GetExchangeRates();							
		}

		protected virtual void OnPortfolioActionActivated (object sender, System.EventArgs e)
		{
			MPC.Accounts.PortfolioGui win = new MPC.Accounts.PortfolioGui (db.Securities, db.Accounts, db.SecurityQuotes,
			                                                               db.InvestmentTransactions, db.GetNumberOfSharesView,
			                                                               db.Currencies, db.ExchangeRates
			                                                               );
			win.Run();																						
		}
				
	}
}
