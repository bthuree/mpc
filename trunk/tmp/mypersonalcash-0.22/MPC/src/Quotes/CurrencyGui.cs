// CurrencyGui.cs
// 
// Copyright (C) 2008 Bengt Thuree
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;
using Gtk;
using MPC.Utils;

namespace MPC.Quotes
{
	
	
	public partial class CurrencyGui : Gtk.Dialog
	{
		private Currency c;
		private System.Collections.ArrayList c_ar;
		private CurrencyStore c_s;
		
		private ListStore CurrencyTreeStore;		
		
		public CurrencyGui(CurrencyStore _cs)
		{
			c_s = _cs;
			this.Build();
			PopulateTreeView(true);			
		}
		
		private void PopulateStore()
		{
			c_ar = c_s.GetList();			
			CurrencyTreeStore = new ListStore (typeof(string), typeof(string), typeof(string));
			foreach (Currency c in c_ar) {
				CurrencyTreeStore.AppendValues (c.Code, c.Country, c.BasePool.ToString());
			}
		}
		
		private void PopulateTreeView(bool define_columns)
		{
			PopulateStore();
			CurrencyTreeView.Model = CurrencyTreeStore;
			if (define_columns) {
				CurrencyTreeView.AppendColumn ("Code", new CellRendererText(), "text", 0);
				CurrencyTreeView.AppendColumn ("Country", new CellRendererText(), "text", 1);
				CurrencyTreeView.AppendColumn ("Base currency", new CellRendererText(), "text", 2);				
			}
			this.ShowAll();
		}

  		private void ClearDetails()
		{
			CurrencyDetailsCode.Text = String.Empty;
			CurrencyDetailsCountry.Text = String.Empty;
			CurrencyDetailsBasePool.Active = false;
			c = null;			
		}		
		
		protected Currency FetchCurrencyFromRow()
		{
			TreePath path = null;
			TreeViewColumn column = null;
			CurrencyTreeView.GetCursor (out path, out column);
			if (path != null) {
				int row = System.Convert.ToInt32 (path.ToString());
				Currency c = c_ar[row] as Currency;
				return c;
			}			
			return null;
		}
		
		private string GuiText (string new_txt)
		{
			if (new_txt == null)
				return String.Empty;
			else
				return new_txt;
		}		

		protected virtual void OnButtonCancelClicked (object sender, System.EventArgs e)
		{
			this.Destroy();			
		}

		protected virtual void OnButtonOkClicked (object sender, System.EventArgs e)
		{
			Log.Information("Clicked OK");															
		}

		protected virtual void OnButtonSaveClicked (object sender, System.EventArgs e)
		{
			bool NewCurrency = false;
			if (c == null) {
				c = new Currency("empty");
				NewCurrency = true;
			}
			c.Code = CurrencyDetailsCode.Text;
			c.Country = CurrencyDetailsCountry.Text;
			c.BasePool	= CurrencyDetailsBasePool.Active;

			if (NewCurrency)
				c_s.Add(c);
			else
				c_s.Change(c);
			PopulateTreeView(false);
			ClearDetails();				
		}

		protected virtual void OnNewActionActivated (object sender, System.EventArgs e)
		{
			ClearDetails();									
		}

		protected virtual void OnDeleteActionActivated (object sender, System.EventArgs e)
		{
			c = FetchCurrencyFromRow();
			if (c != null) {
				c_s.Del(c);
				PopulateTreeView(false);
				ClearDetails();
			}									
		}

		protected virtual void OnDefaultActionActivated (object sender, System.EventArgs e)
		{
			ClearDetails();
			c_s.AddDefault();
			PopulateTreeView(false);									
		}

		protected virtual void OnCurrencyTreeViewCursorChanged (object sender, System.EventArgs e)
		{
			ClearDetails();
			c = FetchCurrencyFromRow();
			if (c != null) {
				CurrencyDetailsCode.Text = GuiText (c.Code);				
				CurrencyDetailsCountry.Text = GuiText (c.Country);				
				CurrencyDetailsBasePool.Active = c.BasePool;								
			}						
		}
	}
}
