// Currency.cs
// 
// Copyright (C) 2008 Bengt Thuree
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//


// http://www.xe.com/iso4217.php
using System;

namespace MPC.Quotes
{
	
	public class Currency : MPC.Utils.DbItem
	{
		
		public Currency (string _code)
		{
			code = _code.Trim();
			country = null;
			base_pool = false;
		}
		
		private string code;
		public string Code {
			get { return code; }
			set { code = value; }
		}

		private string country;
		public string Country {
			get { return country; }
			set { country = value; }
		}

		private bool base_pool;
		public bool BasePool {
			get { return base_pool; }
			set { base_pool = value; }
		}
		
		
		public override bool Equals(Object obj) 
		{
			//Check for null and compare run-time types.
			if (obj == null || GetType() != obj.GetType()) 
				return false;
			Currency c2 = (Currency)obj;
			return (Code == c2.code);
		}
		
		public override int GetHashCode() 
		{
			return code.GetHashCode();
		}		
	}
}
