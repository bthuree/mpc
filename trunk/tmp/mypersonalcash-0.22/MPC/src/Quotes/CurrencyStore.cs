// CurrencyStore.cs
// 
// Copyright (C) 2008 Bengt Thuree
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//


using System;
using System.IO;
using System.Data;



namespace MPC.Quotes
{
	public class CurrencyStore : MPC.Utils.DbGeneralStore
	{
		private string DefaultCurrencyFile = "/home/bengt/Development/mpc/trunk/MPC.Test/OnlineQuotesTest/TestData/CurrencyList.csv";
		public CurrencyStore(IDbConnection _dbcon) : base (_dbcon, "Currencies")
		{
			if (Count() == 0)
				AddDefault(DefaultCurrencyFile);
		}

		public void AddDefault ()
		{
			AddDefault(DefaultCurrencyFile);
		}
		
		public void AddDefault (string csv_file)
		{
			MPC.Utils.Log.DebugFormat ("Importing currencies from : {0}", csv_file);
			try {
				StreamReader reader = File.OpenText(csv_file);
				string inputline = null;
				string[] items = null;				
				while ((inputline = reader.ReadLine()) != null)
				{
					try {
					// read each line
						items = inputline.Split(',');
						Currency c = new Currency (items[0]);
						c.Country = items[1];
						Add (c);
					}
					catch (MPC.Utils.NotUniqueException) {
						MPC.Utils.Log.DebugFormat ("Currency already exists : {0}", items[0]);
					}
					catch (Exception e) {
						MPC.Utils.Log.Exception (e);
					}
				
				}
				reader.Close();
			}
			catch (Exception e) {
				MPC.Utils.Log.Exception (e);
			}
		}
		
		private string GenerateAddSql (Currency c)
		{
			string sql = "INSERT into Currencies (code, country, basepool) " +
				String.Format ("VALUES ( '{0}', '{1}', {2} )", 
								   c.Code, c.Country, c.BasePool
				               );	
			return sql;
		}		
		public void Add (Currency c)
		{
			base.Add (GenerateAddSql (c));
		}
		public int Add_GetIndex (Currency c)
		{
			return base.Add_GetIndex (GenerateAddSql (c));
		}		
		
		public void Del (Currency c)
		{
			Del (c.Id);
		}
		
		public void Del (int index)
		{
			string sql = string.Format ("DELETE FROM Currencies WHERE id = {0}", index);
			base.Del (sql);			
		}		

		public new void Del (string _code)
		{
			string sql = string.Format ("DELETE FROM Currencies WHERE code = '{0}'", _code);				
			base.Del (sql);			
		}				
		
		public void Change (Currency c)
		{
			string sql = "UPDATE Currencies SET " +
				String.Format ("code='{0}', country='{1}', basepool={2} ", 
								   c.Code, c.Country, c.BasePool ) +
				String.Format ("WHERE id = {0}", c.Id); 
			base.Change (sql);			
		}

		public override MPC.Utils.DbItem DecodeReader (IDataReader reader)
		{
			Currency c = new Currency (reader.GetString(reader.GetOrdinal("code")));					
			c.Id = reader.GetInt32(reader.GetOrdinal("id"));
			c.Country = reader.GetString(reader.GetOrdinal("country"));
			c.BasePool = reader.GetBoolean(reader.GetOrdinal("basepool"));
			return (MPC.Utils.DbItem) c;
		}
		
		public Currency Get (int id)
		{
			string sql = "SELECT id, code, country, basepool " +
				"FROM Currencies " +
				String.Format ("WHERE id = {0}", id); 
			return base.Get (sql) as Currency;							
		}

		public new Currency Get (string _code)
		{
			string sql = "SELECT id, code, country, basepool " +
				"FROM Currencies " +
				String.Format ("WHERE code = '{0}'", _code); 
			return base.Get (sql) as Currency;							
		}

		public System.Collections.ArrayList GetList ()
		{
			string sql = "SELECT id, code, country, basepool " +
					"FROM Currencies ORDER BY code"; 
			return base.GetList (sql);							
		}

		public System.Collections.ArrayList GetBaseCurrencies ()
		{
			string sql = "SELECT id, code, country, basepool " +
					"FROM Currencies " +
					"WHERE basepool = true " + 
					"ORDER BY code";
			return base.GetList (sql);							
		}
		
	}
}

