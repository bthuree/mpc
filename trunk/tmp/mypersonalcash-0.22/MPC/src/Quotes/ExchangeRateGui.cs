// ExchangeRateGui.cs
// 
// Copyright (C) 2008 Bengt Thuree
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;
using Gtk;
using MPC;
using MPC.Accounts;
using MPC.Utils;

namespace MPC.Quotes
{	
	public partial class ExchangeRateGui : Gtk.Dialog
	{
		private System.Collections.ArrayList basepool_ar, active_ar;

		CurrencyStore c_s;
		SecurityStore s_s;
		NumberOfSharesView nos_v;
		ExchangeRateStore er_s;
		
		private ListStore BaseTreeStore, ActiveTreeStore, ExchangeRateTreeStore;
		
		public ExchangeRateGui()
		{
			c_s = MPC.Database.GetDatabase.Currencies;
			nos_v = MPC.Database.GetDatabase.GetNumberOfSharesView;
			er_s = MPC.Database.GetDatabase.ExchangeRates;
			s_s = MPC.Database.GetDatabase.Securities;
			this.Build();
			PopulateActiveTreeView(true);	
			PopulateToBaseTreeView(true);		
			PopulateExchangeRateTreeView (true);			
		}

		private void PopulateBaseStore()
		{
			basepool_ar = c_s.GetBaseCurrencies();			
			BaseTreeStore = new ListStore (typeof (string), typeof (string));
			foreach (Currency c in basepool_ar) {
				BaseTreeStore.AppendValues (c.Code, c.Country);				
			}
		}
		
		private void PopulateActiveStore()
		{
			System.Collections.ArrayList nos_ar = nos_v.GetList();
			active_ar = new System.Collections.ArrayList();
			foreach (NumberOfShares nos in nos_ar) {
				Security sec = s_s.Get (nos.SecurityNr);
				Currency c = c_s.Get (sec.Currency);
				if (!active_ar.Contains (c))
					active_ar.Add(c);
			}
			ActiveTreeStore = new ListStore (typeof (string), typeof (string));
			foreach (Currency c in active_ar) {
				ActiveTreeStore.AppendValues (c.Code, c.Country);
			}
		}
		
		private void PopulateActiveTreeView(bool define_columns)
		{
			PopulateActiveStore();
			treeview_from_active.Model = ActiveTreeStore;
			if (define_columns) {
				treeview_from_active.AppendColumn ("Code", new CellRendererText(), "text", 0);
				treeview_from_active.AppendColumn ("Country", new CellRendererText(), "text", 1);				
			}
		}		
		
		private void PopulateToBaseTreeView(bool define_columns)
		{
			PopulateBaseStore();
			treeview_to_basepool.Model = BaseTreeStore;
			if (define_columns) {
				treeview_to_basepool.AppendColumn ("Code", new CellRendererText(), "text", 0);
				treeview_to_basepool.AppendColumn ("Country", new CellRendererText(), "text", 1);				
			}
		}
		
		private void ClearFromActive()
		{
			treeview_from_active.Selection.UnselectAll();
		}
		

		private void PopulateExchangeRateStore(ExchangeRate _er)
		{
			System.Collections.ArrayList tmp_ar = er_s.GetList (_er);			
			ExchangeRateTreeStore = new ListStore (typeof (string), typeof (string), typeof (string));
			foreach (ExchangeRate er in tmp_ar) {
				ExchangeRateTreeStore.AppendValues (er.TimeStamp.ToString(), er.ExchangeRateValue.ToString(), er.Quote_Type.ToString());				
			}
		}
		
		private void PopulateExchangeRateTreeView(ExchangeRate er)
		{
			PopulateExchangeRateStore(er);
			treeview_result.Model = ExchangeRateTreeStore;
		}
		
		private void PopulateExchangeRateTreeView(bool define_columns)
		{
			if (define_columns) {
				treeview_result.AppendColumn ("Date", new CellRendererText(), "text", 0);
				treeview_result.AppendColumn ("Exchange rate", new CellRendererText(), "text", 1);
				treeview_result.AppendColumn ("Type", new CellRendererText(), "text", 2);								
			}
		}
				
		
		protected Currency FetchCurrencyFromRow(Gtk.TreeView treeView, System.Collections.ArrayList def_arr)
		{
			TreePath path = null;
			TreeViewColumn column = null;
			treeView.GetCursor (out path, out column);
			if ( (treeView.Selection.CountSelectedRows() > 0) && (path != null) ) {
				int row = System.Convert.ToInt32 (path.ToString());
				Currency c = def_arr[row] as Currency;
				return c;
			}			
			return null;
		}				
		
		private void PopulateExchangeRates()
		{			
			Currency from_c = FetchCurrencyFromRow(treeview_from_active, active_ar);
			Currency to_c = FetchCurrencyFromRow(treeview_to_basepool, basepool_ar);
			if ( (from_c != null) && (to_c != null) ) {
				ExchangeRate er = new ExchangeRate();
				er.FromCurrency = from_c.Id;
				er.ToCurrency = to_c.Id;
				PopulateExchangeRateTreeView (er);				
			}
		}
		
		protected virtual void OnRefreshActionActivated (object sender, System.EventArgs e)
		{
			MPC.Quotes.OnlineExchangeRates oer = new MPC.Quotes.OnlineExchangeRates ();
			oer.GetExchangeRates();										
		}

		protected virtual void OnTreeviewFromActiveCursorChanged (object sender, System.EventArgs e)
		{
			PopulateExchangeRates();			
		}

		protected virtual void OnTreeviewToBasepoolCursorChanged (object sender, System.EventArgs e)
		{
			PopulateExchangeRates();			
		}

		protected virtual void OnButtonCancelClicked (object sender, System.EventArgs e)
		{
			this.Destroy();									
		}

		protected virtual void OnButtonOkClicked (object sender, System.EventArgs e)
		{
			this.Destroy();									
		}
	}
}
