// OnlineExchangeRates.cs
// 
// Copyright (C) 2008 Bengt Thuree
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;
using MPC;
using MPC.Accounts;


namespace MPC.Quotes
{
	
	
	public class OnlineExchangeRates
	{
		private ExchangeRateStore er_s;
		private SecurityStore s_s;
		private CurrencyStore c_s;
		private NumberOfSharesView nos_v;		
		private QuoteSource ExchangeRateUri;
		
		public OnlineExchangeRates ()
		{
			er_s = MPC.Database.GetDatabase.ExchangeRates;
			s_s = MPC.Database.GetDatabase.Securities;
			c_s = MPC.Database.GetDatabase.Currencies;
			nos_v = MPC.Database.GetDatabase.GetNumberOfSharesView;			
			ExchangeRateUri = new QuoteSource ("Yahoo ExchangeRates");
//			ExchangeRateUri.Uri = "http://download.finance.yahoo.com/d/quotes.csv?s=%1=X&f=sl1d1t1c1ohgv&e=.csv";
//http://finance.yahoo.com/d/quotes.csv?s=USDSEK=X&f=sl1d1t1   with time			
			ExchangeRateUri.Uri = "http://finance.yahoo.com/d/quotes.csv?s=%1=X&f=sl1d1";		

			ExchangeRateUri.Date_regexp = "[^,]*,[^,]*,\"([^\"]*)\"";
			ExchangeRateUri.Date_format = "M/d/yyyy";
			ExchangeRateUri.Quote_regexp = "[^,]*,([^,]*),.*";
			ExchangeRateUri.Symbol_regexp = "\"([^,\"]*)\",.*";
		}

		private ExchangeRate FoqToExchangeRate (FetchOnlineQuote foq, Currency c_from, Currency c_to)
		{
			ExchangeRate er = new ExchangeRate();
			er.TimeStamp = foq.Date;
			er.FromCurrency = c_from.Id;
			er.ToCurrency = c_to.Id;
			er.ExchangeRateValue = foq.Quote_value;
			er.Quote_Type = MPC.Quotes.UpdateTypeEnum.online;
			return er;
		}
		
		private void GetExchangeRate (QuoteSource qs, Currency c_from, Currency c_to)
		{
			if (c_from.Code == c_to.Code)
			  return;
			FetchOnlineQuote foq = new FetchOnlineQuote (qs, c_from.Code, c_to.Code);				
			foq.GetOnlineQuote();
			if (foq.Success) {
				try {
					er_s.Add (FoqToExchangeRate (foq, c_from, c_to));
				}
				catch (MPC.Utils.NotUniqueException)
				{}
				catch (Exception e)
				{
					MPC.Utils.Log.Exception (e);
				}
			}
		}		
		
		private void GetActiveExchangeRates (System.Collections.ArrayList active_list, System.Collections.ArrayList base_list)
		{
			foreach (int i in active_list)
			{
				foreach (Currency base_currency in base_list)
				{
					Currency c_from = c_s.Get (i);
					GetExchangeRate ( ExchangeRateUri, c_from, base_currency );
				}
			}
		}

		private System.Collections.ArrayList GetActiveSecurities()
		{
			System.Collections.ArrayList active_sec = new System.Collections.ArrayList();		
			System.Collections.ArrayList shares_list = nos_v.GetList();
			foreach (NumberOfShares nos in shares_list)
			{
				Security s = s_s.Get(nos.SecurityNr);
				if (!active_sec.Contains(s))
					active_sec.Add (s);
			}
			return active_sec;
		}		
		
		private System.Collections.ArrayList GetActiveCurrencies ()
		{
			System.Collections.ArrayList s_db = GetActiveSecurities();
			System.Collections.ArrayList active_list = new System.Collections.ArrayList();
			foreach (Security s in s_db)
				if (!active_list.Contains(s.Currency))
				    active_list.Add (s.Currency);
			return active_list;
		}
		
		public void GetExchangeRates ()
		{
			System.Collections.ArrayList active_list, base_list;		
			active_list = GetActiveCurrencies();
			base_list = c_s.GetBaseCurrencies();
			GetActiveExchangeRates (active_list, base_list);
		}			

	}
}
