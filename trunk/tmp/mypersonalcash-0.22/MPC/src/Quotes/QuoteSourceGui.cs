// QuoteSourceGui.cs
// 
// Copyright (C) 2008 Bengt Thuree
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;
using Gtk;
using MPC.Utils;

namespace MPC.Quotes
{
	
	
	public partial class QuoteSourceGui : Gtk.Dialog
	{
		

				
		private QuoteSource qs;
		private System.Collections.ArrayList qs_ar;
		private QuoteSourceStore qss;
		
		private ListStore QuoteSourceTreeStore;
				
		public QuoteSourceGui(QuoteSourceStore _qss)
		{
			qss = _qss;
			this.Build();
			PopulateTreeView(true);			
		}

		private void PopulateStore()
		{
			qs_ar = qss.GetList();			
			QuoteSourceTreeStore = new ListStore (typeof (string));
			foreach (QuoteSource q in qs_ar) {
				QuoteSourceTreeStore.AppendValues (q.Name);				
			}
		}
		
		private void PopulateTreeView(bool define_columns)
		{
			PopulateStore();
			QuoteSourceTreeView.Model = QuoteSourceTreeStore;
			if (define_columns) {
				QuoteSourceTreeView.AppendColumn ("Quote Source", new CellRendererText(), "text", 0);
			}
			this.ShowAll();
		}

  		private void ClearDetails()
		{
			QuoteSourceDetails_DateFormat.Text = String.Empty;
			QuoteSourceDetails_DateRowRegExp.Text = String.Empty;
			QuoteSourceDetails_DateRegExp.Text = String.Empty;
			QuoteSourceDetails_Encoding.Text = String.Empty;
			QuoteSourceDetails_QuoteRegExp.Text = String.Empty;
			QuoteSourceDetails_SymbolRegExp.Text = String.Empty;
			QuoteSourceDetails_Name.Text = String.Empty;
			QuoteSourceDetails_URI.Text = String.Empty;
			qs = null;			
		}

		protected QuoteSource FetchQSFromRow()
		{
			TreePath path = null;
			TreeViewColumn column = null;
			QuoteSourceTreeView.GetCursor (out path, out column);
			if (path != null) {
				int row = System.Convert.ToInt32 (path.ToString());
				QuoteSource qs = qs_ar[row] as QuoteSource;
				return qs;
			}			
			return null;
		}
		
		protected virtual void OnButtonCancelClicked (object sender, System.EventArgs e)
		{
			this.Destroy();
		}

		protected virtual void OnButtonOkClicked (object sender, System.EventArgs e)
		{
			Log.Information("Clicked OK");												
		}

		private string GuiText (string new_txt)
		{
			if (new_txt == null)
				return String.Empty;
			else
				return new_txt;
		}
		
		protected virtual void OnButtonSaveClicked (object sender, System.EventArgs e)
		{
			bool NewQS = false;
			if (qs == null) {
				qs = new QuoteSource("empty");
				NewQS = true;
			}
			qs.Date_format = QuoteSourceDetails_DateFormat.Text;
			qs.Date_row_regexp = QuoteSourceDetails_DateRowRegExp.Text;
			qs.Date_regexp = QuoteSourceDetails_DateRegExp.Text;
			qs.Encoding = QuoteSourceDetails_Encoding.Text;
			qs.Quote_regexp = QuoteSourceDetails_QuoteRegExp.Text;
			qs.Symbol_regexp = QuoteSourceDetails_SymbolRegExp.Text;
			qs.Name = QuoteSourceDetails_Name.Text;
			qs.Uri = QuoteSourceDetails_URI.Text;
			if (NewQS)
				qss.Add(qs);
			else
				qss.Change(qs);
			PopulateTreeView(false);
			ClearDetails();				
		}

		protected virtual void OnDeleteQuoteSourceClicked (object sender, System.EventArgs e)
		{

		}

		protected virtual void OnNewActionActivated (object sender, System.EventArgs e)
		{
			ClearDetails();						
		}

		protected virtual void OnDeleteActionActivated (object sender, System.EventArgs e)
		{
			qs = FetchQSFromRow();
			if (qs != null) {
				qss.Del(qs);
				PopulateTreeView(false);
				ClearDetails();
			}						
		}

		protected virtual void OnDefaultsActionActivated (object sender, System.EventArgs e)
		{
			ClearDetails();
			qss.AddDefault();
			PopulateTreeView(false);						
		}

		protected virtual void OnQuoteSourceTreeViewCursorChanged (object sender, System.EventArgs e)
		{
			ClearDetails();
			qs = FetchQSFromRow();
			if (qs != null) {
				QuoteSourceDetails_DateRowRegExp.Text = GuiText (qs.Date_row_regexp);				
				QuoteSourceDetails_Encoding.Text = GuiText (qs.Encoding);
				QuoteSourceDetails_DateFormat.Text = GuiText (qs.Date_format);
				QuoteSourceDetails_DateRegExp.Text = GuiText (qs.Date_regexp);
				QuoteSourceDetails_QuoteRegExp.Text = GuiText (qs.Quote_regexp);
				QuoteSourceDetails_SymbolRegExp.Text = GuiText (qs.Symbol_regexp);
				QuoteSourceDetails_Name.Text = GuiText (qs.Name);
				QuoteSourceDetails_URI.Text = GuiText (qs.Uri);
			}						
		}
	}
}
