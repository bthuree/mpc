// QuoteSource.cs
// 
// Copyright (C) 2008 Bengt Thuree
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//


/*
History from Google.
http://finance.google.com/finance/historical?q=NASDAQ:ERIC_output=csv
http://finance.google.com/finance/historical?cid=198224_startdate=Dec+27%2C+2001_enddate=Dec+26%2C+2007_output=csv
http://finance.google.com/finance/historical?q=NASDAQ:ERIC_histperiod=weekly_output=csv
*/
using System;

namespace MPC.Quotes
{
	
	public class QuoteSource : MPC.Utils.DbItem
	{
		
		public QuoteSource (string _name)
		{
			uri = null;
			encoding = null;
			name = _name;
			date_format = null;
			date_row_regexp = null;			
			date_regexp = null;
			quote_regexp = null;
			symbol_regexp = null;
		}
		
		private string name;
		public string Name {
			get { return name; }
			set { name = value; }
		}

		private string uri;
		public string Uri {
			get { return uri; }
			set { uri = value; }
		}
		
		private string encoding;
		public string Encoding {
			get { return encoding; }
			set { encoding = value; }
		}
		
		private string date_format;	// "%x %x %x", where x is y, m, or d
		public string Date_format {
			get { return date_format; }
			set { date_format = value; }
		}

		private string date_row_regexp;	// To find the date row, if not on the same row in the quote table
		public string Date_row_regexp {
			get { return date_row_regexp; }
			set { date_row_regexp = value; }
		}

		private string date_regexp;
		public string Date_regexp {
			get { return date_regexp; }
			set { date_regexp = value; }
		}
		
		private string quote_regexp;
		public string Quote_regexp {
			get { return quote_regexp; }
			set { quote_regexp = value; }
		}
		
		private string symbol_regexp;
		public string Symbol_regexp {
			get { return symbol_regexp; }
			set { symbol_regexp = value; }
		}
		
		public override bool Equals(Object obj) 
		{
			//Check for null and compare run-time types.
			if (obj == null || GetType() != obj.GetType()) 
				return false;
			QuoteSource qs2 = (QuoteSource)obj;
			return (name == qs2.name);
		}
		
		public override int GetHashCode() 
		{
			return name.GetHashCode();
		}		
		
	}
}
