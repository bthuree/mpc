// SecurityQuoteGui.cs
// 
// Copyright (C) 2008 Bengt Thuree
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;
using Gtk;
using MPC;
using MPC.Accounts;
using MPC.Utils;

namespace MPC.Quotes
{
	
//FIXME Change a quote, 
	//FIXME Test with 50 securities, and 5,000 quotes each.
	
	public partial class SecurityQuoteGui : Gtk.Dialog
	{
		private System.Collections.ArrayList s_ar, sq_ar;
		private SecurityStore s_s; 
		private SecurityQuoteStore sq_s;
		
		private ListStore SecurityTreeStore;		
		private ListStore QuoteTreeStore;				
		
		public SecurityQuoteGui()
		{
			s_s = MPC.Database.GetDatabase.Securities;
			sq_s = MPC.Database.GetDatabase.SecurityQuotes;

			this.Build();
			PopulateSecurityTreeView(true);
			PopulateQuoteTreeView (null, true);	
			SecurityQuote_Symbol.AllowNew = false; 
		}		
		
		private void PopulateSecurityStore()
		{
			s_ar = s_s.GetList();			
			SecurityTreeStore = new ListStore (typeof (string), typeof (string));
			foreach (Security s in s_ar) {
				SecurityTreeStore.AppendValues (s.Symbol, s.Name);				
			}
		}
		
		private void PopulateSecurityTreeView(bool define_columns)
		{
			PopulateSecurityStore();
			SecurityTreeView.Model = SecurityTreeStore;
			if (define_columns) {
				SecurityTreeView.AppendColumn ("Symbol", new CellRendererText(), "text", 0);
				SecurityTreeView.AppendColumn ("Name", new CellRendererText(), "text", 1);				
			}
		}
		
		private void PopulateQuoteStore(Security s)
		{
			sq_ar = sq_s.GetList(s);	
			if (sq_ar.Count > 0) {
				QuoteTreeStore = new ListStore (typeof (string), typeof(string), typeof(string));
				foreach (SecurityQuote sq in sq_ar) {
					QuoteTreeStore.AppendValues (sq.TimeStamp.ToString(), sq.Quote_Value.ToString(), sq.Quote_Type.ToString());				
				}
			}
			else
				QuoteTreeStore = null;
		}
		
		private void PopulateQuoteTreeView(Security s, bool define_columns)
		{
			if (s != null)
				PopulateQuoteStore(s);
			QuoteTreeView.Model = QuoteTreeStore;
			if (define_columns) {
				QuoteTreeView.AppendColumn ("Date", new CellRendererText(), "text", 0);				
				QuoteTreeView.AppendColumn ("Quote", new CellRendererText(), "text", 1);				
				QuoteTreeView.AppendColumn ("Type", new CellRendererText(), "text", 2);								
			}
		}		

		protected Security FetchSecurityFromRow()
		{
			TreePath path = null;
			TreeViewColumn column = null;
			SecurityTreeView.GetCursor (out path, out column);
			if (path != null) {
				int row = System.Convert.ToInt32 (path.ToString());
				Security s = s_ar[row] as Security;
				return s;
			}			
			return null;
		}
		
		protected SecurityQuote FetchQuoteFromRow()
		{
			TreePath path = null;
			TreeViewColumn column = null;
			QuoteTreeView.GetCursor (out path, out column);
			if (path != null) {
				int row = System.Convert.ToInt32 (path.ToString());
				SecurityQuote sq = sq_ar[row] as SecurityQuote;
				return sq;
			}			
			return null;
		}		
		
		private SecurityQuote FetchFromDetails()
		{
			SecurityQuote sq = new SecurityQuote();
			Security s = SecurityQuote_Symbol.Selection;			
			sq.Symbol = s.Id;
			sq.TimeStamp = System.DateTime.Parse (SecurityQuote_Date.Text);
			sq.Quote_Value = System.Convert.ToDecimal (SecurityQuote_Quote.Text);	
			return sq;
		}
		
		private void ClearDetails ()
		{
			SecurityQuote_Symbol.Selection = null;
			SecurityQuote_Date.Text = String.Empty;
			SecurityQuote_Quote.Text = String.Empty;
		}
			
		private void PopulateDetails (SecurityQuote sq)
		{
			if (sq == null)
				ClearDetails();
			else {
				Security s = FetchSecurityFromRow();
				SecurityQuote_Symbol.Selection = s;
				SecurityQuote_Date.Text = sq.TimeStamp.ToString();
				SecurityQuote_Quote.Text = sq.Quote_Value.ToString();
			}
		}
		
		protected virtual void OnSecurityTreeViewCursorChanged (object sender, System.EventArgs e)
		{
			Log.Debug ("CursorChanged");			
			Security s = FetchSecurityFromRow();
			PopulateQuoteTreeView (s, false);	
			ClearDetails();									
		}
		
		protected virtual void OnDeleteActionActivated (object sender, System.EventArgs e)
		{
			Log.Debug ("Delete");
			SecurityQuote sq = FetchQuoteFromRow();
			if (sq != null) {
				sq_s.Del(sq);
				Security s = FetchSecurityFromRow();
				PopulateQuoteTreeView (s, false);	
			}			
		}

		protected virtual void OnNewActionActivated (object sender, System.EventArgs e)
		{
			ClearDetails();			
		}

		protected virtual void OnRefreshActionActivated (object sender, System.EventArgs e)
		{
			MPC.Quotes.OnlineQuotes oq = new MPC.Quotes.OnlineQuotes ();
			oq.GetQuote();			
		}

		protected virtual void OnButtonCancelClicked (object sender, System.EventArgs e)
		{
			this.Destroy();												
		}

		protected virtual void OnButtonOkClicked (object sender, System.EventArgs e)
		{
			this.Destroy();												
		}

		protected virtual void OnSaveQuoteClicked (object sender, System.EventArgs e)
		{
			SecurityQuote sq = FetchFromDetails();
			if (sq != null) {
				sq_s.Add(sq);
				Security s = FetchSecurityFromRow();
				PopulateQuoteTreeView (s, false);	
			}
		}

		protected virtual void OnQuoteTreeViewCursorChanged (object sender, System.EventArgs e)
		{
			SecurityQuote sq = FetchQuoteFromRow();
			PopulateDetails (sq);									
		}

		
// Below is bogus testing code...
		
		private void AddSecurityQuote (decimal _quote, int _security, int year, int month, int day, int hour, MPC.Quotes.UpdateTypeEnum type )
		{
			SecurityQuote sq1 = new SecurityQuote();
			sq1.Symbol = _security;
			sq1.TimeStamp = new DateTime (year, month, day, hour, 0, 0);
			sq1.Quote_Value = _quote;
			sq1.Quote_Type = type;
			sq_s.Add (sq1);
		}
		
		protected virtual void OnDialogWarningActionActivated (object sender, System.EventArgs e)
		{
			Log.Debug ("Will generate Loads of Security Quotes now....");
			System.DateTime FromTime = new System.DateTime (1960, 01, 01);
			System.DateTime ToTime = new System.DateTime (1970, 12, 31);
			UpdateQuotes_ProgressBar.Text = String.Format ("Generating bogus quotes between {0} and {1}", FromTime.ToString(), ToTime.ToString()); 			
			double DiffDays = ToTime.Subtract (FromTime).TotalDays;
			for (System.DateTime dt = FromTime; dt <= ToTime; dt = dt.AddDays(1)) {
				double DoneDays = dt.Subtract (FromTime).TotalDays;
				double DoneFraction = DoneDays / DiffDays; 
				UpdateQuotes_ProgressBar.Fraction = DoneFraction;
				foreach (Security s in s_ar) {
					AddSecurityQuote (s.Id*50000+dt.Subtract(FromTime).Days, s.Id, dt.Year, dt.Month,  dt.Day, 10, MPC.Quotes.UpdateTypeEnum.manual);
					AddSecurityQuote (s.Id*50000+dt.Subtract(FromTime).Days, s.Id, dt.Year, dt.Month,  dt.Day, 11, MPC.Quotes.UpdateTypeEnum.buy);
					AddSecurityQuote (s.Id*50000+dt.Subtract(FromTime).Days, s.Id, dt.Year, dt.Month,  dt.Day, 12, MPC.Quotes.UpdateTypeEnum.sell);
					AddSecurityQuote (s.Id*50000+dt.Subtract(FromTime).Days, s.Id, dt.Year, dt.Month,  dt.Day, 13, MPC.Quotes.UpdateTypeEnum.online);
				}
			}
			Log.Debug ("DONE - Loading bogus Security Quotes now....");	
			UpdateQuotes_ProgressBar.Text = String.Format ("Done loading bogus quotes between {0} and {1}", FromTime.ToString(), ToTime.ToString()); 			
		}

	}
}
