// OnlineQuotes.cs
// 
// Copyright (C) 2008 Bengt Thuree
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;
using MPC;
using MPC.Accounts;

namespace MPC.Quotes
{
	
	
	public class OnlineQuotes
	{

		private QuoteSourceStore qs_s;
		private SecurityQuoteStore sq_s;
		private SecurityStore s_s;
		private NumberOfSharesView nos_v;
		
		
		public OnlineQuotes()
		{
			qs_s = MPC.Database.GetDatabase.QuoteSources;
			sq_s = MPC.Database.GetDatabase.SecurityQuotes;
			s_s = MPC.Database.GetDatabase.Securities;
			nos_v = MPC.Database.GetDatabase.GetNumberOfSharesView;
		}

		private SecurityQuote FoqToQuote (FetchOnlineQuote foq, Security s)
		{
			SecurityQuote q = new SecurityQuote();
			q.TimeStamp = foq.Date;
			q.Symbol = s.Id;
			q.Quote_Value = foq.Quote_value;
			q.Quote_Type = MPC.Quotes.UpdateTypeEnum.online;
			return q;
		}
		
		private void GetSecurityQuote (QuoteSource qs, Security s)
		{
			FetchOnlineQuote foq = new FetchOnlineQuote (qs, s.Symbol);				
			foq.GetOnlineQuote();
			if (foq.Success) {
				try {
					sq_s.Add (FoqToQuote (foq, s));
				}
				catch (MPC.Utils.NotUniqueException)
				{}
				catch (Exception e)
				{
					MPC.Utils.Log.Exception (e);
				}
			}
		}		
		
		private void GetQuoteFromSecurityList (System.Collections.ArrayList s_db)
		{
			foreach (Security s in s_db) {
				GetQuote (s);
			}			
		}
		
		private System.Collections.ArrayList GetActiveSecurities()
		{
			return GetActiveSecurities(null);
		}
				
		private System.Collections.ArrayList GetActiveSecurities(QuoteSource qs)
		{
			System.Collections.ArrayList active_sec = new System.Collections.ArrayList();		
			System.Collections.ArrayList shares_list = nos_v.GetList();
			foreach (NumberOfShares nos in shares_list)
			{
				Security s = s_s.Get(nos.SecurityNr);
				if (qs == null)
				{
					if (!active_sec.Contains(s))
						active_sec.Add (s);
				}
				else
				{
					if ( (s.Quote_Source == qs.Id) && (!active_sec.Contains(s)) )
						active_sec.Add (s);
				}
			}
			return active_sec;
		}
		
		public void GetQuote (Security s)
		{
			QuoteSource qs = qs_s.Get (s.Quote_Source);
			GetSecurityQuote (qs, s);
		}

		public void GetQuote (QuoteSource qs)
		{
			System.Collections.ArrayList s_db = GetActiveSecurities(qs);
			GetQuoteFromSecurityList (s_db);
		}		

		public void GetQuote ()
		{
			System.Collections.ArrayList s_db = GetActiveSecurities();
			GetQuoteFromSecurityList (s_db);
		}			
		

	}
}
