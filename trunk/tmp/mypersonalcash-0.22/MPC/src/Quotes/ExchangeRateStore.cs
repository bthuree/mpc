// ExchangeRateStore.cs
//
// Copyright (C) 2008 Bengt Thuree
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;
using System.IO;
using System.Data;
using Npgsql;

namespace MPC.Quotes
{
	public class ExchangeRateStore  : MPC.Utils.DbGeneralStore
	{
		
		public ExchangeRateStore (IDbConnection _dbcon) : base (_dbcon, "ExchangeRates")
		{
		}


		public string GenerateAddSql (ExchangeRate er)
		{
			string sql = "INSERT into ExchangeRates (timestamp, from_currency, to_currency, exchange_rate, quote_type) " +
				String.Format ("VALUES ( '{0}', '{1}', '{2}', '{3}', '{4}' )",
					               er.TimeStamp, er.FromCurrency, er.ToCurrency, er.ExchangeRateValue, (int) er.Quote_Type);
			return sql;
		}
		
		public void Add (ExchangeRate er)
		{
			base.Add (GenerateAddSql (er));
		}
		public int Add_GetIndex (ExchangeRate er)
		{
			return base.Add_GetIndex (GenerateAddSql (er));
		}

		public void Del (ExchangeRate er)
		{
			Del (er.Id);
		}

		public void Del (int index)
		{
			string sql = string.Format ("DELETE FROM ExchangeRates WHERE id = {0}", index);
			base.Del (sql);
		}


//FIXME Need to cope with ' embeeded in text strings.
		public void Change (ExchangeRate er)
		{
			string sql = "UPDATE ExchangeRates SET " +
				String.Format ("timestamp='{0}', from_currency={1}, to_currency={2}, exchange_rate={3}, quote_type={4} ",
					               er.TimeStamp, er.FromCurrency, er.ToCurrency, er.ExchangeRateValue,  (int) er.Quote_Type) +
				String.Format ("WHERE id = {0}", er.Id);
			base.Change (sql);			
		}

		public override MPC.Utils.DbItem DecodeReader (IDataReader reader)
		{
			ExchangeRate er = new ExchangeRate ();
			er.Id = reader.GetInt32(reader.GetOrdinal("id"));
			er.TimeStamp = reader.GetDateTime(reader.GetOrdinal("timestamp"));
			er.FromCurrency = reader.GetInt32(reader.GetOrdinal("from_currency"));
			er.ToCurrency = reader.GetInt32(reader.GetOrdinal("to_currency"));
			er.ExchangeRateValue = reader.GetDecimal (reader.GetOrdinal("exchange_rate"));
			er.Quote_Type = (MPC.Quotes.UpdateTypeEnum) reader.GetInt32(reader.GetOrdinal("quote_type"));
			return (MPC.Utils.DbItem) er;
		}		
		
		public ExchangeRate Get (int id)
		{
			string sql = "SELECT id, timestamp, from_currency, to_currency, exchange_rate, quote_type " +
				"FROM ExchangeRates " +
				String.Format ("WHERE id = {0}", id);
			return base.Get (sql) as ExchangeRate;							
		}

		public ExchangeRate GetLastDayQuote (int _from_currency, int _to_currency, System.DateTime _date)
		{
			ExchangeRate er = new ExchangeRate();
			er.FromCurrency = _from_currency;
			er.ToCurrency = _to_currency;
			er.TimeStamp = _date;
			return GetLastDayQuote (er);
		}
		
		public ExchangeRate GetLastDayQuote (ExchangeRate _er)
		{
			if (_er.FromCurrency == _er.ToCurrency) {
				_er.ExchangeRateValue = 1;
				return _er;
			} else {
				string sql = "SELECT id, timestamp, from_currency, to_currency, exchange_rate, quote_type " +
					"FROM ExchangeRates " +
					String.Format ("WHERE from_currency = '{0}' AND to_currency = '{1}' and timestamp = '{2}' ", _er.FromCurrency, _er.ToCurrency, _er.TimeStamp) +
					"ORDER BY from_currency, to_currency, timestamp ";
				return base.Get (sql) as ExchangeRate;	
			}
		}
		
		public System.Collections.ArrayList GetList (ExchangeRate _er)
		{
			string sql = "SELECT id, timestamp, from_currency, to_currency, exchange_rate, quote_type " +
				"FROM ExchangeRates " +
				String.Format ("WHERE from_currency = '{0}' AND to_currency = '{1}' ", _er.FromCurrency, _er.ToCurrency) +					
				"ORDER BY from_currency, to_currency, timestamp";
			return base.GetList (sql);							
		}
		
		public System.Collections.ArrayList GetList ()
		{
			string sql = "SELECT id, timestamp, from_currency, to_currency, exchange_rate, quote_type " +
				"FROM ExchangeRates " +
				"ORDER BY from_currency, to_currency, timestamp";
			return base.GetList (sql);							
		}
	}
}
