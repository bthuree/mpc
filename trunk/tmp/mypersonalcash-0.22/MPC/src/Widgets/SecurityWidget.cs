// SecurityWidget.cs
// 
// Copyright (C) 2008 Bengt Thuree
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;
using Gtk;
using MPC.Quotes;

namespace MPC.Widgets 
{
	
	
	public partial class SecurityWidget : Gtk.Bin
	{
		System.Collections.ArrayList ComboIndexes;
		SecurityStore s_s;

// FIXME Sensitive and AllowNew DEFAULTS (from Properties) is not working!!!
		
		public SecurityWidget()
		{
			this.Build();
			s_s = MPC.Database.GetDatabase.Securities;
			ComboIndexes = new System.Collections.ArrayList();	
			BuildComboTree();
		}

		public event EventHandler SelectionChanged;
		
		public bool AllowNew
		{
			get { return newButton.Visible; }
			set { newButton.Visible = value; }
		}
		
		public Security Selection
		{
			get { return (combo.Active >= 0 ? (Security) ComboIndexes [combo.Active] : null); }
			set { combo.Active = (value == null ? -1 : ComboIndexes.IndexOf (value)); }
		}		
		public int SecurityId
		{
			set { Selection = s_s.Get (value); }
		}
		
		private void BuildComboTree()
		{
			ComboIndexes.Clear();
			combo.Active = -1;
			System.Collections.ArrayList s_arr = s_s.GetList();
			ListStore ComboTreeStore = new ListStore (typeof (string), typeof (string));
			foreach (Security sec in s_arr) {
				ComboTreeStore.AppendValues (sec.Symbol, sec.Name); 
				ComboIndexes.Add (sec);
			}
			combo.Model = ComboTreeStore;			
		}		

		public void ComboHasChanged(object sender)
		{
			BuildComboTree();
		}
		
		protected virtual void OnComboChanged (object sender, System.EventArgs e)
		{
			if (SelectionChanged != null)
				SelectionChanged (this, e);
		}

		protected virtual void OnNewButtonClicked (object sender, System.EventArgs e)
		{
			MPC.Quotes.SecurityGui win = new MPC.Quotes.SecurityGui ();
			win.EventName += new MPC.Quotes.EventNameEventHandler (this.ComboHasChanged);						
			win.Run();	
		}
	}
}
