// PortfolioGuiProperties.cs
// 
// Copyright (C) 2008 Bengt Thuree
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;
using Gtk;
using MPC;
using MPC.Accounts;
using MPC.Quotes;
using MPC.Utils;

namespace MPC.Accounts
{

	public delegate void EventNameEventHandler (object sender);		
	
	public partial class PortfolioGuiProperties : Gtk.Dialog
	{

		System.Collections.ArrayList AccountHeaderIndexes, BaseCurrencyIndexes;
		AccountHeaderStore ah_s;
		CurrencyStore c_s;

		// http://www.csharphelp.com/archives/archive51.html

		public event EventNameEventHandler EventName; 		
		
		public PortfolioGuiProperties()
		{
			this.Build();
			ah_s = MPC.Database.GetDatabase.Accounts;
			c_s = MPC.Database.GetDatabase.Currencies;
			AccountHeaderIndexes = new System.Collections.ArrayList();	
			BaseCurrencyIndexes = new System.Collections.ArrayList();															
			allInvestmentAccounts.Active = true;
			allSecurity.Active = true;
			investmentAccount.Sensitive = false;	
			security.Sensitive = false;
			security.AllowNew = false;
			BuildInvestmentAccountTree();
			BuildBaseCurrencyTree();
		}

		private void NotifySubscriber()
		{
			if (EventName != null)
				EventName(this);
		}
		
		public void KillDialog()
		{
			this.Destroy();
		}
		
		public System.DateTime ReportDate
		{
			get { return calendar.Date; }
			set { calendar.Date = value; }			
		}

		public AccountHeader AccountFilter
		{
			get { 
				if (allInvestmentAccounts.Active == true)
					return null;
				else
					return (investmentAccount.Active >= 0 ? (AccountHeader) AccountHeaderIndexes [investmentAccount.Active] : null); 
			}
			set {
				if (value == null) {
					allInvestmentAccounts.Active = true; 
					specifyInvestmentAccount.Active = false;
				} else {
					allInvestmentAccounts.Active = false; 
					specifyInvestmentAccount.Active = true;					
					investmentAccount.Active = AccountHeaderIndexes.IndexOf (value);
				}
			}
		}
		
		public Security SecurityFilter
		{
			get { 
				if (allSecurity.Active == true)
					return null;
				else
					return (security.Selection); 
			}
			set {
				if (value == null) {
					allSecurity.Active = true; 
					specifySecurity.Active = false;
				} else {
					allSecurity.Active = false; 
					specifySecurity.Active = true;					
					security.Selection = value;
				}
			}
		}		
		
		public int UseBaseCurrency
		{
			get { return (baseCurrency.Active >= 0 ? (int) BaseCurrencyIndexes [baseCurrency.Active] : -1); }
			set { baseCurrency.Active = BaseCurrencyIndexes.IndexOf (value); }			
		}						
		
		protected virtual void OnAllInvestmentAccountsToggled (object sender, System.EventArgs e)
		{
			investmentAccount.Sensitive = (!allInvestmentAccounts.Active);			
		}

		protected virtual void OnAllSecurityToggled (object sender, System.EventArgs e)
		{
			security.Sensitive = (!allSecurity.Active);			
		}
		
		private void BuildInvestmentAccountTree()
		{
			AccountHeaderIndexes.Clear();
			investmentAccount.Active = -1;
			System.Collections.ArrayList ah_arr = ah_s.GetList();
			ListStore AccountHeaderTreeStore = new ListStore (typeof (string));
			foreach (AccountHeader ah in ah_arr) {
				AccountHeaderTreeStore.AppendValues (ah.Name); 
				AccountHeaderIndexes.Add (ah);
			}
			investmentAccount.Model = AccountHeaderTreeStore;			
		}				
		
		private void BuildBaseCurrencyTree()
		{
			BaseCurrencyIndexes.Clear();
			baseCurrency.Active = -1;
			System.Collections.ArrayList bc_arr = c_s.GetBaseCurrencies();
			ListStore TreeStore = new ListStore (typeof (string), typeof (string));
			foreach (Currency bc in bc_arr) {
				TreeStore.AppendValues (bc.Code, bc.Country); 
				BaseCurrencyIndexes.Add (bc.Id);
			}
			baseCurrency.Model = TreeStore;			
		}		

		protected virtual void OnButtonCancelClicked (object sender, System.EventArgs e)
		{
			this.Destroy();												
		}

		protected virtual void OnButtonOkClicked (object sender, System.EventArgs e)
		{
			NotifySubscriber();												
		}
				
	}
}
