// InvestmentTransactionStore.cs
// 
// Copyright (C) 2008 Bengt Thuree
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//


using System;
using System.IO;
using System.Data;



namespace MPC.Accounts
{
	public class InvestmentTransactionStore : MPC.Utils.DbGeneralStore
	{

		public InvestmentTransactionStore (IDbConnection _dbcon) : base (_dbcon, "InvestmentTransactions")
		{
		}

		private string CreateAddSql (InvestmentTransaction it)
		{
			string sql = "INSERT into InvestmentTransactions " + 
				" (account, timestamp, memo, security_id, shares, share_price, reconsiled, trans_type) " +
				String.Format ("VALUES ( {0}, '{1}', '{2}', {3}, {4}, {5}, {6}, {7})", 
					it.InvestmentAccount, it.TimeStamp, it.Memo, it.Security_id, it.Shares, it.Share_price, it.Reconsiled, (int) it.TransType
					);	
			return sql;
		}
		
		public void Add (InvestmentTransaction it)
		{
			base.Add (CreateAddSql (it));
		}

		public int Add_GetIndex (InvestmentTransaction it)
		{
			return base.Add_GetIndex (CreateAddSql (it));
		}
		
		public void Del (InvestmentTransaction it)
		{
			Del (it.Id);
		}
		
		public void Del (int index)
		{
			string sql = string.Format ("DELETE FROM InvestmentTransactions WHERE id = {0}", index);
			base.Del (sql);			
		}		

		public void Change (InvestmentTransaction it)
		{
			string sql = "UPDATE InvestmentTransactions SET " +
				String.Format ("account='{0}', timestamp='{1}', memo='{2}', " +
					" security_id='{3}', shares='{4}', share_price='{5}', reconsiled='{6}', trans_type='{7}' ", 
					it.InvestmentAccount, it.TimeStamp, it.Memo, it.Security_id, it.Shares, it.Share_price, it.Reconsiled, (int) it.TransType ) +
				String.Format ("WHERE id = {0}", it.Id); 
			base.Change (sql);			
		}

		public override MPC.Utils.DbItem DecodeReader (IDataReader reader)
		{
			InvestmentTransaction it = new InvestmentTransaction ();					
			it.Id = reader.GetInt32(reader.GetOrdinal("id"));
			it.InvestmentAccount = reader.GetInt32(reader.GetOrdinal("account"));
			it.TimeStamp = reader.GetDateTime(reader.GetOrdinal("timestamp"));
			it.Memo = reader.GetString(reader.GetOrdinal("memo"));
			it.Security_id = reader.GetInt32(reader.GetOrdinal("security_id"));
			it.Shares = reader.GetDecimal(reader.GetOrdinal("shares"));
			it.Share_price = reader.GetInt32(reader.GetOrdinal("share_price"));
			it.Reconsiled = reader.GetBoolean(reader.GetOrdinal("reconsiled"));
			it.TransType = (SecurityTransactionType) reader.GetInt32(reader.GetOrdinal("trans_type"));			
			return (MPC.Utils.DbItem) it;
		}
		
		public InvestmentTransaction Get (InvestmentTransaction it)
		{
			return Get (it.Id);							
		}
		
		public InvestmentTransaction Get (int id)
		{
			string sql = "SELECT id, account, timestamp, memo, security_id, shares, share_price, reconsiled, trans_type " +
				"FROM InvestmentTransactions " +
				String.Format ("WHERE id = {0}", id); 
			return base.Get (sql) as InvestmentTransaction;							
		}

		public System.Collections.ArrayList GetList (AccountHeader a, MPC.Quotes.Security s)
		{
			string sql = "SELECT id, account, timestamp, memo, security_id, shares, share_price, reconsiled, trans_type " +
					"FROM InvestmentTransactions " +
					String.Format ("WHERE security_id = {0} and account = {1} ", s.Id, a.Id) +
					" ORDER BY timestamp, id "; 
			return base.GetList (sql);							
		}

		public System.Collections.ArrayList GetList (AccountHeader a)
		{
			string sql = "SELECT id, account, timestamp, memo, security_id, shares, share_price, reconsiled, trans_type " +
					"FROM InvestmentTransactions " +
					String.Format ("WHERE account = {0} ", a.Id) +
					" ORDER BY timestamp, id "; 
			return base.GetList (sql);							
		}
				
		
		public System.Collections.ArrayList GetList (MPC.Quotes.Security s, System.DateTime split_date)
		{
			string sql = "SELECT id, account, timestamp, memo, security_id, shares, share_price, reconsiled, trans_type " +
					" FROM InvestmentTransactions " +
					String.Format ("WHERE security_id = {0} and timestamp < '{1}' ", s.Id, split_date) +
					" ORDER BY timestamp, id "; 
			return base.GetList (sql);							
		}

		public System.Collections.ArrayList GetList ()
		{
			string sql = "SELECT id, account, timestamp, memo, security_id, shares, share_price, reconsiled, trans_type " +
					"FROM InvestmentTransactions ORDER BY timestamp, id"; 
			return base.GetList (sql);							
		}
	}
}

