// SplitSecurity.cs
// 
// Copyright (C) 2008 Bengt Thuree
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;

using MPC.Accounts;
using MPC.Quotes;
using MPC.Utils;

namespace MPC.Accounts
{
	
	
	public class SplitSecurity
	{
		SecurityQuoteStore sq_s;
		InvestmentTransactionStore it_s;
		SecurityStore s_s;
		System.Collections.ArrayList account_list;
		
		public SplitSecurity()
		{
			sq_s = MPC.Database.GetDatabase.SecurityQuotes;
			s_s = MPC.Database.GetDatabase.Securities;
			it_s = MPC.Database.GetDatabase.InvestmentTransactions;					
			account_list = new System.Collections.ArrayList();
		}

		private decimal Share_SplitRatio (decimal _before_shares, decimal _after_shares)
		{
			return _after_shares / _before_shares;
		}
		private decimal Quote_SplitRatio (decimal _before_shares, decimal _after_shares)
		{
			return (1 / Share_SplitRatio (_before_shares, _after_shares));
		}		
		
		private void SplitQuotes (int sec, decimal _before_shares, decimal _after_shares, System.DateTime split_date)
		{
			System.Collections.ArrayList quotes_list = sq_s.GetList(s_s.Get(sec), split_date);
			foreach (SecurityQuote sq in quotes_list)
			{
				sq.Quote_Value = sq.Quote_Value * Quote_SplitRatio (_before_shares, _after_shares);
				sq_s.Change (sq);
			}
		}
		
		private void AddUniqueAccountToArray (InvestmentTransaction it)
		{
			if (!account_list.Contains (it.InvestmentAccount))
				account_list.Add (it.InvestmentAccount);
		}
		
		private void SplitInvTrans (int sec, decimal _before_shares, decimal _after_shares, System.DateTime split_date)
		{
			System.Collections.ArrayList it_list = it_s.GetList(s_s.Get(sec), split_date);
			foreach (InvestmentTransaction it in it_list)
			{
				AddUniqueAccountToArray (it);
				it.Shares = it.Shares * Share_SplitRatio (_before_shares, _after_shares);
				it_s.Change (it);
			}			
		}
		
		private void InsertHeader (int sec, decimal _before_shares, decimal _after_shares, System.DateTime split_date)
		{
			foreach (int act in account_list)
			{
				InvestmentTransaction it = new InvestmentTransaction();
				it.InvestmentAccount = act;
				it.TimeStamp = split_date;
				it.Memo = String.Format ("Stock Split. {0} : {1}", _before_shares, _after_shares);
				it.Security_id = sec;
				it.Shares = 0;
				it.Share_price = 0;
				it.Reconsiled = false;						
				it_s.Add (it);
			}			
		}
		
		
		public void PerformSplit (int sec, decimal _before_shares, decimal _after_shares, System.DateTime split_date)
		{
			try {
				s_s.BeginTran();
				SplitInvTrans (sec, _before_shares, _after_shares, split_date);
				SplitQuotes   (sec, _before_shares, _after_shares, split_date);
				InsertHeader (sec, _before_shares, _after_shares, split_date);
				s_s.EndTran();
			} catch (Exception e) {
				s_s.RollbackTran();
				Log.Error ("Doing a stock split failed!!");
				Log.Exception (e);
				throw (e);
			}
		}
	}
}
