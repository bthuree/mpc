// AccountHeaderGui.cs
// 
// Copyright (C) 2008 Bengt Thuree
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;
using Gtk;
using MPC.Accounts;
using MPC.Quotes;
using MPC.Utils;

namespace MPC.Accounts
{
	
	
	public partial class AccountHeaderGui : Gtk.Dialog
	{
		private AddressDetailsStore ad_s;
		private ContactDetailsStore cd_s;
		private CurrencyStore c_s;
		private AccountHeaderStore ah_s;
		private System.Collections.ArrayList AddressDetailsIndexes, ContactDetailsIndexes;
		private System.Collections.ArrayList AccountHeaderIndexes, CurrencyIndexes;
		private System.Collections.ArrayList ah_arr;		
		private ListStore AccountHeaderTreeStore;				

		private AccountHeader ah;
		
		public AccountHeaderGui (AccountHeaderStore _ahs, AddressDetailsStore _ads, ContactDetailsStore _cds, CurrencyStore _cs)
		{
			ad_s = _ads;
			ah_s = _ahs;
			c_s = _cs;
			cd_s = _cds;
			ah_arr = null;
			this.Build();
			AccountHeaderIndexes = new System.Collections.ArrayList();	
			CurrencyIndexes = new System.Collections.ArrayList();			
			AddressDetailsIndexes = new System.Collections.ArrayList();			
			ContactDetailsIndexes = new System.Collections.ArrayList();		
			PopulateTreeView(true);					
			BuildAddressTree();		
			BuildContactTree();
			BuildCurrencyTree();
			BuildParentTree();
		}
		
		private void BuildParentTree()
		{
			AccountHeaderIndexes.Clear();
			details_parent.Active = -1;
			System.Collections.ArrayList ah_arr = ah_s.GetList();
			Log.Debug (String.Format ("Number of Array Account Headers  {0}...", ah_arr.Count));
			ListStore AccountHeaderTreeStore = new ListStore (typeof (string));
			foreach (AccountHeader ah in ah_arr) {
				AccountHeaderTreeStore.AppendValues (ah.Name);
				AccountHeaderIndexes.Add (ah.Id);
				Log.Information (String.Format ("Added {0}", ah.Name));
			}
//			AccountHeaderTreeStore.AppendValues ("--------");
//			AccountHeaderTreeStore.AppendValues ("New account");
			details_parent.Model = AccountHeaderTreeStore;			
		}		
		
//FIXME Build Contact Tree according to Address!!!		
		private void BuildContactTree()
		{
			ContactDetailsIndexes.Clear();
			details_contact.Active = -1;
			System.Collections.ArrayList cd_arr = cd_s.GetList();
			Log.Debug (String.Format ("Number of Array Contacts  {0}...", cd_arr.Count));
			ListStore ContactDetailTreeStore = new ListStore (typeof (string));
			foreach (ContactDetail cd in cd_arr) {
				ContactDetailTreeStore.AppendValues (cd.Name);
				ContactDetailsIndexes.Add (cd.Id);
				Log.Information (String.Format ("Added {0}", cd.Name));
			}
			ContactDetailTreeStore.AppendValues ("--------");
			ContactDetailTreeStore.AppendValues ("New contact");
			details_contact.Model = ContactDetailTreeStore;
		}		
		
		private void BuildAddressTree()
		{
			AddressDetailsIndexes.Clear();
			details_address.Active = -1;
			System.Collections.ArrayList ad_arr = ad_s.GetList();
			Log.Debug (String.Format ("Number of Array Addresses  {0}...", ad_arr.Count));
			ListStore AddressDetailTreeStore = new ListStore (typeof (string), typeof(string));
			foreach (AddressDetail ad in ad_arr) {
				AddressDetailTreeStore.AppendValues (ad.Name, ad.Country);
				AddressDetailsIndexes.Add (ad.Id);
				Log.Information (String.Format ("Added {0}, {1}", ad.Name, ad.Id));
			}
			AddressDetailTreeStore.AppendValues ("--------");
			AddressDetailTreeStore.AppendValues ("New address");
			details_address.Model = AddressDetailTreeStore;			
		}		
		
		private void BuildCurrencyTree()
		{
			CurrencyIndexes.Clear();
			details_currency.Model = null;
			System.Collections.ArrayList cs_arr = c_s.GetList(); 
			Log.Debug (String.Format ("Number of Array Currencys  {0}...", cs_arr.Count));
			ListStore CurrencyTreeStore = new ListStore (typeof (string), typeof (string));
			foreach (Currency c in cs_arr) {
			Log.Debug (String.Format ("Currencie  {0}, {1}, {2}", c.Id, c.Code, c.Country));				
				CurrencyTreeStore.AppendValues (c.Code, c.Country);
				CurrencyIndexes.Add (c.Id);
			}
			CurrencyTreeStore.AppendValues ("--------");
			CurrencyTreeStore.AppendValues ("New Currency");
			details_currency.Model = CurrencyTreeStore;			
		}
		
		private void PopulateStore()
		{
			ah_arr = ah_s.GetList();			
			AccountHeaderTreeStore = new ListStore (typeof(string));
			foreach (AccountHeader ah in ah_arr) {
				AccountHeaderTreeStore.AppendValues (ah.Name);
			}
		}
		
		private void PopulateTreeView(bool define_columns)
		{
			PopulateStore();
			treeview.Model = AccountHeaderTreeStore;
			if (define_columns) {
				treeview.AppendColumn ("Name", new CellRendererText(), "text", 0);
			}
			this.ShowAll();
		}		
		
  		private void ClearDetails()
		{
			details_name.Text = String.Empty;
			details_accountnr.Text = String.Empty;
			details_type.Text = String.Empty;
			details_parent.Active = -1;
			details_address.Active = -1;
			details_contact.Active = -1;
			details_currency.Active = -1;			
			ah = null;			
		}		
		
		protected AccountHeader FetchAccountHeaderFromRow()
		{
			TreePath path = null;
			TreeViewColumn column = null;
			treeview.GetCursor (out path, out column);
			if (path != null) {
				int row = System.Convert.ToInt32 (path.ToString());
				AccountHeader ah = ah_arr[row] as AccountHeader;
				return ah;
			}			
			return null;
		}
		
		private string GuiText (string new_txt)
		{
			if (new_txt == null)
				return String.Empty;
			else
				return new_txt;
		}		
		
		protected virtual void OnButtonCancelClicked (object sender, System.EventArgs e)
		{
			this.Destroy();									
		}

		protected virtual void OnButtonOkClicked (object sender, System.EventArgs e)
		{
			this.Destroy();									
		}

		protected virtual void OnButtonSaveClicked (object sender, System.EventArgs e)
		{
			bool NewDetail = false;
			if (ah == null) {
				ah = new AccountHeader();
				NewDetail = true;
			}
			ah.Name = details_name.Text;
			ah.Account_nr = details_accountnr.Text;
			//FIXME Fix an Accounttype
			ah.AccountType = 0; //details_type.Text;
			ah.Parent = details_parent.Active >= 0 ? (int) AccountHeaderIndexes [details_parent.Active] : 0;
			ah.Address_id = (int) AddressDetailsIndexes [details_address.Active];
			ah.Contact_id = (int) ContactDetailsIndexes [details_contact.Active];
			ah.Currency_id = (int) CurrencyIndexes [details_currency.Active];

			if (NewDetail)
				ah_s.Add(ah);
			else
				ah_s.Change(ah);
			PopulateTreeView(false);
			BuildParentTree();
			ClearDetails();					
		}

		protected virtual void OnNewActionActivated (object sender, System.EventArgs e)
		{
			ClearDetails();												
		}

		protected virtual void OnDeleteActionActivated (object sender, System.EventArgs e)
		{
			ah = FetchAccountHeaderFromRow();
			if (ah != null) {
				ah_s.Del(ah);
				PopulateTreeView(false);
				ClearDetails();
			}												
		}

		protected virtual void OnTreeviewCursorChanged (object sender, System.EventArgs e)
		{
			ClearDetails();
			ah = FetchAccountHeaderFromRow();
			if (ah != null) {
				details_name.Text = GuiText (ah.Name);
				details_accountnr.Text = GuiText (ah.Account_nr);
				details_type.Text = GuiText (ah.AccountType.ToString());
				details_address.Active = AddressDetailsIndexes.IndexOf (ah.Address_id);
				details_contact.Active = ContactDetailsIndexes.IndexOf (ah.Contact_id);
				details_parent.Active = AccountHeaderIndexes.IndexOf (ah.Parent);
				details_currency.Active = CurrencyIndexes.IndexOf (ah.Currency_id);				
			}												
		}

		protected virtual void OnDetailsParentChanged (object sender, System.EventArgs e)
		{
			Log.Debug ("Changing Parent");
		}

		protected virtual void OnDetailsAddressChanged (object sender, System.EventArgs e)
		{
			if (details_address.Active > AddressDetailsIndexes.Count) 
			{
				MPC.Accounts.AddressGui win = new MPC.Accounts.AddressGui (ad_s);
				win.Run();				
				BuildAddressTree();
				details_address.Active = -1;
			}			
			BuildContactTree();				
			details_contact.Active = -1;			
		}

		protected virtual void OnDetailsContactChanged (object sender, System.EventArgs e)
		{
			if (details_contact.Active > ContactDetailsIndexes.Count) 
			{
				MPC.Accounts.ContactGui win = new MPC.Accounts.ContactGui (cd_s, ad_s);
				win.Run();				
				BuildContactTree();
				details_contact.Active = -1;
			}									
		}

		protected virtual void OnDetailsCurrencyChanged (object sender, System.EventArgs e)
		{
			if (details_currency.Active > CurrencyIndexes.Count) 
			{
				Log.Information ("Calling Currency...");
				MPC.Quotes.CurrencyGui win = new MPC.Quotes.CurrencyGui (c_s);
				win.Run();				
				BuildCurrencyTree();
				details_currency.Active = -1;
			}			
		}
	}
}
