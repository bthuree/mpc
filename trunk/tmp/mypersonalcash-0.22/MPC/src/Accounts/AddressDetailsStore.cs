// AddressDetailsStore.cs
// 
// Copyright (C) 2008 Bengt Thuree
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//


using System;
using System.IO;
using System.Data;



namespace MPC.Accounts
{
	public class AddressDetailsStore : MPC.Utils.DbGeneralStore
	{
		
		public AddressDetailsStore (IDbConnection _dbcon) : base (_dbcon, "AddressDetails")
		{
		}

		private string GenerateAddSql (AddressDetail adr)
		{
			string sql = "INSERT into AddressDetails " + 
				" (name, address1, address2, zip, city, country, uri, swift, phone, fax, notes) " +
				String.Format ("VALUES ( '{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}', '{10}' )", 
					adr.Name, adr.Address1, adr.Address2, adr.Zip, adr.City, adr.Country, 
					adr.Uri, adr.Swift, adr.Phone, adr.Fax, adr.Notes
				               );	
			return sql;
		}		
		
		public void Add (AddressDetail adr)
		{
			base.Add (GenerateAddSql (adr));
		}
		
		public int Add_GetIndex (AddressDetail adr)
		{
			return base.Add_GetIndex (GenerateAddSql (adr));
		}		
		
		public void Del (AddressDetail adr)
		{
			Del (adr.Id);
		}
		
		public void Del (int index)
		{
			string sql = string.Format ("DELETE FROM AddressDetails WHERE id = {0}", index);
			base.Del (sql);			
		}		

		public new void Del (string _name)
		{
			string sql = string.Format ("DELETE FROM AddressDetails WHERE name = '{0}'", _name);
			base.Del (sql);			
		}				

		public void Change (AddressDetail adr)
		{
			string sql = "UPDATE AddressDetails SET " +
				String.Format ("name='{0}', address1='{1}', address2='{2}', zip='{3}', city='{4}', " +
								"country='{5}', uri='{6}', swift='{7}', phone='{8}', fax='{9}', notes='{10}' ", 
					adr.Name, adr.Address1, adr.Address2, adr.Zip, adr.City, adr.Country, 
					adr.Uri, adr.Swift, adr.Phone, adr.Fax, adr.Notes) +
				String.Format ("WHERE id = {0}", adr.Id); 
			base.Change (sql);			
		}

		public override MPC.Utils.DbItem DecodeReader (IDataReader reader)
		{
			AddressDetail adr = new AddressDetail ();					
			adr.Id = reader.GetInt32(reader.GetOrdinal("id"));
			adr.Name = reader.GetString(reader.GetOrdinal("name"));
			adr.Address1 = reader.GetString(reader.GetOrdinal("address1"));
			adr.Address2 = reader.GetString(reader.GetOrdinal("address2"));
			adr.Zip = reader.GetString(reader.GetOrdinal("zip"));
			adr.City = reader.GetString(reader.GetOrdinal("city"));
			adr.Country = reader.GetString(reader.GetOrdinal("country"));
			adr.Uri = reader.GetString(reader.GetOrdinal("uri"));
			adr.Swift = reader.GetString(reader.GetOrdinal("swift"));
			adr.Phone = reader.GetString(reader.GetOrdinal("phone"));
			adr.Fax = reader.GetString(reader.GetOrdinal("fax"));
			adr.Notes = reader.GetString(reader.GetOrdinal("notes"));
			return (MPC.Utils.DbItem) adr;
		}

		public AddressDetail Get (AddressDetail ad)
		{
			return Get (ad.Id);							
		}		
		
		public AddressDetail Get (int id)
		{
			string sql = "SELECT id, name, address1, address2, zip, city, country, uri, swift, phone, fax, notes " +
				"FROM AddressDetails " +
				String.Format ("WHERE id = {0}", id); 
			return base.Get (sql) as AddressDetail;							
		}

		public new AddressDetail Get (string _name)
		{
			string sql = "SELECT id, name, address1, address2, zip, city, country, uri, swift, phone, fax, notes " +
				"FROM AddressDetails " +
				String.Format ("WHERE name = '{0}'", _name); 
			return base.Get (sql) as AddressDetail;							
		}

		public System.Collections.ArrayList GetList ()
		{
			string sql = "SELECT id, name, address1, address2, zip, city, country, uri, swift, phone, fax, notes " +
					"FROM AddressDetails ORDER BY name"; 
			return base.GetList (sql);							
		}
	}
}

