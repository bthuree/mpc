// ContactDetail.cs
// 
// Copyright (C) 2008 Bengt Thuree
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;

namespace MPC.Accounts
{
	
	public class ContactDetail : MPC.Utils.DbItem
	{
		
		public ContactDetail ()
		{
			name = null;
			phone = null;
			mobile = null;
			other_phone = null;
			fax = null;
			work_mail = null;
			private_mail = null;
			other_mail = null;
			address_id = 0;
			uri = null;
			notes = null;
		}
		
		private int address_id;
		public int Address_id {
			get { return address_id; }
			set { address_id = value; }
		}

		private string name;
		public string Name {
			get { return name; }
			set { name = value; }
		}
		
		private string phone;
		public string Phone {
			get { return phone; }
			set { phone = value; }
		}

		private string mobile;
		public string Mobile {
			get { return mobile; }
			set { mobile = value; }
		}
		
		private string other_phone;
		public string Other_phone {
			get { return other_phone; }
			set { other_phone = value; }
		}
		private string fax;
		public string Fax {
			get { return fax; }
			set { fax = value; }
		}
		private string work_mail;
		public string Work_mail {
			get { return work_mail; }
			set { work_mail = value; }
		}
		private string private_mail;
		public string Private_mail {
			get { return private_mail; }
			set { private_mail = value; }
		}
		
		private string other_mail;
		public string Other_mail {
			get { return other_mail; }
			set { other_mail = value; }
		}
		
		private string uri;
		public string Uri {
			get { return uri; }
			set { uri = value; }
		}
		
		private string notes;
		public string Notes {
			get { return notes; }
			set { notes = value; }
		}
		
		public override bool Equals(Object obj) 
		{
			//Check for null and compare run-time types.
			if (obj == null || GetType() != obj.GetType()) 
				return false;
			ContactDetail c2 = (ContactDetail)obj;
			return (Name == c2.Name);
		}
		
		public override int GetHashCode() 
		{
			return Name.GetHashCode();
		}		
	}
}
