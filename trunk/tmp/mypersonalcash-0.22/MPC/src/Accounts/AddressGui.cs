// AddressGui.cs
// 
// Copyright (C) 2008 Bengt Thuree
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;
using Gtk;
using MPC.Utils;

namespace MPC.Accounts
{
	
	
	public partial class AddressGui : Gtk.Dialog
	{
		private AddressDetail ad;
		private System.Collections.ArrayList ad_ar;
		private AddressDetailsStore ad_s;
		
		private ListStore AddressDetailTreeStore;		
		
		public AddressGui(AddressDetailsStore _ads)
		{
			ad_s = _ads;
			this.Build();
			PopulateTreeView(true);						
		}
		
		private void PopulateStore()
		{
			ad_ar = ad_s.GetList();			
			AddressDetailTreeStore = new ListStore (typeof(string), typeof(string));
			foreach (AddressDetail ad in ad_ar) {
				AddressDetailTreeStore.AppendValues (ad.Name, ad.Country);
			}
		}
		
		private void PopulateTreeView(bool define_columns)
		{
			PopulateStore();
			treeview.Model = AddressDetailTreeStore;
			if (define_columns) {
				treeview.AppendColumn ("Name", new CellRendererText(), "text", 0);
				treeview.AppendColumn ("Country", new CellRendererText(), "text", 1);
			}
			this.ShowAll();
		}		
		
  		private void ClearDetails()
		{
			details_name.Text = String.Empty;
			details_address1.Text = String.Empty;
			details_address2.Text = String.Empty;
			details_zip.Text = String.Empty;
			details_city.Text = String.Empty;
			details_country.Text = String.Empty;
			details_uri.Text = String.Empty;
			details_swift.Text = String.Empty;
			details_phone.Text = String.Empty;
			details_fax.Text = String.Empty;
			details_notes.Text = String.Empty;
			ad = null;			
		}		
		
		protected AddressDetail FetchAddressDetailFromRow()
		{
			TreePath path = null;
			TreeViewColumn column = null;
			treeview.GetCursor (out path, out column);
			if (path != null) {
				int row = System.Convert.ToInt32 (path.ToString());
				AddressDetail ad = ad_ar[row] as AddressDetail;
				return ad;
			}			
			return null;
		}
		
		private string GuiText (string new_txt)
		{
			if (new_txt == null)
				return String.Empty;
			else
				return new_txt;
		}		

		protected virtual void OnButtonSaveClicked (object sender, System.EventArgs e)
		{
			bool NewAddressDetail = false;
			if (ad == null) {
				ad = new AddressDetail();
				NewAddressDetail = true;
			}
			ad.Name = details_name.Text;
			ad.Address1 = details_address1.Text;
			ad.Address2 = details_address2.Text;
			ad.Zip = details_zip.Text;
			ad.City = details_city.Text;
			ad.Country = details_country.Text;
			ad.Uri = details_uri.Text;
			ad.Swift = details_swift.Text;
			ad.Phone = details_phone.Text;
			ad.Fax = details_fax.Text;
			ad.Notes = details_notes.Text;

			if (NewAddressDetail)
				ad_s.Add(ad);
			else
				ad_s.Change(ad);
			PopulateTreeView(false);
			ClearDetails();				
		}

		protected virtual void OnNewActionActivated (object sender, System.EventArgs e)
		{
			ClearDetails();									
		}

		protected virtual void OnDeleteActionActivated (object sender, System.EventArgs e)
		{
			ad = FetchAddressDetailFromRow();
			if (ad != null) {
				ad_s.Del(ad);
				PopulateTreeView(false);
				ClearDetails();
			}									
		}

		protected virtual void OnButtonCancelClicked (object sender, System.EventArgs e)
		{
			this.Destroy();						
		}

		protected virtual void OnButtonOkClicked (object sender, System.EventArgs e)
		{
			this.Destroy();						
		}

		protected virtual void OnTreeviewCursorChanged (object sender, System.EventArgs e)
		{
			ClearDetails();
			ad = FetchAddressDetailFromRow();
			if (ad != null) {
				details_name.Text = GuiText (ad.Name);
				details_address1.Text = GuiText (ad.Address1);
				details_address2.Text = GuiText (ad.Address2);
				details_zip.Text = GuiText (ad.Zip);
				details_city.Text = GuiText (ad.City);
				details_country.Text = GuiText (ad.Country);
				details_uri.Text = GuiText (ad.Uri);
				details_swift.Text = GuiText (ad.Swift);
				details_phone.Text = GuiText (ad.Phone);
				details_fax.Text = GuiText (ad.Fax);
				details_notes.Text = GuiText (ad.Notes);			
			}									
		}
	}
}
