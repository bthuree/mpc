// NumberOfShares.cs
// 
// Copyright (C) 2008 Bengt Thuree
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;

namespace MPC.Accounts
{
	
	
	public class NumberOfShares : MPC.Utils.DbItem
	{
		
		public NumberOfShares()
		{
			account_nr = System.Int32.MinValue;
			security_nr = System.Int32.MinValue;
			number_shares = 0m;
		}
		
		private int account_nr;
		public int AccountNr {
			get { return account_nr; }
			set { account_nr = value; }
		}
		private string account_name;
		public string AccountName {
			get { return account_name; }
			set { account_name = value; }
		}						
		
		private int security_nr;
		public int SecurityNr {
			get { return security_nr; }
			set { security_nr = value; }
		}
		private string symbol;
		public string Symbol {
			get { return symbol; }
			set { symbol = value; }
		}
		
		private decimal number_shares;
		public decimal NumberShares {
			get { return number_shares; }
			set { number_shares = value; }
		}	
				
		public override bool Equals(Object obj) 
		{
			//Check for null and compare run-time types.
			if (obj == null || GetType() != obj.GetType()) 
				return false;
			NumberOfShares c2 = (NumberOfShares)obj;
			return ((account_nr == c2.account_nr) && (security_nr == c2.security_nr) && (number_shares == c2.number_shares));
		}
		
		public override int GetHashCode() 
		{
			return account_nr.GetHashCode() ^ security_nr.GetHashCode() ^ number_shares.GetHashCode();
		}		

	}
}