// ContactGui.cs
// 
// Copyright (C) 2008 Bengt Thuree
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;
using Gtk;
using MPC.Utils;

namespace MPC.Accounts
{
	
	
	public partial class ContactGui : Gtk.Dialog
	{
		private ContactDetail cd;
		private System.Collections.ArrayList cd_ar;
		private ContactDetailsStore cd_s;
		private AddressDetailsStore ad_s;
		private System.Collections.ArrayList AddressDetailsIndexes;		
		
		private ListStore ContactDetailTreeStore;		
		
		public ContactGui(ContactDetailsStore _cds, AddressDetailsStore _ads)
		{
			cd_s = _cds;
			ad_s = _ads;
			AddressDetailsIndexes = new System.Collections.ArrayList();			
			this.Build();
			PopulateTreeView(true);		
			BuildAddressTree();			
		}
		
		private void BuildAddressTree()
		{
			AddressDetailsIndexes.Clear();
			details_address.Active = -1;
			System.Collections.ArrayList ad_arr = ad_s.GetList();
			Log.Debug (String.Format ("Number of Array Quotes  {0}...", ad_arr.Count));
			ListStore AddressDetailTreeStore = new ListStore (typeof (string), typeof(string));
			foreach (AddressDetail ad in ad_arr) {
				AddressDetailTreeStore.AppendValues (ad.Name, ad.Country);
				AddressDetailsIndexes.Add (ad.Id);
				Log.Information (String.Format ("Added {0}, {1}", ad.Name, ad.Id));
			}
			AddressDetailTreeStore.AppendValues ("--------");
			AddressDetailTreeStore.AppendValues ("New address");
			details_address.Model = AddressDetailTreeStore;			
		}		
		
		private void PopulateStore()
		{
			cd_ar = cd_s.GetList();			
			ContactDetailTreeStore = new ListStore (typeof(string));
			foreach (ContactDetail cd in cd_ar) {
				ContactDetailTreeStore.AppendValues (cd.Name);
			}
		}
		
		private void PopulateTreeView(bool define_columns)
		{
			PopulateStore();
			treeview.Model = ContactDetailTreeStore;
			if (define_columns) {
				treeview.AppendColumn ("Name", new CellRendererText(), "text", 0);
			}
			this.ShowAll();
		}		
		
  		private void ClearDetails()
		{
			details_name.Text = String.Empty;
			details_phone.Text = String.Empty;
			details_mobile.Text = String.Empty;
			details_other_phone.Text = String.Empty;
			details_fax.Text = String.Empty;
			details_work_mail.Text = String.Empty;
			details_private_mail.Text = String.Empty;
			details_other_mail.Text = String.Empty;
			details_address.Active = -1;
			details_uri.Text = String.Empty;
			details_notes.Text = String.Empty;
			cd = null;			
		}		
		
		protected ContactDetail FetchContactDetailFromRow()
		{
			TreePath path = null;
			TreeViewColumn column = null;
			treeview.GetCursor (out path, out column);
			if (path != null) {
				int row = System.Convert.ToInt32 (path.ToString());
				ContactDetail cd = cd_ar[row] as ContactDetail;
				return cd;
			}			
			return null;
		}
		
		private string GuiText (string new_txt)
		{
			if (new_txt == null)
				return String.Empty;
			else
				return new_txt;
		}		

		protected virtual void OnButtonSaveClicked (object sender, System.EventArgs e)
		{
			bool NewContactDetail = false;
			if (cd == null) {
				cd = new ContactDetail();
				NewContactDetail = true;
			}
			cd.Name = details_name.Text;
			cd.Phone = details_phone.Text;
			cd.Mobile = details_mobile.Text;
			cd.Other_phone = details_other_phone.Text;
			cd.Fax = details_fax.Text;
			cd.Work_mail = details_work_mail.Text;
			cd.Private_mail = details_private_mail.Text;
			cd.Other_mail = details_other_mail.Text;
			cd.Address_id = (int) AddressDetailsIndexes [details_address.Active];			
			cd.Uri = details_uri.Text;
			cd.Notes = details_notes.Text;

			if (NewContactDetail)
				cd_s.Add(cd);
			else
				cd_s.Change(cd);
			PopulateTreeView(false);
			ClearDetails();				
		}

		protected virtual void OnNewActionActivated (object sender, System.EventArgs e)
		{
			ClearDetails();									
		}

		protected virtual void OnDeleteActionActivated (object sender, System.EventArgs e)
		{
			cd = FetchContactDetailFromRow();
			if (cd != null) {
				cd_s.Del(cd);
				PopulateTreeView(false);
				ClearDetails();
			}									
		}

		protected virtual void OnButtonCancelClicked (object sender, System.EventArgs e)
		{
			this.Destroy();						
		}

		protected virtual void OnButtonOkClicked (object sender, System.EventArgs e)
		{
			this.Destroy();						
		}

		protected virtual void OnTreeviewCursorChanged (object sender, System.EventArgs e)
		{
			ClearDetails();
			cd = FetchContactDetailFromRow();
			if (cd != null) {
				details_name.Text = GuiText (cd.Name);
				details_phone.Text = GuiText (cd.Phone);
				details_mobile.Text = GuiText (cd.Mobile);
				details_other_phone.Text = GuiText (cd.Other_phone);
				details_fax.Text = GuiText (cd.Fax);
				details_work_mail.Text = GuiText (cd.Work_mail);
				details_private_mail.Text = GuiText (cd.Private_mail);
				details_other_mail.Text = GuiText (cd.Other_mail);
				details_address.Active = AddressDetailsIndexes.IndexOf (cd.Address_id);
				details_uri.Text = GuiText (cd.Uri);
				details_notes.Text = GuiText (cd.Notes);			
			}									
		}

		protected virtual void OnDetailsAddressChanged (object sender, System.EventArgs e)
		{
			if (details_address.Active > AddressDetailsIndexes.Count) 
			{
				MPC.Accounts.AddressGui win = new MPC.Accounts.AddressGui (ad_s);
				win.Run();				
				BuildAddressTree();
				details_address.Active = -1;
			}			
		}
		
	}
}
