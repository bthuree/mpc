// NumberOfSharesView.cs
// 
// Copyright (C) 2008 Bengt Thuree
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;
using System.Data;

using MPC;
using MPC.Utils;
using MPC.Quotes;

namespace MPC.Accounts
{
	
	
	public class NumberOfSharesView : MPC.Utils.DbGeneralStore
	{
		NumberOfShares nos;
		public NumberOfSharesView(IDbConnection _dbcon) : base (_dbcon, "view")
		{
		}
		public override MPC.Utils.DbItem DecodeReader (IDataReader reader)
		{
			NumberOfShares nos = new NumberOfShares ();					
			try {
				nos.AccountNr = reader.GetInt32(reader.GetOrdinal("accountid"));
				nos.AccountName = reader.GetString(reader.GetOrdinal("name"));				
			} catch (Exception e)
			{ // If error, we did not request account details. Ignore it.
				if (e == null)
				{}
			} 
			nos.SecurityNr = reader.GetInt32(reader.GetOrdinal("securityid"));
			nos.Symbol = reader.GetString(reader.GetOrdinal("symbol"));			
			nos.NumberShares = reader.GetDecimal(reader.GetOrdinal("sum"));
			return (MPC.Utils.DbItem) nos;
		}

		private System.Collections.ArrayList RemoveEmptyLots (System.Collections.ArrayList nos_db)
		{
			System.Collections.ArrayList new_db = new System.Collections.ArrayList();
			foreach (NumberOfShares nos in nos_db)
			{
				if (nos.NumberShares > 0)
				{
					new_db.Add (nos);
				}
			}
			return new_db;
		}
		
		private decimal ReturnShareNumbers (NumberOfShares nos)
		{
			if (nos == null)
				return 0;
			else
				return nos.NumberShares;			
		}
		
		public decimal CountShares (Security sec)
		{
			return nos.NumberShares;
		}
		public decimal CountShares (int sec)
		{
			string sql = "SELECT sec.id as securityid, sec.symbol, SUM(it.shares) " +
				"FROM investmenttransactions it, securities sec " +
				String.Format ("WHERE security_id = {0} AND sec.id = it.security_id ", sec) +					
				"GROUP BY sec.id, sec.symbol " +
				"ORDER BY sec.symbol";			
			nos = base.Get (sql) as NumberOfShares;
			return ReturnShareNumbers(nos);
		}
		
		public decimal CountShares (AccountHeader act, Security sec)
		{
			return CountShares (act.Id, sec.Id, System.DateTime.Today);
		}		
		public decimal CountShares (int act, int sec)
		{
			return CountShares (act, sec, System.DateTime.Today);
		}
		public decimal CountShares (AccountHeader act, Security sec, DateTime dt)
		{
			return CountShares (act.Id, sec.Id, dt);
		}	
		public decimal CountShares (int act, int sec, DateTime dt)
		{
			string sql = "SELECT sec.id as securityid, sec.symbol, SUM(it.shares) " +
				"FROM investmenttransactions it, securities sec " +
				String.Format ("WHERE account = {0} AND security_id = {1} AND sec.id = it.security_id AND timestamp <= '{2}' ", act, sec, dt.ToString()) +					
				"GROUP BY sec.id, sec.symbol " +
				"ORDER BY sec.symbol";			
			nos = base.Get (sql) as NumberOfShares;
			return ReturnShareNumbers(nos);
		}			
		
		public decimal CountShares (Security sec, DateTime dt)
		{
			return CountShares (sec.Id, dt);
		}						
		public decimal CountShares (int sec, DateTime dt)
		{
			string sql = "SELECT sec.id as securityid, sec.symbol, SUM(it.shares) " +
				"FROM investmenttransactions it, securities sec " +
				String.Format ("WHERE it.security_id = {0} AND sec.id = it.security_id AND timestamp <= '{1}' ", sec, dt.ToString()) +
				"GROUP BY sec.id, sec.symbol " +
				"ORDER BY sec.symbol";			
			nos = base.Get (sql) as NumberOfShares;
			return ReturnShareNumbers(nos);
		}

		public System.Collections.ArrayList GetList ()
		{
			return GetList (System.DateTime.Today);
		}
		public System.Collections.ArrayList GetList (DateTime dt)
		{
			string sql = "SELECT ah.id AS accountid, ah.name, sec.id AS securityid, sec.symbol, SUM(it.shares) " +
				"FROM investmenttransactions it, accountheaders ah, securities sec " +
				"WHERE ah.id = it.account AND sec.id = it.security_id " +
				String.Format ("AND timestamp <= '{0}' ", dt.ToString()) +					
				"GROUP BY ah.name, ah.id, sec.id, sec.symbol " +
				"ORDER BY ah.name, sec.symbol";					
			return RemoveEmptyLots(base.GetList (sql));							
		}
		
		public System.Collections.ArrayList GetList (Security sec)
		{
			return GetList (sec, System.DateTime.Today);
		}
		public System.Collections.ArrayList GetList (Security sec, DateTime dt)
		{
			string sql = "SELECT sec.id AS securityid, sec.symbol, SUM(it.shares) " +
				"FROM investmenttransactions it, securities sec " +
				"WHERE sec.id = it.security_id " +
				String.Format ("AND sec.id = '{0}' ", sec.Id) +										
				String.Format ("AND timestamp <= '{0}' ", dt.ToString()) +					
				"GROUP BY sec.id, sec.symbol " +
				"ORDER BY sec.symbol";								
			return RemoveEmptyLots(base.GetList (sql));							
		}		
//		public System.Collections.ArrayList GetList (Security sec, DateTime dt)
//		{
//			string sql = "SELECT ah.id AS accountid, ah.name, sec.id AS securityid, sec.symbol, SUM(it.shares) " +
//				"FROM investmenttransactions it, accountheaders ah, securities sec " +
//				"WHERE ah.id = it.account AND sec.id = it.security_id " +
//				String.Format ("AND sec.id = '{0}' ", sec.Id) +										
//				String.Format ("AND timestamp <= '{0}' ", dt.ToString()) +					
//				"GROUP BY ah.name, ah.id, sec.id, sec.symbol " +
//				"ORDER BY ah.name, sec.symbol";								
//			return RemoveEmptyLots(base.GetList (sql));							
//		}				

		public System.Collections.ArrayList GetList (AccountHeader ah)
		{
			return GetList (ah, System.DateTime.Today);
		}
		public System.Collections.ArrayList GetList (AccountHeader ah, DateTime dt)
		{
			string sql = "SELECT ah.id AS accountid, ah.name, sec.id AS securityid, sec.symbol, SUM(it.shares) " +
				"FROM investmenttransactions it, accountheaders ah, securities sec " +
				"WHERE ah.id = it.account AND sec.id = it.security_id " +
				String.Format ("AND ah.id = '{0}' ", ah.Id) +															
				String.Format ("AND timestamp <= '{0}' ", dt.ToString()) +					
				"GROUP BY ah.name, ah.id, sec.id, sec.symbol " +
				"ORDER BY ah.name, sec.symbol";					
			return RemoveEmptyLots(base.GetList (sql));							
		}		
		
		public System.Collections.ArrayList GetList (AccountHeader ah, Security sec)
		{
			return GetList (ah, sec, System.DateTime.Today);
		}
		public System.Collections.ArrayList GetList (AccountHeader ah, Security sec, DateTime dt)
		{
			string sql = "SELECT ah.id AS accountid, ah.name, sec.id AS securityid, sec.symbol, SUM(it.shares) " +
				"FROM investmenttransactions it, accountheaders ah, securities sec " +
				"WHERE ah.id = it.account AND sec.id = it.security_id " +
				String.Format ("AND ah.id = '{0}' ", ah.Id) +
				String.Format ("AND sec.id = '{0}' ", sec.Id) +										
				String.Format ("AND timestamp <= '{0}' ", dt.ToString()) +					
				"GROUP BY ah.name, ah.id, sec.id, sec.symbol " +
				"ORDER BY ah.name, sec.symbol";					
			return RemoveEmptyLots(base.GetList (sql));							
		}				
		
	}
}
