// Db.cs
// 
// Copyright (C) 2008 Bengt Thuree
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;
using MPC;
using MPC.Quotes;
using MPC.Utils;
using MPC.Accounts;
using System.Data;
using Npgsql;

namespace MPC
{

	public static class Database
	{
		private static Db db;
		private static bool testDB = false;
		public static void SetTestDB()
		{
			testDB = true;
		}
		
		public static Db GetDatabase {
			get { 
				if (db == null) {
					// Load the database, upgrading/creating it as needed
//					string base_directory = FSpot.Global.BaseDirectory;
//					if (! File.Exists (base_directory))
//						Directory.CreateDirectory (base_directory);
					
					db = new Db ();
					try {
						//db.Init (Path.Combine (base_directory, "photos.db"), true);
						if (testDB)
							db.Init (true);
						else
							db.Init();
					} catch (System.Exception e) {
						Log.Exception(e);
//						new RepairDbDialog (e, db.Repair (), null);
//						db.Init (Path.Combine (base_directory, "photos.db"), true);
						db.Init ();
					}
				}
				return db; 
			}
		}
					
	}

	public class Db : IDisposable
	{
		QuoteSourceStore quotesource_store;
		SecurityStore security_store;
		SecurityQuoteStore security_quote_store;
		CurrencyStore currency_store;
		ExchangeRateStore exchange_rate_store;
		
		AddressDetailsStore address_details_store;
		ContactDetailsStore contact_details_store;

		InvestmentTransactionStore investmenttransactions_store;
		AccountHeaderStore accounts_store;	
		
		NumberOfSharesView number_of_shares_view;

		IDbConnection dbcon;
		
//		bool empty;
//		string path;		
		
		public QuoteSourceStore QuoteSources {
			get { return quotesource_store; }
		}
		public SecurityStore Securities {
			get { return security_store; }
		}		
		public SecurityQuoteStore SecurityQuotes {
			get { return security_quote_store; }
		}			
		public CurrencyStore Currencies {
			get { return currency_store; }
		}			
		public ExchangeRateStore ExchangeRates {
			get { return exchange_rate_store; }
		}					

		public AddressDetailsStore AddressDetails {
			get { return address_details_store; }
		}					
		public ContactDetailsStore ContactDetails {
			get { return contact_details_store; }
		}					
		
		public AccountHeaderStore Accounts {
			get { return accounts_store; }
		}					
		public InvestmentTransactionStore InvestmentTransactions {
			get { return investmenttransactions_store; }
		}	
		
		public NumberOfSharesView GetNumberOfSharesView {
			get { return number_of_shares_view; }
		}							
		
		
//		public void Init (string path, bool create_if_missing)
		public void Init (bool test)
		{ 
			Init("mpc_test");
		}
		
		public void Init ()
		{
			Init ("mpc");
		}
		
		public void Init (string db_name)		

		{
			try {
			Log.Debug ("Starting to load", "Really starting to open the database");
			uint timer = Log.DebugTimerStart ();
			string connectionString = String.Format (
				"Server={0};" +
				"Database={1};" +
				"User ID={2};" +
				"Password={3};", "localhost", db_name, "postgres", "postgres");
			dbcon = new NpgsqlConnection(connectionString);
			dbcon.Open();			
/*
			bool new_db = ! File.Exists (path);
			this.path = path;

			if (new_db && ! create_if_missing)
				throw new Exception (path + ": File not found");

			database = new QueuedSqliteDatabase(path);
			if (database.GetFileVersion(path) == 2)
				SqliteUpgrade ();

			// Load or create the meta table
	 		meta_store = new MetaStore (Database, new_db);

			// Update the database schema if necessary
			FSpot.Database.Updater.Run (this);

			Database.BeginTransaction ();
*/
			quotesource_store = new QuoteSourceStore ( dbcon );
			currency_store = new CurrencyStore ( dbcon );
			security_store = new SecurityStore ( dbcon );
			security_quote_store = new SecurityQuoteStore ( dbcon );	
			exchange_rate_store = new ExchangeRateStore ( dbcon );	

			address_details_store = new AddressDetailsStore ( dbcon );	
			contact_details_store = new ContactDetailsStore ( dbcon );	
			
			accounts_store = new AccountHeaderStore ( dbcon );	
			investmenttransactions_store = new InvestmentTransactionStore ( dbcon );
				
			number_of_shares_view = new NumberOfSharesView ( dbcon );

			
//			FSpot.DBusProxyFactory.Load (this);
			
//			Database.CommitTransaction ();

//			empty = new_db;
			Log.DebugTimerPrint (timer, "Db Initialization took {0}");
			} catch (Exception ex)
			{
				MPC.Utils.Log.ErrorFormat ("FAILED to open database {0}", db_name );			
				MPC.Utils.Log.Exception (ex);
			}
		}					

		~Db()
		{
			Log.Debug ("Closing the open connection to the DB");	
			dbcon.Close();
			dbcon = null;
		}
		
		public void Dispose ()
		{
			Log.Debug ("In Dispose DB");
		}		
	}
}