// /home/bengt/development/mpc/mpc/Main.cs created with MonoDevelop
// User: bengt at 3:19 PM 1/5/2008
//
//
// project created on 1/5/2008 at 3:19 PM
using System;
using Gtk;
using MPC.Utils;

namespace MPC
{
	
//TODO Open the databases globally.
//FIXME Add a global TRY / Catch
//FIXME Check with SunBin how to avoid having everything as public methods!!! Not Good!
	public class MainClass
	{
		
		static void TestLynx ()
		{
			MPC.Quotes.QuoteSource qss = new MPC.Quotes.QuoteSource ("test uri");
			MPC.Quotes.FetchOnlineQuote foq = new MPC.Quotes.FetchOnlineQuote (qss, "ERIC" );
			string html_txt1 = "<html><head><title>Title</title></head><body>This web page do have HTML tags</body></html>";
			string nohtml_txt1 = "This web page do have HTML tags\n\n";		
			string res_str = foq.RemoveHtmlTags (html_txt1);
			// below just to remove warnings.
			if (res_str == nohtml_txt1)
			{
				Log.Debug("Lynx Works fine");
			}
			
		}		
		static void Version ()
		{
			Console.WriteLine (
				"F-Spot  {0} - (c)2003-2008, Novell Inc" + Environment.NewLine +
				"Personal photo management for the GNOME Desktop" + Environment.NewLine, 
				0.0);
		}

		static void Versions ()
		{
		    Version ();
	            Console.WriteLine (".NET Version: " + Environment.Version.ToString());
	            Console.WriteLine (String.Format("{0}Assembly Version Information:", Environment.NewLine));
	            
//	            foreach(Assembly asm in AppDomain.CurrentDomain.GetAssemblies()) {
//			    AssemblyName name = asm.GetName();
//	                    Console.WriteLine ("\t" + name.Name + " (" + name.Version.ToString () + ")");
//			}	
		}
		
		static void Help ()
		{
			Version ();
			Console.WriteLine ();
			Console.WriteLine (
				"Usage: f-spot [options] " + Environment.NewLine +
				"Options:" + Environment.NewLine +
			  	"-b -basedir PARAM   path to the photo database folder" + Environment.NewLine +
			    	"-? -help -usage     Show this help list" + Environment.NewLine +
				"-i -import PARAM    import from the given uri" + Environment.NewLine +
				"-p -photodir PARAM  default import folder" + Environment.NewLine +
				"-shutdown           shutdown a running f-spot instance" + Environment.NewLine +
				"-slideshow          display a slideshow" + Environment.NewLine +
				"-V -version         Display version and licensing information" + Environment.NewLine +
				"-versions           Display version and dependencies informations" + Environment.NewLine +
				"-v -view            view file(s) or directory(ies)");
		}
		
		public static int Main (string[] args)
		{
			bool get_quotes = false;
			Log.Debugging = true;
	
			Log.InformationFormat ("MPC tag = {0}, Version number = {1}", MPC.Version.Tag, MPC.Version.Number);

			
			for (int i = 0; i < args.Length; i++) {
				switch (args [i]) {
				case "-h": case "-?": case "-help": case "--help": case "-usage":
					Help ();
					return 0;
//					break;
				case "-versions": case "--versions":
					Versions ();
					return 0;
//					break;
				
				case "-V": case "-version": case "--version":
					Version ();
					return 0;
//					break;
					
				case "-getquotes": case "--getquotes":
					get_quotes = true;
					break;
					
				case "--debug":
					Log.Debugging = true;
					// Debug GdkPixbuf critical warnings
					GLib.LogFunc logFunc = new GLib.LogFunc (GLib.Log.PrintTraceLogFunction);
					GLib.Log.SetLogHandler ("GdkPixbuf", GLib.LogLevelFlags.Critical, logFunc);
			
					// Debug Gtk critical warnings
					GLib.Log.SetLogHandler ("Gtk", GLib.LogLevelFlags.Critical, logFunc);
					break;					
/*				
				case "-shutdown": case "--shutdown":
					Log.Information ("Shutting down existing F-Spot server...");
					shutdown = true;
					break;

				case "-b": case "-basedir": case "--basedir":
					if (i+1 == args.Length || args[i+1].StartsWith ("-")) {
						Log.Error ("f-spot: -basedir option takes one argument");
						return 1;
					}
					FSpot.Global.BaseDirectory = args [++i];
					Log.InformationFormat ("BaseDirectory is now {0}", FSpot.Global.BaseDirectory);
					break;

				case "-p": case "-photodir": case "--photodir":
					if (i+1 == args.Length || args[i+1].StartsWith ("-")) {
						Log.Error ("f-spot: -photodir option takes one argument");
						return 1;
					}
					FSpot.Global.PhotoDirectory = System.IO.Path.GetFullPath (args [++i]);
					Log.InformationFormat ("PhotoDirectory is now {0}", FSpot.Global.PhotoDirectory);
					break;

				case "-i": case "-import": case "--import":
					if (i+1 == args.Length) {
						Log.Error ("f-spot: -import option takes one argument");
						return 1;
					}
					import_uri = args [++i];
					break;
				
				case "-slideshow": case "--slideshow":
					slideshow = true;
					break;

				case "-v": case "-view": case "--view":
					if (i+1 == args.Length || args[i+1].StartsWith ("-")) {
						Log.Error ("f-spot: -view option takes (at least) one argument");
						return 1;
					}
					view = true;
					while (!(i+1 == args.Length) && !args[i+1].StartsWith ("-"))
						uris.Add (args [++i]);
	//				if (!System.IO.Directory.Exists (args[i+1]) && !System.IO.File.Exists (args[i+1])) {
	//					Log.Error ("f-spot: -view argument must be an existing file or directory");
	//					return 1;
	//				}
					break;

				case "-versions": case "--versions":
					Versions ();
					return 0;
				
				case "-V": case "-version": case "--version":
					Version ();
					return 0;
				
				case "--debug":
					Log.Debugging = true;
					// Debug GdkPixbuf critical warnings
					GLib.LogFunc logFunc = new GLib.LogFunc (GLib.Log.PrintTraceLogFunction);
					GLib.Log.SetLogHandler ("GdkPixbuf", GLib.LogLevelFlags.Critical, logFunc);
			
					// Debug Gtk critical warnings
					GLib.Log.SetLogHandler ("Gtk", GLib.LogLevelFlags.Critical, logFunc);

					break;
				case "--uninstalled": case "--gdb":
					break;
*/					
				default:
					if (args [i].StartsWith ("--profile"))
						break;
					if (args [i].StartsWith ("--trace"))
						break;
					Log.DebugFormat ("Unparsed argument >>{0}<<", args [i]);
					Help ();
					return 1;
				}
			}

			try {
				Application.Init ();
				if (get_quotes)
				{
					Log.Information ("Updating online security quotes...");
					MPC.Quotes.OnlineQuotes oq = new MPC.Quotes.OnlineQuotes ();
					oq.GetQuote();	

					Log.Information ("Updating online exchange rates...");					
					MPC.Quotes.OnlineExchangeRates oer = new MPC.Quotes.OnlineExchangeRates ();
					oer.GetExchangeRates();
					
					return 0;					
				}
				MainWindow win = new MainWindow (Database.GetDatabase);
				win.Show ();
				Application.Run ();
			} catch (Exception e) {
					Log.Exception (e);
			}			
			return 0;
		}
	}
}