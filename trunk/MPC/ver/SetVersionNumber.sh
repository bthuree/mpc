#!/bin/sh

DIR=`dirname $0`
VER=`svnversion ${DIR}/..`

sed "s/_NUMBER_/${VER}/" ${DIR}/Version.template > ${DIR}/Version.cs
