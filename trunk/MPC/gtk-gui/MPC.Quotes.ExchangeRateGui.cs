
// This file has been generated by the GUI designer. Do not modify.
namespace MPC.Quotes
{
	public partial class ExchangeRateGui
	{
		private global::Gtk.UIManager UIManager;

		private global::Gtk.Action refreshAction;

		private global::Gtk.VBox vbox2;

		private global::Gtk.Toolbar toolbar1;

		private global::Gtk.HBox hbox1;

		private global::Gtk.Frame frame1;

		private global::Gtk.Alignment GtkAlignment7;

		private global::Gtk.HBox hbox2;

		private global::Gtk.Frame frame4;

		private global::Gtk.Alignment GtkAlignment5;

		private global::Gtk.ScrolledWindow GtkScrolledWindow1;

		private global::Gtk.TreeView treeview_from_active;

		private global::Gtk.Label GtkLabel5;

		private global::Gtk.Label GtkLabel3;

		private global::Gtk.Frame frame2;

		private global::Gtk.Alignment GtkAlignment3;

		private global::Gtk.Frame frame6;

		private global::Gtk.Alignment GtkAlignment2;

		private global::Gtk.ScrolledWindow GtkScrolledWindow2;

		private global::Gtk.TreeView treeview_to_basepool;

		private global::Gtk.Label GtkLabel8;

		private global::Gtk.Label GtkLabel6;

		private global::Gtk.Frame frame5;

		private global::Gtk.Alignment GtkAlignment6;

		private global::Gtk.Frame frame7;

		private global::Gtk.Alignment GtkAlignment8;

		private global::Gtk.ScrolledWindow GtkScrolledWindow3;

		private global::Gtk.TreeView treeview_result;

		private global::Gtk.Label GtkLabel9;

		private global::Gtk.Label GtkLabel7;

		private global::Gtk.VBox vbox3;

		private global::Gtk.Label label1;

		private global::Gtk.ProgressBar progressbar;

		private global::Gtk.Button buttonCancel;

		private global::Gtk.Button buttonOk;

		protected virtual void Build ()
		{
			global::Stetic.Gui.Initialize (this);
			// Widget MPC.Quotes.ExchangeRateGui
			this.UIManager = new global::Gtk.UIManager ();
			global::Gtk.ActionGroup w1 = new global::Gtk.ActionGroup ("Default");
			this.refreshAction = new global::Gtk.Action ("refreshAction", null, null, "gtk-refresh");
			w1.Add (this.refreshAction, null);
			this.UIManager.InsertActionGroup (w1, 0);
			this.AddAccelGroup (this.UIManager.AccelGroup);
			this.Name = "MPC.Quotes.ExchangeRateGui";
			this.Title = global::Mono.Unix.Catalog.GetString ("Exchange rates");
			this.WindowPosition = ((global::Gtk.WindowPosition)(4));
			// Internal child MPC.Quotes.ExchangeRateGui.VBox
			global::Gtk.VBox w2 = this.VBox;
			w2.Name = "dialog1_VBox";
			w2.BorderWidth = ((uint)(2));
			// Container child dialog1_VBox.Gtk.Box+BoxChild
			this.vbox2 = new global::Gtk.VBox ();
			this.vbox2.Name = "vbox2";
			this.vbox2.Spacing = 6;
			// Container child vbox2.Gtk.Box+BoxChild
			this.UIManager.AddUiFromString ("<ui><toolbar name='toolbar1'><toolitem name='refreshAction' action='refreshAction'/></toolbar></ui>");
			this.toolbar1 = ((global::Gtk.Toolbar)(this.UIManager.GetWidget ("/toolbar1")));
			this.toolbar1.Name = "toolbar1";
			this.toolbar1.ShowArrow = false;
			this.toolbar1.ToolbarStyle = ((global::Gtk.ToolbarStyle)(0));
			this.toolbar1.IconSize = ((global::Gtk.IconSize)(3));
			this.vbox2.Add (this.toolbar1);
			global::Gtk.Box.BoxChild w3 = ((global::Gtk.Box.BoxChild)(this.vbox2[this.toolbar1]));
			w3.Position = 0;
			w3.Expand = false;
			w3.Fill = false;
			// Container child vbox2.Gtk.Box+BoxChild
			this.hbox1 = new global::Gtk.HBox ();
			this.hbox1.Name = "hbox1";
			this.hbox1.Spacing = 6;
			// Container child hbox1.Gtk.Box+BoxChild
			this.frame1 = new global::Gtk.Frame ();
			this.frame1.Name = "frame1";
			// Container child frame1.Gtk.Container+ContainerChild
			this.GtkAlignment7 = new global::Gtk.Alignment (0f, 0f, 1f, 1f);
			this.GtkAlignment7.Name = "GtkAlignment7";
			this.GtkAlignment7.LeftPadding = ((uint)(12));
			// Container child GtkAlignment7.Gtk.Container+ContainerChild
			this.hbox2 = new global::Gtk.HBox ();
			this.hbox2.Name = "hbox2";
			this.hbox2.Spacing = 6;
			// Container child hbox2.Gtk.Box+BoxChild
			this.frame4 = new global::Gtk.Frame ();
			this.frame4.Name = "frame4";
			this.frame4.ShadowType = ((global::Gtk.ShadowType)(0));
			// Container child frame4.Gtk.Container+ContainerChild
			this.GtkAlignment5 = new global::Gtk.Alignment (0f, 0f, 1f, 1f);
			this.GtkAlignment5.Name = "GtkAlignment5";
			this.GtkAlignment5.LeftPadding = ((uint)(12));
			// Container child GtkAlignment5.Gtk.Container+ContainerChild
			this.GtkScrolledWindow1 = new global::Gtk.ScrolledWindow ();
			this.GtkScrolledWindow1.Name = "GtkScrolledWindow1";
			this.GtkScrolledWindow1.ShadowType = ((global::Gtk.ShadowType)(1));
			// Container child GtkScrolledWindow1.Gtk.Container+ContainerChild
			this.treeview_from_active = new global::Gtk.TreeView ();
			this.treeview_from_active.WidthRequest = 150;
			this.treeview_from_active.CanFocus = true;
			this.treeview_from_active.Name = "treeview_from_active";
			this.GtkScrolledWindow1.Add (this.treeview_from_active);
			this.GtkAlignment5.Add (this.GtkScrolledWindow1);
			this.frame4.Add (this.GtkAlignment5);
			this.GtkLabel5 = new global::Gtk.Label ();
			this.GtkLabel5.Name = "GtkLabel5";
			this.GtkLabel5.LabelProp = global::Mono.Unix.Catalog.GetString ("<b>Active currencies</b>");
			this.GtkLabel5.UseMarkup = true;
			this.frame4.LabelWidget = this.GtkLabel5;
			this.hbox2.Add (this.frame4);
			global::Gtk.Box.BoxChild w7 = ((global::Gtk.Box.BoxChild)(this.hbox2[this.frame4]));
			w7.Position = 0;
			w7.Expand = false;
			w7.Fill = false;
			this.GtkAlignment7.Add (this.hbox2);
			this.frame1.Add (this.GtkAlignment7);
			this.GtkLabel3 = new global::Gtk.Label ();
			this.GtkLabel3.Name = "GtkLabel3";
			this.GtkLabel3.LabelProp = global::Mono.Unix.Catalog.GetString ("<b>From Currency</b>");
			this.GtkLabel3.UseMarkup = true;
			this.frame1.LabelWidget = this.GtkLabel3;
			this.hbox1.Add (this.frame1);
			global::Gtk.Box.BoxChild w10 = ((global::Gtk.Box.BoxChild)(this.hbox1[this.frame1]));
			w10.Position = 0;
			w10.Expand = false;
			w10.Fill = false;
			// Container child hbox1.Gtk.Box+BoxChild
			this.frame2 = new global::Gtk.Frame ();
			this.frame2.Name = "frame2";
			// Container child frame2.Gtk.Container+ContainerChild
			this.GtkAlignment3 = new global::Gtk.Alignment (0f, 0f, 1f, 1f);
			this.GtkAlignment3.Name = "GtkAlignment3";
			this.GtkAlignment3.LeftPadding = ((uint)(12));
			// Container child GtkAlignment3.Gtk.Container+ContainerChild
			this.frame6 = new global::Gtk.Frame ();
			this.frame6.Name = "frame6";
			this.frame6.ShadowType = ((global::Gtk.ShadowType)(0));
			// Container child frame6.Gtk.Container+ContainerChild
			this.GtkAlignment2 = new global::Gtk.Alignment (0f, 0f, 1f, 1f);
			this.GtkAlignment2.Name = "GtkAlignment2";
			this.GtkAlignment2.LeftPadding = ((uint)(12));
			// Container child GtkAlignment2.Gtk.Container+ContainerChild
			this.GtkScrolledWindow2 = new global::Gtk.ScrolledWindow ();
			this.GtkScrolledWindow2.Name = "GtkScrolledWindow2";
			this.GtkScrolledWindow2.ShadowType = ((global::Gtk.ShadowType)(1));
			// Container child GtkScrolledWindow2.Gtk.Container+ContainerChild
			this.treeview_to_basepool = new global::Gtk.TreeView ();
			this.treeview_to_basepool.WidthRequest = 150;
			this.treeview_to_basepool.CanFocus = true;
			this.treeview_to_basepool.Name = "treeview_to_basepool";
			this.GtkScrolledWindow2.Add (this.treeview_to_basepool);
			this.GtkAlignment2.Add (this.GtkScrolledWindow2);
			this.frame6.Add (this.GtkAlignment2);
			this.GtkLabel8 = new global::Gtk.Label ();
			this.GtkLabel8.Name = "GtkLabel8";
			this.GtkLabel8.LabelProp = global::Mono.Unix.Catalog.GetString ("<b>Base pool</b>");
			this.GtkLabel8.UseMarkup = true;
			this.frame6.LabelWidget = this.GtkLabel8;
			this.GtkAlignment3.Add (this.frame6);
			this.frame2.Add (this.GtkAlignment3);
			this.GtkLabel6 = new global::Gtk.Label ();
			this.GtkLabel6.Name = "GtkLabel6";
			this.GtkLabel6.LabelProp = global::Mono.Unix.Catalog.GetString ("<b>To currency</b>");
			this.GtkLabel6.UseMarkup = true;
			this.frame2.LabelWidget = this.GtkLabel6;
			this.hbox1.Add (this.frame2);
			global::Gtk.Box.BoxChild w16 = ((global::Gtk.Box.BoxChild)(this.hbox1[this.frame2]));
			w16.Position = 1;
			w16.Expand = false;
			w16.Fill = false;
			// Container child hbox1.Gtk.Box+BoxChild
			this.frame5 = new global::Gtk.Frame ();
			this.frame5.Name = "frame5";
			// Container child frame5.Gtk.Container+ContainerChild
			this.GtkAlignment6 = new global::Gtk.Alignment (0f, 0f, 1f, 1f);
			this.GtkAlignment6.Name = "GtkAlignment6";
			this.GtkAlignment6.LeftPadding = ((uint)(12));
			// Container child GtkAlignment6.Gtk.Container+ContainerChild
			this.frame7 = new global::Gtk.Frame ();
			this.frame7.Name = "frame7";
			this.frame7.ShadowType = ((global::Gtk.ShadowType)(0));
			// Container child frame7.Gtk.Container+ContainerChild
			this.GtkAlignment8 = new global::Gtk.Alignment (0f, 0f, 1f, 1f);
			this.GtkAlignment8.Name = "GtkAlignment8";
			this.GtkAlignment8.LeftPadding = ((uint)(12));
			// Container child GtkAlignment8.Gtk.Container+ContainerChild
			this.GtkScrolledWindow3 = new global::Gtk.ScrolledWindow ();
			this.GtkScrolledWindow3.Name = "GtkScrolledWindow3";
			this.GtkScrolledWindow3.ShadowType = ((global::Gtk.ShadowType)(1));
			// Container child GtkScrolledWindow3.Gtk.Container+ContainerChild
			this.treeview_result = new global::Gtk.TreeView ();
			this.treeview_result.WidthRequest = 250;
			this.treeview_result.CanFocus = true;
			this.treeview_result.Name = "treeview_result";
			this.GtkScrolledWindow3.Add (this.treeview_result);
			this.GtkAlignment8.Add (this.GtkScrolledWindow3);
			this.frame7.Add (this.GtkAlignment8);
			this.GtkLabel9 = new global::Gtk.Label ();
			this.GtkLabel9.Name = "GtkLabel9";
			this.GtkLabel9.LabelProp = global::Mono.Unix.Catalog.GetString ("<b>Exchange rates</b>");
			this.GtkLabel9.UseMarkup = true;
			this.frame7.LabelWidget = this.GtkLabel9;
			this.GtkAlignment6.Add (this.frame7);
			this.frame5.Add (this.GtkAlignment6);
			this.GtkLabel7 = new global::Gtk.Label ();
			this.GtkLabel7.Name = "GtkLabel7";
			this.GtkLabel7.LabelProp = global::Mono.Unix.Catalog.GetString ("<b>Result</b>");
			this.GtkLabel7.UseMarkup = true;
			this.frame5.LabelWidget = this.GtkLabel7;
			this.hbox1.Add (this.frame5);
			global::Gtk.Box.BoxChild w22 = ((global::Gtk.Box.BoxChild)(this.hbox1[this.frame5]));
			w22.Position = 2;
			w22.Expand = false;
			w22.Fill = false;
			this.vbox2.Add (this.hbox1);
			global::Gtk.Box.BoxChild w23 = ((global::Gtk.Box.BoxChild)(this.vbox2[this.hbox1]));
			w23.Position = 1;
			// Container child vbox2.Gtk.Box+BoxChild
			this.vbox3 = new global::Gtk.VBox ();
			this.vbox3.Name = "vbox3";
			this.vbox3.Spacing = 6;
			// Container child vbox3.Gtk.Box+BoxChild
			this.label1 = new global::Gtk.Label ();
			this.label1.Name = "label1";
			this.label1.LabelProp = global::Mono.Unix.Catalog.GetString ("label1");
			this.vbox3.Add (this.label1);
			global::Gtk.Box.BoxChild w24 = ((global::Gtk.Box.BoxChild)(this.vbox3[this.label1]));
			w24.Position = 0;
			w24.Expand = false;
			w24.Fill = false;
			// Container child vbox3.Gtk.Box+BoxChild
			this.progressbar = new global::Gtk.ProgressBar ();
			this.progressbar.Name = "progressbar";
			this.vbox3.Add (this.progressbar);
			global::Gtk.Box.BoxChild w25 = ((global::Gtk.Box.BoxChild)(this.vbox3[this.progressbar]));
			w25.Position = 1;
			w25.Expand = false;
			w25.Fill = false;
			this.vbox2.Add (this.vbox3);
			global::Gtk.Box.BoxChild w26 = ((global::Gtk.Box.BoxChild)(this.vbox2[this.vbox3]));
			w26.Position = 2;
			w26.Expand = false;
			w26.Fill = false;
			w2.Add (this.vbox2);
			global::Gtk.Box.BoxChild w27 = ((global::Gtk.Box.BoxChild)(w2[this.vbox2]));
			w27.Position = 0;
			// Internal child MPC.Quotes.ExchangeRateGui.ActionArea
			global::Gtk.HButtonBox w28 = this.ActionArea;
			w28.Name = "dialog1_ActionArea";
			w28.Spacing = 6;
			w28.BorderWidth = ((uint)(5));
			w28.LayoutStyle = ((global::Gtk.ButtonBoxStyle)(4));
			// Container child dialog1_ActionArea.Gtk.ButtonBox+ButtonBoxChild
			this.buttonCancel = new global::Gtk.Button ();
			this.buttonCancel.CanDefault = true;
			this.buttonCancel.CanFocus = true;
			this.buttonCancel.Name = "buttonCancel";
			this.buttonCancel.UseStock = true;
			this.buttonCancel.UseUnderline = true;
			this.buttonCancel.Label = "gtk-cancel";
			this.AddActionWidget (this.buttonCancel, -6);
			global::Gtk.ButtonBox.ButtonBoxChild w29 = ((global::Gtk.ButtonBox.ButtonBoxChild)(w28[this.buttonCancel]));
			w29.Expand = false;
			w29.Fill = false;
			// Container child dialog1_ActionArea.Gtk.ButtonBox+ButtonBoxChild
			this.buttonOk = new global::Gtk.Button ();
			this.buttonOk.CanDefault = true;
			this.buttonOk.CanFocus = true;
			this.buttonOk.Name = "buttonOk";
			this.buttonOk.UseStock = true;
			this.buttonOk.UseUnderline = true;
			this.buttonOk.Label = "gtk-ok";
			this.AddActionWidget (this.buttonOk, -5);
			global::Gtk.ButtonBox.ButtonBoxChild w30 = ((global::Gtk.ButtonBox.ButtonBoxChild)(w28[this.buttonOk]));
			w30.Position = 1;
			w30.Expand = false;
			w30.Fill = false;
			if ((this.Child != null)) {
				this.Child.ShowAll ();
			}
			this.DefaultWidth = 730;
			this.DefaultHeight = 327;
			this.Show ();
			this.refreshAction.Activated += new global::System.EventHandler (this.OnRefreshActionActivated);
			this.treeview_from_active.CursorChanged += new global::System.EventHandler (this.OnTreeviewFromActiveCursorChanged);
			this.treeview_to_basepool.CursorChanged += new global::System.EventHandler (this.OnTreeviewToBasepoolCursorChanged);
			this.buttonCancel.Clicked += new global::System.EventHandler (this.OnButtonCancelClicked);
			this.buttonOk.Clicked += new global::System.EventHandler (this.OnButtonOkClicked);
		}
	}
}
