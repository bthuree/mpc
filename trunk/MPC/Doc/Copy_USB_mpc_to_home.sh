#!/bin/sh

TO="/home/bengt/Development/mpc/trunk/"

PWD=`pwd`

cd  /media/USB\ DISK/mpc/trunk

cp mpc/src/*.cs ${TO}/mpc/src/
cp mpc/Doc/ToDo.txt ${TO}/mpc/Doc/ToDo.txt

cp mpc/src/Accounts/*.cs ${TO}/mpc/src/Accounts/
# cp mpc/src/AccountTest/*.cs ${TO}/mpc/AccountTest/

cp mpc/src/Quotes/*.cs ${TO}/mpc/src/Quotes/
# cp Quotes/QuotesTest/*.cs ${TO}/Quotes/QuotesTest/

cp MPC.Utils/src/*.cs ${TO}/MPC.Utils/src/
