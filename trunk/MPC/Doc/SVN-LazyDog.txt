Some subversion commands
http://svnbook.red-bean.com/en/1.0/svn-book.html#svn-ch-4-sect-2.1

svn mkdir file:///home/bengt/Development/svn/mpc/branches

svn list file:///home/bengt/Development/svn/mpc/tags

svn copy 	file:///home/bengt/Development/svn/mpc/trunk file:///home/bengt/Development/svn/mpc/tags/0.02           -m "Tag message"
	
svn copy 	file:///home/bengt/Development/svn/mpc/trunk file:///home/bengt/Development/svn/mpc/branches/TestBranch -m "Branch message"
		