// SecurityQuote.cs
// 
// Copyright (C) 2008 Bengt Thuree
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;

namespace MPC.Quotes 
{
	
	public enum UpdateTypeEnum {
		manual = 1,
		buy = 2,
		sell = 3,
		online = 4
	}
	
	public class SecurityQuote  : MPC.Utils.DbItem
	{
		
		public SecurityQuote()
		{
			symbol = System.Int32.MinValue;
			timestamp = System.DateTime.MinValue;
			quote_value = System.Decimal.MinValue;
			quote_type = UpdateTypeEnum.manual;
		}
		
		private int symbol;
		public int Symbol {
			get { return symbol; }
			set { symbol = value; }
		}

		private UpdateTypeEnum quote_type;
		public UpdateTypeEnum Quote_Type {
			get { return quote_type; }
			set { quote_type = value; }
		}
		

		private decimal quote_value;
		public decimal Quote_Value {
			get { return quote_value; }
			set { quote_value = value; }
		}
		
		private System.DateTime timestamp;
		public System.DateTime TimeStamp {
			get { return timestamp; }
			set { timestamp = value; }
		}	

		public override bool Equals(Object obj) 
		{
			//Check for null and compare run-time types.
			if (obj == null || GetType() != obj.GetType()) 
				return false;
			SecurityQuote q2 = (SecurityQuote)obj;
			return ( Id == q2.Id );
		}
		
//FIXME Create a beter HASH code for Quote
		public override int GetHashCode() 
		{
			return symbol.GetHashCode();
		}		
		
	}
}