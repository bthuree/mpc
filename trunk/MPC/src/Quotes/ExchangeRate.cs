// ExchangeRate.cs
// 
// Copyright (C) 2008 Bengt Thuree
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;

namespace MPC.Quotes 
{
	

	public class ExchangeRate  : MPC.Utils.DbItem
	{
		
		public ExchangeRate()
		{
			timestamp = System.DateTime.MinValue;
			from_currency = System.Int32.MinValue;
			to_currency = System.Int32.MinValue;
			exchange_rate = System.Decimal.MinValue;
			quote_type = UpdateTypeEnum.manual;
		}
		

		private MPC.Quotes.UpdateTypeEnum quote_type;
		public MPC.Quotes.UpdateTypeEnum Quote_Type {
			get { return quote_type; }
			set { quote_type = value; }
		}

		private int from_currency;
		public int FromCurrency {
			get { return from_currency; }
			set { from_currency = value; }
		}		
		
		private int to_currency;
		public int ToCurrency {
			get { return to_currency; }
			set { to_currency = value; }
		}		
		
		private decimal exchange_rate;
		public decimal ExchangeRateValue {
			get { return exchange_rate; }
			set { exchange_rate = value; }
		}
		
		private System.DateTime timestamp;
		public System.DateTime TimeStamp {
			get { return timestamp; }
			set { timestamp = value; }
		}	

		public override bool Equals(Object obj) 
		{
			//Check for null and compare run-time types.
			if (obj == null || GetType() != obj.GetType()) 
				return false;
			ExchangeRate er2 = (ExchangeRate)obj;
			return ( Id == er2.Id );
		}
		
//FIXME Create a beter HASH code for Quote
		public override int GetHashCode() 
		{
			return from_currency.GetHashCode() + to_currency.GetHashCode();
		}		
		
	}
}