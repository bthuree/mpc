// Security.cs
// 
// Copyright (C) 2008 Bengt Thuree
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;

namespace MPC.Quotes
{
	
	public class Security  : MPC.Utils.DbItem
	{
		
		public Security (string _symbol)
		{
			symbol = _symbol;
			name = null;
			currency = System.Int32.MinValue;
			quote_source = System.Int32.MinValue;
			quote_is_percentage = false;
		}
		
		private string name;
		public string Name {
			get { return name; }
			set { name = value; }
		}

		private string symbol;
		public string Symbol {
			get { return symbol; }
			set { symbol = value; }
		}
		
		private int currency;
		public int Currency {
			get { return currency; }
			set { currency = value; }
		}
		
		private int quote_source;
		public int Quote_Source {
			get { return quote_source; }
			set { quote_source = value; }
		}

		private bool quote_is_percentage;
		public bool QuoteIsPercentage {
			get { return quote_is_percentage; }
			set { quote_is_percentage = value; }
		}		
		
		public override bool Equals(Object obj) 
		{
			//Check for null and compare run-time types.
			if (obj == null || GetType() != obj.GetType()) 
				return false;
			Security qs2 = (Security)obj;
			return (symbol == qs2.symbol);
		}
		
		public override int GetHashCode() 
		{
			return symbol.GetHashCode();
		}		
		
	}
}
