// QuoteSourceStore.cs
// 
// Copyright (C) 2008 Bengt Thuree
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//



using System;
using System.IO;
using System.Data;
using Npgsql;

namespace MPC.Quotes
{
	public class QuoteSourceStore : MPC.Utils.DbGeneralStore
	{
		
		public QuoteSourceStore(IDbConnection _dbcon) : base (_dbcon, "QuoteSources")
		{
			if (Count() == 0)
				AddDefault();
		}

		public void AddDefault()
		{
			QuoteSource qs = new QuoteSource("Yahoo Global");
			qs.Uri = "http://finance.yahoo.com/d/quotes.csv?s=%1&f=sl1d1";				
			qs.Date_regexp = "[^,]*,[^,]*,\"([^\"]*)\"";
			qs.Date_format = "M/d/yyyy";		
			qs.Quote_regexp = "[^,]*,([^,]*),.*";
			qs.Symbol_regexp = "\"([^,\"]*)\",.*";			
			Add (qs);
			
			qs = new QuoteSource ("SEB Fonds");
			qs.Uri = "http://www.seb.se/pow/fmk/2100/Senaste_fondkurserna.TXT";
			qs.Date_regexp = "^(.*);.*;.*;.*";
			qs.Date_format = "yyyy-MM-dd";
			qs.Quote_regexp = "^.*;.*;(.*);.*";
			qs.Symbol_regexp = "^.*;(.*);.*;.*";
			qs.Encoding = "ISO-8859-1";			
			Add (qs);
			
			qs = new QuoteSource ("SEB Aktieobligation");
			qs.Uri = "http://www.seb.se/pow/BorsFinans/listor/id_bonds_struct.asp?emittent=Visa&EMITTENTLISTA=&typ=Visa&EMITTENTLISTA=Aktieobligation&ffd=Visa&EMITTENTLISTA=";
			qs.Date_regexp = @"Uppdaterad: ([0-9-]*).*";
			qs.Date_format = "yyyy-MM-dd";			
			qs.Date_row_regexp = "Uppdaterad:";
			qs.Quote_regexp = ".*SEB Aktieobligation[ ]*[^ ]*[ ]*[^ ]*[ ]*[^ ]*[ ]*([^ ]*)[ ]*[^ ]* .*";
			qs.Symbol_regexp = "[ ]*([.]*) SEB Aktieobligation.*";
			qs.Encoding = "ISO-8859-1";
			Add (qs);
			
			qs = new QuoteSource ("Yahoo Global Exchange Rates");
			qs.Uri = "http://download.finance.yahoo.com/d/quotes.csv?s=%1%2=X&f=sl1d1t1ba&e=.csv";		
			qs.Date_regexp = "[^,]*,[^,]*,\"([^\"]*)\"";
			qs.Date_format = "M/d/yyyy";		
			qs.Quote_regexp = "[^,]*,([^,]*),.*";
			qs.Symbol_regexp = "\"([^,\"]*)\",.*";			
			Add (qs);								
		}		

//FIXME Need to cope with ' embeeded in text strings.	
		private string GenerateAddSql (QuoteSource qs)
		{
			string sql = "INSERT into QuoteSources (name, uri, encoding, date_format, date_row_regexp, date_regexp, quote_regexp, symbol_regexp) " +
				String.Format ("VALUES ( '{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}' )", 
					               qs.Name, qs.Uri, qs.Encoding, qs.Date_format, qs.Date_row_regexp, qs.Date_regexp, qs.Quote_regexp, qs.Symbol_regexp );				
			return sql;
		}

		public void Add (QuoteSource qs)
		{
			base.Add (GenerateAddSql (qs));
		}
		
		public int Add_GetIndex (QuoteSource qs)
		{
			return base.Add_GetIndex (GenerateAddSql (qs));
		}		
		
		public void Del (QuoteSource qs)
		{
			Del (qs.Id);
		}
		
		public void Del (int index)
		{
			string sql = string.Format ("DELETE FROM QuoteSources WHERE id = {0}", index);				
			base.Del (sql);			
		}		

		public new void Del (string _name)
		{
			string sql = string.Format ("DELETE FROM QuoteSources WHERE name = '{0}'", _name);				
			base.Del (sql);			
		}				
		

//FIXME Need to cope with ' embeeded in text strings.	
		public void Change (QuoteSource qs)
		{
			string sql = "UPDATE QuoteSources SET " +
				String.Format ("name='{0}', uri='{1}', encoding='{2}', date_format='{3}', date_row_regexp='{4}', date_regexp='{5}', quote_regexp='{6}', symbol_regexp='{7}' ", 
					               qs.Name, qs.Uri, qs.Encoding, qs.Date_format, qs.Date_row_regexp, qs.Date_regexp, qs.Quote_regexp, qs.Symbol_regexp ) +
				String.Format ("WHERE id = {0}", qs.Id); 
			base.Change (sql);			
		}
		
		public override MPC.Utils.DbItem DecodeReader (IDataReader reader)
		{
			QuoteSource qs = new QuoteSource (reader.GetString(reader.GetOrdinal("name")));					
			qs.Id = reader.GetInt32(reader.GetOrdinal("id"));
			qs.Uri = reader.GetString(reader.GetOrdinal("uri"));
			qs.Encoding = reader.GetString(reader.GetOrdinal("encoding"));
			qs.Date_format = reader.GetString(reader.GetOrdinal("date_format"));
			qs.Date_row_regexp = reader.GetString(reader.GetOrdinal("date_row_regexp"));
			qs.Date_regexp = reader.GetString(reader.GetOrdinal("date_regexp"));					
			qs.Quote_regexp = reader.GetString(reader.GetOrdinal("quote_regexp"));
			qs.Symbol_regexp = reader.GetString(reader.GetOrdinal("symbol_regexp"));
			return (MPC.Utils.DbItem) qs;
		}		
		public QuoteSource Get (int id)
		{
			string sql = "SELECT id, name, uri, encoding, date_format, date_row_regexp, date_regexp, quote_regexp, symbol_regexp " +
				"FROM QuoteSources " +
				String.Format ("WHERE id = {0}", id); 
			return base.Get (sql) as QuoteSource;							
		}

		public new QuoteSource Get (string _name)
		{
			string sql = "SELECT id, name, uri, encoding, date_format, date_row_regexp, date_regexp, quote_regexp, symbol_regexp " +
				"FROM QuoteSources " +
				String.Format ("WHERE name = '{0}'", _name); 
			return base.Get (sql) as QuoteSource;							
		}		
		
		
		public System.Collections.ArrayList GetList ()
		{
			string sql = "SELECT id, name, uri, encoding, date_format, date_row_regexp, date_regexp, quote_regexp, symbol_regexp " +
				"FROM QuoteSources ORDER BY name"; 
			return base.GetList (sql);
		}
	}
}
