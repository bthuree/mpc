// SecurityQuoteStore.cs
//
// Copyright (C) 2008 Bengt Thuree
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;
using System.IO;
using System.Data;
using Npgsql;

namespace MPC.Quotes
{
	public class SecurityQuoteStore  : MPC.Utils.DbGeneralStore
	{
		
		public SecurityQuoteStore (IDbConnection _dbcon) : base (_dbcon, "SecurityQuotes")
		{
		}

		private string GenerateTimeStampFilter (System.DateTime _date, int _date_range)
		{
			string sql;
			if (_date_range == 0)
				sql = String.Format("timestamp <= '{0}' ", _date);
			else
				{
					System.DateTime from_date;
					System.DateTime to_date;
					to_date = _date.AddDays(_date_range);
					from_date = _date.AddDays(-(_date_range)); 
					sql = String.Format ("timestamp >= '{0}' AND timestamp <= '{1}' ", from_date, to_date);
				}
			return sql;
		}

		private string GenerateAddSql (SecurityQuote q)
		{
			string sql = "INSERT into SecurityQuotes (symbol, timestamp, quote_value, quote_type) " +
				String.Format ("VALUES ( '{0}', '{1}', '{2}', '{3}' )",
					               q.Symbol, q.TimeStamp, q.Quote_Value, (int) q.Quote_Type);
			return sql;
		}

		public void Add (SecurityQuote q)
		{
			base.Add (GenerateAddSql (q));
		}		

		public int Add_GetIndex (SecurityQuote q)
		{
			return base.Add_GetIndex (GenerateAddSql (q));
		}				

		public void Del (SecurityQuote q)
		{
			Del (q.Id);
		}

		public void Del (int index)
		{
			string sql = string.Format ("DELETE FROM SecurityQuotes WHERE id = {0}", index);
			base.Del (sql);
		}


//FIXME Need to cope with ' embeeded in text strings.
		public void Change (SecurityQuote q)
		{
			string sql = "UPDATE SecurityQuotes SET " +
				String.Format ("timestamp='{0}', quote_value={1}, quote_type={2} ",
					               q.TimeStamp, q.Quote_Value, (int) q.Quote_Type) +
				String.Format ("WHERE id = {0}", q.Id);
			base.Change (sql);			
		}

		public override MPC.Utils.DbItem DecodeReader (IDataReader reader)
		{
			SecurityQuote q = new SecurityQuote ();
			q.Id = reader.GetInt32(reader.GetOrdinal("id"));
			q.Symbol = reader.GetInt32(reader.GetOrdinal("symbol"));
			q.TimeStamp = reader.GetDateTime(reader.GetOrdinal("timestamp"));
			q.Quote_Value = reader.GetDecimal (reader.GetOrdinal("quote_value"));
			q.Quote_Type = (MPC.Quotes.UpdateTypeEnum) reader.GetInt32(reader.GetOrdinal("quote_type"));
			return (MPC.Utils.DbItem) q;
		}		

		public SecurityQuote Get (SecurityQuote sq)
		{
			return Get (sq.Id);
		}

		public SecurityQuote Get (int id)
		{
			string sql = "SELECT id, symbol, timestamp, quote_value, quote_type " +
				"FROM SecurityQuotes " +
				String.Format ("WHERE id = {0}", id);
			return base.Get (sql) as SecurityQuote;							
		}

		public SecurityQuote GetLastDayQuote (int _symbol, System.DateTime _date)
		{
			SecurityQuote q = new SecurityQuote();
			q.Symbol = _symbol;
			q.TimeStamp = _date;
			return GetLastDayQuote (q);
		}
		
		public SecurityQuote GetLastDayQuote (SecurityQuote _q)
		{
			string sql = "SELECT id, symbol, timestamp, quote_value, quote_type " +
				"FROM SecurityQuotes " +
				String.Format ("WHERE symbol = '{0}' AND timestamp = '{1}' ", _q.Symbol, _q.TimeStamp) +
				"ORDER BY timestamp DESC, id DESC " +
				"LIMIT 1 ";
			return base.Get (sql) as SecurityQuote;							
		}

		public SecurityQuote GetQuoteClosestTo (int _symbol, System.DateTime _date)
		{
			return GetQuoteClosestTo (_symbol, _date, 0);
		}		
		public SecurityQuote GetQuoteClosestTo (int _symbol, System.DateTime _date, int _date_range)
		{
			SecurityQuote q = new SecurityQuote();
			q.Symbol = _symbol;
			q.TimeStamp = _date;
			return GetQuoteClosestTo (q, _date_range);
		}	
		public SecurityQuote GetQuoteClosestTo (SecurityQuote _q)
		{
			return GetQuoteClosestTo (_q, 0);	
		}		
		public SecurityQuote GetQuoteClosestTo (SecurityQuote _q, int _date_range)
		{
			string sql = "SELECT id, symbol, timestamp, quote_value, quote_type " +
				"FROM SecurityQuotes " +
				String.Format ("WHERE symbol = '{0}' AND {1} ", _q.Symbol, GenerateTimeStampFilter(_q.TimeStamp, _date_range)) +
				"ORDER BY timestamp DESC, id DESC " +
				"LIMIT 1 ";
			return base.Get (sql) as SecurityQuote;	
		}
		
		public System.Collections.ArrayList GetList ()
		{
			string sql = "SELECT id, symbol, timestamp, quote_value, quote_type " +
				"FROM SecurityQuotes " +
				"ORDER BY symbol, timestamp, id ";
			return base.GetList (sql);							
		}

		public System.Collections.ArrayList GetList (Security sec)
		{
			return GetList (sec.Id, System.DateTime.MaxValue);							
		}
		
		public System.Collections.ArrayList GetList (Security sec, System.DateTime split_date)
		{
			return GetList (sec.Id, split_date);							
		}
		
		public System.Collections.ArrayList GetList (int sec_id, System.DateTime split_date)
		{
			string sql = "SELECT id, symbol, timestamp, quote_value, quote_type " +
				"FROM SecurityQuotes " +
				String.Format ("WHERE symbol = {0} and timestamp < '{1}' ", sec_id, split_date) +
				"ORDER BY symbol, timestamp, id ";
			return base.GetList (sql);							
		}
	}
}
