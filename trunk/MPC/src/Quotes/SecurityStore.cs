// SecurityStore.cs
// 
// Copyright (C) 2008 Bengt Thuree
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//



using System;
using System.IO;
using System.Data;
using Npgsql;

namespace MPC.Quotes
{
	public class SecurityStore  : MPC.Utils.DbGeneralStore
	{
	
		public SecurityStore(IDbConnection _dbcon) : base (_dbcon, "Securities")
		{
		}
		
//FIXME Need to cope with ' embeeded in text strings.	
		private string GenerateAddSql (Security s)
		{
			string sql = "INSERT into Securities (symbol, name, currency_id, quote_source_id, quote_is_percentage) " +
				String.Format ("VALUES ( '{0}', '{1}', '{2}', '{3}', '{4}' )", 
					               s.Symbol.Trim(), s.Name, s.Currency, s.Quote_Source, s.QuoteIsPercentage );				
			return sql;
		}
		
		public void Add (Security s)
		{
			base.Add (GenerateAddSql (s));
		}
		
		public int Add_GetIndex (Security s)
		{
			return base.Add_GetIndex (GenerateAddSql (s));
		}
		
		public void Del (Security qs)
		{
			 Del (qs.Id);
		}
		
		public void Del (int index)
		{
			string sql = string.Format ("DELETE FROM Securities WHERE id = {0}", index);				
			base.Del (sql);
		}		

		public new void Del (string _symbol)
		{
			string sql = string.Format ("DELETE FROM Securities WHERE symbol = '{0}'", _symbol.Trim());				
			base.Del (sql);
		}
		
//FIXME Need to cope with ' embeeded in text strings.	
		public void Change (Security s)
		{
			string sql = "UPDATE Securities SET " +
				String.Format ("symbol='{0}', name='{1}', currency_id={2}, quote_source_id={3}, quote_is_percentage={4} ", 
					               s.Symbol.Trim(), s.Name, s.Currency, s.Quote_Source, s.QuoteIsPercentage ) +
				String.Format ("WHERE id = {0}", s.Id); 
			base.Change (sql);
		}

		public override MPC.Utils.DbItem DecodeReader (IDataReader reader)
		{
			Security s = new Security (reader.GetString(reader.GetOrdinal("symbol")));					
			s.Id = reader.GetInt32(reader.GetOrdinal("id"));
			s.Name = reader.GetString(reader.GetOrdinal("name"));
			s.Currency = reader.GetInt32(reader.GetOrdinal("currency_id"));
			s.Quote_Source = reader.GetInt32(reader.GetOrdinal("quote_source_id"));
			s.QuoteIsPercentage = reader.GetBoolean(reader.GetOrdinal("quote_is_percentage"));			
			return (MPC.Utils.DbItem) s;
		}
		
		public Security Get (int id)
		{
			string sql = "SELECT symbol, id, name, currency_id, quote_source_id, quote_is_percentage " +
				"FROM Securities " +
				String.Format ("WHERE id = {0}", id); 
			return base.Get (sql) as Security;
		}
		
		public new Security Get (string _symbol)
		{
			string sql = "SELECT symbol, id, name, currency_id, quote_source_id, quote_is_percentage " +
				"FROM Securities " +
				String.Format ("WHERE symbol = '{0}'", _symbol.Trim()); 
			return base.Get (sql) as Security;
		}

		public System.Collections.ArrayList GetList ()
		{
			string sql = "SELECT id, symbol, name, currency_id, quote_source_id, quote_is_percentage " +
				"FROM Securities ORDER BY symbol";
			return base.GetList (sql);							
		}
		
		public System.Collections.ArrayList GetList (QuoteSource qs)
		{
			string sql = "SELECT id, symbol, name, currency_id, quote_source_id, quote_is_percentage " +
				"FROM Securities " +
				String.Format ("WHERE quote_source_id = {0} ", qs.Id) +
				"ORDER BY symbol ";
			return base.GetList (sql);							
		}				
	}
}
