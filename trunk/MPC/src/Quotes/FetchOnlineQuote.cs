// FetchOnlineQuote.cs
// 
// Copyright (C) 2008 Bengt Thuree
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//


using System;

namespace MPC.Quotes
{
	
public class FetchOnlineQuote
{
	QuoteSource qss;
	string find_symbol;
	
	private bool success = false;
	public bool Success {
		get { return success; }
	}

	private DateTime date;
	public DateTime Date {
		get { return date; }
	}	

	private decimal quote_value;
	public decimal Quote_value {
		get { return quote_value; }
	}	

	private string symbol;
	public string Symbol {
		get { return symbol; }
	}	

	public FetchOnlineQuote(QuoteSource _qss, string _symbol1, string _symbol2)
	{
		qss = _qss;
		find_symbol = String.Format ("{0}{1}", _symbol1, _symbol2);
	}

	public FetchOnlineQuote(QuoteSource _qss, string _symbol)
	{
		qss = _qss;
		find_symbol = _symbol;
	}
	
	public bool IsItHtmlPage ( string in_page)
	{
		if (in_page == null)
			return false;
		string trimed_inpage = in_page.Trim();
		if ((trimed_inpage == null) || (trimed_inpage.Length == 0))
			return false;
		return trimed_inpage.StartsWith("<html>");
	}
	
	public string StoreInpageInTempFile (string inpage)
	{
		bool append = false;
		string tempfilename = System.IO.Path.GetTempFileName();
		System.IO.StreamWriter sw = null;
		sw = CreateStreamWriter (tempfilename, append, qss.Encoding);
		sw.Write(inpage);
		sw.Close();
		return tempfilename;
	}
	
	public string BuildLynxArguments (string in_file)
	{
		string base_cmd = "-nomargins -dump -nolist -force_html";
		if (!String.IsNullOrEmpty(qss.Encoding) )
			base_cmd = String.Format ("{0} -display_charset UTF8 -assume_charset {1}", base_cmd, qss.Encoding);
		return String.Format ("{0} {1}", base_cmd, in_file);			
	}
	
	public string LynxProcessedInpage (string inpage)
	{
		string lynx_in_file = StoreInpageInTempFile (inpage);
		string lynx_args = BuildLynxArguments (lynx_in_file);

		System.Diagnostics.Process pr = new System.Diagnostics.Process();
		pr.StartInfo.FileName = "lynx";
		pr.StartInfo.Arguments = lynx_args;
		pr.StartInfo.UseShellExecute = false;
		pr.StartInfo.RedirectStandardOutput = true;
		pr.Start();   
		
		string lynx_page = pr.StandardOutput.ReadToEnd();

		pr.WaitForExit();
		pr.Close();
/*
MPC.Utils.Log.Debug (String.Format ("Lynx in   : {0}", inpage));
MPC.Utils.Log.Debug (String.Format ("Lynx file : {0}", lynx_in_file));
MPC.Utils.Log.Debug (String.Format ("Lynx cmd  : {0}", lynx_args));
MPC.Utils.Log.Debug (String.Format ("Lynx res  : {0}", lynx_page));
*/
//		System.IO.File.Delete (lynx_in_file);
		
		return lynx_page;
	}
	
	public string RemoveHtmlTags (string inpage)
	{
		string new_inpage = inpage;
		if (IsItHtmlPage (inpage))
			new_inpage = LynxProcessedInpage(inpage);
		return new_inpage;
	}
	
	private string RemoveErrorMessage (string inpage)
	{
		if (inpage.StartsWith ("Missing Format Variable.") )
			return String.Empty;
		else
			return inpage;
	}
		
	private bool UriContainsOneSymbol ( string in_uri)
	{
		return in_uri.Contains ("%1");
	}
		
	public string BuildQuoteUri (string in_uri, string symbol)
	{
		string result_str = in_uri;
		if (symbol == null)
			symbol = "";
		if (UriContainsOneSymbol (in_uri))
			result_str = System.Text.RegularExpressions.Regex.Replace (in_uri, "%1", symbol);
		return result_str;
	}
	
	private System.IO.StreamReader CreateStreamReader (System.IO.Stream st, string encoding)
	{
		if (String.IsNullOrEmpty (encoding))
			return new System.IO.StreamReader (st);
		else
			return new System.IO.StreamReader (st, System.Text.Encoding.GetEncoding(encoding));
	}
	
	private System.IO.StreamWriter CreateStreamWriter (string path, bool append, string encoding)
	{
		if (String.IsNullOrEmpty (encoding))
			return new System.IO.StreamWriter (path, append );
		else
			return new System.IO.StreamWriter (path, append, System.Text.Encoding.GetEncoding(encoding));
	}	
	
	private string GetUriPage ()
	{
		return GetUriPage (BuildQuoteUri (qss.Uri, find_symbol) );
	}

	private string GetUriPage (string uri)
	{
		System.IO.Stream st = null;
		System.IO.StreamReader sr = null;
		System.Net.WebResponse resp = null;
		try
		{
			// make a Web request
			System.Net.WebRequest req = System.Net.WebRequest.Create( uri );
			
			// get the response and read from the result stream
			resp = req.GetResponse();
			
			st = resp.GetResponseStream();
			sr = CreateStreamReader (st, qss.Encoding);
	
			// read all the text in it
			return sr.ReadToEnd();
		}
		catch (Exception ex)
		{
			MPC.Utils.Log.Warning (String.Format ("Warning: Exception when getting Online Quotes for : {0}", ex) );
			return string.Empty;
		}
		finally
		{
		// always close readers and streams
			if (resp != null)
				resp.Close();
			if (sr != null)
				sr.Close();
			if (st != null)
				st.Close();
		}
	}
	
	public bool ValidateQuoteData (decimal quote_value)
	{
		if (quote_value <= 0) 
			return false;
		return true;
	}

//HACK NOT GOOD!
	public bool ValidateSymbolData (string symbol1, string symbol2)
	{
		symbol1 = symbol1.ToUpper();
		symbol2 = symbol2.ToUpper();
		string CurrencySymbol = String.Format("{0}=X", symbol2);			
		bool res1 = (symbol1 == symbol2);
		bool res2 = (CurrencySymbol == symbol1);
		return (res1 || res2);
	}
	
	public bool ValidateDate (DateTime _date)
	{
		if (_date == DateTime.MinValue) 
			return false;
		return true;
	}
		
	public DateTime ConvertDateStr (string date_str, string date_format)
	{
		if (String.IsNullOrEmpty (date_str))
			return DateTime.MinValue;
		date_str = date_str.Trim();
		if ( (String.IsNullOrEmpty (date_str)) || (String.Compare (date_str, "N/A", true) == 0) )
			return DateTime.MinValue;
		System.Globalization.DateTimeFormatInfo dtfi = new System.Globalization.DateTimeFormatInfo();
		DateTime slask = DateTime.ParseExact (date_str, date_format, dtfi);
		return slask;
	}

	private bool CommaAsDecimal (string quote_str)
	{
		int comma_pos = quote_str.IndexOf (",");
		int dot_pos = quote_str.IndexOf (".");
		if (comma_pos > dot_pos)
			return true;
		else
			return false;
	}

	private bool SpaceAsGroupSeparator (string quote_str)
	{
		int space_pos = quote_str.IndexOf (" ");
		if (space_pos > 0)
			return true;
		else
			return false;
	}
		
	public decimal ConvertQuoteStr (string quote_str)
	{
		System.Globalization.NumberFormatInfo nfi = new System.Globalization.NumberFormatInfo();
		nfi.NumberDecimalSeparator = ".";
		nfi.NumberGroupSeparator = ",";
		if (CommaAsDecimal(quote_str))
		{		
			nfi.NumberDecimalSeparator = ",";
			nfi.NumberGroupSeparator = ".";
		}
		if (SpaceAsGroupSeparator(quote_str))
		{		
			nfi.NumberGroupSeparator = " ";
		}		
		return Convert.ToDecimal (quote_str, nfi);
	}

	private string GetSymbolFromRow (string datarow)
	{
		symbol = System.Text.RegularExpressions.Regex.Replace (datarow, qss.Symbol_regexp, "$1");
		return symbol.Trim();			
	}
		
	private void AnalyzeQuotePage (string quoterow, string datepage)
	{		
		try {	
			string date_str, quote_str;
			if (qss.Date_regexp == "<today>")
				date = System.DateTime.Now;
			else {
				date_str = System.Text.RegularExpressions.Regex.Replace	(datepage, qss.Date_regexp,   "$1");
				date = ConvertDateStr (date_str, qss.Date_format);
			}

			symbol = GetSymbolFromRow (quoterow);

			quote_str = System.Text.RegularExpressions.Regex.Replace (quoterow, qss.Quote_regexp,  "$1");			
			quote_value = ConvertQuoteStr (quote_str);

			success = ValidateQuoteData(quote_value) && ValidateSymbolData(symbol, find_symbol) && ValidateDate(date);

		} catch (Exception ex) {
			MPC.Utils.Log.Warning (String.Format ("Warning: Exception when converting Online Quotes {0} : {1}", find_symbol, ex));
			success = false;
		}
	}
	
	private string GetSpecialRowFromUriPage (string in_page, string find_symbol)
	{
		string result_str = "";
		string regex_pattern = String.Format (".*{0}.*", find_symbol);
		System.Text.RegularExpressions.Regex expression = 
			new System.Text.RegularExpressions.Regex( regex_pattern );
		foreach ( System.Text.RegularExpressions.Match myMatch in expression.Matches( in_page ) ) {
			result_str = myMatch.ToString();
			string tmp_symbol = GetSymbolFromRow (result_str);
			if (tmp_symbol == find_symbol)
				break;
		}
		return result_str;
	}
	
	public void GetOnlineQuote()
	{
		string uripage = GetUriPage();
		string daterow, quoterow;
		uripage = RemoveErrorMessage(uripage);
		uripage = RemoveHtmlTags(uripage);
		daterow = uripage;
		quoterow = uripage;
		if (!UriContainsOneSymbol(qss.Uri))
			quoterow = GetSpecialRowFromUriPage (quoterow, find_symbol);
		if ( (qss.Date_row_regexp == null) || (qss.Date_row_regexp == "") )
			daterow = quoterow;
		else
			daterow = GetSpecialRowFromUriPage (daterow, qss.Date_row_regexp);
		if (uripage != null)
			AnalyzeQuotePage(quoterow, daterow);
	}

}
}
