// SecurityGui.cs
// 
// Copyright (C) 2008 Bengt Thuree
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;
using Gtk;
using MPC;
using MPC.Utils;

namespace MPC.Quotes
{
	public delegate void EventNameEventHandler (object sender);			
	
	public partial class SecurityGui : Gtk.Dialog
	{
  		private Security s;
		private System.Collections.ArrayList s_ar;
		private System.Collections.ArrayList QuoteSourceIndexes, CurrencyIndexes;
		private SecurityStore ss; 
		private QuoteSourceStore qss;
		private CurrencyStore cs;
		
		private ListStore SecurityTreeStore;	
		
		public event EventNameEventHandler EventName; 				
		
		public SecurityGui ()
		{
			ss = MPC.Database.GetDatabase.Securities;
			qss = MPC.Database.GetDatabase.QuoteSources;
			cs = MPC.Database.GetDatabase.Currencies;
			this.Build();
			PopulateTreeView(true);
			QuoteSourceIndexes = new System.Collections.ArrayList();
			CurrencyIndexes = new System.Collections.ArrayList();
			BuildQuoteSourceTree();
			BuildCurrencyTree();
		}
			
		private void BuildCurrencyTree()
		{
			CurrencyIndexes.Clear();
			SecurityDetails_Currency.Model = null;
			System.Collections.ArrayList cs_arr = cs.GetList(); 
			ListStore CurrencyTreeStore = new ListStore (typeof (string), typeof (string));
			foreach (Currency c in cs_arr) {
				CurrencyTreeStore.AppendValues (c.Code, c.Country);
				CurrencyIndexes.Add (c.Id);
			}
			CurrencyTreeStore.AppendValues ("--------");
			CurrencyTreeStore.AppendValues ("New Currency");
			SecurityDetails_Currency.Model = CurrencyTreeStore;			
		}

		private void BuildQuoteSourceTree()
		{
			QuoteSourceIndexes.Clear();
			SecurityDetails_QuoteSource.Model = null;
			System.Collections.ArrayList qss_arr = qss.GetList(); 
			ListStore QuoteSourceTreeStore = new ListStore (typeof (string));
			foreach (QuoteSource qs in qss_arr) {
				QuoteSourceTreeStore.AppendValues (qs.Name);
				QuoteSourceIndexes.Add (qs.Id);
			}
			QuoteSourceTreeStore.AppendValues ("--------");
			QuoteSourceTreeStore.AppendValues ("New Quote Source");
			SecurityDetails_QuoteSource.Model = QuoteSourceTreeStore;			
		}
		
		private void PopulateStore()
		{
			s_ar = ss.GetList();			
			SecurityTreeStore = new ListStore (typeof (string), typeof (string));
			foreach (Security s in s_ar) {
				//TreeIter iter = QuoteSourceTreeStore.AppendValues (q.Name);
				SecurityTreeStore.AppendValues (s.Symbol, s.Name);				
			}
		}
		
		private void PopulateTreeView(bool define_columns)
		{
			PopulateStore();
			SecurityTreeView.Model = SecurityTreeStore;
			if (define_columns) {
				SecurityTreeView.AppendColumn ("Symbol", new CellRendererText(), "text", 0);
				SecurityTreeView.AppendColumn ("Name", new CellRendererText(), "text", 1);				
			}
			// this.ShowAll();
		}

		private void ClearDetails()
		{
			SecurityDetails_Symbol.Text = String.Empty;
			SecurityDetails_Name.Text = String.Empty;
			SecurityDetails_Currency.Active = -1;
			SecurityDetails_QuoteSource.Active = -1;
			SecurityDetails_QuoteIsPercentage.Active = false;
			s = null;		
			// this.ShowAll();			
		}

		protected Security FetchSecurityFromRow()
		{
			TreePath path = null;
			TreeViewColumn column = null;
			SecurityTreeView.GetCursor (out path, out column);
			if (path != null) {
				int row = System.Convert.ToInt32 (path.ToString());
				Security s = s_ar[row] as Security;
				return s;
			}			
			return null;
		}
		
		private string GuiText (string new_txt)
		{
			if (new_txt == null)
				return String.Empty;
			else
				return new_txt;
		}				

		private void sanitizeGui()
		{
			SecurityDetails_Symbol.Text = SecurityDetails_Symbol.Text.Trim();
		}
		
		protected virtual void OnCurrencySaveClicked (object sender, System.EventArgs e)
		{
			bool NewS = false;
			if (s == null) {
				s = new Security("empty");
				NewS = true;
			}
			sanitizeGui();			
			s.Name = SecurityDetails_Name.Text;
			s.Symbol = SecurityDetails_Symbol.Text;
			s.Currency = (int) CurrencyIndexes [SecurityDetails_Currency.Active];
			s.Quote_Source = (int) QuoteSourceIndexes [SecurityDetails_QuoteSource.Active];		
			s.QuoteIsPercentage = SecurityDetails_QuoteIsPercentage.Active;
			if (NewS)
				ss.Add(s);
			else
				ss.Change(s);
			PopulateTreeView(false);
			ClearDetails();			
			NotifySubscriber();															
		}
		
		private void NotifySubscriber()
		{
			if (EventName != null)
				EventName(this);
		}		

		protected virtual void OnButtonCancelClicked (object sender, System.EventArgs e)
		{
			this.Destroy();						
		}

		protected virtual void OnButtonOkClicked (object sender, System.EventArgs e)
		{
			NotifySubscriber();															
		}

		protected virtual void OnNewActionActivated (object sender, System.EventArgs e)
		{
			ClearDetails();
			PopulateTreeView(false);						
		}

		protected virtual void OnDeleteActionActivated (object sender, System.EventArgs e)
		{
			s = FetchSecurityFromRow();
			if (s != null) {
Log.Debug("Need to ensure the security is not in use!!!");				
				ss.Del(s);
				PopulateTreeView(false);
				ClearDetails();
				NotifySubscriber();																			
			}						
		}

		protected virtual void OnSecurityDetailsQuoteSourceChanged (object sender, System.EventArgs e)
		{
			Log.DebugFormat("Active QuoteSource = {0}",SecurityDetails_QuoteSource.Active);
			if (SecurityDetails_QuoteSource.Active > QuoteSourceIndexes.Count) 
			{
				Log.Information ("Calling Quote Sources...");
				MPC.Quotes.QuoteSourceGui win = new MPC.Quotes.QuoteSourceGui (qss);
				win.Run();				
				BuildQuoteSourceTree();
				SecurityDetails_QuoteSource.Active = -1;
			}			
		}

		protected virtual void OnSecurityDetailsCurrencyChanged (object sender, System.EventArgs e)
		{
			Log.DebugFormat("Active Currency = {0}",SecurityDetails_Currency.Active);	
			if (SecurityDetails_Currency.Active > CurrencyIndexes.Count) 
			{
				Log.Information ("Calling Currency...");
				MPC.Quotes.CurrencyGui win = new MPC.Quotes.CurrencyGui ();
				win.Run();				
				BuildCurrencyTree();
				SecurityDetails_Currency.Active = -1;
			}			
		}

		protected virtual void OnTestQuoteClicked (object sender, System.EventArgs e)
		{
			sanitizeGui();
			Log.DebugFormat ("Symbol = {0}", SecurityDetails_Symbol.Text);
			Log.DebugFormat("Active Currency = {0}", SecurityDetails_Currency.Active);	
			Log.DebugFormat("Active QuoteSource = {0}", SecurityDetails_QuoteSource.Active);			
			int qs_ind2 = SecurityDetails_QuoteSource.Active;
			int qs_ind = (int) QuoteSourceIndexes[qs_ind2];
			string symbol = SecurityDetails_Symbol.Text;
			SecurityStatus.Pop (1);
			SecurityStatus.Push (1, String.Format ("Trying to fetch a quote for symbol {0}", symbol));
			SecurityStatus.Show();
			//Log.Debug (String.Format("Quote Source ID = {0}", qs_ind));
			
			QuoteSource qs = qss.Get (qs_ind);
			//Log.Debug (String.Format ("Quote regexp {0}, url {1}", qs.Quote_regexp, qs.Uri));
			FetchOnlineQuote foq = new FetchOnlineQuote (qs, symbol);	
			foq.GetOnlineQuote();
			Log.Debug (String.Format("Fetched Quote for {0} : {1}, Quote = {2}, Quote Date = {3}", foq.Symbol, foq.Success, foq.Quote_value, foq.Date)); 
			if (foq.Success) {
				SecurityStatus.Pop (1);				
				SecurityStatus.Push (1, String.Format ("Succeded to fetch quote {0} for symbol {1} at {2}", foq.Quote_value, foq.Symbol, foq.Date));
			}
			else 
			{
				SecurityStatus.Pop (1);				
				SecurityStatus.Push (1, "Failed to fetch quote");
			}					
		}

		protected virtual void OnSecurityTreeViewCursorChanged (object sender, System.EventArgs e)
		{
			ClearDetails();
			s = FetchSecurityFromRow();
			if (s != null) {
				SecurityDetails_Name.Text = GuiText (s.Name);				
				SecurityDetails_Symbol.Text = GuiText (s.Symbol);			
				SecurityDetails_Currency.Active = CurrencyIndexes.IndexOf (s.Currency);
				SecurityDetails_QuoteSource.Active = QuoteSourceIndexes.IndexOf (s.Quote_Source);
				SecurityDetails_QuoteIsPercentage.Active = s.QuoteIsPercentage;
			}
		}
		

	}
}
