// ContactDetailsStore.cs
// 
// Copyright (C) 2008 Bengt Thuree
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//


using System;
using System.IO;
using System.Data;



namespace MPC.Accounts
{
	public class ContactDetailsStore : MPC.Utils.DbGeneralStore
	{
		
		public ContactDetailsStore (IDbConnection _dbcon) : base (_dbcon, "ContactDetails")
		{
		}

		private string GenerateAddSql (ContactDetail cdr)
		{
			string sql = "INSERT into ContactDetails " +
				" (name, phone, mobile, other_phone, fax, work_mail, private_mail, other_mail, address_id, uri, notes) " +
				String.Format ("VALUES ( '{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}', '{10}' )", 
								   cdr.Name, cdr.Phone, cdr.Mobile, cdr.Other_phone, cdr.Fax, cdr.Work_mail, 
								   cdr.Private_mail, cdr.Other_mail, cdr.Address_id, cdr.Uri, cdr.Notes
				               );	
			return sql;
		}		
		
		public void Add (ContactDetail cdr)
		{
			base.Add (GenerateAddSql (cdr) );
		}
		
		public int Add_GetIndex (ContactDetail cdr)
		{
			return base.Add_GetIndex (GenerateAddSql (cdr) );
		}		
		
		public void Del (ContactDetail cdr)
		{
			Del (cdr.Id);
		}
		
		public void Del (int index)
		{
			string sql = string.Format ("DELETE FROM ContactDetails WHERE id = {0}", index);
			base.Del (sql);			
		}		

		public new void Del (string _name)
		{
			string sql = string.Format ("DELETE FROM ContactDetails WHERE name = '{0}'", _name);
			base.Del (sql);			
		}		
		
		public void Change (ContactDetail cdr)
		{
			string sql = "UPDATE ContactDetails SET " +
				String.Format ("name='{0}', phone='{1}', mobile='{2}', other_phone='{3}', fax='{4}', " +
						"work_mail='{5}', private_mail='{6}', other_mail='{7}', address_id='{8}', uri='{9}', notes='{10}' ", 
						cdr.Name, cdr.Phone, cdr.Mobile, cdr.Other_phone, cdr.Fax, cdr.Work_mail, 
						cdr.Private_mail, cdr.Other_mail, cdr.Address_id, cdr.Uri, cdr.Notes ) +
				String.Format ("WHERE id = {0}", cdr.Id); 
			base.Change (sql);			
		}

		public override MPC.Utils.DbItem DecodeReader (IDataReader reader)
		{
			ContactDetail cdr = new ContactDetail ();					
			cdr.Id = reader.GetInt32(reader.GetOrdinal("id"));
			cdr.Name = reader.GetString(reader.GetOrdinal("name"));
			cdr.Phone = reader.GetString(reader.GetOrdinal("phone"));
			cdr.Mobile = reader.GetString(reader.GetOrdinal("mobile"));
			cdr.Other_phone = reader.GetString(reader.GetOrdinal("other_phone"));
			cdr.Fax = reader.GetString(reader.GetOrdinal("fax"));
			cdr.Work_mail = reader.GetString(reader.GetOrdinal("work_mail"));
			cdr.Private_mail = reader.GetString(reader.GetOrdinal("private_mail"));
			cdr.Other_mail = reader.GetString(reader.GetOrdinal("other_mail"));
			cdr.Address_id = reader.GetInt32(reader.GetOrdinal("address_id"));
			cdr.Uri = reader.GetString(reader.GetOrdinal("uri"));
			cdr.Notes = reader.GetString(reader.GetOrdinal("notes"));
			return (MPC.Utils.DbItem) cdr;
		}

		public ContactDetail Get (ContactDetail cd)
		{
			return Get (cd.Id);							
		}		
		
		public ContactDetail Get (int id)
		{
			string sql = "SELECT id, name, phone, mobile, other_phone, fax, work_mail, private_mail, other_mail, address_id, uri, notes " +
				"FROM ContactDetails " +
				String.Format ("WHERE id = {0}", id); 
			return base.Get (sql) as ContactDetail;							
		}

		public new ContactDetail Get (string _name)
		{
			string sql = "SELECT id, name, phone, mobile, other_phone, fax, work_mail, private_mail, other_mail, address_id, uri, notes " +
				"FROM ContactDetails " +
				String.Format ("WHERE name = '{0}'", _name); 
			return base.Get (sql) as ContactDetail;							
		}

		public System.Collections.ArrayList GetList ()
		{
			string sql = "SELECT id, name, phone, mobile, other_phone, fax, work_mail, private_mail, other_mail, address_id, uri, notes " +
					"FROM ContactDetails ORDER BY name"; 
			return base.GetList (sql);							
		}
	}
}

