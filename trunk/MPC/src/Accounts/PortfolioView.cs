// PortfolioView.cs
// 
// Copyright (C) 2008 Bengt Thuree
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;

using MPC.Accounts;
using MPC.Quotes;
using MPC.Utils;

namespace MPC.Accounts
{
	
//FIXME Should have a type indicating what kind of portfolio we want
	/*
	 * all securites, no accounts
	 * all accounts, and all securities
	 * one security and all its accounts
	 * Change Base currency in run time...
	 * All accounts, no securities
	 * Specify which Accounts we want
	 * Specify which Securities we want
	 */
	
//FIXME Check if need to use a db select to find information. Exchange Rantes, and Quotes
	
	public class PortfolioView
	{
		SecurityQuoteStore sq_s;
		InvestmentTransactionStore it_s;
		SecurityStore s_s;
		CurrencyStore c_s;
		ExchangeRateStore er_s;
		NumberOfSharesView nos_v;
		string BaseCurrencyCode, LocalCurrencyCode;
		int BaseCurrency;
		System.Collections.ArrayList PortfolioList;
		
		public PortfolioView (SecurityQuoteStore _sqs, SecurityStore _ss, InvestmentTransactionStore _its, 
		                      NumberOfSharesView _nosv, CurrencyStore _cs, ExchangeRateStore _ers)
		{
			sq_s = _sqs;
			s_s = _ss;
			it_s = _its;		
			nos_v = _nosv;
			c_s = _cs;
			er_s = _ers;
			BaseCurrency = -1;
			BaseCurrencyCode = String.Empty;
			LocalCurrencyCode = String.Empty;
			PortfolioList = new System.Collections.ArrayList();
		}
		public int SetBaseCurrency {
			set { BaseCurrency = value ; }
		}
		public string GetBaseCurrencyCode {
			get { return BaseCurrencyCode; }
		}		
		public string GetLocalCurrencyCode {
			get { return LocalCurrencyCode; }
		}				
		public System.Collections.ArrayList GetPortfolio {
			get { return PortfolioList ; }
		}
		public decimal LocalTotal {
			get { decimal sum = 0;
				bool one_currency = true;
				string cur_code = String.Empty;
				foreach (PortfolioEntry pe in PortfolioList) {
					if (one_currency && cur_code != String.Empty)
						one_currency = (cur_code == pe.CurrencyCode);
					sum += pe.Value;
					cur_code = pe.CurrencyCode;
				}
				return (one_currency ? sum : 0);
			}
		}
		public decimal BaseTotal {
			get { decimal sum = 0;
				foreach (PortfolioEntry pe in PortfolioList) {
					sum += pe.BaseValue;
				}
				return sum;
			}
		}		

		private void CreatePortfolioList (System.Collections.ArrayList shares_list)
		{
			foreach (NumberOfShares nos in shares_list)
			{
				PortfolioEntry pe = new PortfolioEntry();
				pe.AccountNr = nos.AccountNr;
				pe.AccountName = nos.AccountName;
				pe.SecurityNr = nos.SecurityNr;
				pe.SecuritySymbol = nos.Symbol;
				pe.NumberShares = nos.NumberShares;
				PortfolioList.Add (pe);
			}
		}
		
		private void GetActiveSecurities (System.DateTime ViewDate)
		{
			System.Collections.ArrayList shares_list = nos_v.GetList(ViewDate);
			CreatePortfolioList (shares_list);
		}
		
		private void GetActiveSecurities (Security sec, System.DateTime ViewDate)
		{
			System.Collections.ArrayList shares_list = nos_v.GetList(sec, ViewDate);
			CreatePortfolioList (shares_list);
		}

		private void GetActiveSecurities (AccountHeader ah, System.DateTime ViewDate)
		{
			System.Collections.ArrayList shares_list = nos_v.GetList(ah, ViewDate);
			CreatePortfolioList (shares_list);
		}		
		
		private void GetActiveSecurities (AccountHeader ah, Security sec, System.DateTime ViewDate)
		{
			System.Collections.ArrayList shares_list = nos_v.GetList(ah, sec, ViewDate);
			CreatePortfolioList (shares_list);
		}				
		private void CountShares (System.DateTime ViewDate)
		{
			for (int k=0; k < PortfolioList.Count; k++)
			{
				int actnr = (PortfolioList[k] as PortfolioEntry).AccountNr;
				int secnr = (PortfolioList[k] as PortfolioEntry).SecurityNr;
				decimal oldnumber = (PortfolioList[k] as PortfolioEntry).NumberShares;
				if (actnr >= 0)
					(PortfolioList[k] as PortfolioEntry).NumberShares = nos_v.CountShares (actnr, secnr, ViewDate);
				else
					(PortfolioList[k] as PortfolioEntry).NumberShares = nos_v.CountShares (secnr, ViewDate);
				if ((PortfolioList[k] as PortfolioEntry).NumberShares != oldnumber)
					Log.DebugFormat ("Old number {0}, new number {1}", oldnumber, (PortfolioList[k] as PortfolioEntry).NumberShares);
				else
					Log.Debug ("same number...");
			}		
		}
		
		private void FindQuotes (System.DateTime ViewDate)
		{
			for (int k=0; k < PortfolioList.Count; k++)
			{
				int secnr = (PortfolioList[k] as PortfolioEntry).SecurityNr;
				SecurityQuote sq = sq_s.GetQuoteClosestTo (secnr, ViewDate);
				(PortfolioList[k] as PortfolioEntry).QuoteValue = (sq == null ? 0 : sq.Quote_Value);
			}		
		}
		
		private void ConvertToBase (System.DateTime ViewDate)
		{
			for (int k=0; k < PortfolioList.Count; k++)
			{
				int secnr = (PortfolioList[k] as PortfolioEntry).SecurityNr;
				Security sec = s_s.Get (secnr);
				Currency cur = c_s.Get (sec.Currency);
				LocalCurrencyCode = cur.Code;
				Currency baseCur = null;
				if (BaseCurrency >= 0) {
					baseCur = c_s.Get (BaseCurrency);
					BaseCurrencyCode = baseCur.Code;
				}
				ExchangeRate er = new ExchangeRate();
				er.FromCurrency = sec.Currency;
				er.ToCurrency = BaseCurrency;
				er.TimeStamp = ViewDate;
				er = er_s.GetLastDayQuote (er);
				(PortfolioList[k] as PortfolioEntry).ExchangeRateToBase = (er == null ? 0 : er.ExchangeRateValue);
				(PortfolioList[k] as PortfolioEntry).QuoteIsPercentage = sec.QuoteIsPercentage;			
				(PortfolioList[k] as PortfolioEntry).CurrencyCode = cur.Code;
				(PortfolioList[k] as PortfolioEntry).CurrencyBaseCode =  (baseCur == null ? "" : baseCur.Code);											
			}		
		}

		private void FindHistoryQuotes (System.DateTime ViewDate)
		{
			Log.Debug ("Code is missing for FindHistoryQuotes");
			for (int k=0; k < PortfolioList.Count; k++)
			{
				int secnr = (PortfolioList[k] as PortfolioEntry).SecurityNr;
//FIXME Put some code here...		
// use a hashtable (or something) (days, plus_minus_days)		
				// (PortfolioList[k] as PortfolioEntry).QuoteValue = sq_s.GetQuoteClosestTo (secnr, ViewDate, plus_minus_days);
			}		
		}
		
		private void CalculateIncomeAndCosts (System.DateTime ViewDate)
		{
			Log.Debug ("Code is missing for CalculateIncomeAndCosts");
			for (int k=0; k < PortfolioList.Count; k++)
			{
				int actnr = (PortfolioList[k] as PortfolioEntry).AccountNr;			
				int secnr = (PortfolioList[k] as PortfolioEntry).SecurityNr;
				decimal sharecount = (PortfolioList[k] as PortfolioEntry).NumberShares;				
//FIXME Put some code here...								
// From ViewDate, go backwards until you have sharecount number of shares. Add all together
				// (PortfolioList[k] as PortfolioEntry).TotalCost = GetTotalCosts (actnr, secnr, ViewDate, sharecount);
				// (PortfolioList[k] as PortfolioEntry).TotalIncome = GetTotalIncome (actnr, secnr, ViewDate, sharecount);
			}		
		}

		private void CalculatePortfolio (System.DateTime ViewDate)
		{
// FIXME CountShares is not needed, since the number of shares are counted in nos_v.GetList(			
//			CountShares (ViewDate);
			FindQuotes (ViewDate);
//FIXME Should this be done here??? Probably not.
			ConvertToBase (ViewDate);
			FindHistoryQuotes (ViewDate);
			CalculateIncomeAndCosts (ViewDate);
		}		

		public void PreparePortfolio ()
		{
			PreparePortfolio (System.DateTime.Today);
		}		
		
		public void PreparePortfolio (System.DateTime _viewdate)
		{
			GetActiveSecurities (_viewdate);
			CalculatePortfolio (_viewdate);
		}
		
		public void PreparePortfolio (Security sec)
		{
			PreparePortfolio (sec, System.DateTime.Today);
		}				
		
		public void PreparePortfolio (Security sec, System.DateTime _viewdate)
		{
			GetActiveSecurities (sec, _viewdate);
			CalculatePortfolio (_viewdate);
		}		

		public void PreparePortfolio (AccountHeader ah)
		{
			PreparePortfolio (ah, System.DateTime.Today);
		}				
		
		public void PreparePortfolio (AccountHeader ah, System.DateTime _viewdate)
		{
			GetActiveSecurities (ah, _viewdate);
			CalculatePortfolio (_viewdate);
		}				

		public void PreparePortfolio (AccountHeader ah, Security sec)
		{
			PreparePortfolio (ah, sec, System.DateTime.Today);
		}				
		
		public void PreparePortfolio (AccountHeader ah, Security sec, System.DateTime _viewdate)
		{
			GetActiveSecurities (ah, sec, _viewdate);
			CalculatePortfolio (_viewdate);
		}						
	}
}
