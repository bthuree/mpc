// Account.cs
// 
// Copyright (C) 2008 Bengt Thuree
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;

namespace MPC.Accounts
{
	
	public class AccountHeader : MPC.Utils.DbItem
	{
		
		public AccountHeader ()
		{
			parent = System.Int32.MinValue;
			name = null;
			address_id =System.Int32.MinValue;
			contact_id = System.Int32.MinValue;
			account_nr = null;
			currency_id = System.Int32.MinValue;
		}
		
		private int address_id;
		public int Address_id {
			get { return address_id; }
			set { address_id = value; }
		}

		private string name;
		public string Name {
			get { return name; }
			set { name = value; }
		}
		
		private int parent;
		public int Parent {
			get { return parent; }
			set { parent = value; }
		}

		private int contact_id;
		public int Contact_id {
			get { return contact_id; }
			set { contact_id = value; }
		}
		
		private string account_nr;
		public string Account_nr {
			get { return account_nr; }
			set { account_nr = value; }
		}
		private int currency_id;
		public int Currency_id {
			get { return currency_id; }
			set { currency_id = value; }
		}
		private int account_type;
		public int AccountType {
			get { return account_type; }
			set { account_type = value; }
		}
						
				
		public override bool Equals(Object obj) 
		{
			//Check for null and compare run-time types.
			if (obj == null || GetType() != obj.GetType()) 
				return false;
			AccountHeader c2 = (AccountHeader)obj;
			return (Name == c2.Name);
		}
		
		public override int GetHashCode() 
		{
			return Name.GetHashCode();
		}		
	}	
}
