// SplitSecurityGui.cs
// 
// Copyright (C) 2008 Bengt Thuree
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;
using Gtk;

using MPC.Accounts;
using MPC.Quotes;
using MPC.Utils;

namespace MPC.Accounts
{
	
	public partial class SplitSecurityGui : Gtk.Dialog
	{
		
		public SplitSecurityGui()
		{
			this.Build();
			split_security.AllowNew = false;
		}

		private bool validateInput()
		{
			if (split_security.Selection == null)
				return false;
			try {
				if (System.Convert.ToDecimal (split_old_shares.Text) <=0)
					return false;
				if (System.Convert.ToDecimal (split_new_shares.Text) <=0)
					return false;
			} catch {
				return false;
			}
			return true;
		}
		
		private void presentNotOkInput()
		{
			//FIXME Display a NOT OK gui
			Log.Debug ("NOT OK INPUT");
		}
		
		private bool confirmSplit(int secId, decimal from, decimal to, DateTime date)
		{
			//FIXME Display a Confirmation dialog
			Security sec = split_security.Selection;
			Log.DebugFormat ("Confirming split : {0}, {1}, {2}->{3}", sec.Name, date.ToString(), from, to);
			return true;
		}		
		
		protected virtual void OnButtonOkClicked (object sender, System.EventArgs e)
		{
			if (!validateInput()) {
				presentNotOkInput();
				return;
			}
			Log.Debug ("Ok input");
			Security sec = split_security.Selection;			
			decimal fromShares = System.Convert.ToDecimal (split_old_shares.Text);			
			decimal newShares = System.Convert.ToDecimal (split_new_shares.Text);
			if (confirmSplit(sec.Id, fromShares, newShares, split_date.Date))
			{
				SplitSecurity splitSec = new SplitSecurity ();
				splitSec.PerformSplit (sec.Id, fromShares, newShares, split_date.Date);
			}
		}

		protected virtual void OnButtonCancelClicked (object sender, System.EventArgs e)
		{
			this.Destroy();
		}
	}
}
