// PortfolioGui.cs
// 
// Copyright (C) 2008 Bengt Thuree
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;
using Gtk;
using MPC.Accounts;
using MPC.Quotes;

namespace MPC.Accounts
{
	

	public partial class PortfolioGui : Gtk.Dialog
	{
		AccountHeaderStore ah_s;
		SecurityStore s_s;		
		SecurityQuoteStore sq_s;
		InvestmentTransactionStore it_s;
		NumberOfSharesView nos_v;
		CurrencyStore c_s;
		ExchangeRateStore er_s;	
//		bool useAllSecurities;
		AccountHeader accountFilter;
		Security securityFilter;
		int baseCurrency;
		System.DateTime portfolioDate;
		MPC.Accounts.PortfolioGuiProperties propertiesGui;		

		private ListStore PortfolioTreeStore;
		
		public PortfolioGui(SecurityStore _ss, AccountHeaderStore _ahs,
		                    SecurityQuoteStore _sqs, InvestmentTransactionStore _its, 
		                    NumberOfSharesView _nosv, CurrencyStore _cs, ExchangeRateStore _ers)	
		{
			this.Build();
			s_s = _ss;
			ah_s = _ahs;
			sq_s = _sqs;
			it_s = _its;
			nos_v = _nosv;
			c_s = _cs;
			er_s = _ers;
			accountFilter = null;
			securityFilter = null;
//			useAllSecurities = true;
			portfolioDate = System.DateTime.Today;
			PopulateTreeView(true);			
			ShowPropertiesGui();
		}

		private void ShowPropertiesGui()
		{
			propertiesGui = new MPC.Accounts.PortfolioGuiProperties();
			SetPortfolioProperties();			
			propertiesGui.EventName += new EventNameEventHandler(this.EvenAnnouncer);			
			propertiesGui.Run();																
		}
		
		private void RemovePropertiesGui()
		{
			if (propertiesGui != null)
				propertiesGui.KillDialog();
			propertiesGui = null;
		}		
		
		protected virtual void OnPropertiesActionActivated (object sender, System.EventArgs e)
		{
			ShowPropertiesGui();
		}

		protected virtual void OnButtonCancelClicked (object sender, System.EventArgs e)
		{
			RemovePropertiesGui();			
			this.Destroy();												
		}

		protected virtual void OnButtonOkClicked (object sender, System.EventArgs e)
		{
			RemovePropertiesGui();	
			this.Destroy();												
		}

		private void SetPortfolioProperties()
		{
			propertiesGui.AccountFilter = accountFilter;
			propertiesGui.SecurityFilter = securityFilter;			
			propertiesGui.ReportDate = portfolioDate;
			propertiesGui.UseBaseCurrency = baseCurrency;
		}		

		private void FetchPortfolioProperties()
		{
			accountFilter = propertiesGui.AccountFilter;			
			securityFilter = propertiesGui.SecurityFilter;
			portfolioDate = propertiesGui.ReportDate;
			baseCurrency = propertiesGui.UseBaseCurrency;
		}
		
		public void EvenAnnouncer(object sender)
		{
			FetchPortfolioProperties();
			RemovePropertiesGui();
			CreatePortfolioReport();
		}
		
		private string Dec2Str (decimal dec)
		{
			return dec.ToString();
		}
		
		private void PopulateStore(System.Collections.ArrayList pv_list)
		{
			PortfolioTreeStore = new ListStore (typeof (string), typeof(string), 
			                                    typeof(string), typeof(string), 
			                                    typeof(string), typeof(string),
			                                    typeof(string), typeof(string));			                                    
			foreach (PortfolioEntry pe in pv_list) {
				PortfolioTreeStore.AppendValues (pe.AccountName, pe.SecuritySymbol, 
				                                 pe.NumberShares.ToString("N"), pe.QuoteValue.ToString("N"), 
				                                 pe.Value.ToString("N"), pe.CurrencyCode,
				                                 pe.BaseValue.ToString("N"), pe.CurrencyBaseCode);				
			}
		}
		
		private void PopulateTreeView(bool define_columns, System.Collections.ArrayList pv_list)
		{
			if (pv_list != null)
				PopulateStore(pv_list);
			treeview.Model = PortfolioTreeStore;
			if (define_columns) {
				treeview.AppendColumn ("Account", new CellRendererText(), "text", 0);
				treeview.AppendColumn ("Symbol", new CellRendererText(), "text", 1);
				CellRendererText crt = new CellRendererText();
				crt.Alignment = Pango.Alignment.Right;
//				crt.Foreground = "red";
//				crt.Strikethrough = true;
				treeview.AppendColumn ("Number Shares", crt, "text", 2);
				treeview.AppendColumn ("Quote", crt, "text", 3);
				treeview.AppendColumn ("Value", crt, "text", 4);
				treeview.AppendColumn ("", crt, "text", 5);				
				treeview.AppendColumn ("Base Value", crt, "text", 6);
				treeview.AppendColumn ("", crt, "text", 7);								
			}
			this.ShowAll();
		}
		private void PopulateTreeView(bool define_columns)
		{
			PopulateTreeView (define_columns, null);
		}
		
		private void toggleLocalTotal(bool flag)
		{
			localTotalLabel.Visible = flag;
			localTotal.Visible = flag;
			localTotalCurrency.Visible = flag;
		}
		
		private void DisplayTotals(PortfolioView p_v)
		{
			if (p_v.LocalTotal == 0)
				toggleLocalTotal (false);
			else
			{
				toggleLocalTotal (true);
				localTotal.Text = p_v.LocalTotal.ToString();
				localTotalCurrency.Text = p_v.GetLocalCurrencyCode;
			}
			baseTotal.Text = p_v.BaseTotal.ToString();
			baseTotalCurrency.Text = p_v.GetBaseCurrencyCode;
		}
		
		private void CreatePortfolioReport()
		{
			PortfolioView p_v = new PortfolioView (sq_s, s_s, it_s, nos_v, c_s, er_s);
			if (baseCurrency >= 0)
				p_v.SetBaseCurrency = baseCurrency;
			if ((accountFilter == null) && (securityFilter == null))
				p_v.PreparePortfolio(portfolioDate);
			else if (accountFilter == null)
				p_v.PreparePortfolio(securityFilter, portfolioDate);	
			else if (securityFilter == null)
				p_v.PreparePortfolio(accountFilter, portfolioDate);							
			else
				p_v.PreparePortfolio(accountFilter, securityFilter, portfolioDate);
			System.Collections.ArrayList pv_list = p_v.GetPortfolio;
			PopulateTreeView(false, pv_list);
			DisplayTotals (p_v);
			foreach (PortfolioEntry pe in pv_list) {
				MPC.Utils.Log.DebugFormat (";{0};{1};{2};{3};{4};{5};{6};{7}", 
				                           pe.AccountName, pe.SecuritySymbol, 
				                           pe.NumberShares, pe.QuoteValue, 
				                           pe.Value, pe.CurrencyCode,
				                           pe.BaseValue, pe.CurrencyBaseCode);
			}
		}
	}
}
