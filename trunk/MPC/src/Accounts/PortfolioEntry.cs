// PortfolioEntry.cs
// 
// Copyright (C) 2008 Bengt Thuree
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;

namespace MPC.Accounts
{
	
	public class PortfolioEntry 
	{

//FIXME Possible, add a method to toggle between local currency, and base currency.
		
		public PortfolioEntry()
		{
		}
		
		private decimal CalculateQuoteDiffPercentage (decimal _compareTo)
		{
			return (_compareTo > 0 ? 100 * System.Decimal.Round( (QuoteValue - _compareTo) / _compareTo, 4) : 0); 
		}
		
		private int account_nr;
		public int AccountNr {
			get { return account_nr; }
			set { account_nr = value; }
		}
		private string account_name;
		public string AccountName {
			get { return account_name; }
			set { account_name = value; }
		}		
		private int security_nr;
		public int SecurityNr {
			get { return security_nr; }
			set { security_nr = value; }
		}		
		private string symbol;
		public string SecuritySymbol {
			get { return symbol; }
			set { symbol = value; }
		}				
		private decimal number_shares;
		public decimal NumberShares {
			get { return number_shares; }
			set { number_shares = value; }
		}	
		private decimal quote_value;
		public decimal QuoteValue {
			get { return quote_value; }
			set { quote_value = value; }
		}
		private bool quote_is_percentage;
		public bool QuoteIsPercentage {
			get { return quote_is_percentage; }
			set { quote_is_percentage = value; }
		}		
		private decimal weight_in_portfolio;
		public decimal WeightInPortfolio {
			get { return weight_in_portfolio; }
			set { weight_in_portfolio = value; }
		}		
		private decimal exchange_rate_to_base;
		public decimal ExchangeRateToBase {
			get { return exchange_rate_to_base; }
			set { exchange_rate_to_base = value; }
		}		
		private string currency_code;
		public string CurrencyCode {
			get { return currency_code; }
			set { currency_code = value; }
		}						
		private string currency_base_code;
		public string CurrencyBaseCode {
			get { return currency_base_code; }
			set { currency_base_code = value; }
		}				
		private bool fuzzy_quote_value;
		public bool FuzzyQuoteValue {
			get { return fuzzy_quote_value; }
			set { fuzzy_quote_value = value; }
		}
		private decimal total_cost;
		public decimal TotalCost {
			get { return total_cost; }
			set { total_cost = value; }
		}
		private decimal total_income;
		public decimal TotalIncome {
			get { return total_income; }
			set { total_income = value; }
		}			
		private decimal yesterday_quote;
		public decimal YesterdayQuote {
			get { return yesterday_quote; }
			set { yesterday_quote = value; }
		}
		private decimal one_week_quote;
		public decimal OneWeekQuote {
			get { return one_week_quote; }
			set { one_week_quote = value; }
		}
		private decimal one_month_quote;
		public decimal OneMonthQuote {
			get { return one_month_quote; }
			set { one_month_quote = value; }
		}
		private decimal three_month_quote;
		public decimal ThreeMonthQuote {
			get { return three_month_quote; }
			set { three_month_quote = value; }
		}
		private decimal six_month_quote;
		public decimal SixMonthQuote {
			get { return six_month_quote; }
			set { six_month_quote = value; }
		}
		private decimal one_year_quote;
		public decimal OneYearQuote {
			get { return one_year_quote; }
			set { one_year_quote = value; }
		}
		private decimal two_year_quote;
		public decimal TwoYearQuote {
			get { return two_year_quote; }
			set { two_year_quote = value; }
		}
		
		public decimal YesterdayDiff { 
			get { return CalculateQuoteDiffPercentage (YesterdayQuote); }
		}							
		public decimal WeekDiff { 
			get { return CalculateQuoteDiffPercentage (OneWeekQuote); }
		}							
		public decimal MonthDiff { 
			get { return CalculateQuoteDiffPercentage (OneMonthQuote); }
		}							
		public decimal ThreeMonthsDiff { 
			get { return CalculateQuoteDiffPercentage (ThreeMonthQuote); }
		}							
		public decimal SixMonthsDiff { 
			get { return CalculateQuoteDiffPercentage (SixMonthQuote); }
		}							
		public decimal OneYearDiff { 
			get { return CalculateQuoteDiffPercentage (OneYearQuote); }
		}							
		public decimal TwoYearDiff { 
			get { return CalculateQuoteDiffPercentage (TwoYearQuote); }
		}							
		
		public decimal AverageCost {
			get { return (number_shares > 0 ? System.Decimal.Round (total_cost / number_shares, 2) : 0); }
		}			
		public decimal Value {
			get { 
				return System.Decimal.Round(quote_value * number_shares * (quote_is_percentage ? (decimal)  1/100 : 1), 2); }
		}		
		public decimal BaseValue {
			get { return System.Decimal.Round(exchange_rate_to_base * Value, 2); }
		}				
		public decimal ProfitLoss { 
			get { return System.Decimal.Round((Value + total_income) - total_cost, 2); }
		}			
		public decimal ProfitLossPercent { 
			get { return (total_cost > 0 ? 100 * System.Decimal.Round(ProfitLoss / total_cost, 4) : 100); }
		}							
				
		public override bool Equals(Object obj) 
		{
			//Check for null and compare run-time types.
			if (obj == null || GetType() != obj.GetType()) 
				return false;
			PortfolioEntry c2 = (PortfolioEntry)obj;
			return ((account_nr == c2.AccountNr) && (security_nr == c2.SecurityNr));
		}
		
		public override int GetHashCode() 
		{
			return account_nr.GetHashCode() ^ security_nr.GetHashCode();
		}			
	}
}