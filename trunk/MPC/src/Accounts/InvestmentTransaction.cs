// InvestmentTransaction.cs
// 
// Copyright (C) 2008 Bengt Thuree
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;

namespace MPC.Accounts
{
	public enum SecurityTransactionType {
		Buy = 0,
		Sell = 1,
		Dividend = 2,
		ReinvestDividend = 3,
		Yield = 4,
		AddShares = 5,
		RemoveShares =  6
	}
		
	public class InvestmentTransaction : MPC.Utils.DbItem
	{
		
		public InvestmentTransaction ()
		{
			investmentaccount = System.Int32.MinValue;
			timestamp = System.DateTime.MinValue;
			memo = null;
			security_id = System.Int32.MinValue;
			shares = 0;
			share_price = System.Int32.MinValue;
			reconsiled = false;
		}
		
		private int investmentaccount;
		public int InvestmentAccount {
			get { return investmentaccount; }
			set { investmentaccount = value; }
		}

		private System.DateTime timestamp;
		public System.DateTime TimeStamp {
			get { return timestamp; }
			set { timestamp = value; }
		}
		
		private string memo;
		public string Memo {
			get { return memo; }
			set { memo = value; }
		}

		private int security_id;
		public int Security_id {
			get { return security_id; }
			set { security_id = value; }
		}
		
		private decimal shares;
		public decimal Shares {
			get { return shares; }
			set { shares = value; }
		}
		private int share_price;
		public int Share_price {
			get { return share_price; }
			set { share_price = value; }
		}
		private bool reconsiled;
		public bool Reconsiled {
			get { return reconsiled; }
			set { reconsiled = value; }
		}
		private SecurityTransactionType trans_type;
		public SecurityTransactionType TransType {
			get { return trans_type; }
			set { trans_type = value; }
		}
				
		public override bool Equals(Object obj) 
		{
			//Check for null and compare run-time types.
			if (obj == null || GetType() != obj.GetType()) 
				return false;
			InvestmentTransaction c2 = (InvestmentTransaction)obj;
			return (Id == c2.Id);
		}
		
		public override int GetHashCode() 
		{
			return Memo.GetHashCode();
		}		
	}	
}
