// AddressDetail.cs
// 
// Copyright (C) 2008 Bengt Thuree
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;

namespace MPC.Accounts
{
	
	public class AddressDetail : MPC.Utils.DbItem
	{
		
		public AddressDetail ()
		{
			swift = null;
			phone = null;
			fax = null; 
			name = null;
			address1 = null;
			address2 = null;
			zip	= null;
			city = null;
			country = null;
			uri = null;
			notes = null;
		}
		
		private string swift;
		public string Swift {
			get { return swift; }
			set { swift = value; }
		}

		private string phone;
		public string Phone {
			get { return phone; }
			set { phone = value; }
		}
		
		private string fax;
		public string Fax {
			get { return fax; }
			set { fax = value; }
		}

		private string name;
		public string Name {
			get { return name; }
			set { name = value; }
		}
		
		private string address1;
		public string Address1 {
			get { return address1; }
			set { address1 = value; }
		}
		private string address2;
		public string Address2 {
			get { return address2; }
			set { address2 = value; }
		}
		private string zip;
		public string Zip {
			get { return zip; }
			set { zip = value; }
		}
		private string city;
		public string City {
			get { return city; }
			set { city = value; }
		}
		
		private string country;
		public string Country {
			get { return country; }
			set { country = value; }
		}
		
		private string uri;
		public string Uri {
			get { return uri; }
			set { uri = value; }
		}
		
		private string notes;
		public string Notes {
			get { return notes; }
			set { notes = value; }
		}
		
		public override bool Equals(Object obj) 
		{
			//Check for null and compare run-time types.
			if (obj == null || GetType() != obj.GetType()) 
				return false;
			AddressDetail c2 = (AddressDetail)obj;
			return (Name == c2.Name);
		}
		
		public override int GetHashCode() 
		{
			return Name.GetHashCode();
		}		
	}
}
