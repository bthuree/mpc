// InvestmentTransactionsGui.cs
// 
// Copyright (C) 2008 Bengt Thuree
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;
using Gtk;
using MPC.Accounts;
using MPC.Quotes;
using MPC.Utils;

namespace MPC.Accounts
{

	public partial class InvestmentTransactionsGui : Gtk.Dialog
	{
		SecurityQuoteStore sq_s;
		InvestmentTransactionStore it_s;
		SecurityStore s_s;
		InvestmentTransaction it;
		System.Collections.ArrayList it_ar;
		private ListStore InvestmentTransactionsTreeStore;				

		
		public InvestmentTransactionsGui()
		{
			sq_s = MPC.Database.GetDatabase.SecurityQuotes;
			s_s = MPC.Database.GetDatabase.Securities;
			it_s = MPC.Database.GetDatabase.InvestmentTransactions;	
			it = null;
			this.Build();
			BuildActivityTree();
			RemoveAllDetails();		
			PopulateTreeView(true);	
			security.Sensitive = true;
		}

		private void PopulateStore()
		{
			AccountHeader ah = getAccountHeader();
			if (ah != null) {
				it_ar = it_s.GetList(ah);			
				InvestmentTransactionsTreeStore = new ListStore (typeof(string));
				foreach (InvestmentTransaction it in it_ar) {
					InvestmentTransactionsTreeStore.AppendValues (it.Memo);
				}
			}
		}
		
		private void PopulateTreeView(bool define_columns)
		{
			PopulateStore();
			treeview.Model = InvestmentTransactionsTreeStore;
			if (define_columns) {
				treeview.AppendColumn ("Memo", new CellRendererText(), "text", 0);
			}
			this.ShowAll();
		}		

		private void ClearTreeView()
		{
			treeview.Model = null ;

		}				
		
		private void BuildActivityTree()
		{
			details_transtype.Active = -1;
			ListStore ActivityTreeStore = new ListStore (typeof (string));	
			foreach (string s in Enum.GetNames(typeof(MPC.Accounts.SecurityTransactionType) ))
			{
				ActivityTreeStore.AppendValues (s);
			}
			details_transtype.Model = ActivityTreeStore;
		}
		
		private void setTotalLabel (bool visible)
		{
			totalLabel.Visible = visible;
			total.Visible = visible;
		}
		private void setFees (bool visible)
		{
			feesLabel.Visible = visible;
			fees.Visible = visible;
		}		
		private void setPricePerShare (bool visible)
		{
			pricePerShare.Visible = visible;
			pricePerShareLabel.Visible = visible;
		}		
		private void setShares (bool visible)
		{
			sharesLabel.Visible = visible;
			shares.Visible = visible;
		}			
		private void setCashAccount (bool visible)
		{
			cashAccount.Visible = visible;
			cashAccountLabel.Visible = visible;
		}
		private void setFeeCategory (bool visible)
		{
			feeCategory.Visible = visible;
			feeCategoryLabel.Visible = visible;
		}
		private void setMemo (bool visible)
		{
			memo.Visible = visible;
			memoLabel.Visible = visible;
		}		
		private void setTransType (bool visible)
		{
			details_transtype.Visible = visible;
			details_transtypeLabel.Visible = visible;
		}						
		private void setSecurity (bool visible)
		{
			security.Visible = visible;
			securityLabel.Visible = visible;
		}										
		private void setDate (bool visible)
		{
			details_date.Visible = visible;
			details_dateLabel.Visible = visible;						
		}												
		private void RemoveAllDetails()
		{
			setTotalLabel(false);
			setFees (false);
			setPricePerShare (false);
			setShares (false);
			setCashAccount (false);
			setFeeCategory (false);
			setMemo (false);
			setDate (false);
			setSecurity (false);
			setTransType (investmentAccount.Selection == null ? false : true);
		}
		private void ShowAddSharesDetails()
		{
			setTotalLabel(false);
			setFees (false);
			setPricePerShare (false);
			setShares (true);
			setCashAccount (false);
			setFeeCategory (false);
			setMemo (true);	
			setDate (true);
			setSecurity (true);
			setTransType (true);
		}		
		protected virtual void OnButtonCancelClicked (object sender, System.EventArgs e)
		{
			this.Destroy();									
		}

		protected virtual void OnButtonOkClicked (object sender, System.EventArgs e)
		{
			this.Destroy();									
		}
		
		private void SaveAddRemoveShares (AccountHeader ah, Security sec, decimal shares, DateTime date, string memoStr, SecurityTransactionType transType)
		{
			bool new_it = false;
			if (it == null) {
				new_it = true;
				it = new InvestmentTransaction();
			}
			it.InvestmentAccount = ah.Id;
			it.TimeStamp = date;
			it.Memo = memoStr;
			it.Security_id = sec.Id;
			it.Shares = shares;
			it.TransType = transType;
			if (new_it)
				it_s.Add (it);
			else
				it_s.Change (it);
		}
		
		private void SaveAddShares ()
		{
			decimal numberOfShares = System.Convert.ToDecimal ( shares.Text );
			Security sec = getSecurity();
			AccountHeader ah = getAccountHeader();
			SaveAddRemoveShares (ah, sec, numberOfShares, details_date.Date, memo.Text, (SecurityTransactionType) details_transtype.Active);
		}

		private void SaveRemoveShares ()
		{
			decimal numberOfShares = System.Convert.ToDecimal ( shares.Text );
			Security sec = getSecurity();
			AccountHeader ah = getAccountHeader();
			SaveAddRemoveShares (ah, sec, -numberOfShares, details_date.Date, memo.Text, (SecurityTransactionType) details_transtype.Active);
		}
		
		protected virtual void OnButtonSaveClicked (object sender, System.EventArgs e)
		{
			switch ( (SecurityTransactionType) details_transtype.Active) {
			case SecurityTransactionType.Buy :
				break;				
			case SecurityTransactionType.Sell :
				break;				
			case SecurityTransactionType.AddShares :
				SaveAddShares();				
				break;
			case SecurityTransactionType.RemoveShares :
				SaveRemoveShares();
				break;				
			case SecurityTransactionType.Dividend :
				break;				
			case SecurityTransactionType.ReinvestDividend :
				break;			
			case SecurityTransactionType.Yield :
				break;				
			default :
				Log.WarningFormat ("Should not be here... OnButtonSaveClicked, Active = {0}", details_transtype.Active);
				break;
			}			
			PopulateTreeView(false);							
		}

		protected virtual void OnDetailsTranstypeChanged (object sender, System.EventArgs e)
		{
			switch ( (SecurityTransactionType) details_transtype.Active) {
			case SecurityTransactionType.Buy :
				RemoveAllDetails();				
				break;				
			case SecurityTransactionType.Sell :
				RemoveAllDetails();				
				break;				
			case SecurityTransactionType.AddShares :
				ShowAddSharesDetails();				
				break;
			case SecurityTransactionType.RemoveShares :
				ShowAddSharesDetails();
				break;				
			case SecurityTransactionType.Dividend :
				RemoveAllDetails();
				break;				
			case SecurityTransactionType.ReinvestDividend :
				RemoveAllDetails();
				break;			
			case SecurityTransactionType.Yield :
				RemoveAllDetails();
				break;				
			default :
				Log.WarningFormat ("Should not be here... OnDetailsTranstypeChanged, Active = {0}", details_transtype.Active);
				break;
			}
		}
		
		private Security getSecurity()
		{
			return security.Selection;
		}
		
		private AccountHeader getAccountHeader()
		{
			AccountHeader ah = null;
			ah = investmentAccount.Selection;
			return ah;
		}		

		protected virtual void OnSecurityChanged (object sender, System.EventArgs e)
		{
			Security sec = getSecurity();
			if (sec != null)
				Log.DebugFormat ("Using Security {0}", sec.Name);
		}
	
		protected InvestmentTransaction FetchInvTransDetailFromRow()
		{
			TreePath path = null;
			TreeViewColumn column = null;
			treeview.GetCursor (out path, out column);
			if (path != null) {
				int row = System.Convert.ToInt32 (path.ToString());
				InvestmentTransaction it = it_ar[row] as InvestmentTransaction;
				return it;
			}			
			return null;
		}		

		private void ClearDetails()
		{
			security.Selection = null;
			memo.Text = String.Empty;
			details_date.Date = System.DateTime.Today;
			shares.Text = String.Empty;
			details_transtype.Active = -1;
		}
		
		protected virtual void OnTreeviewCursorChanged (object sender, System.EventArgs e)
		{
			ClearDetails();
			it = FetchInvTransDetailFromRow();
			if (it != null) {
				security.SecurityId = it.Security_id;
				memo.Text = it.Memo;
				details_date.Date = it.TimeStamp;
				shares.Text = it.Shares.ToString();
				details_transtype.Active = (int) it.TransType;
			}												
		}

		protected virtual void OnNewActionActivated (object sender, System.EventArgs e)
		{
			it = null;
			ClearDetails ();
		}

		protected virtual void OnDeleteActionActivated (object sender, System.EventArgs e)
		{
			it = FetchInvTransDetailFromRow();
			if (it != null) {
				it_s.Del (it);
				it = null;
				PopulateTreeView(false);				
			}															
		}

		protected virtual void OnInvestmentAccountSelectionChanged (object sender, System.EventArgs e)
		{
			if (investmentAccount.Selection == null)
			{
				setTransType (false);
				ClearTreeView();				
			}		
			else 
			{
				setTransType(true);
				PopulateTreeView(false);										
			}			
		}
	}
}