// AccountStore.cs
// 
// Copyright (C) 2008 Bengt Thuree
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//


using System;
using System.IO;
using System.Data;



namespace MPC.Accounts
{
	public class AccountHeaderStore : MPC.Utils.DbGeneralStore
	{
	
		public AccountHeaderStore (IDbConnection _dbcon) : base (_dbcon, "AccountHeaders")
		{
		}

		private string GenerateAddSql (AccountHeader a)
		{
			string sql = "INSERT into AccountHeaders " + 
				" (parent, name, address_id, contact_id, account_nr, currency_id) " +
				String.Format ("VALUES ( '{0}', '{1}', '{2}', '{3}', '{4}', '{5}' )", 
					a.Parent, a.Name, a.Address_id, a.Contact_id, a.Account_nr, a.Currency_id
					);	
			return sql;
		}
		
		public void Add (AccountHeader a)
		{
			base.Add (GenerateAddSql (a) );
		}		
		
		public int Add_GetIndex (AccountHeader a)
		{
			return base.Add_GetIndex (GenerateAddSql (a) );
		}		
				
		public void Del (AccountHeader a)
		{
			Del (a.Id);
		}
		
		public void Del (int index)
		{
			string sql = string.Format ("DELETE FROM AccountHeaders WHERE id = {0}", index);
			base.Del (sql);			
		}		

		public new void Del (string name)
		{
			string sql = string.Format ("DELETE FROM AccountHeaders WHERE name = '{0}'", name);
			base.Del (sql);			
		}				

		public void Change (AccountHeader a)
		{
			string sql = "UPDATE AccountHeaders SET " +
				String.Format ("parent='{0}', name='{1}', address_id='{2}', contact_id='{3}', " +
					"account_nr='{4}', currency_id='{5}' ", 
					a.Parent, a.Name, a.Address_id, a.Contact_id, a.Account_nr, a.Currency_id) +
				String.Format (" WHERE id = {0}", a.Id); 
			base.Change (sql);			
		}

		public override MPC.Utils.DbItem DecodeReader (IDataReader reader)
		{
			AccountHeader a = new AccountHeader ();					
			a.Id = reader.GetInt32(reader.GetOrdinal("id"));
			a.Parent = reader.GetInt32(reader.GetOrdinal("parent"));
			a.Name = reader.GetString(reader.GetOrdinal("name"));
			a.Address_id = reader.GetInt32(reader.GetOrdinal("address_id"));
			a.Contact_id = reader.GetInt32(reader.GetOrdinal("contact_id"));
			a.Account_nr = reader.GetString(reader.GetOrdinal("account_nr"));
			a.Currency_id = reader.GetInt32(reader.GetOrdinal("currency_id"));
			return (MPC.Utils.DbItem) a;
		}
		
		public AccountHeader Get (AccountHeader a)
		{
			return Get (a.Id);							
		}
		
		public AccountHeader Get (int id)
		{
			string sql = "SELECT id, parent, name, address_id, contact_id, account_nr, currency_id " +
				"FROM AccountHeaders " +
				String.Format ("WHERE id = {0}", id); 
			return base.Get (sql) as AccountHeader;							
		}

		public new AccountHeader Get (string _name)
		{
			string sql = "SELECT id, parent, name, address_id, contact_id, account_nr, currency_id " +
				"FROM AccountHeaders " +
				String.Format ("WHERE name = '{0}'", _name); 
			return base.Get (sql) as AccountHeader;							
		}

		public System.Collections.ArrayList GetList ()
		{
			string sql = "SELECT id, parent, name, address_id, contact_id, account_nr, currency_id " +
					"FROM AccountHeaders ORDER BY name"; 
			return base.GetList (sql);							
		}
	}
}

