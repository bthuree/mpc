// AccountWidget.cs
// 
// Copyright (C) 2008 Bengt Thuree
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;
using Gtk;
using MPC.Accounts;

namespace MPC.Widgets
{
	
	[System.ComponentModel.ToolboxItem(true)]
	public partial class AccountWidget : Gtk.Bin
	{

		System.Collections.ArrayList ComboIndexes;
		AccountHeaderStore a_s;

// FIXME Sensitive and AllowNew DEFAULTS (from Properties) is not working!!!
		
		public AccountWidget()
		{
			this.Build();
			a_s = MPC.Database.GetDatabase.Accounts;
			ComboIndexes = new System.Collections.ArrayList();	
			BuildComboTree();
		}

		public event EventHandler SelectionChanged;
		
		public bool AllowNew
		{
			get { return newButton.Visible; }
			set { newButton.Visible = value; }
		}
		
		public AccountHeader Selection
		{
			get { return (combo.Active >= 0 ? (AccountHeader) ComboIndexes [combo.Active] : null); }
			set { combo.Active = (value == null ? -1 : ComboIndexes.IndexOf (value)); }
		}		
		public int AccountHeaderId
		{
			set { Selection = a_s.Get (value); }
		}
		
		private void BuildComboTree()
		{
			//FIXME The end result should only be ONE column, not two.!!			
			ComboIndexes.Clear();
			CellRendererText textRenderer = new CellRendererText();			
			combo.Active = -1;
//			combo.PackStart(textRenderer, false);
//			combo.AddAttribute(textRenderer, "text", 0);
			System.Collections.ArrayList a_arr = a_s.GetList();
			ListStore ComboTreeStore = new ListStore (typeof (string));
			foreach (AccountHeader act in a_arr) {
				ComboTreeStore.AppendValues (act.Name); 
				ComboIndexes.Add (act);
			}
			combo.Model = ComboTreeStore;	
		}		

		public void ComboHasChanged(object sender)
		{
			BuildComboTree();
		}
		
		protected virtual void OnComboChanged (object sender, System.EventArgs e)
		{
			if (SelectionChanged != null)
				SelectionChanged (this, e);
		}

		protected virtual void OnNewButtonClicked (object sender, System.EventArgs e)
		{
			MPC.Accounts.AccountHeaderGui win = new MPC.Accounts.AccountHeaderGui ();
			win.EventName += new MPC.Accounts.EventNameEventHandler (this.ComboHasChanged);						
			win.Run();	
		}
	}
}
