// AddressWidget.cs
// 
// Copyright (C) 2008 Bengt Thuree
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;
using Gtk;
using MPC.Accounts;

namespace MPC.Widgets
{
	
	[System.ComponentModel.ToolboxItem(true)]
	public partial class AddressWidget : Gtk.Bin
	{

		System.Collections.ArrayList ComboIndexes;
		AddressDetailsStore adr_s;

// FIXME Sensitive and AllowNew DEFAULTS (from Properties) is not working!!!
		
		public AddressWidget()
		{
			this.Build();
			adr_s = MPC.Database.GetDatabase.AddressDetails;
			ComboIndexes = new System.Collections.ArrayList();	
			BuildComboTree();
		}

		public event EventHandler SelectionChanged;
		
		public bool AllowNew
		{
			get { return newButton.Visible; }
			set { newButton.Visible = value; }
		}
		
		public AddressDetail Selection
		{
			get { return (combo.Active >= 0 ? (AddressDetail) ComboIndexes [combo.Active] : null); }
			set { combo.Active = (value == null ? -1 : ComboIndexes.IndexOf (value)); }
		}		
		public int AddressDetailId
		{
			set { Selection = adr_s.Get (value); }
		}
		
		private void BuildComboTree()
		{
			//FIXME The end result should only be ONE column, not two.!!			
			ComboIndexes.Clear();
			CellRendererText textRenderer = new CellRendererText();			
			combo.Active = -1;
//			combo.PackStart(textRenderer, false);
//			combo.AddAttribute(textRenderer, "text", 0);
			System.Collections.ArrayList adr_arr = adr_s.GetList();
			ListStore ComboTreeStore = new ListStore (typeof (string));
			foreach (AddressDetail adr in adr_arr) {
				ComboTreeStore.AppendValues (adr.Name); 
				ComboIndexes.Add (adr);
			}
			combo.Model = ComboTreeStore;	
		}		

		public void ComboHasChanged(object sender)
		{
			BuildComboTree();
		}
		
		protected virtual void OnComboChanged (object sender, System.EventArgs e)
		{
			if (SelectionChanged != null)
				SelectionChanged (this, e);
		}

		protected virtual void OnNewButtonClicked (object sender, System.EventArgs e)
		{
			MPC.Accounts.AddressGui win = new MPC.Accounts.AddressGui ();
			win.EventName += new MPC.Accounts.EventNameEventHandler (this.ComboHasChanged);						
			win.Run();	
		}
	}
}
