// CurrencyWidget.cs
// 
// Copyright (C) 2008 Bengt Thuree
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;
using Gtk;
using MPC.Quotes;

namespace MPC.Widgets 
{
	
	[System.ComponentModel.ToolboxItem(true)]
	public partial class CurrencyWidget : Gtk.Bin
	{
		System.Collections.ArrayList ComboIndexes;
		CurrencyStore c_s;

// FIXME Sensitive and AllowNew DEFAULTS (from Properties) is not working!!!
		
		public CurrencyWidget()
		{
			this.Build();
			c_s = MPC.Database.GetDatabase.Currencies;
			ComboIndexes = new System.Collections.ArrayList();	
			BuildComboTree();
		}

		public event EventHandler SelectionChanged;
		
		public bool AllowNew
		{
			get { return newButton.Visible; }
			set { newButton.Visible = value; }
		}
		
		public Currency Selection
		{
			get { return (combo.Active >= 0 ? (Currency) ComboIndexes [combo.Active] : null); }
			set { combo.Active = (value == null ? -1 : ComboIndexes.IndexOf (value)); }
		}		
		public int CurrencyId
		{
			set { Selection = c_s.Get (value); }
		}
		
		private void BuildComboTree()
		{
			//FIXME The end result should only be ONE column, not two.!!			
			ComboIndexes.Clear();
			CellRendererText textRenderer = new CellRendererText();			
			combo.Active = -1;
			combo.PackStart(textRenderer, false);
			combo.AddAttribute(textRenderer, "text", 0);
			combo.AddAttribute(textRenderer, "text", 1);						
			System.Collections.ArrayList c_arr = c_s.GetList();
			ListStore ComboTreeStore = new ListStore (typeof (string), typeof (string));
			foreach (Currency cur in c_arr) {
				ComboTreeStore.AppendValues (cur.Code, cur.Country); 
				ComboIndexes.Add (cur);
			}
			combo.Model = ComboTreeStore;	
		}		

		public void ComboHasChanged(object sender)
		{
			BuildComboTree();
		}
		
		protected virtual void OnComboChanged (object sender, System.EventArgs e)
		{
			if (SelectionChanged != null)
				SelectionChanged (this, e);
		}

		protected virtual void OnNewButtonClicked (object sender, System.EventArgs e)
		{
			MPC.Quotes.CurrencyGui win = new MPC.Quotes.CurrencyGui ();
			win.EventName += new MPC.Quotes.EventNameEventHandler (this.ComboHasChanged);						
			win.Run();	
		}

	}
}
