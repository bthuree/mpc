// SplitSecurityTest.cs
// 
// Copyright (C) 2008 Bengt Thuree
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;
using NUnit.Framework;

using MPC.Quotes;
using MPC.Accounts;
using MPC;

namespace AccountTest
{
	
	
	[TestFixture()]
	public class SplitSecurityTest
	{
		
		public SecurityQuote sq1, sq2, sq3, sq4, sq_tmp, sq_tmp2;
		public QuoteSource qs1, qs2, qs3, qs4, qs_tmp, qs_tmp2;		
		public Currency c1, c2, c3, c4, c_tmp, c_tmp2;		
		public Security s1, s2, s3, s4, s_tmp, s_tmp2;		
		public InvestmentTransaction it1, it2, it3, it4, it_tmp, it_tmp2;
		public AddressDetail ad1, ad2, ad3, ad4, ad_tmp, ad_tmp2;
		public ContactDetail cd1, cd2, cd3, cd4, cd_tmp, cd_tmp2;
		public AccountHeader a1, a2, a3, a4, a_tmp, a_tmp2;
		public AccountHeaderStore a_s;		
		public AddressDetailsStore ad_s;
		public ContactDetailsStore cd_s;		
		public SecurityQuoteStore sq_s;
		public SecurityStore s_s;
		public CurrencyStore c_s;
		public QuoteSourceStore qs_s;
		public InvestmentTransactionStore it_s;
		
		public SplitSecurity split;
		
		public DateTime dt, dt2;
		public decimal OriginalShares, NewShares;
		public decimal SplitRatio;
		System.Collections.ArrayList org_db, new_db;

		[TestFixtureSetUp]
		public void OpenDatabaseConnection()
		{
			MPC.Database.SetTestDB();
		}

		[SetUp]
		public void MySetup()
		{		
			c_s = MPC.Database.GetDatabase.Currencies;
			c1 = c_s.Get ("SEK");
			c2 = c_s.Get ("USD");
			c3 = c_s.Get ("HKD");
			c4 = c_s.Get ("AUD");			

			qs_s = MPC.Database.GetDatabase.QuoteSources;
			qs_s.Del_All();			
				
			s_s = MPC.Database.GetDatabase.Securities;
			s_s.Del_All();

			sq_s = MPC.Database.GetDatabase.SecurityQuotes;
			sq_s.Del_All();

			it_s = MPC.Database.GetDatabase.InvestmentTransactions;
			it_s.Del_All();

			ad_s = MPC.Database.GetDatabase.AddressDetails;
			ad_s.Del_All();
			
			cd_s = MPC.Database.GetDatabase.ContactDetails;
			cd_s.Del_All();
				
			a_s = MPC.Database.GetDatabase.Accounts;
			a_s.Del_All();			
				
			ad1 = new AddressDetail();
			ad1.Swift = "swift";
			ad1.Phone = "Phone";
			ad1.Fax = "Fax";
			ad1.Name = "Name";
			ad1.Address1 = "Address1";
			ad1.Address2 = "Address2";
			ad1.Zip	= "zip";
			ad1.City = "city";
			ad1.Country = "country";
			ad1.Uri = "uri";
			ad1.Notes = "notes";
				
			ad2 = new AddressDetail();
			ad2.Swift = "2swift";
			ad2.Phone = "2Phone";
			ad2.Fax = "2Fax";
			ad2.Name = "2Name";
			ad2.Address1 = "2Address1";
			ad2.Address2 = "2Address2";
			ad2.Zip	= "2zip";
			ad2.City = "2city";
			ad2.Country = "2country";
			ad2.Uri = "2uri";
			ad2.Notes = "2notes";

			ad3 = new AddressDetail();
			ad3.Swift = "3swift";
			ad3.Phone = "3Phone";
			ad3.Fax = "3Fax";
			ad3.Name = "3Name";
			ad3.Address1 = "3Address1";
			ad3.Address2 = "3Address2";
			ad3.Zip	= "3zip";
			ad3.City = "3city";
			ad3.Country = "3country";
			ad3.Uri = "3uri";
			ad3.Notes = "3notes";

			ad4 = new AddressDetail();
			ad4.Swift = "4swift";
			ad4.Phone = "4Phone";
			ad4.Fax = "Fax4";
			ad4.Name = "Name4";
			ad4.Address1 = "4Address1";
			ad4.Address2 = "4Address2";
			ad4.Zip	= "zip4";
			ad4.City = "city4";
			ad4.Country = "4country";
			ad4.Uri = "uri4";
			ad4.Notes = "notes4";	
				
			ad1.Id = ad_s.Add_GetIndex (ad1);
			ad2.Id = ad_s.Add_GetIndex (ad2);
			ad3.Id = ad_s.Add_GetIndex (ad3);
			ad4.Id = ad_s.Add_GetIndex (ad4);	

			cd1 = new ContactDetail();
			cd1.Name = "cdBengt";
			cd1.Phone = "cdPhone";
			cd1.Mobile = "cdMobile";
			cd1.Other_phone = "cdOtherPhone";
			cd1.Fax = "Fax";
			cd1.Work_mail = "cdWorkMail";
			cd1.Private_mail = "cdPrivateMail";
			cd1.Other_mail = "cdOtherMail";
			cd1.Address_id	= ad1.Id;
			cd1.Uri = "cdUri";
			cd1.Notes = "cdNotes";
				
			cd2 = new ContactDetail();
			cd2.Name = "2cdBengt";
			cd2.Phone = "2cdPhone";
			cd2.Mobile = "2cdMobile";
			cd2.Other_phone = "2cdOtherPhone";
			cd2.Fax = "2Fax";
			cd2.Work_mail = "2cdWorkMail";
			cd2.Private_mail = "2cdPrivateMail";
			cd2.Other_mail = "2cdOtherMail";
			cd2.Address_id	= ad2.Id;
			cd2.Uri = "2cdUri";
			cd2.Notes = "2cdNotes";
				
			cd3 = new ContactDetail();
			cd3.Name = "3cdBengt";
			cd3.Phone = "3cdPhone";
			cd3.Mobile = "3cdMobile";
			cd3.Other_phone = "3cdOtherPhone";
			cd3.Fax = "3Fax";
			cd3.Work_mail = "3cdWorkMail";
			cd3.Private_mail = "3cdPrivateMail";
			cd3.Other_mail = "3cdOtherMail";
			cd3.Address_id	= ad3.Id;
			cd3.Uri = "3cdUri";
			cd3.Notes = "3cdNotes";
				
			cd4 = new ContactDetail();
			cd4.Name = "4cdBengt";
			cd4.Phone = "4cdPhone";
			cd4.Mobile = "4cdMobile";
			cd4.Other_phone = "4cdOtherPhone";
			cd4.Fax = "4Fax";
			cd4.Work_mail = "4cdWorkMail";
			cd4.Private_mail = "4cdPrivateMail";
			cd4.Other_mail = "4cdOtherMail";
			cd4.Address_id	= ad4.Id;
			cd4.Uri = "4cdUri";
			cd4.Notes = "4cdNotes";

			cd1.Id = cd_s.Add_GetIndex (cd1);
			cd2.Id = cd_s.Add_GetIndex (cd2);
			cd3.Id = cd_s.Add_GetIndex (cd3);
			cd4.Id = cd_s.Add_GetIndex (cd4);	

			a1 = new AccountHeader();
			a1.Name = "Saving";
			a1.Address_id = ad1.Id;
			a1.Contact_id = cd1.Id;
			a1.Account_nr = "123456789";
			a1.Currency_id = c1.Id;

			a2 = new AccountHeader();
			a2.Name = "Investment";
			a2.Address_id = ad2.Id;
			a2.Contact_id = cd2.Id;
			a2.Account_nr = "123456789";
			a2.Currency_id = c2.Id;

			a3 = new AccountHeader();
			a3.Name = "Play";
			a3.Address_id = ad3.Id;
			a3.Contact_id = cd3.Id;
			a3.Account_nr = "123456789";
			a3.Currency_id = c3.Id;
			
			a4 = new AccountHeader();
			a4.Name = "House";
			a4.Address_id = ad4.Id;
			a4.Contact_id = cd4.Id;
			a4.Account_nr = "123456789";
			a4.Currency_id = c4.Id;

			a1.Id = a_s.Add_GetIndex (a1);
			a2.Id = a_s.Add_GetIndex (a2);
			a3.Id = a_s.Add_GetIndex (a3);					
			a4.Id = a_s.Add_GetIndex (a4);
				
			qs1 = new QuoteSource("Yahoo global");
			qs1.Uri = "http://finance.yahoo.com/d/quotes.csv?s=%1&f=sl1d1";
			qs2 = new QuoteSource("Yahoo local");
			qs2.Uri = "http://finance.yahoo.se/d/quotes.csv?s=%1&f=sl1d1";
			qs3 = new QuoteSource("Yahoo Denmark");
			qs3.Uri = "http://finance.yahoo.dk/d/quotes.csv?s=%1&f=sl1d1";
				
			qs1.Id = qs_s.Add_GetIndex (qs1);
			qs2.Id = qs_s.Add_GetIndex (qs2);
			qs3.Id = qs_s.Add_GetIndex (qs3);								
				
			s1 = new Security("ERIC");
			s1.Name = "Ericsson";
			s1.Currency = c1.Id;
			s1.Quote_Source = qs1.Id;

			s2 = new Security("MSFT");
			s2.Name = "Microsoft";
			s2.Currency = c2.Id;
			s2.Quote_Source = qs2.Id;

			s3 = new Security("SEB Europa Fond");
			s3.Name = "SEB Europa Fond";
			s3.Currency = c3.Id;
			s3.Quote_Source = qs3.Id;

			s4 = new Security("AZN");
			s4.Name = "Amazon";
			s4.Currency = c3.Id;
			s4.Quote_Source = qs3.Id;

			s1.Id = s_s.Add_GetIndex (s1);
			s2.Id = s_s.Add_GetIndex (s2);
			s3.Id = s_s.Add_GetIndex (s3);
			s4.Id = s_s.Add_GetIndex (s4);

			dt = new DateTime (2007, 3, 2, 0, 0, 0);
			dt2 = new DateTime (2007, 3, 10, 0, 0, 0);				
			
			OriginalShares = 1000m;
			NewShares = 10000m;
			SplitRatio = NewShares / OriginalShares;
			
			split = new SplitSecurity();			
			
		}

		[TestFixtureTearDown]
		public void CloseDatabaseConnection()
		{
		}

		private void AddSecurityQuote (decimal _quote, int times, int _security, int year, int month, MPC.Quotes.UpdateTypeEnum type )
		{
			for (int k=0; k < times; k++)
			{
				sq1 = new SecurityQuote();
				sq1.Symbol = _security;
				sq1.TimeStamp = new DateTime (year, month, 1+k, 0, 0, 0);
				sq1.Quote_Value = _quote + k;
				sq1.Quote_Type = type;
				sq_s.Add (sq1);
			}
		}
		
		private void AddTransaction (decimal _quote, int shares, int times, int year, int month, int account, int _security)
		{
			for (int k=0; k < times; k++)
			{
				sq1 = new SecurityQuote();
				sq1.Symbol = _security;
				sq1.TimeStamp = new DateTime (year, month, 1+k, 0, 0, 0);
				sq1.Quote_Value = _quote + k;
				sq1.Quote_Type = (shares > 0 ? MPC.Quotes.UpdateTypeEnum.buy : MPC.Quotes.UpdateTypeEnum.sell );
				sq1.Id = sq_s.Add_GetIndex (sq1);				
				
				it1 = new InvestmentTransaction();
				it1.InvestmentAccount = account;
				it1.TimeStamp = sq1.TimeStamp;
				it1.Memo = String.Format ("Action {0}", k);
				it1.Security_id = _security;
				it1.Shares = shares;
				it1.Share_price = sq1.Id;
				it1.Id = it_s.Add_GetIndex (it1);
			}
		}
		
		private void AddMultipleSet()
		{
			AddTransaction (100, 30, 1, 2006, 10, a2.Id, s1.Id);				
			AddTransaction (90.5m, 20, 2, 2006, 10, a1.Id, s1.Id);
			AddTransaction (100, 30, 1, 2006, 11, a2.Id, s1.Id);	
			AddTransaction (110, 35, 1, 2006, 12, a2.Id, s1.Id);	
			AddTransaction (120, 35, 1, 2007,  1, a2.Id, s1.Id);	
			AddTransaction (150, 35, 1, 2007,  2, a2.Id, s1.Id);
			AddTransaction (160, 35, 1, 2007,  3, a2.Id, s1.Id);	
			AddTransaction (120, 35, 1, 2007,  4, a2.Id, s1.Id);	
			AddTransaction (146, 35, 1, 2007,  5, a2.Id, s1.Id);				
			AddTransaction (23.4567m, 500, 1, 2007, 2, a1.Id, s2.Id);	
			AddTransaction (9.76m, 50, 1, 2007, 2, a1.Id, s3.Id);	
			AddTransaction (135, 60, 1, 2007, 4, a1.Id, s1.Id);	
			AddTransaction (10.08m, 30, 1, 2007, 4, a3.Id, s3.Id);				
			AddTransaction (112, 35, 1, 2006, 12, a1.Id, s1.Id);	
			AddTransaction (20, -15, 2, 2007, 12, a1.Id, s3.Id);	
			AddTransaction (119.12m, -10, 2, 2007, 1, a1.Id, s1.Id);						
			AddTransaction (16.75m, -50, 3, 2007, 5, a3.Id, s3.Id);						
			AddTransaction (123, -40, 4, 2007, 5, a1.Id, s1.Id);	

			AddSecurityQuote (100, 20, s1.Id, 2006, 10, MPC.Quotes.UpdateTypeEnum.online);				
			AddSecurityQuote (120, 20, s1.Id, 2006, 11, MPC.Quotes.UpdateTypeEnum.online);				
			AddSecurityQuote (140, 20, s1.Id, 2006, 12, MPC.Quotes.UpdateTypeEnum.online);				
			AddSecurityQuote (135, 20, s1.Id, 2007, 1, MPC.Quotes.UpdateTypeEnum.online);				
			AddSecurityQuote (155, 20, s1.Id, 2007, 2, MPC.Quotes.UpdateTypeEnum.online);				
			AddSecurityQuote (180, 20, s1.Id, 2007, 3, MPC.Quotes.UpdateTypeEnum.online);				
			AddSecurityQuote (300, 20, s1.Id, 2007, 4, MPC.Quotes.UpdateTypeEnum.online);				
			AddSecurityQuote (150, 20, s1.Id, 2007, 5, MPC.Quotes.UpdateTypeEnum.online);				
			AddSecurityQuote (100, 20, s1.Id, 2007, 6, MPC.Quotes.UpdateTypeEnum.online);				

			AddSecurityQuote (149, 4, s1.Id, 2007, 1, MPC.Quotes.UpdateTypeEnum.manual);													
		}
		
		private decimal TotalSum (InvestmentTransaction it)
		{
			SecurityQuote sq = sq_s.Get(it.Share_price);
			return System.Decimal.Round (it.Shares * sq.Quote_Value, 2);
		}
		
		private bool SecurityQuoteCalculateSplitResult (
			System.Collections.ArrayList org_db,
			System.Collections.ArrayList new_db,
			decimal	old_shares,
			decimal new_shares,
			DateTime dt,
			int sec
			)
		{
			if (org_db.Count != new_db.Count)
				return false;
			for (int k = 0; k < org_db.Count; k++)
			{
				if ( (org_db[k] as SecurityQuote).Id != (new_db[k] as SecurityQuote).Id)
				{
					System.Console.WriteLine ("id:{0}, id:{1}, k:{2}", (org_db[k] as SecurityQuote).Id, (new_db[k] as SecurityQuote).Id, k);
					return false;
				}					
				if ( (org_db[k] as SecurityQuote).Symbol == sec)
				{
					if ( (org_db[k] as SecurityQuote).TimeStamp < dt)
					{
						if ( System.Decimal.Round((org_db[k] as SecurityQuote).Quote_Value * (old_shares/new_shares), 2) != (new_db[k] as SecurityQuote).Quote_Value)
							return false;
					} else
					{
						if ( (org_db[k] as SecurityQuote).Quote_Value != (new_db[k] as SecurityQuote).Quote_Value)
							return false;
					}
					
				} else
					if ( (org_db[k] as SecurityQuote).Quote_Value != (new_db[k] as SecurityQuote).Quote_Value)
						return false;
			}
			return true;
		}
		
		private System.Collections.ArrayList RemoveSplitHeader (System.Collections.ArrayList org_db, System.Collections.ArrayList new_db)
		{
			int t = -1;
			for (int k = 0; k < org_db.Count; k++)
			{
				t++;
				if ( (org_db[k] as InvestmentTransaction).Id != (new_db[t] as InvestmentTransaction).Id)
				{
					if ((new_db[t] as InvestmentTransaction).Memo.StartsWith ("Stock Split. "))
					{
						new_db.RemoveAt(t);
						t--;
					}
				}
			}
			return new_db;
		}
		
		private bool InvTranCalculateSplitResult (
			System.Collections.ArrayList org_db,
			System.Collections.ArrayList new_db,
			decimal	old_shares,
			decimal new_shares,
			DateTime dt,
			int sec
			)
		{
			new_db = RemoveSplitHeader(org_db, new_db);
			if (org_db.Count != new_db.Count)
				return false;
			for (int k = 0; k < org_db.Count; k++)
			{
				if ( (org_db[k] as InvestmentTransaction).Id != (new_db[k] as InvestmentTransaction).Id)
				{
					return false;
				}					
				if ( (org_db[k] as InvestmentTransaction).Security_id == sec)
				{
					if ( (org_db[k] as InvestmentTransaction).TimeStamp < dt)
					{
						if ( System.Decimal.Round((org_db[k] as InvestmentTransaction).Shares * (new_shares/old_shares), 4) != (new_db[k] as InvestmentTransaction).Shares)
						{
							return false;
						}
					} else
					{
						if ( (org_db[k] as InvestmentTransaction).Shares != (new_db[k] as InvestmentTransaction).Shares)
						{
							return false;
						}
					}
					
				} else
					if ( (org_db[k] as InvestmentTransaction).Shares != (new_db[k] as InvestmentTransaction).Shares)
					{
						return false;
					}
			}
			return true;
		}
		
		[Test()]
		public void One_Quote_Split_simple()
		{
			AddSecurityQuote (149, 1, s1.Id, dt.Year, dt.Month, MPC.Quotes.UpdateTypeEnum.manual);	
			org_db = sq_s.GetList();							
			split.PerformSplit (s1.Id, 1m, 2m, dt2);
			new_db = sq_s.GetList();				
			Assert.AreEqual (System.Decimal.Round((org_db[0] as SecurityQuote).Quote_Value/2, 2), (new_db[0] as SecurityQuote).Quote_Value );
		}

		[Test()]
		public void One_Quote_ReverseSplit_simple()
		{
			AddSecurityQuote (149, 1, s1.Id, dt.Year, dt.Month, MPC.Quotes.UpdateTypeEnum.manual);
			org_db = sq_s.GetList();							
			split.PerformSplit (s1.Id, 2m, 1m, dt2);
			new_db = sq_s.GetList();				
			Assert.AreEqual ( System.Decimal.Round((org_db[0] as SecurityQuote).Quote_Value * 2, 2), (new_db[0] as SecurityQuote).Quote_Value);
		}

		[Test()]
		public void One_Quote_Split_float()
		{
			AddSecurityQuote (149, 1, s1.Id, dt.Year, dt.Month, MPC.Quotes.UpdateTypeEnum.manual);
			org_db = sq_s.GetList();							
			split.PerformSplit (s1.Id, 1.75m, 2.5m, dt2);
			new_db = sq_s.GetList();				
			Assert.AreEqual (System.Decimal.Round((org_db[0] as SecurityQuote).Quote_Value / (2.5m/1.75m), 2), (new_db[0] as SecurityQuote).Quote_Value) ;
		}

		
		[Test()]
		public void One_Quote_ReverseSplit_float()
		{
			AddSecurityQuote (149, 1, s1.Id, dt.Year, dt.Month, MPC.Quotes.UpdateTypeEnum.manual);
			org_db = sq_s.GetList();							
			split.PerformSplit (s1.Id, 2.5m, 1.75m, dt2);
			new_db = sq_s.GetList();				
			Assert.AreEqual (System.Decimal.Round((org_db[0] as SecurityQuote).Quote_Value / (1.75m/2.5m), 2), (new_db[0] as SecurityQuote).Quote_Value) ;
		}
		
		[Test()]
		public void One_Quote_Split_EndShares()
		{
			AddSecurityQuote (149, 1, s1.Id, dt.Year, dt.Month, MPC.Quotes.UpdateTypeEnum.manual);
			org_db = sq_s.GetList();							
			split.PerformSplit (s1.Id, 1234.5678m, 1678.3467m, dt2);
			new_db = sq_s.GetList();				
			Assert.AreEqual (System.Decimal.Round((org_db[0] as SecurityQuote).Quote_Value / (1678.3467m/1234.5678m), 2), (new_db[0] as SecurityQuote).Quote_Value) ;
		}

		
		[Test()]
		public void One_Quote_ReverseSplit_EndShares()
		{
			AddSecurityQuote (149, 1, s1.Id, dt.Year, dt.Month, MPC.Quotes.UpdateTypeEnum.manual);
			org_db = sq_s.GetList();							
			split.PerformSplit (s1.Id, 1678.3467m, 1234.5678m, dt2);
			new_db = sq_s.GetList();				
			Assert.AreEqual (System.Decimal.Round((org_db[0] as SecurityQuote).Quote_Value / (1234.5678m/1678.3467m), 2), (new_db[0] as SecurityQuote).Quote_Value) ;
		}
		
		[Test()]
		public void Two_Quotes_Split()
		{
			AddSecurityQuote (149, 1, s1.Id, dt.Year, dt.Month, MPC.Quotes.UpdateTypeEnum.manual);													
			AddSecurityQuote (169, 1, s1.Id, dt.Year, dt.Month+1, MPC.Quotes.UpdateTypeEnum.manual);													
			org_db = sq_s.GetList();				
			split.PerformSplit (s1.Id, OriginalShares, NewShares, dt2);			
			new_db = sq_s.GetList();				
			Assert.AreEqual (System.Decimal.Round((org_db[0] as SecurityQuote).Quote_Value * (OriginalShares/NewShares), 2), (new_db[0] as SecurityQuote).Quote_Value);						
			Assert.AreEqual ((org_db[1] as SecurityQuote).Quote_Value, (new_db[1] as SecurityQuote).Quote_Value);			
		}

		[Test()]
		public void Two_Quotes_ReverseSplit()
		{
			AddSecurityQuote (149, 1, s1.Id, dt.Year, dt.Month, MPC.Quotes.UpdateTypeEnum.manual);													
			AddSecurityQuote (169, 1, s1.Id, dt.Year, dt.Month+1, MPC.Quotes.UpdateTypeEnum.manual);													
			org_db = sq_s.GetList();							
			split.PerformSplit (s1.Id, NewShares, OriginalShares, dt2);
			new_db = sq_s.GetList();				
			Assert.AreEqual (System.Decimal.Round((org_db[0] as SecurityQuote).Quote_Value * (NewShares/OriginalShares), 2), (new_db[0] as SecurityQuote).Quote_Value);						
			Assert.AreEqual ((org_db[1] as SecurityQuote).Quote_Value, (new_db[1] as SecurityQuote).Quote_Value);						
		}
		
		[Test()]
		public void Multiple_Quotes_Split()
		{
			System.Collections.ArrayList org_db, new_db;
			AddMultipleSet();
			org_db = sq_s.GetList();	
			split.PerformSplit (s1.Id, OriginalShares, NewShares, dt2);
			new_db = sq_s.GetList();				
			Assert.AreEqual (true, SecurityQuoteCalculateSplitResult(
					org_db, new_db, OriginalShares, NewShares, dt2, s1.Id
					) );
		}
		
		[Test()]
		public void Multiple_Quotes_ReverseSplit()
		{
			System.Collections.ArrayList org_db, new_db;
			AddMultipleSet();
			org_db = sq_s.GetList();	
			split.PerformSplit (s1.Id, NewShares, OriginalShares, dt2);
			new_db = sq_s.GetList();				
			Assert.AreEqual (true, SecurityQuoteCalculateSplitResult(
					org_db, new_db, NewShares, OriginalShares, dt2, s1.Id
					) );
		}

		[Test()]
		public void One_InvTran_Split_simple()
		{
			AddTransaction (100, 30, 1, dt.Year, dt.Month, a2.Id, s1.Id);	
			org_db = it_s.GetList();							
			split.PerformSplit (s1.Id, 1m, 2m, dt2);
			new_db = it_s.GetList();	
			Assert.AreEqual (System.Decimal.Round((org_db[0] as InvestmentTransaction).Shares * 2, 4), (new_db[0] as InvestmentTransaction).Shares);
		}
		
		[Test()]
		public void One_InvTran_Split_sum()
		{
			AddTransaction (100, 30, 1, dt.Year, dt.Month, a2.Id, s1.Id);	
			org_db = it_s.GetList();
			decimal before_sum = TotalSum ( (org_db[0] as InvestmentTransaction) );
			split.PerformSplit (s1.Id, 1m, 2m, dt2);
			new_db = it_s.GetList();	
			Assert.AreEqual ( before_sum, TotalSum ( (new_db[0] as InvestmentTransaction) ) );
		}		

		[Test()]
		public void One_InvTran_ReverseSplit_simple()
		{
			AddTransaction (100, 30, 1, dt.Year, dt.Month, a2.Id, s1.Id);
			org_db = it_s.GetList();							
			split.PerformSplit (s1.Id, 2m, 1m, dt2);
			new_db = it_s.GetList();	
			Assert.AreEqual (System.Decimal.Round((org_db[0] as InvestmentTransaction).Shares / 2, 4), (new_db[0] as InvestmentTransaction).Shares);			
		}
		
		[Test()]
		public void One_InvTran_ReverseSplit_sum()
		{
			AddTransaction (100, 30, 1, dt.Year, dt.Month, a2.Id, s1.Id);
			org_db = it_s.GetList();
			decimal before_sum = TotalSum ( (org_db[0] as InvestmentTransaction) );			
			split.PerformSplit (s1.Id, 2m, 1m, dt2);
			new_db = it_s.GetList();	
			Assert.AreEqual ( before_sum, TotalSum ( (new_db[0] as InvestmentTransaction) ) );
		}		

		[Test()]
		public void One_InvTran_Split_float()
		{
			AddTransaction (100, 30, 1, dt.Year, dt.Month, a2.Id, s1.Id);	
			org_db = it_s.GetList();							
			split.PerformSplit (s1.Id, 1.75m, 2.5m, dt2);
			new_db = it_s.GetList();	
			Assert.AreEqual (System.Decimal.Round((org_db[0] as InvestmentTransaction).Shares * (2.5m/1.75m), 4), (new_db[0] as InvestmentTransaction).Shares);			
		}

		
		[Test()]
		public void One_InvTran_ReverseSplit_float()
		{
			AddTransaction (100, 30, 1, dt.Year, dt.Month, a2.Id, s1.Id);
			org_db = it_s.GetList();							
			split.PerformSplit (s1.Id, 2.5m, 1.75m, dt2);
			new_db = it_s.GetList();	
			Assert.AreEqual (System.Decimal.Round((org_db[0] as InvestmentTransaction).Shares * (1.75m/2.5m), 4), (new_db[0] as InvestmentTransaction).Shares);						
		}
		
		[Test()]
		public void One_InvTran_Split_EndShares()
		{
			AddTransaction (100, 30, 1, dt.Year, dt.Month, a2.Id, s1.Id);
			org_db = it_s.GetList();							
			split.PerformSplit (s1.Id, 1234.5678m, 1678.3467m, dt2);
			new_db = it_s.GetList();	
			Assert.AreEqual (System.Decimal.Round((org_db[0] as InvestmentTransaction).Shares * (1678.3467m/1234.5678m), 4), (new_db[0] as InvestmentTransaction).Shares);						
		}

		[Test()]
		public void One_InvTran_ReverseSplit_EndShares()
		{
			AddTransaction (100, 30, 1, dt.Year, dt.Month, a2.Id, s1.Id);
			org_db = it_s.GetList();							
			split.PerformSplit (s1.Id, 1678.3467m, 1234.5678m, dt2);
			new_db = it_s.GetList();	
			Assert.AreEqual (System.Decimal.Round((org_db[0] as InvestmentTransaction).Shares * (1234.5678m/1678.3467m), 4), (new_db[0] as InvestmentTransaction).Shares);						
		}

		[Test()]
		public void Two_InvTran_Split()
		{
			AddTransaction (100, 30, 1, dt.Year, dt.Month, a2.Id, s1.Id);				
			AddTransaction (100, 30, 1, dt.Year, dt.Month+1, a2.Id, s1.Id);				
			org_db = it_s.GetList();				
			split.PerformSplit (s1.Id, OriginalShares, NewShares, dt2);			
			new_db = it_s.GetList();	
			Assert.AreEqual (System.Decimal.Round((org_db[0] as InvestmentTransaction).Shares * (NewShares/OriginalShares), 4), (new_db[0] as InvestmentTransaction).Shares);						
			Assert.AreEqual ((org_db[1] as InvestmentTransaction).Shares, (new_db[2] as InvestmentTransaction).Shares);						
		}

		[Test()]
		public void Two_InvTran_ReverseSplit()
		{
			AddTransaction (100, 30, 1, dt.Year, dt.Month, a2.Id, s1.Id);				
			AddTransaction (100, 30, 1, dt.Year, dt.Month+1, a2.Id, s1.Id);				
			org_db = it_s.GetList();				
			split.PerformSplit (s1.Id, NewShares, OriginalShares, dt2);
			new_db = it_s.GetList();	
			Assert.AreEqual (System.Decimal.Round((org_db[0] as InvestmentTransaction).Shares * (OriginalShares/NewShares), 4), (new_db[0] as InvestmentTransaction).Shares);						
			Assert.AreEqual ((org_db[1] as InvestmentTransaction).Shares, (new_db[2] as InvestmentTransaction).Shares);						
		}
		
		[Test()]
		public void Multiple_InvTran_Split()
		{
			System.Collections.ArrayList org_db, new_db;
			AddMultipleSet();
			org_db = it_s.GetList();	
			split.PerformSplit (s1.Id, OriginalShares, NewShares, dt2);
			new_db = it_s.GetList();				
			Assert.AreEqual (true, InvTranCalculateSplitResult(
					org_db, new_db, OriginalShares, NewShares, dt2, s1.Id
					) );
		}
		
		[Test()]
		public void Multiple_InvTran_ReverseSplit()
		{
			System.Collections.ArrayList org_db, new_db;
			AddMultipleSet();
			org_db = it_s.GetList();	
			split.PerformSplit (s1.Id, NewShares, OriginalShares, dt2);
			new_db = it_s.GetList();				
			Assert.AreEqual (true, InvTranCalculateSplitResult(
					org_db, new_db, NewShares, OriginalShares, dt2, s1.Id
					) );
		}		
		
		[Test()]
		public void Header_Check()
		{
			string slogan = String.Format ("Stock Split. {0} : {1}", OriginalShares, NewShares);
			AddTransaction (100, 30, 1, dt.Year, dt.Month, a1.Id, s1.Id);				
			int first_index = it1.Id;
			AddTransaction (100, 30, 1, dt.Year, dt.Month, a3.Id, s2.Id);	
			AddTransaction (100, 30, 1, dt.Year, dt.Month-1, a2.Id, s1.Id);
			split.PerformSplit (s1.Id, OriginalShares, NewShares, dt2);

			it_tmp = it_s.Get(first_index+3);	
			Assert.AreEqual (a2.Id, it_tmp.InvestmentAccount);						
			Assert.AreEqual (s1.Id, it_tmp.Security_id);
			Assert.AreEqual (dt2, it_tmp.TimeStamp);						
			Assert.AreEqual (slogan, it_tmp.Memo);	
			it_tmp2 = it_s.Get(first_index+4);
			Assert.AreEqual (a1.Id, it_tmp2.InvestmentAccount);	
			Assert.AreEqual (s1.Id, it_tmp2.Security_id);
			Assert.AreEqual (dt2, it_tmp2.TimeStamp);
			Assert.AreEqual (slogan, it_tmp2.Memo);	
		}

		
	}
}
