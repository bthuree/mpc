// NumberOfSharesTest.cs
// 
// Copyright (C) 2008 Bengt Thuree
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;
using NUnit.Framework;

using MPC.Quotes;
using MPC.Accounts;
using MPC;

namespace AccountTest
{
	
	
	[TestFixture()]
	public class NumberOfSharesTest
	{
		
		public SecurityQuote q1, q2, q3, q4, q_tmp, q_tmp2;
		public QuoteSource qs1, qs2, qs3, qs4, qs_tmp, qs_tmp2;		
		public Currency c1, c2, c3, c4, c_tmp, c_tmp2;		
		public Security s1, s2, s3, s4, s_tmp, s_tmp2;		
		public InvestmentTransaction it1, it2, it3, it4, it_tmp, it_tmp2;
		public AddressDetail ad1, ad2, ad3, ad4, ad_tmp, ad_tmp2;
		public ContactDetail cd1, cd2, cd3, cd4, cd_tmp, cd_tmp2;
		public AccountHeader a1, a2, a3, a4, a_tmp, a_tmp2;
		public AccountHeaderStore a_s;		
		public AddressDetailsStore ad_s;
		public ContactDetailsStore cd_s;		
		public SecurityQuoteStore qs;
		public SecurityStore ss;
		public CurrencyStore cs;
		public QuoteSourceStore qss;
		public InvestmentTransactionStore its;
		
		public NumberOfSharesView noss;
		public NumberOfShares nos;

		[TestFixtureSetUp]
		public void OpenDatabaseConnection()
		{
			MPC.Database.SetTestDB();
		}

		[SetUp]
		public void MySetup()
		{

			noss = MPC.Database.GetDatabase.GetNumberOfSharesView;
			
			cs = MPC.Database.GetDatabase.Currencies;
			c1 = cs.Get ("SEK");
			c2 = cs.Get ("USD");
			c3 = cs.Get ("HKD");
			c4 = cs.Get ("AUD");			

			qss = MPC.Database.GetDatabase.QuoteSources;
			qss.Del_All();			
				
			ss = MPC.Database.GetDatabase.Securities;
			ss.Del_All();

			qs = MPC.Database.GetDatabase.SecurityQuotes;
			qs.Del_All();

			its = MPC.Database.GetDatabase.InvestmentTransactions;
			its.Del_All();

			ad_s = MPC.Database.GetDatabase.AddressDetails;
			ad_s.Del_All();
			
			cd_s = MPC.Database.GetDatabase.ContactDetails;
			cd_s.Del_All();
				
			a_s = MPC.Database.GetDatabase.Accounts;
			a_s.Del_All();			
				
			ad1 = new AddressDetail();
			ad1.Swift = "swift";
			ad1.Phone = "Phone";
			ad1.Fax = "Fax";
			ad1.Name = "Name";
			ad1.Address1 = "Address1";
			ad1.Address2 = "Address2";
			ad1.Zip	= "zip";
			ad1.City = "city";
			ad1.Country = "country";
			ad1.Uri = "uri";
			ad1.Notes = "notes";
				
			ad2 = new AddressDetail();
			ad2.Swift = "2swift";
			ad2.Phone = "2Phone";
			ad2.Fax = "2Fax";
			ad2.Name = "2Name";
			ad2.Address1 = "2Address1";
			ad2.Address2 = "2Address2";
			ad2.Zip	= "2zip";
			ad2.City = "2city";
			ad2.Country = "2country";
			ad2.Uri = "2uri";
			ad2.Notes = "2notes";

			ad3 = new AddressDetail();
			ad3.Swift = "3swift";
			ad3.Phone = "3Phone";
			ad3.Fax = "3Fax";
			ad3.Name = "3Name";
			ad3.Address1 = "3Address1";
			ad3.Address2 = "3Address2";
			ad3.Zip	= "3zip";
			ad3.City = "3city";
			ad3.Country = "3country";
			ad3.Uri = "3uri";
			ad3.Notes = "3notes";

			ad4 = new AddressDetail();
			ad4.Swift = "4swift";
			ad4.Phone = "4Phone";
			ad4.Fax = "Fax4";
			ad4.Name = "Name4";
			ad4.Address1 = "4Address1";
			ad4.Address2 = "4Address2";
			ad4.Zip	= "zip4";
			ad4.City = "city4";
			ad4.Country = "4country";
			ad4.Uri = "uri4";
			ad4.Notes = "notes4";	
				
			ad1.Id = ad_s.Add_GetIndex (ad1);
			ad2.Id = ad_s.Add_GetIndex (ad2);
			ad3.Id = ad_s.Add_GetIndex (ad3);
			ad4.Id = ad_s.Add_GetIndex (ad4);	

			cd1 = new ContactDetail();
			cd1.Name = "cdBengt";
			cd1.Phone = "cdPhone";
			cd1.Mobile = "cdMobile";
			cd1.Other_phone = "cdOtherPhone";
			cd1.Fax = "Fax";
			cd1.Work_mail = "cdWorkMail";
			cd1.Private_mail = "cdPrivateMail";
			cd1.Other_mail = "cdOtherMail";
			cd1.Address_id	= ad1.Id;
			cd1.Uri = "cdUri";
			cd1.Notes = "cdNotes";
				
			cd2 = new ContactDetail();
			cd2.Name = "2cdBengt";
			cd2.Phone = "2cdPhone";
			cd2.Mobile = "2cdMobile";
			cd2.Other_phone = "2cdOtherPhone";
			cd2.Fax = "2Fax";
			cd2.Work_mail = "2cdWorkMail";
			cd2.Private_mail = "2cdPrivateMail";
			cd2.Other_mail = "2cdOtherMail";
			cd2.Address_id	= ad2.Id;
			cd2.Uri = "2cdUri";
			cd2.Notes = "2cdNotes";
				
			cd3 = new ContactDetail();
			cd3.Name = "3cdBengt";
			cd3.Phone = "3cdPhone";
			cd3.Mobile = "3cdMobile";
			cd3.Other_phone = "3cdOtherPhone";
			cd3.Fax = "3Fax";
			cd3.Work_mail = "3cdWorkMail";
			cd3.Private_mail = "3cdPrivateMail";
			cd3.Other_mail = "3cdOtherMail";
			cd3.Address_id	= ad3.Id;
			cd3.Uri = "3cdUri";
			cd3.Notes = "3cdNotes";
				
			cd4 = new ContactDetail();
			cd4.Name = "4cdBengt";
			cd4.Phone = "4cdPhone";
			cd4.Mobile = "4cdMobile";
			cd4.Other_phone = "4cdOtherPhone";
			cd4.Fax = "4Fax";
			cd4.Work_mail = "4cdWorkMail";
			cd4.Private_mail = "4cdPrivateMail";
			cd4.Other_mail = "4cdOtherMail";
			cd4.Address_id	= ad4.Id;
			cd4.Uri = "4cdUri";
			cd4.Notes = "4cdNotes";

			cd1.Id = cd_s.Add_GetIndex (cd1);
			cd2.Id = cd_s.Add_GetIndex (cd2);
			cd3.Id = cd_s.Add_GetIndex (cd3);
			cd4.Id = cd_s.Add_GetIndex (cd4);	

			a1 = new AccountHeader();
			a1.Name = "Saving";
			a1.Address_id = ad1.Id;
			a1.Contact_id = cd1.Id;
			a1.Account_nr = "123456789";
			a1.Currency_id = c1.Id;

			a2 = new AccountHeader();
			a2.Name = "Investment";
			a2.Address_id = ad2.Id;
			a2.Contact_id = cd2.Id;
			a2.Account_nr = "123456789";
			a2.Currency_id = c2.Id;

			a3 = new AccountHeader();
			a3.Name = "Play";
			a3.Address_id = ad3.Id;
			a3.Contact_id = cd3.Id;
			a3.Account_nr = "123456789";
			a3.Currency_id = c3.Id;
			
			a4 = new AccountHeader();
			a4.Name = "House";
			a4.Address_id = ad4.Id;
			a4.Contact_id = cd4.Id;
			a4.Account_nr = "123456789";
			a4.Currency_id = c4.Id;
			
			a1.Id = a_s.Add_GetIndex (a1);
			a2.Id = a_s.Add_GetIndex (a2);
			a3.Id = a_s.Add_GetIndex (a3);					
			a4.Id = a_s.Add_GetIndex (a4);
				
			qs1 = new QuoteSource("Yahoo global");
			qs1.Uri = "http://finance.yahoo.com/d/quotes.csv?s=%1&f=sl1d1";
			qs2 = new QuoteSource("Yahoo local");
			qs2.Uri = "http://finance.yahoo.se/d/quotes.csv?s=%1&f=sl1d1";
			qs3 = new QuoteSource("Yahoo Denmark");
			qs3.Uri = "http://finance.yahoo.dk/d/quotes.csv?s=%1&f=sl1d1";
				
			qs1.Id = qss.Add_GetIndex (qs1);
			qs2.Id = qss.Add_GetIndex (qs2);
			qs3.Id = qss.Add_GetIndex (qs3);								
				
			s1 = new Security("ERIC");
			s1.Name = "Ericsson";
			s1.Currency = c1.Id;
			s1.Quote_Source = qs1.Id;

			s2 = new Security("MSFT");
			s2.Name = "Microsoft";
			s2.Currency = c2.Id;
			s2.Quote_Source = qs2.Id;

			s3 = new Security("SEB Europa Fond");
			s3.Name = "SEB Europa Fond";
			s3.Currency = c3.Id;
			s3.Quote_Source = qs3.Id;

			s4 = new Security("AZN");
			s4.Name = "Amazon";
			s4.Currency = c3.Id;
			s4.Quote_Source = qs3.Id;

			s1.Id = ss.Add_GetIndex (s1);
			s2.Id = ss.Add_GetIndex (s2);
			s3.Id = ss.Add_GetIndex (s3);
			s4.Id = ss.Add_GetIndex (s4);

			q1 = new SecurityQuote();
			q1.Symbol = s1.Id;
			q1.TimeStamp = new DateTime (2008, 12, 24, 13, 30, 0);
			q1.Quote_Value = 100;
			q1.Quote_Type = MPC.Quotes.UpdateTypeEnum.manual;

			q2 = new SecurityQuote();
			q2.Symbol = s1.Id;
			q2.TimeStamp = new DateTime (2008, 12, 24, 14, 30, 0);
			q2.Quote_Value = 110.00m;
			q2.Quote_Type = MPC.Quotes.UpdateTypeEnum.manual;

			q3 = new SecurityQuote();
			q3.Symbol = s1.Id;
			q3.TimeStamp = new DateTime (2008, 12, 26, 18, 30, 0);
			q3.Quote_Value = 120.00m;
			q3.Quote_Type = MPC.Quotes.UpdateTypeEnum.manual;

			q4 = new SecurityQuote();
			q4.Symbol = s2.Id;
			q4.TimeStamp = new DateTime (2008, 12, 27, 19, 30, 0);
			q4.Quote_Value = 12000.00m;
			q4.Quote_Type = MPC.Quotes.UpdateTypeEnum.manual;

			q4 = new SecurityQuote();
			q4.Symbol = s2.Id;
			q4.TimeStamp = new DateTime (2008, 12, 27, 19, 30, 0);
			q4.Quote_Value = 12000.00m;
			q4.Quote_Type = MPC.Quotes.UpdateTypeEnum.manual;
				
			it1 = new InvestmentTransaction();
			it1.InvestmentAccount = a1.Id;
			it1.TimeStamp = System.DateTime.MinValue;
			it1.Memo = "it1";
			it1.Security_id = s1.Id;
			it1.Shares = 100;
			it1.Share_price = 0;
			it1.Reconsiled = false;	
				
			it2 = new InvestmentTransaction();
			it2.InvestmentAccount = a3.Id;
			it2.TimeStamp = System.DateTime.MinValue;
			it2.Memo = "it1";
			it2.Security_id = s1.Id;
			it2.Shares = 200;
			it2.Share_price = 0;
			it2.Reconsiled = false;			
				
			it3 = new InvestmentTransaction();
			it3.InvestmentAccount = a3.Id;
			it3.TimeStamp = System.DateTime.MinValue;
			it3.Memo = "it1";
			it3.Security_id = s1.Id;
			it3.Shares = -200;
			it3.Share_price = 0;
			it3.Reconsiled = false;			

			it4 = new InvestmentTransaction();
			it4.InvestmentAccount = a2.Id;
			it4.TimeStamp = System.DateTime.MinValue;
			it4.Memo = "it1";
			it4.Security_id = s3.Id;
			it4.Shares = 400;
			it4.Share_price = 0;
			it4.Reconsiled = false;						
		}

		[TestFixtureTearDown]
		public void CloseDatabaseConnection()
		{
		}

		private void AddTransaction (int shares, int times, int year, int month, int account, int security)
		{
			for (int k=0; k < times; k++)
			{
				it1 = new InvestmentTransaction();
				it1.InvestmentAccount = account;
				it1.TimeStamp = new DateTime (year, month, 10+k, 12, 30, 0);
				it1.Memo = String.Format ("Action {0}", k);
				it1.Security_id = security;
				it1.Shares = shares;
				its.Add (it1);
			}
		}
		
		private void AddMultipleSet()
		{
			AddTransaction (30, 1, 2006, 10, a2.Id, s1.Id);				
			AddTransaction (20, 2, 2006, 10, a1.Id, s1.Id);
			AddTransaction (30, 1, 2006, 11, a2.Id, s1.Id);	
			AddTransaction (35, 1, 2006, 12, a2.Id, s1.Id);	
			AddTransaction (35, 1, 2007,  1, a2.Id, s1.Id);	
			AddTransaction (35, 1, 2007,  2, a2.Id, s1.Id);
			AddTransaction (35, 1, 2007,  3, a2.Id, s1.Id);	
			AddTransaction (35, 1, 2007,  4, a2.Id, s1.Id);	
			AddTransaction (35, 1, 2007,  5, a2.Id, s1.Id);				
			AddTransaction (500, 1, 2007, 4, a1.Id, s2.Id);	
			AddTransaction (400, 1, 2007, 4, a1.Id, s3.Id);	
			AddTransaction (300, 1, 2007, 4, a1.Id, s1.Id);	
			AddTransaction (300, 1, 2007, 4, a3.Id, s3.Id);				
			AddTransaction (35, 1, 2006, 12, a1.Id, s1.Id);	
			AddTransaction (-15, 2, 2007, 12, a1.Id, s3.Id);	
			AddTransaction (-10, 2, 2007, 1, a1.Id, s1.Id);						
			AddTransaction (-50, 3, 2007, 5, a3.Id, s3.Id);						
			AddTransaction (-40, 4, 2007, 5, a1.Id, s1.Id);						
		}
		
		[Test()]
		public void Add_1_set()
		{
			AddTransaction (20, 1, 2006, 10, a1.Id, s1.Id);
			Assert.AreEqual (20, noss.CountShares(a1.Id, s1.Id));
		}
		
		[Test()]
		public void Add_1_set_CountWrongSec()
		{
			AddTransaction (20, 1, 2006, 10, a1.Id, s1.Id);
			Assert.AreEqual (0, noss.CountShares (a1.Id, s2.Id));		
		}		
		
		[Test()]
		public void Add_1_set_CountWrongAccount()
		{
			AddTransaction (20, 1, 2006, 10, a1.Id, s1.Id);
			Assert.AreEqual (0, noss.CountShares (a2.Id, s1.Id));		
		}		

		[Test()]
		public void Add_1_set_CountWrongDate()
		{
			DateTime dt = new DateTime (2006, 1, 1, 12, 30, 0);			
			AddTransaction (20, 1, 2006, 10, a1.Id, s1.Id);
			Assert.AreEqual (0, noss.CountShares (a2.Id, s1.Id, dt));		
		}					
		
		[Test()]
		public void Add_1_set_CountCorrectDate()
		{
			DateTime dt = new DateTime (2006, 10, 20, 12, 30, 0);			
			AddTransaction (20, 1, 2006, 10, a1.Id, s1.Id);
			Assert.AreEqual (20, noss.CountShares (a1.Id, s1.Id, dt));		
		}					
		
		[Test()]
		public void Add_5_set()
		{
			AddTransaction (20, 5, 2006, 10, a1.Id, s1.Id);
			Assert.AreEqual (100, noss.CountShares (a1.Id, s1.Id));		
		}	
		
		[Test()]
		public void Add_Multiple_set_CountSecurity()
		{
			AddMultipleSet();
			Assert.AreEqual (465, noss.CountShares (s1.Id));	
		}				
		
		[Test()]
		public void Add_Multiple_set_CountAccount()
		{
			AddMultipleSet();
			Assert.AreEqual (195, noss.CountShares (a1.Id, s1.Id));		
			Assert.AreEqual (270, noss.CountShares (a2.Id, s1.Id));
		}				
		
		[Test()]
		public void Add_Multiple_set_CountAccountDate()
		{
			AddMultipleSet();
			DateTime dt = new DateTime (2006, 12, 20, 12, 30, 0);						
			Assert.AreEqual (75, noss.CountShares (a1.Id, s1.Id, dt));	
			Assert.AreEqual (95, noss.CountShares (a2.Id, s1.Id, dt));	
		}				
		
		[Test()]
		public void Add_Multiple_set_CountSecurityDate()
		{
			AddMultipleSet();
			DateTime dt = new DateTime (2006, 12, 20, 12, 30, 0);
System.Console.WriteLine ("{0}", noss.CountShares (s1.Id, dt));
System.Console.WriteLine ("{0}", noss.CountShares (s2.Id, dt));
System.Console.WriteLine ("{0}", noss.CountShares (s3.Id, dt));
			Assert.AreEqual (170, noss.CountShares (s1.Id, dt));	
			Assert.AreEqual (0, noss.CountShares (s2.Id, dt));
			Assert.AreEqual (0, noss.CountShares (s3.Id, dt));				
		}
		
		[Test()]
		public void GetList()
		{
			AddMultipleSet();
			System.Collections.ArrayList nos_db = noss.GetList();
			Assert.AreEqual (5, nos_db.Count);
		}

		[Test()]
		public void GetList_AtDate()
		{
			AddMultipleSet();
			DateTime dt = new DateTime (2006, 12, 20, 12, 30, 0);									
			System.Collections.ArrayList nos_db = noss.GetList(dt);
			Assert.AreEqual (2, nos_db.Count);
			Assert.AreEqual (95m, (nos_db[0] as NumberOfShares).NumberShares);
			Assert.AreEqual (75m, (nos_db[1] as NumberOfShares).NumberShares);			
		}

		[Test()]
		[Ignore("WEIRD2")]				
		public void GetList_Security()
		{
			AddMultipleSet();
			DateTime dt = new DateTime (2006, 12, 20, 12, 30, 0);									
			System.Collections.ArrayList nos_db = noss.GetList(dt);
			Assert.AreEqual (2, nos_db.Count);
			Assert.AreEqual (95m, (nos_db[0] as NumberOfShares).NumberShares);
			Assert.AreEqual (75m, (nos_db[1] as NumberOfShares).NumberShares);			
		}

		[Test()]
		[Ignore("WEIRD2")]				
		public void GetList_SecurityDate()
		{
			AddMultipleSet();
			DateTime dt = new DateTime (2006, 12, 20, 12, 30, 0);									
			System.Collections.ArrayList nos_db = noss.GetList(dt);
			Assert.AreEqual (2, nos_db.Count);
			Assert.AreEqual (95m, (nos_db[0] as NumberOfShares).NumberShares);
			Assert.AreEqual (75m, (nos_db[1] as NumberOfShares).NumberShares);			
		}		
		
		[Test()]
	[Ignore("WEIRD2")]				
		public void GetList_Account()
		{
			AddMultipleSet();
			DateTime dt = new DateTime (2006, 12, 20, 12, 30, 0);									
			System.Collections.ArrayList nos_db = noss.GetList(dt);
			Assert.AreEqual (2, nos_db.Count);
			Assert.AreEqual (95m, (nos_db[0] as NumberOfShares).NumberShares);
			Assert.AreEqual (75m, (nos_db[1] as NumberOfShares).NumberShares);			
		}		

		[Test()]
	[Ignore("WEIRD")]				
		public void GetList_AccountDate()
		{
			AddMultipleSet();
			DateTime dt = new DateTime (2006, 12, 20, 12, 30, 0);									
			System.Collections.ArrayList nos_db = noss.GetList(dt);
			Assert.AreEqual (2, nos_db.Count);
			Assert.AreEqual (95m, (nos_db[0] as NumberOfShares).NumberShares);
			Assert.AreEqual (75m, (nos_db[1] as NumberOfShares).NumberShares);			
		}			
		
		[Test()]
		public void GetList_RemoveEmptyNumbers()
		{
			AddTransaction (20, 2, 2006, 10, a1.Id, s1.Id);
			AddTransaction (20, 2, 2006, 10, a1.Id, s2.Id);		
			AddTransaction (20, 2, 2006, 10, a2.Id, s1.Id);
			AddTransaction (20, 2, 2006, 10, a1.Id, s3.Id);
			AddTransaction (-20, 2, 2006, 12, a1.Id, s1.Id);
			
			System.Collections.ArrayList nos_db = noss.GetList();
			Assert.AreEqual (3, nos_db.Count);
			
			DateTime dt = new DateTime (2006, 11, 20, 12, 30, 0);									
			System.Collections.ArrayList nos_db2 = noss.GetList(dt);
			Assert.AreEqual (4, nos_db2.Count);		
		}	
		
	}
}
