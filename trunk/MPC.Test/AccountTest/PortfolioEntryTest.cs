// PortfolioTest.cs
// 
// Copyright (C) 2008 Bengt Thuree
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;
using NUnit.Framework;

using MPC.Accounts;

namespace AccountTest
{
	
	
	[TestFixture()]
	public class PortfolioEntryTest
	{
		public PortfolioEntry p1;

		[TestFixtureSetUp]
		public void OpenDatabaseConnection()
		{
		}

		[SetUp]
		public void MySetup()
		{		
		}

		[TestFixtureTearDown]
		public void CloseDatabaseConnection()
		{
		}

		[Test()]
		public void Gav_0_shares()
		{
			p1 = new PortfolioEntry();
			p1.NumberShares = 0;
			p1.TotalCost = 1000;
			Assert.AreEqual (System.Decimal.Round(0, 2), p1.AverageCost );
		}	
		
		[Test()]
		public void Gav()
		{
			p1 = new PortfolioEntry();
			p1.NumberShares = 2000;
			p1.TotalCost = 65000;
			Assert.AreEqual (System.Decimal.Round(65000m/2000m, 2), p1.AverageCost );
		}	

		[Test()]
		public void Value()
		{
			p1 = new PortfolioEntry();
			p1.QuoteValue = 20;
			p1.NumberShares = 2000;
			Assert.AreEqual (System.Decimal.Round(40000, 2), p1.Value );
		}	

		[Test()]
		public void Value_QuoteIsPercent()
		{
			p1 = new PortfolioEntry();
			p1.QuoteValue = 80;
			p1.NumberShares = 2000;
			p1.QuoteIsPercentage = true;
			Assert.AreEqual (System.Decimal.Round(1600, 2), p1.Value );
		}			
		
		[Test()]
		public void BaseValue()
		{
			p1 = new PortfolioEntry();
			p1.QuoteValue = 20;
			p1.NumberShares = 2000;
			p1.ExchangeRateToBase = 10;			
			Assert.AreEqual (System.Decimal.Round(400000, 2), p1.BaseValue );
		}			
		
		[Test()]
		public void ProfitLoss()
		{
			p1 = new PortfolioEntry();
			p1.QuoteValue = 20;
			p1.NumberShares = 2000;
			p1.TotalIncome = 1000;
			p1.TotalCost = 35550;
			Assert.AreEqual (System.Decimal.Round((40000+1000)-35550, 2), p1.ProfitLoss );
		}					

		[Test()]
		public void ProfitLoss_Percent_0cost()
		{
			p1 = new PortfolioEntry();
			p1.QuoteValue = 20;
			p1.NumberShares = 2000;
			p1.TotalIncome = 1000;
			p1.TotalCost = 0;
			Assert.AreEqual (100, p1.ProfitLossPercent );
		}		
		
		[Test()]
		public void ProfitLoss_Percent()
		{
			p1 = new PortfolioEntry();
			p1.QuoteValue = 20;
			p1.NumberShares = 2000;
			p1.TotalIncome = 1000;
			p1.TotalCost = 35550;
			Assert.AreEqual (System.Decimal.Round(100*(((40000m+1000m)-35550m)/35550m), 2), p1.ProfitLossPercent );
		}							
	}
}
