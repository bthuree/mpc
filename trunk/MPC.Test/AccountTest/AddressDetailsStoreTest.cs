// AddressDetailsStoreTest.cs
// 
// Copyright (C) 2008 Bengt Thuree
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

//FIXME Need to clear all data in Setup, also need to tie in with data to SecurityStore



using System;
using System.IO;
using NUnit.Framework;

using MPC;
using MPC.Quotes;
using MPC.Accounts;
using MPC.Utils;
using System.Data;

namespace AccountTest
{
	
	
[TestFixture()]
public class AddressDetailsStoreTest
{ 
	public AddressDetail ad1, ad2, ad3, ad4, ad_tmp, ad_tmp2;
	public AddressDetailsStore ad_s;
	
	int first_index;			

	[TestFixtureSetUp]
	public void OpenDatabaseConnection()
	{
		MPC.Database.SetTestDB();
	}

	[SetUp]
	public void MySetup()
	{
		ad_s = MPC.Database.GetDatabase.AddressDetails;
		ad_s.Del_All();
					
		ad1 = new AddressDetail();
		ad1.Swift = "swift";
		ad1.Phone = "Phone";
		ad1.Fax = "Fax";
		ad1.Name = "Name";
		ad1.Address1 = "Address1";
		ad1.Address2 = "Address2";
		ad1.Zip	= "zip";
		ad1.City = "city";
		ad1.Country = "country";
		ad1.Uri = "uri";
		ad1.Notes = "notes";

		ad2 = new AddressDetail();
		ad2.Swift = "2swift";
		ad2.Phone = "2Phone";
		ad2.Fax = "2Fax";
		ad2.Name = "2Name";
		ad2.Address1 = "2Address1";
		ad2.Address2 = "2Address2";
		ad2.Zip	= "2zip";
		ad2.City = "2city";
		ad2.Country = "2country";
		ad2.Uri = "2uri";
		ad2.Notes = "2notes";

		ad3 = new AddressDetail();
		ad3.Swift = "3swift";
		ad3.Phone = "3Phone";
		ad3.Fax = "3Fax";
		ad3.Name = "3Name";
		ad3.Address1 = "3Address1";
		ad3.Address2 = "3Address2";
		ad3.Zip	= "3zip";
		ad3.City = "3city";
		ad3.Country = "3country";
		ad3.Uri = "3uri";
		ad3.Notes = "3notes";

		ad4 = new AddressDetail();
		ad4.Swift = "4swift";
		ad4.Phone = "4Phone";
		ad4.Fax = "Fax4";
		ad4.Name = "Name4";
		ad4.Address1 = "4Address1";
		ad4.Address2 = "4Address2";
		ad4.Zip	= "zip4";
		ad4.City = "city4";
		ad4.Country = "4country";
		ad4.Uri = "uri4";
		ad4.Notes = "notes4";
			
		
	}		
		
	[TestFixtureTearDown]
	public void CloseDatabaseConnection()
	{
	}
	
	[Test()]
	public void Add()
	{
		Assert.AreEqual (0, ad_s.Count() );					
		ad_s.Add (ad1);
		Assert.AreEqual (1, ad_s.Count() );		
	}

	[Test()]
	public void CheckParameters()
	{
		ad_s.Add (ad1);
		ad_tmp = ad_s.Get (ad1.Name);			
		Assert.AreEqual (ad1.Swift, ad_tmp.Swift);
		Assert.AreEqual (ad1.Phone, ad_tmp.Phone);
		Assert.AreEqual (ad1.Fax, ad_tmp.Fax);
		Assert.AreEqual (ad1.Name, ad_tmp.Name);
		Assert.AreEqual (ad1.Address1, ad_tmp.Address1);
		Assert.AreEqual (ad1.Address2, ad_tmp.Address2);			
		Assert.AreEqual (ad1.Zip, ad_tmp.Zip);
		Assert.AreEqual (ad1.City, ad_tmp.City);
		Assert.AreEqual (ad1.Country, ad_tmp.Country);
		Assert.AreEqual (ad1.Uri, ad_tmp.Uri);
		Assert.AreEqual (ad1.Notes, ad_tmp.Notes);
	}		
		
	[Test()]
	//[ExpectedException("MPC.Utils.NotUniqueException")]	
	[Ignore("WEIRD")]				
	public void Add_SameAccount()
	{
		ad_s.Add (ad1);
		ad_s.Add (ad1);			
	}
		
	[Test()]
	public void Del_Id()
	{
		ad_s.Add (ad1);
		ad_s.Add (ad2);	
		ad_s.Add (ad3);			
		Assert.AreEqual (3, ad_s.Count() );			
		ad_tmp = ad_s.Get (ad2.Name);
		ad_s.Del (ad_tmp.Id);
		Assert.AreEqual (2, ad_s.Count() );
		ad_tmp = ad_s.Get (ad1.Name);
		Assert.AreEqual (ad1.Name, ad_tmp.Name);			
		ad_tmp = ad_s.Get (ad3.Name);	
		Assert.AreEqual (ad3.Name, ad_tmp.Name);						
	}	
		
	[Test()]
	public void Del_Object()
	{
		ad_s.Add (ad1);
		ad_tmp = ad_s.Get (ad1.Name);			
		ad_s.Del (ad_tmp);
		Assert.AreEqual (0, ad_s.Count() );		
	}
		
	[Test()]
	public void Del_All()
	{
		ad_s.Add (ad1);
		ad_s.Add (ad2);
		ad_s.Add (ad3);		
		ad_s.Del_All();			
		Assert.AreEqual  ( 0, ad_s.Count() );
	}		

	[Test()]
	public void Change_Object()
	{
		ad_s.Add (ad1);
		ad_tmp = ad_s.Get (ad1.Name);
		ad_tmp.Name = ad2.Name;
		ad_tmp.Swift = ad2.Swift;
		ad_s.Change (ad_tmp);
		ad_tmp2 = ad_s.Get (ad_tmp);
		Assert.AreEqual (ad2.Swift, ad_tmp2.Swift);	
		Assert.AreEqual (ad2.Name, ad_tmp2.Name);									
	}		

	[Test()]
	public void Get_Id()
	{
		ad_s.Add (ad1);
		ad_tmp = ad_s.Get (ad1.Name);
		first_index = ad_tmp.Id;
		ad_tmp2 = ad_s.Get (first_index);
		Assert.AreEqual (ad1.Swift, ad_tmp2.Swift);	
		Assert.AreEqual (ad1.Name, ad_tmp2.Name);									
	}				

	[Test()]
	public void Get_Name()
	{
		ad_s.Add (ad1);
		ad_tmp = ad_s.Get (ad1.Name);
		Assert.AreEqual (ad1.Swift, ad_tmp.Swift);	
		Assert.AreEqual (ad1.Name, ad_tmp.Name);									
	}	

	[Test()]
	public void CountTest()
	{
		Assert.AreEqual  ( 0, ad_s.Count() );
		ad_s.Add (ad1);
		ad1 = ad_s.Get (ad1.Name);
		Assert.AreEqual  ( 1, ad_s.Count() );
		ad_s.Add (ad2);
		ad2 = ad_s.Get (ad2.Name);		
		Assert.AreEqual  ( 2, ad_s.Count() );			
		ad_s.Add (ad3);
		ad3 = ad_s.Get (ad3.Name);		
		Assert.AreEqual  ( 3, ad_s.Count() );	
			
		ad_s.Del (ad1);			
		Assert.AreEqual  ( 2, ad_s.Count() );			
		ad_s.Del (ad2);
		Assert.AreEqual  ( 1, ad_s.Count() );
		ad_s.Del (ad3);
		Assert.AreEqual  ( 0, ad_s.Count() );									
	}		
	
	[Test()]
	public void GetList_Counts()
	{
		Assert.AreEqual (0, (ad_s.GetList()).Count);
		ad_s.Add (ad1) ;
		ad_s.Add (ad2);
		ad_s.Add (ad3);			
		ad1 = ad_s.Get (ad1.Name);				
		ad2 = ad_s.Get (ad2.Name);				
		ad3 = ad_s.Get (ad3.Name);				
		Assert.AreEqual (3, (ad_s.GetList()).Count);
		ad_s.Del (ad1);
		Assert.AreEqual (2, (ad_s.GetList()).Count);			
		ad_s.Del (ad3);			
		ad_s.Del (ad2);						
		Assert.AreEqual (0, (ad_s.GetList()).Count);
	}	
	
	[Test()]
	public void GetList_Contents()
	{
		System.Collections.ArrayList a_db;
		ad_s.Add (ad1) ;
		ad_s.Add (ad2);	
		ad_s.Add (ad3);	
		ad1 = ad_s.Get (ad1.Name);				
		ad2 = ad_s.Get (ad2.Name);				
		ad3 = ad_s.Get (ad3.Name);						
		a_db = ad_s.GetList();
		Assert.AreEqual ( a_db.Count, 3);
		Assert.AreEqual ( (a_db[0] as AddressDetail).Name, ad2.Name);
		Assert.AreEqual ( (a_db[1] as AddressDetail).Name, ad3.Name);
		Assert.AreEqual ( (a_db[2] as AddressDetail).Name, ad1.Name);		
		ad_s.Del (ad1.Name);
		a_db = ad_s.GetList();
		Assert.AreEqual ( a_db.Count, 2);			
		Assert.AreEqual ( (a_db[0] as AddressDetail).Name, ad2.Name);
		Assert.AreEqual ( (a_db[1] as AddressDetail).Name, ad3.Name);		
	}	

	[Test()]
	public void UniqueIndex()
	{
		ad_s.Add (ad1) ;
		ad_tmp = ad_s.Get (ad1.Name);
		first_index = ad_tmp.Id;															
		ad_tmp= ad_s.Get(first_index);
		Assert.AreEqual (first_index, ad_tmp.Id);			
		ad_s.Add (ad2);
		ad_tmp= ad_s.Get(first_index+1);
		Assert.AreEqual (first_index+1, ad_tmp.Id);	
		ad_s.Add (ad3);
		ad_tmp= ad_s.Get(first_index+2);
		Assert.AreEqual (first_index+2, ad_tmp.Id);	
		ad_s.Del (first_index+1);
		ad_s.Add (ad4);
		ad_tmp= ad_s.Get(first_index+3);
		Assert.AreEqual (first_index+3, ad_tmp.Id);				
	}		

}
}