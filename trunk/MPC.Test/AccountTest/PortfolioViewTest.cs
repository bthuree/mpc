// PortfolioViewTest.cs
// 
// Copyright (C) 2008 Bengt Thuree
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//


using System;
using NUnit.Framework;

using MPC.Quotes;
using MPC.Accounts;
using MPC;

namespace AccountTest
{
	
	
	[TestFixture()]
	public class PortfolioViewTest
	{
		
		public SecurityQuote sq1, sq2, sq3, sq4, sq_tmp, sq_tmp2;
		public ExchangeRate er1, er2, er3, er4, er_tmp, er_tmp2;		
		public QuoteSource qs1, qs2, qs3, qs4, qs_tmp, qs_tmp2;		
		public Currency c1, c2, c3, c4, c_tmp, c_tmp2;		
		public Security s1, s2, s3, s4, s_tmp, s_tmp2;		
		public InvestmentTransaction it1, it2, it3, it4, it_tmp, it_tmp2;
		public AddressDetail ad1, ad2, ad3, ad4, ad_tmp, ad_tmp2;
		public ContactDetail cd1, cd2, cd3, cd4, cd_tmp, cd_tmp2;
		public AccountHeader a1, a2, a3, a4, a_tmp, a_tmp2;
		public PortfolioEntry p1, p2, p3, p4, p_tmp, p_tmp2;
		public AccountHeaderStore a_s;		
		public AddressDetailsStore ad_s;
		public ContactDetailsStore cd_s;		
		public SecurityQuoteStore sq_s;
		public SecurityStore s_s;
		public CurrencyStore c_s;
		public ExchangeRateStore er_s;
		public QuoteSourceStore qs_s;
		public InvestmentTransactionStore it_s;
		public NumberOfSharesView nos_v;		
		public PortfolioView p_v;
		
		public DateTime dt, dt2;
		public decimal OriginalShares, NewShares;
		public decimal SplitRatio;

		[TestFixtureSetUp]
		public void OpenDatabaseConnection()
		{
			MPC.Database.SetTestDB();
		}

		[SetUp]
		public void MySetup()
		{		
			dt = new DateTime (2007, 3, 2, 0, 0, 0);
			dt2 = new DateTime (2007,1,3,0,0,0);			
						
			c_s = MPC.Database.GetDatabase.Currencies;
			c1 = c_s.Get ("SEK");
			c2 = c_s.Get ("USD");
			c3 = c_s.Get ("HKD");
			c4 = c_s.Get ("AUD");	
			
			qs_s = MPC.Database.GetDatabase.QuoteSources;
			qs_s.Del_All();			
				
			s_s = MPC.Database.GetDatabase.Securities;
			s_s.Del_All();

			sq_s = MPC.Database.GetDatabase.SecurityQuotes;
			sq_s.Del_All();

			it_s = MPC.Database.GetDatabase.InvestmentTransactions;
			it_s.Del_All();

			ad_s = MPC.Database.GetDatabase.AddressDetails;
			ad_s.Del_All();
			
			cd_s = MPC.Database.GetDatabase.ContactDetails;
			cd_s.Del_All();
				
			a_s = MPC.Database.GetDatabase.Accounts;
			a_s.Del_All();	
			
			er_s = MPC.Database.GetDatabase.ExchangeRates;
			er_s.Del_All();
			
			er1 = new ExchangeRate();
			er1.TimeStamp = dt2;
			er1.FromCurrency = c1.Id;
			er1.ToCurrency = c4.Id;
			er1.ExchangeRateValue = 2;
			er1.Quote_Type = MPC.Quotes.UpdateTypeEnum.manual;
			er1.Id = er_s.Add_GetIndex (er1);

			er2 = new ExchangeRate();
			er2.FromCurrency = c2.Id;
			er2.ToCurrency = c4.Id;
			er2.TimeStamp = dt2;
			er2.ExchangeRateValue = 3;
			er2.Quote_Type = MPC.Quotes.UpdateTypeEnum.online;
			er2.Id = er_s.Add_GetIndex (er2);

			er3 = new ExchangeRate();
			er3.FromCurrency = c3.Id;
			er3.ToCurrency = c4.Id;
			er3.TimeStamp = dt2;
			er3.ExchangeRateValue = 4;
			er3.Quote_Type = MPC.Quotes.UpdateTypeEnum.online;
			er3.Id = er_s.Add_GetIndex (er3);

			nos_v = MPC.Database.GetDatabase.GetNumberOfSharesView;			

			p_v = new PortfolioView (sq_s, s_s, it_s, nos_v, c_s, er_s);
				
			ad1 = new AddressDetail();
			ad1.Swift = "swift";
			ad1.Phone = "Phone";
			ad1.Fax = "Fax";
			ad1.Name = "Name";
			ad1.Address1 = "Address1";
			ad1.Address2 = "Address2";
			ad1.Zip	= "zip";
			ad1.City = "city";
			ad1.Country = "country";
			ad1.Uri = "uri";
			ad1.Notes = "notes";
				
			ad2 = new AddressDetail();
			ad2.Swift = "2swift";
			ad2.Phone = "2Phone";
			ad2.Fax = "2Fax";
			ad2.Name = "2Name";
			ad2.Address1 = "2Address1";
			ad2.Address2 = "2Address2";
			ad2.Zip	= "2zip";
			ad2.City = "2city";
			ad2.Country = "2country";
			ad2.Uri = "2uri";
			ad2.Notes = "2notes";

			ad3 = new AddressDetail();
			ad3.Swift = "3swift";
			ad3.Phone = "3Phone";
			ad3.Fax = "3Fax";
			ad3.Name = "3Name";
			ad3.Address1 = "3Address1";
			ad3.Address2 = "3Address2";
			ad3.Zip	= "3zip";
			ad3.City = "3city";
			ad3.Country = "3country";
			ad3.Uri = "3uri";
			ad3.Notes = "3notes";

			ad4 = new AddressDetail();
			ad4.Swift = "4swift";
			ad4.Phone = "4Phone";
			ad4.Fax = "Fax4";
			ad4.Name = "Name4";
			ad4.Address1 = "4Address1";
			ad4.Address2 = "4Address2";
			ad4.Zip	= "zip4";
			ad4.City = "city4";
			ad4.Country = "4country";
			ad4.Uri = "uri4";
			ad4.Notes = "notes4";	
				
			ad1.Id = ad_s.Add_GetIndex (ad1);
			ad2.Id = ad_s.Add_GetIndex (ad2);
			ad3.Id = ad_s.Add_GetIndex (ad3);
			ad4.Id = ad_s.Add_GetIndex (ad4);	

			cd1 = new ContactDetail();
			cd1.Name = "cdBengt";
			cd1.Phone = "cdPhone";
			cd1.Mobile = "cdMobile";
			cd1.Other_phone = "cdOtherPhone";
			cd1.Fax = "Fax";
			cd1.Work_mail = "cdWorkMail";
			cd1.Private_mail = "cdPrivateMail";
			cd1.Other_mail = "cdOtherMail";
			cd1.Address_id	= ad1.Id;
			cd1.Uri = "cdUri";
			cd1.Notes = "cdNotes";
				
			cd2 = new ContactDetail();
			cd2.Name = "2cdBengt";
			cd2.Phone = "2cdPhone";
			cd2.Mobile = "2cdMobile";
			cd2.Other_phone = "2cdOtherPhone";
			cd2.Fax = "2Fax";
			cd2.Work_mail = "2cdWorkMail";
			cd2.Private_mail = "2cdPrivateMail";
			cd2.Other_mail = "2cdOtherMail";
			cd2.Address_id	= ad2.Id;
			cd2.Uri = "2cdUri";
			cd2.Notes = "2cdNotes";
				
			cd3 = new ContactDetail();
			cd3.Name = "3cdBengt";
			cd3.Phone = "3cdPhone";
			cd3.Mobile = "3cdMobile";
			cd3.Other_phone = "3cdOtherPhone";
			cd3.Fax = "3Fax";
			cd3.Work_mail = "3cdWorkMail";
			cd3.Private_mail = "3cdPrivateMail";
			cd3.Other_mail = "3cdOtherMail";
			cd3.Address_id	= ad3.Id;
			cd3.Uri = "3cdUri";
			cd3.Notes = "3cdNotes";
				
			cd4 = new ContactDetail();
			cd4.Name = "4cdBengt";
			cd4.Phone = "4cdPhone";
			cd4.Mobile = "4cdMobile";
			cd4.Other_phone = "4cdOtherPhone";
			cd4.Fax = "4Fax";
			cd4.Work_mail = "4cdWorkMail";
			cd4.Private_mail = "4cdPrivateMail";
			cd4.Other_mail = "4cdOtherMail";
			cd4.Address_id	= ad4.Id;
			cd4.Uri = "4cdUri";
			cd4.Notes = "4cdNotes";

			cd1.Id = cd_s.Add_GetIndex (cd1);
			cd2.Id = cd_s.Add_GetIndex (cd2);
			cd3.Id = cd_s.Add_GetIndex (cd3);
			cd4.Id = cd_s.Add_GetIndex (cd4);	

			a1 = new AccountHeader();
			a1.Name = "Saving";
			a1.Address_id = ad1.Id;
			a1.Contact_id = cd1.Id;
			a1.Account_nr = "123456789";
			a1.Currency_id = c1.Id;

			a2 = new AccountHeader();
			a2.Name = "Investment";
			a2.Address_id = ad2.Id;
			a2.Contact_id = cd2.Id;
			a2.Account_nr = "123456789";
			a2.Currency_id = c2.Id;

			a3 = new AccountHeader();
			a3.Name = "Play";
			a3.Address_id = ad3.Id;
			a3.Contact_id = cd3.Id;
			a3.Account_nr = "123456789";
			a3.Currency_id = c3.Id;

			a4 = new AccountHeader();
			a4.Name = "House";
			a4.Address_id = ad4.Id;
			a4.Contact_id = cd4.Id;
			a4.Account_nr = "123456789";
			a4.Currency_id = c4.Id;

			a1.Id = a_s.Add_GetIndex (a1);
			a2.Id = a_s.Add_GetIndex (a2);
			a3.Id = a_s.Add_GetIndex (a3);					
			a4.Id = a_s.Add_GetIndex (a4);
				
			qs1 = new QuoteSource("Yahoo global");
			qs1.Uri = "http://finance.yahoo.com/d/quotes.csv?s=%1&f=sl1d1";
			qs2 = new QuoteSource("Yahoo local");
			qs2.Uri = "http://finance.yahoo.se/d/quotes.csv?s=%1&f=sl1d1";
			qs3 = new QuoteSource("Yahoo Denmark");
			qs3.Uri = "http://finance.yahoo.dk/d/quotes.csv?s=%1&f=sl1d1";
				
			qs1.Id = qs_s.Add_GetIndex (qs1);
			qs2.Id = qs_s.Add_GetIndex (qs2);
			qs3.Id = qs_s.Add_GetIndex (qs3);	
				
			s1 = new Security("ERIC");
			s1.Name = "Ericsson";
			s1.Currency = c1.Id;
			s1.Quote_Source = qs1.Id;

			s2 = new Security("MSFT");
			s2.Name = "Microsoft";
			s2.Currency = c2.Id;
			s2.Quote_Source = qs2.Id;

			s3 = new Security("SEB Europa Fond");
			s3.Name = "SEB Europa Fond";
			s3.Currency = c3.Id;
			s3.Quote_Source = qs3.Id;

			s4 = new Security("AZN");
			s4.Name = "Amazon";
			s4.Currency = c3.Id;
			s4.Quote_Source = qs3.Id;

			s1.Id = s_s.Add_GetIndex (s1);
			s2.Id = s_s.Add_GetIndex (s2);
			s3.Id = s_s.Add_GetIndex (s3);
			s4.Id = s_s.Add_GetIndex (s4);
			
			OriginalShares = 1000m;
			NewShares = 10000m;
			SplitRatio = NewShares / OriginalShares;
					
		}

		[TestFixtureTearDown]
		public void CloseDatabaseConnection()
		{
		}

		private void AddSecurityQuote (decimal _quote, int times, int _security, int year, int month, int day, MPC.Quotes.UpdateTypeEnum type )
		{
			for (int k=0; k < times; k++)
			{
				sq1 = new SecurityQuote();
				sq1.Symbol = _security;
				sq1.TimeStamp = new DateTime (year, month, day, 0, 0, 0);
				sq1.Quote_Value = _quote + k;
				sq1.Quote_Type = type;
				sq_s.Add (sq1);
			}
		}
		
		private void AddTransaction (decimal _quote, int shares, int times, int year, int month, int day, int account, int _security)
		{
			for (int k=0; k < times; k++)
			{
				sq1 = new SecurityQuote();
				sq1.Symbol = _security;
				sq1.TimeStamp = new DateTime (year, month, day, 0, 0, 0);
				sq1.Quote_Value = _quote + k;
				sq1.Quote_Type = (shares > 0 ? MPC.Quotes.UpdateTypeEnum.buy : MPC.Quotes.UpdateTypeEnum.sell );
				try {
					sq1.Id = sq_s.Add_GetIndex (sq1);
				} catch 
				{
				}
				
				it1 = new InvestmentTransaction();
				it1.InvestmentAccount = account;
				it1.TimeStamp = sq1.TimeStamp;
				it1.Memo = String.Format ("Action {0}", k);
				it1.Security_id = _security;
				it1.Shares = shares;
				it1.Share_price = sq1.Id;
				it1.Id = it_s.Add_GetIndex (it1);
			}
		}
		
		private void AddMultipleSet()
		{
			AddTransaction (100, 30, 1, 2006, 10, 10, a2.Id, s1.Id);				
			AddTransaction (90.5m, 20, 1, 2006, 10, 11, a1.Id, s1.Id);
			AddTransaction (100, 30, 1, 2006, 11, 10, a2.Id, s1.Id);	
			AddTransaction (110, 35, 1, 2006, 12, 10, a2.Id, s1.Id);	
			AddTransaction (120, 35, 1, 2007,  1, 10, a2.Id, s1.Id);	
			AddTransaction (150, 35, 1, 2007,  2, 10, a2.Id, s1.Id);
			AddTransaction (160, 35, 1, 2007,  3, 10, a2.Id, s1.Id);	
			AddTransaction (120, 35, 1, 2007,  4, 10, a2.Id, s1.Id);	
			AddTransaction (146, 35, 1, 2007,  5, 10, a2.Id, s1.Id);				
			AddTransaction (23.4567m, 500, 1, 2007, 2, 20, a1.Id, s2.Id);	
			AddTransaction (9.76m, 50, 1, 2007, 2, 22, a1.Id, s3.Id);	
			AddTransaction (135, 60, 1, 2007, 4, 25, a1.Id, s1.Id);	
			AddTransaction (10.08m, 30, 1, 2007, 4, 25, a3.Id, s3.Id);				
			AddTransaction (112, 35, 1, 2006, 12, 25, a1.Id, s1.Id);	
			AddTransaction (20, -15, 2, 2007, 12, 20, a1.Id, s3.Id);	
			AddTransaction (119.12m, -10, 2, 2007, 1, 4, a1.Id, s1.Id);						
			AddTransaction (16.75m, -50, 3, 2007, 5, 10, a3.Id, s3.Id);						
			AddTransaction (123, -40, 4, 2007, 5, 10, a1.Id, s1.Id);	

			AddSecurityQuote (100, 20, s1.Id, 2006, 10, 3, MPC.Quotes.UpdateTypeEnum.online);				
			AddSecurityQuote (120, 20, s1.Id, 2006, 11, 3, MPC.Quotes.UpdateTypeEnum.online);				
			AddSecurityQuote (140, 20, s1.Id, 2006, 12, 3, MPC.Quotes.UpdateTypeEnum.online);				
			AddSecurityQuote (135, 20, s1.Id, 2007, 1, 3, MPC.Quotes.UpdateTypeEnum.online);				
			AddSecurityQuote (155, 20, s1.Id, 2007, 2, 3, MPC.Quotes.UpdateTypeEnum.online);				
			AddSecurityQuote (180, 20, s1.Id, 2007, 3, 3, MPC.Quotes.UpdateTypeEnum.online);				
			AddSecurityQuote (300, 20, s1.Id, 2007, 4, 3, MPC.Quotes.UpdateTypeEnum.online);				
			AddSecurityQuote (150, 20, s1.Id, 2007, 5, 3, MPC.Quotes.UpdateTypeEnum.online);				
			AddSecurityQuote (100, 20, s1.Id, 2007, 6, 3, MPC.Quotes.UpdateTypeEnum.online);				

			AddSecurityQuote (149, 4, s1.Id, 2007, 1, 3, MPC.Quotes.UpdateTypeEnum.manual);													
		}

	
		[Test()]
		public void OneAccount()
		{
			AddTransaction (100, 30, 1, 2006, 10, 10, a1.Id, s1.Id);	
			p_v.PreparePortfolio(dt2);
			System.Collections.ArrayList pv_list = p_v.GetPortfolio;
			PortfolioEntry pe = (pv_list[0] as PortfolioEntry);
			Assert.AreEqual (3000m, pe.Value );
			Assert.AreEqual (1, pv_list.Count );
		}

		[Test()]
		public void OneAccountNoBase()
		{
			AddTransaction (100, 30, 1, 2006, 10, 10, a1.Id, s1.Id);	
			p_v.PreparePortfolio(dt2);
			System.Collections.ArrayList pv_list = p_v.GetPortfolio;
			PortfolioEntry pe = (pv_list[0] as PortfolioEntry);
			Assert.AreEqual (3000m, pe.Value );
			Assert.AreEqual (1, pv_list.Count );			
		}

		[Test()]
		public void OneAccountOneBase()
		{
			AddTransaction (100, 30, 1, 2006, 10, 10, a1.Id, s1.Id);	
			c4.BasePool = true;
			c_s.Change(c4);
			p_v.SetBaseCurrency = c4.Id;
			p_v.PreparePortfolio(dt2);
			System.Collections.ArrayList pv_list = p_v.GetPortfolio;
			PortfolioEntry pe = (pv_list[0] as PortfolioEntry);
			Assert.AreEqual (3000m, pe.Value );
			Assert.AreEqual (1, pv_list.Count );			
		}


		[Test()]
		public void Simple_AccountView()
		{
			AddTransaction (100, 30, 1, 2006, 10, 10, a1.Id, s1.Id);				
			AddTransaction (100, 40, 1, 2006, 10, 10, a2.Id, s2.Id);	
			p_v.PreparePortfolio(dt2);
			System.Collections.ArrayList pv_list = p_v.GetPortfolio;
			PortfolioEntry pe0 = (pv_list[0] as PortfolioEntry);	
			PortfolioEntry pe1 = (pv_list[1] as PortfolioEntry);						
			Assert.AreEqual (4000m, pe0.Value );
			Assert.AreEqual (3000m, pe1.Value );
			Assert.AreEqual (2, pv_list.Count );						
		}

		[Test()]
		public void Simple_AccountViewBaseCurrency()
		{
			AddTransaction (100, 30, 1, 2006, 10, 10, a1.Id, s1.Id);				
			AddTransaction (100, 40, 1, 2006, 10, 10, a2.Id, s2.Id);
			p_v.SetBaseCurrency = c4.Id;
			p_v.PreparePortfolio(dt2);
			System.Collections.ArrayList pv_list = p_v.GetPortfolio;
			PortfolioEntry pe0 = (pv_list[0] as PortfolioEntry);	
			PortfolioEntry pe1 = (pv_list[1] as PortfolioEntry);						
			Assert.AreEqual (4000m, pe0.Value );
			Assert.AreEqual (3000m, pe1.Value );
			Assert.AreEqual (2, pv_list.Count );						
		}
		
		[Test()]
		public void Simple_SecurityView()
		{
			AddTransaction (100, 30, 1, 2006, 10, 10, a1.Id, s1.Id);				
			AddTransaction (100, 40, 1, 2006, 10, 10, a2.Id, s2.Id);
			p_v.PreparePortfolio(s1, dt2);
			System.Collections.ArrayList pv_list = p_v.GetPortfolio;
			PortfolioEntry pe0 = (pv_list[0] as PortfolioEntry);	
			Assert.AreEqual (3000m, pe0.Value );
			Assert.AreEqual (1, pv_list.Count );						
		}

		[Test()]
		public void AccountView()
		{
			AddTransaction (100, 300, 1, 2006, 10, 10, a1.Id, s1.Id);				
			AddTransaction (100, 40, 1, 2006, 10, 10, a2.Id, s1.Id);
			AddTransaction (110, -40, 1, 2006, 10, 20, a2.Id, s2.Id);				
			AddTransaction (120, 20, 1, 2006, 11, 10, a2.Id, s3.Id);				
			AddTransaction (140, 40, 1, 2006, 12, 10, a2.Id, s1.Id);				
			AddTransaction (150, -60, 1, 2006, 12, 20, a1.Id, s1.Id);				
			p_v.PreparePortfolio(a1, dt2);
			System.Collections.ArrayList pv_list = p_v.GetPortfolio;
			PortfolioEntry pe0 = (pv_list[0] as PortfolioEntry);	
			Assert.AreEqual (36000m, pe0.Value );
			Assert.AreEqual (1, pv_list.Count );									
		}
		[Test()]
		public void SecurityView()
		{
			AddTransaction (100, 300, 1, 2006, 10, 10, a1.Id, s1.Id);				
			AddTransaction (100, 40, 1, 2006, 10, 10, a2.Id, s1.Id);
			AddTransaction (110, -40, 1, 2006, 10, 20, a2.Id, s1.Id);				
			AddTransaction (120, 20, 1, 2006, 11, 10, a2.Id, s1.Id);				
			AddTransaction (140, 40, 1, 2006, 12, 10, a2.Id, s1.Id);				
			AddTransaction (150, -60, 1, 2006, 12, 20, a1.Id, s1.Id);				
			p_v.PreparePortfolio(s1, dt2);
			System.Collections.ArrayList pv_list = p_v.GetPortfolio;
			PortfolioEntry pe0 = (pv_list[0] as PortfolioEntry);	
			Assert.AreEqual (45000m, pe0.Value );
			Assert.AreEqual (1, pv_list.Count );												
		}
		
		[Test()]
		public void TwoAccounts_TwoSecurities()
		{
			AddTransaction (100, 30, 1, 2006, 10, 10, a1.Id, s1.Id);				
			AddTransaction (100, 40, 1, 2006, 10, 10, a2.Id, s2.Id);				
			p_v.PreparePortfolio(new System.DateTime(2007,1,3,0,0,0));
			System.Collections.ArrayList pv_list = p_v.GetPortfolio;
			PortfolioEntry pe0 = (pv_list[0] as PortfolioEntry);	
			PortfolioEntry pe1 = (pv_list[1] as PortfolioEntry);						
			Assert.AreEqual (4000m, pe0.Value );
			Assert.AreEqual (3000m, pe1.Value );
			Assert.AreEqual (2, pv_list.Count );						
		}
		
		[Test()]
		public void BaseTotal()
		{
			AddTransaction (100, 30, 1, 2006, 10, 10, a1.Id, s1.Id);				
			AddTransaction (100, 40, 1, 2006, 10, 10, a2.Id, s2.Id);		
			p_v.SetBaseCurrency = c4.Id;						
			p_v.PreparePortfolio(new System.DateTime(2007,1,3,0,0,0));
//			System.Collections.ArrayList pv_list = p_v.GetPortfolio;
			Assert.AreEqual (18000m, p_v.BaseTotal );
		}		

		[Test()]
		public void LocalTotal_differentLocals()
		{
			AddTransaction (100, 30, 1, 2006, 10, 10, a1.Id, s1.Id);				
			AddTransaction (100, 40, 1, 2006, 10, 10, a2.Id, s2.Id);	
			p_v.SetBaseCurrency = c4.Id;						
			p_v.PreparePortfolio(new System.DateTime(2007,1,3,0,0,0));
//			System.Collections.ArrayList pv_list = p_v.GetPortfolio;
			Assert.AreEqual (0m, p_v.LocalTotal );
		}

		[Test()]
		public void LocalTotal_SameLocals()
		{
			AddTransaction (100, 30, 1, 2006, 10, 10, a1.Id, s3.Id);				
			AddTransaction (100, 40, 1, 2006, 10, 10, a2.Id, s4.Id);	
			p_v.SetBaseCurrency = c4.Id;						
			p_v.PreparePortfolio(new System.DateTime(2007,1,3,0,0,0));
//			System.Collections.ArrayList pv_list = p_v.GetPortfolio;
			Assert.AreEqual (7000m, p_v.LocalTotal );
			Assert.AreEqual (28000m, p_v.BaseTotal );			
		}				
		
	}
}
