// InvestmentTransactionStoreTest.cs
// 
// Copyright (C) 2008 Bengt Thuree
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;
using NUnit.Framework;

using MPC.Quotes;
using MPC.Accounts;
using MPC;

namespace AccountTest
{
	
[TestFixture()]
public class InvestmentTransactionStoreTest
{
		
	public SecurityQuote q1, q2, q3, q4, q_tmp, q_tmp2;
	public QuoteSource qs1, qs2, qs3, qs4, qs_tmp, qs_tmp2;		
	public Currency c1, c2, c3, c4, c_tmp, c_tmp2;		
	public Security s1, s2, s3, s4, s_tmp, s_tmp2;		
	public InvestmentTransaction it1, it2, it3, it4, it_tmp, it_tmp2;
	public AddressDetail ad1, ad2, ad3, ad4, ad_tmp, ad_tmp2;
	public ContactDetail cd1, cd2, cd3, cd4, cd_tmp, cd_tmp2;
	public AccountHeader a1, a2, a3, a4, a_tmp, a_tmp2;
	public AccountHeaderStore a_s;		
	public AddressDetailsStore ad_s;
	public ContactDetailsStore cd_s;		
	public SecurityQuoteStore qs;
	public SecurityStore ss;
	public CurrencyStore cs;
	public QuoteSourceStore qss;
	public InvestmentTransactionStore its;
	int first_index;

	[TestFixtureSetUp]
	public void OpenDatabaseConnection()
	{
			MPC.Database.SetTestDB();
	}

	[SetUp]
	public void MySetup()
	{
		cs = MPC.Database.GetDatabase.Currencies;
		cs.Del_All();

		qss = MPC.Database.GetDatabase.QuoteSources;
		qss.Del_All();			
			
		ss = MPC.Database.GetDatabase.Securities;
		ss.Del_All();

		qs = MPC.Database.GetDatabase.SecurityQuotes;
		qs.Del_All();

		its = MPC.Database.GetDatabase.InvestmentTransactions;
		its.Del_All();

		ad_s = MPC.Database.GetDatabase.AddressDetails;
		ad_s.Del_All();
		
		cd_s = MPC.Database.GetDatabase.ContactDetails;
		cd_s.Del_All();
			
		a_s = MPC.Database.GetDatabase.Accounts;
		a_s.Del_All();			

		c1 = new Currency("SEK");
		c1.Country = "Swedish Krona";
		c2 = new Currency("USD");
		c2.Country = "US Dollar";
		c3 = new Currency("AUD");
		c3.Country = "Australian Dollar";
		c4 = new Currency("HKD");
		c4.Country = "HongKong Dollar";
			
		cs.Add (c1);
		c1 = cs.Get (c1.Code);
		cs.Add (c2);
		c2 = cs.Get (c2.Code);
		cs.Add (c3);
		c3 = cs.Get (c3.Code);
		cs.Add (c4);
		c4 = cs.Get (c4.Code);
			
		ad1 = new AddressDetail();
		ad1.Swift = "swift";
		ad1.Phone = "Phone";
		ad1.Fax = "Fax";
		ad1.Name = "Name";
		ad1.Address1 = "Address1";
		ad1.Address2 = "Address2";
		ad1.Zip	= "zip";
		ad1.City = "city";
		ad1.Country = "country";
		ad1.Uri = "uri";
		ad1.Notes = "notes";
			
		ad2 = new AddressDetail();
		ad2.Swift = "2swift";
		ad2.Phone = "2Phone";
		ad2.Fax = "2Fax";
		ad2.Name = "2Name";
		ad2.Address1 = "2Address1";
		ad2.Address2 = "2Address2";
		ad2.Zip	= "2zip";
		ad2.City = "2city";
		ad2.Country = "2country";
		ad2.Uri = "2uri";
		ad2.Notes = "2notes";

		ad3 = new AddressDetail();
		ad3.Swift = "3swift";
		ad3.Phone = "3Phone";
		ad3.Fax = "3Fax";
		ad3.Name = "3Name";
		ad3.Address1 = "3Address1";
		ad3.Address2 = "3Address2";
		ad3.Zip	= "3zip";
		ad3.City = "3city";
		ad3.Country = "3country";
		ad3.Uri = "3uri";
		ad3.Notes = "3notes";

		ad4 = new AddressDetail();
		ad4.Swift = "4swift";
		ad4.Phone = "4Phone";
		ad4.Fax = "Fax4";
		ad4.Name = "Name4";
		ad4.Address1 = "4Address1";
		ad4.Address2 = "4Address2";
		ad4.Zip	= "zip4";
		ad4.City = "city4";
		ad4.Country = "4country";
		ad4.Uri = "uri4";
		ad4.Notes = "notes4";	
			
		ad_s.Add (ad1);
		ad1 = ad_s.Get (ad1.Name);
		ad_s.Add (ad2);
		ad2 = ad_s.Get (ad2.Name);
		ad_s.Add (ad3);
		ad3 = ad_s.Get (ad3.Name);
		ad_s.Add (ad4);
		ad4 = ad_s.Get (ad4.Name);	

		cd1 = new ContactDetail();
		cd1.Name = "cdBengt";
		cd1.Phone = "cdPhone";
		cd1.Mobile = "cdMobile";
		cd1.Other_phone = "cdOtherPhone";
		cd1.Fax = "Fax";
		cd1.Work_mail = "cdWorkMail";
		cd1.Private_mail = "cdPrivateMail";
		cd1.Other_mail = "cdOtherMail";
		cd1.Address_id	= ad1.Id;
		cd1.Uri = "cdUri";
		cd1.Notes = "cdNotes";
			
		cd2 = new ContactDetail();
		cd2.Name = "2cdBengt";
		cd2.Phone = "2cdPhone";
		cd2.Mobile = "2cdMobile";
		cd2.Other_phone = "2cdOtherPhone";
		cd2.Fax = "2Fax";
		cd2.Work_mail = "2cdWorkMail";
		cd2.Private_mail = "2cdPrivateMail";
		cd2.Other_mail = "2cdOtherMail";
		cd2.Address_id	= ad2.Id;
		cd2.Uri = "2cdUri";
		cd2.Notes = "2cdNotes";
			
		cd3 = new ContactDetail();
		cd3.Name = "3cdBengt";
		cd3.Phone = "3cdPhone";
		cd3.Mobile = "3cdMobile";
		cd3.Other_phone = "3cdOtherPhone";
		cd3.Fax = "3Fax";
		cd3.Work_mail = "3cdWorkMail";
		cd3.Private_mail = "3cdPrivateMail";
		cd3.Other_mail = "3cdOtherMail";
		cd3.Address_id	= ad3.Id;
		cd3.Uri = "3cdUri";
		cd3.Notes = "3cdNotes";
			
		cd4 = new ContactDetail();
		cd4.Name = "4cdBengt";
		cd4.Phone = "4cdPhone";
		cd4.Mobile = "4cdMobile";
		cd4.Other_phone = "4cdOtherPhone";
		cd4.Fax = "4Fax";
		cd4.Work_mail = "4cdWorkMail";
		cd4.Private_mail = "4cdPrivateMail";
		cd4.Other_mail = "4cdOtherMail";
		cd4.Address_id	= ad4.Id;
		cd4.Uri = "4cdUri";
		cd4.Notes = "4cdNotes";

		cd_s.Add (cd1);
		cd1 = cd_s.Get (cd1.Name);
		cd_s.Add (cd2);
		cd2 = cd_s.Get (cd2.Name);
		cd_s.Add (cd3);
		cd3 = cd_s.Get (cd3.Name);
		cd_s.Add (cd4);
		cd4 = cd_s.Get (cd4.Name);	

		a1 = new AccountHeader();
		a1.Name = "Saving";
		a1.Address_id = ad1.Id;
		a1.Contact_id = cd1.Id;
		a1.Account_nr = "123456789";
		a1.Currency_id = c1.Id;

		a2 = new AccountHeader();
		a2.Name = "Investment";
		a2.Address_id = ad2.Id;
		a2.Contact_id = cd2.Id;
		a2.Account_nr = "123456789";
		a2.Currency_id = c2.Id;

		a3 = new AccountHeader();
		a3.Name = "Play";
		a3.Address_id = ad3.Id;
		a3.Contact_id = cd3.Id;
		a3.Account_nr = "123456789";
		a3.Currency_id = c3.Id;
		
		a4 = new AccountHeader();
		a4.Name = "House";
		a4.Address_id = ad4.Id;
		a4.Contact_id = cd4.Id;
		a4.Account_nr = "123456789";
		a4.Currency_id = c4.Id;
		
		a_s.Add (a1);
		a1 = a_s.Get (a1.Name);
		a_s.Add (a2);
		a2 = a_s.Get (a2.Name);
		a_s.Add (a3);
		a3 = a_s.Get (a3.Name);					
		a_s.Add (a4);
		a4 = a_s.Get (a4.Name);
			
		qs1 = new QuoteSource("Yahoo global");
		qs1.Uri = "http://finance.yahoo.com/d/quotes.csv?s=%1&f=sl1d1";
		qs2 = new QuoteSource("Yahoo local");
		qs2.Uri = "http://finance.yahoo.se/d/quotes.csv?s=%1&f=sl1d1";
		qs3 = new QuoteSource("Yahoo Denmark");
		qs3.Uri = "http://finance.yahoo.dk/d/quotes.csv?s=%1&f=sl1d1";
			
		qss.Add (qs1);
		qs1 = qss.Get (qs1.Name);
		qss.Add (qs2);
		qs2 = qss.Get (qs2.Name);
		qss.Add (qs3);
		qs3 = qss.Get (qs3.Name);								
			
		s1 = new Security("ERIC");
		s1.Name = "Ericsson";
		s1.Currency = c1.Id;
		s1.Quote_Source = qs1.Id;

		s2 = new Security("MSFT");
		s2.Name = "Microsoft";
		s2.Currency = c2.Id;
		s2.Quote_Source = qs2.Id;

		s3 = new Security("SEB Europa Fond");
		s3.Name = "SEB Europa Fond";
		s3.Currency = c3.Id;
		s3.Quote_Source = qs3.Id;

		s4 = new Security("AZN");
		s4.Name = "Amazon";
		s4.Currency = c3.Id;
		s4.Quote_Source = qs3.Id;

		ss.Add (s1);
		s1 = ss.Get (s1.Symbol);
		ss.Add (s2);
		s2 = ss.Get (s2.Symbol);
		ss.Add (s3);
		s3 = ss.Get (s3.Symbol);
		ss.Add (s4);
		s4 = ss.Get (s4.Symbol);

		q1 = new SecurityQuote();
		q1.Symbol = s1.Id;
		q1.TimeStamp = new DateTime (2008, 12, 24, 13, 30, 0);
		q1.Quote_Value = 100;
		q1.Quote_Type = MPC.Quotes.UpdateTypeEnum.manual;

		q2 = new SecurityQuote();
		q2.Symbol = s1.Id;
		q2.TimeStamp = new DateTime (2008, 12, 24, 14, 30, 0);
		q2.Quote_Value = 110.00m;
		q2.Quote_Type = MPC.Quotes.UpdateTypeEnum.manual;

		q3 = new SecurityQuote();
		q3.Symbol = s1.Id;
		q3.TimeStamp = new DateTime (2008, 12, 26, 18, 30, 0);
		q3.Quote_Value = 120.00m;
		q3.Quote_Type = MPC.Quotes.UpdateTypeEnum.manual;

		q4 = new SecurityQuote();
		q4.Symbol = s2.Id;
		q4.TimeStamp = new DateTime (2008, 12, 27, 19, 30, 0);
		q4.Quote_Value = 12000.00m;
		q4.Quote_Type = MPC.Quotes.UpdateTypeEnum.manual;

		q4 = new SecurityQuote();
		q4.Symbol = s2.Id;
		q4.TimeStamp = new DateTime (2008, 12, 27, 19, 30, 0);
		q4.Quote_Value = 12000.00m;
		q4.Quote_Type = MPC.Quotes.UpdateTypeEnum.manual;
			
		it1 = new InvestmentTransaction();
		it1.InvestmentAccount = a1.Id;
		it1.TimeStamp = new DateTime(2006, 10, 10, 0, 0, 0);
		it1.Memo = "it1";
		it1.Security_id = s1.Id;
		it1.Shares = 100;
		it1.Share_price = 0;
		it1.Reconsiled = false;	
			
		it2 = new InvestmentTransaction();
		it2.InvestmentAccount = a3.Id;
		it2.TimeStamp = new DateTime(2006, 10, 11, 0, 0, 0);
		it2.Memo = "it1";
		it2.Security_id = s1.Id;
		it2.Shares = 200;
		it2.Share_price = 0;
		it2.Reconsiled = false;			
			
		it3 = new InvestmentTransaction();
		it3.InvestmentAccount = a3.Id;
		it3.TimeStamp = new DateTime(2006, 10, 13, 0, 0, 0);
		it3.Memo = "it1";
		it3.Security_id = s1.Id;
		it3.Shares = -200;
		it3.Share_price = 0;
		it3.Reconsiled = false;			

		it4 = new InvestmentTransaction();
		it4.InvestmentAccount = a2.Id;
		it4.TimeStamp = new DateTime(2006, 10, 12, 0, 0, 0);
		it4.Memo = "it1";
		it4.Security_id = s3.Id;
		it4.Shares = 400;
		it4.Share_price = 0;
		it4.Reconsiled = false;						
	}

	[TestFixtureTearDown]
	public void CloseDatabaseConnection()
	{
	}

	[Test()]
	public void Add()
	{
		Assert.AreEqual  ( 0, its.Count() );			
		its.Add (it1);
		Assert.AreEqual  ( 1, its.Count() );			
	}

	[Test()]
	// [ExpectedException("MPC.Utils.NotUniqueException")]
	[Ignore("WEIRD")]				
	public void Add_SameQuote()
	{
		its.Add (it1);
		its.Add (it1);
	}

	[Test()]
	public void Del_Id()
	{
		it1.Id = its.Add_GetIndex (it1);
		it2.Id = its.Add_GetIndex (it2);
		it3.Id = its.Add_GetIndex (it3);
		its.Del (it2.Id);
		Assert.AreEqual (2, its.Count() );
		it_tmp = its.Get (it1);
		Assert.AreEqual (it1.Shares, it_tmp.Shares);
		it_tmp = its.Get(it3);
		Assert.AreEqual (it3.Shares, it_tmp.Shares);
	}

	[Test()]
	public void Del_All()
	{
		its.Add (it1);
		its.Add (it2);
		its.Add (it3);
		its.Del_All();
		Assert.AreEqual  ( 0, its.Count() );
	}

	[Test()]
	public void Change_IT()
	{
		it1.Id = its.Add_GetIndex (it1);
		it_tmp = its.Get (it1);
		it_tmp.Shares = it3.Shares;
		its.Change (it_tmp);
		it_tmp2 = its.Get (it1);
		Assert.AreEqual (it3.Shares, it_tmp2.Shares);
	}

	[Test()]
	public void Get_Id()
	{
		it1.Id = its.Add_GetIndex (it1);
		it_tmp2 = its.Get (it1);
		Assert.AreEqual (it1.Security_id, it_tmp2.Security_id);
		Assert.AreEqual (it1.Share_price, it_tmp2.Share_price);
		Assert.AreEqual (it1.Shares, it_tmp2.Shares);
	}

	[Test()]
	public void CountTest()
	{
		Assert.AreEqual  ( 0, its.Count() );
		it1.Id = its.Add_GetIndex (it1);
		Assert.AreEqual  ( 1, its.Count() );
		it2.Id = its.Add_GetIndex (it2);
		Assert.AreEqual  ( 2, its.Count() );
		it3.Id = its.Add_GetIndex (it3);
		Assert.AreEqual  ( 3, its.Count() );

		it_tmp = its.Get (it1);
		its.Del (it_tmp);	
		Assert.AreEqual  ( 2, its.Count() );
		it_tmp = its.Get (it2);
		its.Del (it_tmp);
		Assert.AreEqual  ( 1, its.Count() );
		it_tmp = its.Get (it3);
		its.Del (it_tmp);
		Assert.AreEqual  ( 0, its.Count() );
	}

	[Test()]
	public void GetList_Counts()
	{
		Assert.AreEqual (0, (its.GetList()).Count);
		it1.Id = its.Add_GetIndex (it1) ;
		it2.Id = its.Add_GetIndex (it2) ;
		it3.Id = its.Add_GetIndex (it3) ;
		Assert.AreEqual (3, (its.GetList()).Count);
		it_tmp = its.Get (it1);
		its.Del (it_tmp);	
		Assert.AreEqual (2, (its.GetList()).Count);
		it_tmp = its.Get (it2);
		its.Del (it_tmp) ;	
		it_tmp = its.Get (it3);
		its.Del (it_tmp);	
		Assert.AreEqual (0, (its.GetList()).Count);
	}

	[Test()]
	public void GetList_Contents()
	{
		System.Collections.ArrayList it_db;
		it1.Id = its.Add_GetIndex (it1);
		it2.Id = its.Add_GetIndex (it2);
		it3.Id = its.Add_GetIndex (it3);
		it_db = its.GetList();
		Assert.AreEqual ( (it_db[0] as InvestmentTransaction).Shares, it1.Shares);
		Assert.AreEqual ( (it_db[1] as InvestmentTransaction).Shares, it2.Shares);
		Assert.AreEqual ( (it_db[2] as InvestmentTransaction).Shares, it3.Shares);
		it_tmp = its.Get (it1);
		its.Del (it_tmp) ;	
		it_db = its.GetList();
		Assert.AreEqual ( (it_db[0] as InvestmentTransaction).Shares, it2.Shares);
		Assert.AreEqual ( (it_db[1] as InvestmentTransaction).Shares, it3.Shares);
	}

	[Test()]
	public void GetList_SecurityBeforeDate()
	{
		it1.Id = its.Add_GetIndex (it1) ;
		it2.Id = its.Add_GetIndex (it2) ;
		it3.Id = its.Add_GetIndex (it3) ;
		it4.Id = its.Add_GetIndex (it4) ;
		Assert.AreEqual (2, (its.GetList(ss.Get(it1.Security_id), it4.TimeStamp)).Count);
	}	
	
	[Test()]
	public void GetList_AccountSecurity()
	{
		it1.Id = its.Add_GetIndex (it1) ;
		it2.Id = its.Add_GetIndex (it2) ;
		it3.Id = its.Add_GetIndex (it3) ;
		it4.Id = its.Add_GetIndex (it4) ;
		Assert.AreEqual (2, (its.GetList(a_s.Get(it2.InvestmentAccount), ss.Get(it2.Security_id))).Count);
	}		
	

	[Test()]
	public void UniqueIndex()
	{
		it1.Id = its.Add_GetIndex (it1);
		first_index = it1.Id;
		it_tmp2 = its.Get(first_index);
		Assert.AreEqual (first_index, it_tmp2.Id);
		it2.Id = its.Add_GetIndex (it2);
		it_tmp= its.Get(first_index+1);
		Assert.AreEqual (first_index+1, it_tmp.Id);
		it3.Id = its.Add_GetIndex (it3);
		it_tmp= its.Get(first_index+2);
		Assert.AreEqual (first_index+2, it_tmp.Id);
		its.Del (first_index+1) ;
		it4.Id = its.Add_GetIndex (it4) ;
		it_tmp= its.Get(first_index+3);
		Assert.AreEqual (first_index+3, it_tmp.Id);

	}
}
}