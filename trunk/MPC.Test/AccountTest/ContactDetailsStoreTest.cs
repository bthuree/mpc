// ContactDetailsStoreTest.cs
// 
// Copyright (C) 2008 Bengt Thuree
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

//FIXME Need to clear all data in Setup, also need to tie in with data to SecurityStore



using System;
using System.IO;
using NUnit.Framework;

using MPC;
using MPC.Quotes;
using MPC.Accounts;
using MPC.Utils;
using System.Data;

namespace AccountTest
{
	
	
[TestFixture()]
public class ContactDetailsStoreTest
{ 
	public AddressDetail ad1, ad2, ad3, ad4, ad_tmp, ad_tmp2;
	public ContactDetail cd1, cd2, cd3, cd4, cd_tmp, cd_tmp2;
	public AddressDetailsStore ad_s;
	public ContactDetailsStore cd_s;
	
	int first_index;			

	[TestFixtureSetUp]
	public void OpenDatabaseConnection()
	{
			MPC.Database.SetTestDB();
	}

	[SetUp]
	public void MySetup()
	{
		ad_s = MPC.Database.GetDatabase.AddressDetails;
		ad_s.Del_All();
		
		cd_s = MPC.Database.GetDatabase.ContactDetails;
		cd_s.Del_All();

		ad1 = new AddressDetail();
		ad1.Swift = "swift";
		ad1.Phone = "Phone";
		ad1.Fax = "Fax";
		ad1.Name = "Name";
		ad1.Address1 = "Address1";
		ad1.Address2 = "Address2";
		ad1.Zip	= "zip";
		ad1.City = "city";
		ad1.Country = "country";
		ad1.Uri = "uri";
		ad1.Notes = "notes";
			
		ad2 = new AddressDetail();
		ad2.Swift = "2swift";
		ad2.Phone = "2Phone";
		ad2.Fax = "2Fax";
		ad2.Name = "2Name";
		ad2.Address1 = "2Address1";
		ad2.Address2 = "2Address2";
		ad2.Zip	= "2zip";
		ad2.City = "2city";
		ad2.Country = "2country";
		ad2.Uri = "2uri";
		ad2.Notes = "2notes";

		ad3 = new AddressDetail();
		ad3.Swift = "3swift";
		ad3.Phone = "3Phone";
		ad3.Fax = "3Fax";
		ad3.Name = "3Name";
		ad3.Address1 = "3Address1";
		ad3.Address2 = "3Address2";
		ad3.Zip	= "3zip";
		ad3.City = "3city";
		ad3.Country = "3country";
		ad3.Uri = "3uri";
		ad3.Notes = "3notes";

		ad4 = new AddressDetail();
		ad4.Swift = "4swift";
		ad4.Phone = "4Phone";
		ad4.Fax = "Fax4";
		ad4.Name = "Name4";
		ad4.Address1 = "4Address1";
		ad4.Address2 = "4Address2";
		ad4.Zip	= "zip4";
		ad4.City = "city4";
		ad4.Country = "4country";
		ad4.Uri = "uri4";
		ad4.Notes = "notes4";	
			
		ad_s.Add (ad1);
		ad1 = ad_s.Get (ad1.Name);
		ad_s.Add (ad2);
		ad2 = ad_s.Get (ad2.Name);
		ad_s.Add (ad3);
		ad3 = ad_s.Get (ad3.Name);
		ad_s.Add (ad4);
		ad4 = ad_s.Get (ad4.Name);	

		cd1 = new ContactDetail();
		cd1.Name = "cdBengt";
		cd1.Phone = "cdPhone";
		cd1.Mobile = "cdMobile";
		cd1.Other_phone = "cdOtherPhone";
		cd1.Fax = "Fax";
		cd1.Work_mail = "cdWorkMail";
		cd1.Private_mail = "cdPrivateMail";
		cd1.Other_mail = "cdOtherMail";
		cd1.Address_id	= ad1.Id;
		cd1.Uri = "cdUri";
		cd1.Notes = "cdNotes";
			
		cd2 = new ContactDetail();
		cd2.Name = "2cdBengt";
		cd2.Phone = "2cdPhone";
		cd2.Mobile = "2cdMobile";
		cd2.Other_phone = "2cdOtherPhone";
		cd2.Fax = "2Fax";
		cd2.Work_mail = "2cdWorkMail";
		cd2.Private_mail = "2cdPrivateMail";
		cd2.Other_mail = "2cdOtherMail";
		cd2.Address_id	= ad2.Id;
		cd2.Uri = "2cdUri";
		cd2.Notes = "2cdNotes";
			
		cd3 = new ContactDetail();
		cd3.Name = "3cdBengt";
		cd3.Phone = "3cdPhone";
		cd3.Mobile = "3cdMobile";
		cd3.Other_phone = "3cdOtherPhone";
		cd3.Fax = "3Fax";
		cd3.Work_mail = "3cdWorkMail";
		cd3.Private_mail = "3cdPrivateMail";
		cd3.Other_mail = "3cdOtherMail";
		cd3.Address_id	= ad3.Id;
		cd3.Uri = "3cdUri";
		cd3.Notes = "3cdNotes";
			
		cd4 = new ContactDetail();
		cd4.Name = "4cdBengt";
		cd4.Phone = "4cdPhone";
		cd4.Mobile = "4cdMobile";
		cd4.Other_phone = "4cdOtherPhone";
		cd4.Fax = "4Fax";
		cd4.Work_mail = "4cdWorkMail";
		cd4.Private_mail = "4cdPrivateMail";
		cd4.Other_mail = "4cdOtherMail";
		cd4.Address_id	= ad4.Id;
		cd4.Uri = "4cdUri";
		cd4.Notes = "4cdNotes";

	}		
		
	[TestFixtureTearDown]
	public void CloseDatabaseConnection()
	{
	}
	
	[Test()]
	public void Add()
	{
		Assert.AreEqual (0, cd_s.Count() );					
		cd_s.Add (cd1);
		Assert.AreEqual (1, cd_s.Count() );		
	}
		
	[Test()]
	//[ExpectedException("MPC.Utils.NotUniqueException")]		
	[Ignore("WEIRD")]				
	public void Add_SameAccount()
	{
		cd_s.Add (cd1);
		cd_s.Add (cd1);			
	}
		
	[Test()]
	public void Del_Id()
	{
		cd_s.Add (cd1);
		cd_s.Add (cd2);	
		cd_s.Add (cd3);			
		Assert.AreEqual (3, cd_s.Count() );			
		cd_tmp = cd_s.Get (cd2.Name);
		cd_s.Del (cd_tmp.Id);
		Assert.AreEqual (2, cd_s.Count() );
		cd_tmp = cd_s.Get (cd1.Name);
		Assert.AreEqual (cd1.Name, cd_tmp.Name);			
		cd_tmp = cd_s.Get (cd3.Name);	
		Assert.AreEqual (cd3.Name, cd_tmp.Name);						
	}	
		
	[Test()]
	public void Del_Account()
	{
		cd_s.Add (cd1);
		cd_tmp = cd_s.Get (cd1.Name);			
		cd_s.Del (cd_tmp);
		Assert.AreEqual (0, cd_s.Count() );		
	}
		
	[Test()]
	public void Del_All()
	{
		cd_s.Add (cd1);
		cd_s.Add (cd2);
		cd_s.Add (cd3);		
		cd_s.Del_All();			
		Assert.AreEqual  ( 0, cd_s.Count() );
	}		

	[Test()]
	public void Change_Account()
	{
		cd_s.Add (cd1);
		cd_tmp = cd_s.Get (cd1.Name);
		cd_tmp.Phone = cd2.Phone;
		cd_tmp.Name = cd2.Name;
		cd_s.Change (cd_tmp);
		cd_tmp2 = cd_s.Get (cd_tmp);
		Assert.AreEqual (cd2.Phone, cd_tmp2.Phone);	
		Assert.AreEqual (cd2.Name, cd_tmp2.Name);									
	}		

	[Test()]
	public void Get_Id()
	{
		cd_s.Add (cd1);
		cd_tmp = cd_s.Get (cd1.Name);
		first_index = cd_tmp.Id;
		cd_tmp2 = cd_s.Get (first_index);
		Assert.AreEqual (cd1.Phone, cd_tmp2.Phone);	
		Assert.AreEqual (cd1.Name, cd_tmp2.Name);									
	}				

	[Test()]
	public void Get_Name()
	{
		cd_s.Add (cd1);
		cd_tmp = cd_s.Get (cd1.Name);
		Assert.AreEqual (cd1.Phone, cd_tmp.Phone);	
		Assert.AreEqual (cd1.Name, cd_tmp.Name);									
	}	

	[Test()]
	public void CountTest()
	{
		Assert.AreEqual  ( 0, cd_s.Count() );
		cd_s.Add (cd1);
		cd1 = cd_s.Get (cd1.Name);
		Assert.AreEqual  ( 1, cd_s.Count() );
		cd_s.Add (cd2);
		cd2 = cd_s.Get (cd2.Name);		
		Assert.AreEqual  ( 2, cd_s.Count() );			
		cd_s.Add (cd3);
		cd3 = cd_s.Get (cd3.Name);		
		Assert.AreEqual  ( 3, cd_s.Count() );	
			
		cd_s.Del (cd1);			
		Assert.AreEqual  ( 2, cd_s.Count() );			
		cd_s.Del (cd2);
		Assert.AreEqual  ( 1, cd_s.Count() );
		cd_s.Del (cd3);
		Assert.AreEqual  ( 0, cd_s.Count() );									
	}		
	
	[Test()]
	public void GetList_Counts()
	{
		Assert.AreEqual (0, (cd_s.GetList()).Count);
		cd_s.Add (cd1) ;
		cd_s.Add (cd2);
		cd_s.Add (cd3);			
		cd1 = cd_s.Get (cd1.Name);				
		cd2 = cd_s.Get (cd2.Name);				
		cd3 = cd_s.Get (cd3.Name);				
		Assert.AreEqual (3, (cd_s.GetList()).Count);
		cd_s.Del (cd1);
		Assert.AreEqual (2, (cd_s.GetList()).Count);			
		cd_s.Del (cd3);			
		cd_s.Del (cd2);						
		Assert.AreEqual (0, (cd_s.GetList()).Count);
	}	
	
	[Test()]
	public void GetList_Contents()
	{
		System.Collections.ArrayList cd_db;
		cd_s.Add (cd1) ;
		cd_s.Add (cd2);	
		cd_s.Add (cd3);	
		cd1 = cd_s.Get (cd1.Name);				
		cd2 = cd_s.Get (cd2.Name);				
		cd3 = cd_s.Get (cd3.Name);						
		cd_db = cd_s.GetList();
		Assert.AreEqual ( cd_db.Count, 3);
		Assert.AreEqual ( (cd_db[0] as ContactDetail).Name, cd2.Name);
		Assert.AreEqual ( (cd_db[1] as ContactDetail).Name, cd3.Name);
		Assert.AreEqual ( (cd_db[2] as ContactDetail).Name, cd1.Name);		
		cd_s.Del (cd1.Name);
		cd_db = cd_s.GetList();
		Assert.AreEqual ( cd_db.Count, 2);			
		Assert.AreEqual ( (cd_db[0] as ContactDetail).Name, cd2.Name);
		Assert.AreEqual ( (cd_db[1] as ContactDetail).Name, cd3.Name);		
	}	

	[Test()]
	public void UniqueIndex()
	{
		cd_s.Add (cd1) ;
		cd_tmp = cd_s.Get (cd1.Name);
		first_index = cd_tmp.Id;															
		cd_tmp= cd_s.Get(first_index);
		Assert.AreEqual (first_index, cd_tmp.Id);			
		cd_s.Add (cd2);
		cd_tmp= cd_s.Get(first_index+1);
		Assert.AreEqual (first_index+1, cd_tmp.Id);	
		cd_s.Add (cd3);
		cd_tmp= cd_s.Get(first_index+2);
		Assert.AreEqual (first_index+2, cd_tmp.Id);	
		cd_s.Del (first_index+1);
		cd_s.Add (cd4);
		cd_tmp= cd_s.Get(first_index+3);
		Assert.AreEqual (first_index+3, cd_tmp.Id);				
	}		

}
}