// AccountHeaderStoreTest.cs
// 
// Copyright (C) 2008 Bengt Thuree
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

//FIXME Need to clear all data in Setup, also need to tie in with data to SecurityStore



using System;
using System.IO;
using NUnit.Framework;

using MPC;
using MPC.Quotes;
using MPC.Accounts;
using MPC.Utils;
using System.Data;

namespace AccountTest
{
	
	
[TestFixture()]
public class AccountHeaderStoreTest
{ 
	public Currency c1, c2, c3, c4, c_tmp, c_tmp2;
	public AccountHeader a1, a2, a3, a4, a_tmp, a_tmp2;
	public AddressDetail ad1, ad2, ad3, ad4, ad_tmp, ad_tmp2;
	public ContactDetail cd1, cd2, cd3, cd4, cd_tmp, cd_tmp2;
	public CurrencyStore cs;
	public AccountHeaderStore a_s;
	public AddressDetailsStore ad_s;
	public ContactDetailsStore cd_s;
	
	int first_index;			

	[TestFixtureSetUp]
	public void OpenDatabaseConnection()
	{
		MPC.Database.SetTestDB();
	}

	[SetUp]
	public void MySetup()
	{
		cs = MPC.Database.GetDatabase.Currencies;
		cs.Del_All();

		a_s = MPC.Database.GetDatabase.Accounts;
		a_s.Del_All();			
		
		ad_s = MPC.Database.GetDatabase.AddressDetails;
		ad_s.Del_All();
		
		cd_s = MPC.Database.GetDatabase.ContactDetails;
		cd_s.Del_All();

		c1 = new Currency("SEK");
		c1.Country = "Swedish Krona";
		c2 = new Currency("USD");
		c2.Country = "US Dollar";
		c3 = new Currency("AUD");
		c3.Country = "Australian Dollar";
		c4 = new Currency("HKD");
		c4.Country = "HongKong Dollar";
			
		cs.Add (c1);
		c1 = cs.Get (c1.Code);
		cs.Add (c2);
		c2 = cs.Get (c2.Code);
		cs.Add (c3);
		c3 = cs.Get (c3.Code);
		cs.Add (c4);
		c4 = cs.Get (c4.Code);			

		ad1 = new AddressDetail();
		ad1.Name = "Melbourne";
		ad2 = new AddressDetail();
		ad2.Name = "Link�ping";
		ad3 = new AddressDetail();
		ad3.Name = "Stockholm";
		ad4 = new AddressDetail();
		ad4.Name = "Guangzhou";
			
		ad_s.Add (ad1);
		ad1 = ad_s.Get (ad1.Name);
		ad_s.Add (ad2);
		ad2 = ad_s.Get (ad2.Name);
		ad_s.Add (ad3);
		ad3 = ad_s.Get (ad3.Name);
		ad_s.Add (ad4);
		ad4 = ad_s.Get (ad4.Name);	

		cd1 = new ContactDetail();
		cd1.Name = "Bengt";
		cd1.Address_id = ad1.Id;
		cd2 = new ContactDetail();
		cd2.Name = "�ke";
		cd2.Address_id = ad2.Id;		
		cd3 = new ContactDetail();
		cd3.Name = "Lars";
		cd3.Address_id = ad3.Id;		
		cd4 = new ContactDetail();
		cd4.Name = "Jade";
		cd4.Address_id = ad1.Id;

		cd_s.Add (cd1);
		cd1 = cd_s.Get (cd1.Name);
		cd_s.Add (cd2);
		cd2 = cd_s.Get (cd2.Name);
		cd_s.Add (cd3);
		cd3 = cd_s.Get (cd3.Name);
		cd_s.Add (cd4);
		cd4 = cd_s.Get (cd4.Name);	
				
		a1 = new AccountHeader();
		a1.Name = "Saving";
		a1.Address_id = ad1.Id;
		a1.Contact_id = cd1.Id;
		a1.Account_nr = "123456789";
		a1.Currency_id = c1.Id;

		a2 = new AccountHeader();
		a2.Name = "Investment";
		a2.Address_id = ad2.Id;
		a2.Contact_id = cd2.Id;
		a2.Account_nr = "123456789";
		a2.Currency_id = c2.Id;

		a3 = new AccountHeader();
		a3.Name = "Play";
		a3.Address_id = ad3.Id;
		a3.Contact_id = cd3.Id;
		a3.Account_nr = "123456789";
		a3.Currency_id = c3.Id;
		
		a4 = new AccountHeader();
		a4.Name = "House";
		a4.Address_id = ad4.Id;
		a4.Contact_id = cd4.Id;
		a4.Account_nr = "123456789";
		a4.Currency_id = c4.Id;
		
		
	}		
		
	[TestFixtureTearDown]
	public void CloseDatabaseConnection()
	{
	}
	
	[Test()]
	public void Add()
	{
		Assert.AreEqual (0, a_s.Count() );					
		a_s.Add (a1);
		Assert.AreEqual (1, a_s.Count() );		
	}
		
	[Test()]
//	[ExpectedException("MPC.Utils.NotUniqueException")]
	[Ignore("WEIRD")]				
	public void Add_SameAccount()
	{
		a_s.Add (a1);
		a_s.Add (a1);			
	}
		
	[Test()]
	public void Del_Id()
	{
		a_s.Add (a1);
		a_s.Add (a2);	
		a_s.Add (a3);			
		Assert.AreEqual (3, a_s.Count() );			
		a_tmp = a_s.Get (a2.Name);
		a_s.Del (a_tmp.Id);
		Assert.AreEqual (2, a_s.Count() );
		a_tmp = a_s.Get (a1.Name);
		Assert.AreEqual (a1.Name, a_tmp.Name);			
		a_tmp = a_s.Get (a3.Name);	
		Assert.AreEqual (a3.Name, a_tmp.Name);						
	}	
		
	[Test()]
	public void Del_Account()
	{
		a_s.Add (a1);
		a_tmp = a_s.Get (a1.Name);			
		a_s.Del (a_tmp);
		Assert.AreEqual (0, a_s.Count() );		
	}
		
	[Test()]
	public void Del_All()
	{
		a_s.Add (a1);
		a_s.Add (a2);
		a_s.Add (a3);		
		a_s.Del_All();			
		Assert.AreEqual  ( 0, a_s.Count() );
	}		

	[Test()]
	public void Change_Account()
	{
		a_s.Add (a1);
		a_tmp = a_s.Get (a1.Name);
		a_tmp.Account_nr = a2.Account_nr;
		a_tmp.Name = a2.Name;
		a_s.Change (a_tmp);
		a_tmp2 = a_s.Get (a_tmp);
		Assert.AreEqual (a2.Account_nr, a_tmp2.Account_nr);	
		Assert.AreEqual (a2.Name, a_tmp2.Name);									
	}		

	[Test()]
	public void Get_Id()
	{
		a_s.Add (a1);
		a_tmp = a_s.Get (a1.Name);
		first_index = a_tmp.Id;
		a_tmp2 = a_s.Get (first_index);
		Assert.AreEqual (a1.Account_nr, a_tmp2.Account_nr);	
		Assert.AreEqual (a1.Name, a_tmp2.Name);									
	}				

	[Test()]
	public void Get_Name()
	{
		a_s.Add (a1);
		a_tmp = a_s.Get (a1.Name);
		Assert.AreEqual (a1.Account_nr, a_tmp.Account_nr);	
		Assert.AreEqual (a1.Name, a_tmp.Name);									
	}	

	[Test()]
	public void CountTest()
	{
		Assert.AreEqual  ( 0, a_s.Count() );
		a_s.Add (a1);
		a1 = a_s.Get (a1.Name);
		Assert.AreEqual  ( 1, a_s.Count() );
		a_s.Add (a2);
		a2 = a_s.Get (a2.Name);		
		Assert.AreEqual  ( 2, a_s.Count() );			
		a_s.Add (a3);
		a3 = a_s.Get (a3.Name);		
		Assert.AreEqual  ( 3, a_s.Count() );	
			
		a_s.Del (a1);			
		Assert.AreEqual  ( 2, a_s.Count() );			
		a_s.Del (a2);
		Assert.AreEqual  ( 1, a_s.Count() );
		a_s.Del (a3);
		Assert.AreEqual  ( 0, a_s.Count() );									
	}		
	
	[Test()]
	public void GetList_Counts()
	{
		Assert.AreEqual (0, (a_s.GetList()).Count);
		a_s.Add (a1) ;
		a_s.Add (a2);
		a_s.Add (a3);			
		a1 = a_s.Get (a1.Name);				
		a2 = a_s.Get (a2.Name);				
		a3 = a_s.Get (a3.Name);				
		Assert.AreEqual (3, (a_s.GetList()).Count);
		a_s.Del (a1);
		Assert.AreEqual (2, (a_s.GetList()).Count);			
		a_s.Del (a3);			
		a_s.Del (a2);						
		Assert.AreEqual (0, (a_s.GetList()).Count);
	}	
	
	[Test()]
	public void GetList_Contents()
	{
		System.Collections.ArrayList a_db;
		a_s.Add (a1) ;
		a_s.Add (a2);	
		a_s.Add (a3);	
		a1 = a_s.Get (a1.Name);				
		a2 = a_s.Get (a2.Name);				
		a3 = a_s.Get (a3.Name);						
		a_db = a_s.GetList();
		Assert.AreEqual ( a_db.Count, 3);
		Assert.AreEqual ( (a_db[0] as AccountHeader).Name, a2.Name);
		Assert.AreEqual ( (a_db[1] as AccountHeader).Name, a3.Name);
		Assert.AreEqual ( (a_db[2] as AccountHeader).Name, a1.Name);		
		a_s.Del (a1.Name);
		a_db = a_s.GetList();
		Assert.AreEqual ( a_db.Count, 2);			
		Assert.AreEqual ( (a_db[0] as AccountHeader).Name, a2.Name);
		Assert.AreEqual ( (a_db[1] as AccountHeader).Name, a3.Name);		
	}	

	[Test()]
	public void UniqueIndex()
	{
		a_s.Add (a1) ;
		a_tmp = a_s.Get (a1.Name);
		first_index = a_tmp.Id;															
		a_tmp= a_s.Get(first_index);
		Assert.AreEqual (first_index, a_tmp.Id);			
		a_s.Add (a2);
		a_tmp= a_s.Get(first_index+1);
		Assert.AreEqual (first_index+1, a_tmp.Id);	
		a_s.Add (a3);
		a_tmp= a_s.Get(first_index+2);
		Assert.AreEqual (first_index+2, a_tmp.Id);	
		a_s.Del (first_index+1);
		a_s.Add (a4);
		a_tmp= a_s.Get(first_index+3);
		Assert.AreEqual (first_index+3, a_tmp.Id);				
	}		

}
}