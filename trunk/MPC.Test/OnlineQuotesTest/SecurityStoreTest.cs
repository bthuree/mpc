// SecurityStoreTest.cs
//
// Copyright (C) 2008 Bengt Thuree
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

//FIXME Need to clear all data in Setup, also need to tie in with data to SecurityStore

using System;
using NUnit.Framework;

using MPC;
using MPC.Quotes;
using MPC.Utils;
using System.Data;
using Npgsql;

namespace QuotesTest
{

[TestFixture()]
public class SecurityStoreTest
{
	public Currency c, c2, c3, c4, c_tmp, c_tmp2;
	public Security s, s2, s3, s4, s_tmp, s_tmp2;
	public QuoteSource qs, qs2, qs3, qs_tmp, qs_tmp2;

	public CurrencyStore cs;
	public SecurityStore ss;
	public QuoteSourceStore qss;

	int first_index;

	[TestFixtureSetUp]
	public void OpenDatabaseConnection()
	{
			MPC.Database.SetTestDB();
	}

	[SetUp]
	public void MySetup()
	{
		cs = MPC.Database.GetDatabase.Currencies;

		qss = MPC.Database.GetDatabase.QuoteSources;
		qss.Del_All();

		ss = MPC.Database.GetDatabase.Securities;
		ss.Del_All();

		c = cs.Get ("SEK");
		c2 = cs.Get ("USD");
		c3 = cs.Get ("AUD");

		qs = new QuoteSource("Yahoo global");
		qs.Uri = "http://finance.yahoo.com/d/quotes.csv?s=%1&f=sl1d1";
		qs2 = new QuoteSource("Yahoo local");
		qs2.Uri = "http://finance.yahoo.se/d/quotes.csv?s=%1&f=sl1d1";
		qs3 = new QuoteSource("Yahoo Denmark");
		qs3.Uri = "http://finance.yahoo.dk/d/quotes.csv?s=%1&f=sl1d1";
		qss.Add (qs);
		qs = qss.Get (qs.Name);
		qss.Add (qs2);
		qs2 = qss.Get (qs2.Name);
		qss.Add (qs3);
		qs3 = qss.Get (qs3.Name);

		s = new Security("ERIC");
		s.Name = "Ericsson";
		s.Currency = c.Id;
		s.Quote_Source = qs.Id;

		s2 = new Security("MSFT");
		s2.Name = "Microsoft";
		s2.Currency = c2.Id;
		s2.Quote_Source = qs2.Id;

		s3 = new Security("SEB Europa Fond");
		s3.Name = "SEB Europa Fond";
		s3.Currency = c3.Id;
		s3.Quote_Source = qs3.Id;

		s4 = new Security("AZN");
		s4.Name = "Amazon";
		s4.Currency = c3.Id;
		s4.Quote_Source = qs3.Id;
		}

	[TestFixtureTearDown]
	public void CloseDatabaseConnection()
	{
	}

	[Test()]
	public void Add_Add()
	{
		Assert.AreEqual  ( 0, ss.Count() );						
		ss.Add (s);
		Assert.AreEqual  ( 1, ss.Count() );						
	}

	[Test()]
	//[ExpectedException("MPC.Utils.NotUniqueException")]
	[Ignore("WEIRD")]				
	public void Add_SameName()
	{
		ss.Add (s);
		ss.Add (s) ;
	}

	[Test()]
	public void Add_AddTwo()
	{
		ss.Add (s);
		ss.Add (s2) ;
		Assert.AreEqual  ( 2, ss.Count() );									
	}

	[Test()]
	[Ignore("WEIRD2")]				
	public void Add_EmptySymbol()
	{
		s.Symbol = "";
		ss.Add (s);
		s.Symbol = null;
		ss.Add (s);
		s.Symbol = String.Empty;
		ss.Add (s) ;
	}

	[Test()]
	public void Del_BySecurity()
	{
		ss.Add (s);
		s_tmp = ss.Get (s.Symbol);
		ss.Del (s_tmp);
		Assert.AreEqual  ( 0, ss.Count() );												
	}

	[Test()]
	public void Del_BySymbol()
	{
		ss.Add (s);
		ss.Del (s.Symbol);
		Assert.AreEqual  ( 0, ss.Count() );															
	}

	[Test()]
	public void Del_ByIndex()
	{
		ss.Add (s);
		s_tmp = ss.Get (s.Symbol);
		first_index = s_tmp.Id;
		ss.Del (first_index);
		Assert.AreEqual  ( 0, ss.Count() );																		
	}

	[Test()]
	[Ignore("WEIRD2")]				
	public void Del_NonExisting()
	{
		ss.Del (0);
		ss.Del (s.Symbol);
	}

	[Test()]
	public void Del_DeleteSameSecurity()
	{
		ss.Add (s);
		ss.Del (s.Symbol);
		Assert.AreEqual  ( 0, ss.Count() );																					
		ss.Del (s.Symbol);
		Assert.AreEqual  ( 0, ss.Count() );																					
	}

	[Test()]
	public void Del_DeleteSameIndex()
	{
		ss.Add (s);
		s_tmp = ss.Get (s.Symbol);
		first_index = s_tmp.Id;
		ss.Del (first_index);
		Assert.AreEqual  ( 0, ss.Count() );																								
		ss.Del (first_index);
		Assert.AreEqual  ( 0, ss.Count() );																								
	}

	[Test()]
	public void Del_CheckCountAfterDelete()
	{
		ss.Add (s);
		s_tmp = ss.Get (s.Symbol);
		first_index = s_tmp.Id;
		ss.Add (s2) ;
		ss.Del (s.Symbol);
		ss.Del (s2.Symbol);
		ss.Add (s);
		ss.Add (s2);
		ss.Del (first_index + 2);
		Assert.AreEqual  ( 1, ss.Count() );
		ss.Del (first_index + 3);
		Assert.AreEqual  ( 0, ss.Count() );
	}


	[Test()]
	public void Del_Add2Delete3Security()
	{
		ss.Add (s); 
		ss.Add (s2);
		ss.Del (s3);
		Assert.AreEqual  ( 2, ss.Count() );			
	}

	[Test()]
	public void Del_Add2Delete3Index()
	{
		ss.Add (s);
		s_tmp = ss.Get (s.Symbol);
		first_index = s_tmp.Id;
		ss.Add (s2) ;
		ss.Del (first_index+3);
		Assert.AreEqual  ( 2, ss.Count() );						
	}

	[Test()]
	public void Del_All()
	{
		ss.Add (s) ;
		ss.Add (s2) ;
		ss.Add (s3) ;
		ss.Del_All() ;
		Assert.AreEqual  ( 0, ss.Count() );
	}

	[Test()]
	public void UniqueIndex()
	{
		ss.Add (s);
		s_tmp = ss.Get (s.Symbol);
		first_index = s_tmp.Id;
		s_tmp= ss.Get(first_index);
		Assert.AreEqual (first_index, s_tmp.Id);
		ss.Add (s2);
		s_tmp= ss.Get(first_index+1);
		Assert.AreEqual (first_index+1, s_tmp.Id);
		ss.Add (s3);
		s_tmp= ss.Get(first_index+2);
		Assert.AreEqual (first_index+2, s_tmp.Id);
		ss.Del (first_index+1);
		ss.Add (s4) ;
		s_tmp= ss.Get(first_index+3);
		Assert.AreEqual (first_index+3, s_tmp.Id);
	}

	[Test()]
	public void Change_Symbol()
	{
		ss.Add (s) ;
		s_tmp = ss.Get (s.Symbol);
		first_index = s_tmp.Id;
		s_tmp= ss.Get(first_index);
		Assert.AreEqual ("Ericsson", s_tmp.Name);
		Assert.AreEqual ("ERIC", s_tmp.Symbol);
		s_tmp.Name = s2.Name;
		s_tmp.Symbol = s2.Symbol;
		ss.Change(s_tmp);
		s_tmp = ss.Get (first_index);
		Assert.AreEqual (s_tmp.Name, s2.Name);
		Assert.AreEqual (s_tmp.Symbol, s2.Symbol);
	}

	[Test()]
	public void ChangeTest2_All()
	{
		ss.Add (s);
		s_tmp = ss.Get (s.Symbol);
		first_index = s_tmp.Id;
		s_tmp = ss.Get(first_index);
		Assert.AreEqual (s_tmp.Name, s.Name);
		Assert.AreEqual (s_tmp.Symbol, s.Symbol);
		Assert.AreEqual (s_tmp.Currency, s.Currency);
		Assert.AreEqual (s_tmp.Quote_Source, s.Quote_Source);
		s_tmp.Name = s2.Name;
		s_tmp.Symbol = s2.Symbol;
		s_tmp.Currency = s2.Currency;
		s_tmp.Quote_Source = s2.Quote_Source;
		ss.Change(s_tmp);
		s_tmp2 = ss.Get (first_index);
		Assert.AreEqual (s_tmp2.Name, s2.Name);
		Assert.AreEqual (s_tmp2.Symbol, s2.Symbol);
		Assert.AreEqual (s_tmp2.Currency, s2.Currency);
		Assert.AreEqual (s_tmp2.Quote_Source, s2.Quote_Source);
	}

	[Test()]
	//[ExpectedException("MPC.Utils.NotUniqueException")]
	[Ignore("WEIRD")]				
	public void Change_NotUnique()
	{
		ss.Add (s);
		ss.Add (s2);
		s_tmp = ss.Get(s2.Symbol);
		s_tmp.Name = s.Name;
		s_tmp.Symbol = s.Symbol;
		ss.Change(s_tmp);
	}


	[Test()]
	public void Get_Symbol()
	{
		ss.Add (s);
		s_tmp = ss.Get   ( s.Symbol );
		Assert.AreEqual ( s_tmp.Name, s.Name);
		Assert.AreEqual ( s_tmp.Symbol, s.Symbol);
	}

	[Test()]
	public void Get_Add2GetSymbol1()
	{
		ss.Add (s) ;
		ss.Add (s2);
		s_tmp = ss.Get(s.Symbol);
		Assert.AreEqual (s_tmp.Name, s.Name);
		Assert.AreEqual ( s_tmp.Symbol, s.Symbol);

	}

	[Test()]
	public void Get_Add2GetSymbol2()
	{
		ss.Add (s);
		ss.Add (s2) ;
		s_tmp = ss.Get(s2.Symbol);
		Assert.AreEqual (s_tmp.Name, s2.Name);
		Assert.AreEqual ( s_tmp.Symbol, s2.Symbol);

	}

	[Test()]
	public void Get_Id()
	{
		ss.Add (s);
		s_tmp = ss.Get (s.Symbol);
		first_index = s_tmp.Id;
		s_tmp = ss.Get   ( first_index );
		Assert.AreEqual ( s_tmp.Name, s.Name);
		Assert.AreEqual ( s_tmp.Symbol, s.Symbol);
	}

	[Test()]
	public void Get_Add2GetId1()
	{
		ss.Add (s) ;
		s_tmp = ss.Get (s.Symbol);
		first_index = s_tmp.Id;
		ss.Add (s2);
		s_tmp = ss.Get(first_index);
		Assert.AreEqual (s_tmp.Name, s.Name);
	}

	[Test()]
	public void Get_Add2GetId2()
	{
		ss.Add (s);
		s_tmp = ss.Get (s.Symbol);
		first_index = s_tmp.Id;
		ss.Add (s2);
		s_tmp = ss.Get(first_index+1);
		Assert.AreEqual (s_tmp.Name, s2.Name);
	}

	[Test()]
	public void CountTest()
	{
		Assert.AreEqual  ( 0, ss.Count() );
		ss.Add (s);
		Assert.AreEqual  ( 1, ss.Count() );
		ss.Add (s2);
		Assert.AreEqual  ( 2, ss.Count() );
		ss.Add (s3);
		Assert.AreEqual  ( 3, ss.Count() );

		ss.Del (s.Symbol) ;
		Assert.AreEqual  ( 2, ss.Count() );
		ss.Del (s2.Symbol);
		Assert.AreEqual  ( 1, ss.Count() );
		ss.Del (s3.Symbol);
		Assert.AreEqual  ( 0, ss.Count() );
	}

	[Test()]
	public void GetList_Counts()
	{
		Assert.AreEqual (0, (ss.GetList()).Count);
		ss.Add (s) ;
		ss.Add (s2);
		ss.Add (s3) ;
		Assert.AreEqual (3, (ss.GetList()).Count);
		ss.Del (s.Symbol);
		Assert.AreEqual (2, (ss.GetList()).Count);
		ss.Del (s3.Symbol) ;
		ss.Del (s2.Symbol) ;
		Assert.AreEqual (0, (ss.GetList()).Count);
	}

	[Test()]
	public void GetList_Contents()
	{
		System.Collections.ArrayList s_db;
		ss.Add (s) ;
		ss.Add (s2);
		ss.Add (s3);
		s_db = ss.GetList();
		Assert.AreEqual ( (s_db[0] as Security).Symbol, s.Symbol);
		Assert.AreEqual ( (s_db[1] as Security).Symbol, s2.Symbol);
		Assert.AreEqual ( (s_db[2] as Security).Symbol, s3.Symbol);
		ss.Del (s.Symbol);
		s_db = ss.GetList();
		Assert.AreEqual ( (s_db[0] as Security).Symbol, s2.Symbol);
		Assert.AreEqual ( (s_db[1] as Security).Symbol, s3.Symbol);
	}

	[Test()]
	public void GetList_QuoteSource()
	{
		System.Collections.ArrayList s_db;
		ss.Add (s) ;
		ss.Add (s2);
		ss.Add (s3);
		ss.Add (s4);
		s_db = ss.GetList(qs);
		Assert.AreEqual (1, (s_db.Count));
		Assert.AreEqual ( (s_db[0] as Security).Quote_Source, qs.Id);			
		s_db = ss.GetList(qs3);			
		Assert.AreEqual (2, (s_db.Count));				
		Assert.AreEqual ( (s_db[0] as Security).Quote_Source, qs3.Id);
		Assert.AreEqual ( (s_db[1] as Security).Quote_Source, qs3.Id);
	}		
		
}
}
