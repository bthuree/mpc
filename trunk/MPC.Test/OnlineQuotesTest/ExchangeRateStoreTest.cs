// ExchangeRateStoreTest.cs
//
// Copyright (er1) 2008 Bengt Thuree
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

//FIXME Need to clear all data in Setup, also need to tie in with data to SecurityStore

using System;
using System.IO;
using NUnit.Framework;

using MPC;
using MPC.Quotes;
using MPC.Utils;
using System.Data;
using Npgsql;

namespace QuotesTest
{


[TestFixture()]
public class ExchangeRateStoreTest
{
	public Currency c1, c2, c3, c4, c_tmp, c_tmp2;
	public ExchangeRate er1, er2, er3, er4, er_tmp, er_tmp2;
	public CurrencyStore cs;
	public ExchangeRateStore ers;
	int first_index;

	[TestFixtureSetUp]
	public void OpenDatabaseConnection()
	{
		MPC.Database.SetTestDB();
	}

	[SetUp]
	public void MySetup()
	{
		cs = MPC.Database.GetDatabase.Currencies;
		cs.Del_All();

		ers = MPC.Database.GetDatabase.ExchangeRates;
		ers.Del_All();			

		c1 = new Currency("SEK");
		c1.Country = "Swedish Krona";
		c2 = new Currency("USD");
		c2.Country = "US Dollar";
		c3 = new Currency("AUD");
		c3.Country = "Australian Dollar";
		c4 = new Currency("HKD");
		c4.Country = "HongKong Dollar";
			
		cs.Add (c1);
		c1 = cs.Get (c1.Code);
		cs.Add (c2);
		c2 = cs.Get (c2.Code);
		cs.Add (c3);
		c3 = cs.Get (c3.Code);
		cs.Add (c4);
		c4 = cs.Get (c4.Code);

		er1 = new ExchangeRate();
		er1.TimeStamp = new DateTime (2008, 12, 24, 13, 30, 0);
		er1.FromCurrency = c1.Id;
		er1.ToCurrency = c2.Id;
		er1.ExchangeRateValue = 2;
		er1.Quote_Type = MPC.Quotes.UpdateTypeEnum.manual;
		er2 = new ExchangeRate();
		er2.FromCurrency = c1.Id;
		er2.ToCurrency = c2.Id;
		er2.TimeStamp = new DateTime (2008, 12, 24, 14, 30, 0);
		er2.ExchangeRateValue = 3;
		er2.Quote_Type = MPC.Quotes.UpdateTypeEnum.online;

		er3 = new ExchangeRate();
		er3.FromCurrency = c1.Id;
		er3.ToCurrency = c3.Id;
		er3.TimeStamp = new DateTime (2008, 12, 26, 18, 30, 0);
		er3.ExchangeRateValue = 0.25m;
		er3.Quote_Type = MPC.Quotes.UpdateTypeEnum.online;

		er4 = new ExchangeRate();
		er4.FromCurrency = c3.Id;
		er4.ToCurrency = c1.Id;
		er4.TimeStamp = new DateTime (2008, 12, 27, 19, 30, 0);
		er4.ExchangeRateValue = 4m;
		er4.Quote_Type = MPC.Quotes.UpdateTypeEnum.buy;
	}

	[TestFixtureTearDown]
	public void CloseDatabaseConnection()
	{
	}

	[Test()]
	public void Add()
	{
		Assert.AreEqual  ( 0, ers.Count() );			
		ers.Add (er1);
		Assert.AreEqual  ( 1, ers.Count() );			
	}

	[Test()]
	// [ExpectedException("MPC.Utils.NotUniqueException")]
	[Ignore("WEIRD")]		
	public void Add_SameQuote()
	{
		ers.Add (er1);
		ers.Add (er1);
	}

	[Test()]
	public void Del_Id()
	{
		ers.Add (er1);
		ers.Add (er2);
		ers.Add (er3);
		er_tmp = ers.GetLastDayQuote (er2);
		ers.Del (er_tmp.Id);
		Assert.AreEqual (2, ers.Count() );
		er_tmp = ers.GetLastDayQuote (er1);
		Assert.AreEqual (er1.ExchangeRateValue, er_tmp.ExchangeRateValue);
		er_tmp = ers.GetLastDayQuote (er3);
		Assert.AreEqual (er3.ExchangeRateValue, er_tmp.ExchangeRateValue);
	}

	[Test()]
	public void Del_All()
	{
		ers.Add (er1);
		ers.Add (er2);
		ers.Add (er3);
		ers.Del_All();
		Assert.AreEqual  ( 0, ers.Count() );
	}

	[Test()]
	public void Change_Quote()
	{
		ers.Add (er1);
		er_tmp = ers.GetLastDayQuote (er1);
		er_tmp.TimeStamp = er3.TimeStamp;
		er_tmp.ExchangeRateValue= er3.ExchangeRateValue;
		er_tmp.Quote_Type = er3.Quote_Type;
		ers.Change (er_tmp);
		er_tmp2 = ers.GetLastDayQuote (er_tmp);
		Assert.AreEqual (er3.TimeStamp, er_tmp2.TimeStamp);
		Assert.AreEqual (er3.ExchangeRateValue, er_tmp2.ExchangeRateValue);
		Assert.AreEqual (er3.Quote_Type, er_tmp2.Quote_Type);
	}

	[Test()]
	public void Get_Id()
	{
		ers.Add (er1);
		er_tmp = ers.GetLastDayQuote (er1);
		first_index = er_tmp.Id;
		er_tmp2 = ers.Get (first_index);
		Assert.AreEqual (er1.FromCurrency, er_tmp2.FromCurrency);
		Assert.AreEqual (er1.ToCurrency, er_tmp2.ToCurrency);
		Assert.AreEqual (er1.ExchangeRateValue, er_tmp2.ExchangeRateValue);
		Assert.AreEqual (er1.Quote_Type, er_tmp2.Quote_Type);
	}

	[Test()]
	public void Get_GetLastDayQuote()
	{
		ers.Add (er2);
		ers.Add (er1);
		er_tmp = ers.GetLastDayQuote (er1);
		Assert.AreEqual (er1.FromCurrency, er_tmp.FromCurrency);
		Assert.AreEqual (er1.ToCurrency, er_tmp.ToCurrency);
		Assert.AreEqual (er1.ExchangeRateValue, er_tmp.ExchangeRateValue);
		Assert.AreEqual (er1.Quote_Type, er_tmp.Quote_Type);
	}

	[Test()]
	public void CountTest()
	{
		Assert.AreEqual  ( 0, ers.Count() );
		ers.Add (er1);
		Assert.AreEqual  ( 1, ers.Count() );
		ers.Add (er2);
		Assert.AreEqual  ( 2, ers.Count() );
		ers.Add (er3);
		Assert.AreEqual  ( 3, ers.Count() );

		er_tmp = ers.GetLastDayQuote (er1);
		ers.Del (er_tmp);	
		Assert.AreEqual  ( 2, ers.Count() );
		er_tmp = ers.GetLastDayQuote (er2);
		ers.Del (er_tmp);
		Assert.AreEqual  ( 1, ers.Count() );
		er_tmp = ers.GetLastDayQuote (er3);
		ers.Del (er_tmp);
		Assert.AreEqual  ( 0, ers.Count() );
	}

	[Test()]
	public void GetList_Counts()
	{
		Assert.AreEqual (0, (ers.GetList()).Count);
		ers.Add (er1) ;
		ers.Add (er2) ;
		ers.Add (er3) ;
		Assert.AreEqual (3, (ers.GetList()).Count);
		er_tmp = ers.GetLastDayQuote (er1);
		ers.Del (er_tmp);	
		Assert.AreEqual (2, (ers.GetList()).Count);
		er_tmp = ers.GetLastDayQuote (er2);
		ers.Del (er_tmp) ;	
		er_tmp = ers.GetLastDayQuote (er3);
		ers.Del (er_tmp);	
		Assert.AreEqual (0, (ers.GetList()).Count);
	}

	[Test()]
	public void GetList_Contents()
	{
		System.Collections.ArrayList er_db;
		ers.Add (er1);
		ers.Add (er2);
		ers.Add (er3);
		er_db = ers.GetList();
		Assert.AreEqual ( (er_db[0] as ExchangeRate).ExchangeRateValue, er1.ExchangeRateValue);
		Assert.AreEqual ( (er_db[1] as ExchangeRate).ExchangeRateValue, er2.ExchangeRateValue);
		Assert.AreEqual ( (er_db[2] as ExchangeRate).ExchangeRateValue, er3.ExchangeRateValue);
		er_tmp = ers.GetLastDayQuote (er1);
		ers.Del (er_tmp) ;	
		er_db = ers.GetList();
		Assert.AreEqual ( (er_db[0] as ExchangeRate).ExchangeRateValue, er2.ExchangeRateValue);
		Assert.AreEqual ( (er_db[1] as ExchangeRate).ExchangeRateValue, er3.ExchangeRateValue);
	}

	[Test()]
	public void UniqueIndex()
	{
		ers.Add (er1);
		er_tmp = ers.GetLastDayQuote (er1);
		first_index = er_tmp.Id;
		er_tmp2 = ers.Get(first_index);
		Assert.AreEqual (first_index, er_tmp2.Id);
		ers.Add (er2);
		er_tmp = ers.Get(first_index+1);
		Assert.AreEqual (first_index+1, er_tmp.Id);
		ers.Add (er3);
		er_tmp = ers.Get(first_index+2);
		Assert.AreEqual (first_index+2, er_tmp.Id);
		ers.Del (first_index+1) ;
		ers.Add (er4) ;
		er_tmp = ers.Get(first_index+3);
		Assert.AreEqual (first_index+3, er_tmp.Id);
	}
		
	[Test()]
	public void Get_GetExchangeRateList()
	{
		System.Collections.ArrayList er_db;
		ers.Add (er1);
		ers.Add (er2);
		ers.Add (er3);
		ers.Add (er4);			
			er_db = ers.GetList (er1);
		Assert.AreEqual (2, er_db.Count);
		Assert.AreEqual ( (er_db[0] as ExchangeRate).ExchangeRateValue, er1.ExchangeRateValue);
		Assert.AreEqual ( (er_db[1] as ExchangeRate).ExchangeRateValue, er2.ExchangeRateValue);
	}		
		

}
}
