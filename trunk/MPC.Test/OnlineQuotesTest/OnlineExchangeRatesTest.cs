// OnlineExchangeRatesTest.cs
// 
// Copyright (C) 2008 Bengt Thuree
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;
using System.IO;
using NUnit.Framework;

using MPC;
using MPC.Quotes;
using MPC.Accounts;
using MPC.Utils;
using System.Data;
using Npgsql;

namespace QuotesTest
{
	
	
	[TestFixture()]
	public class OnlineExchangeRatesTest
	{
		public Currency c1, c2, c3, c4, c5, c_tmp, c_tmp2;		
		public Security s1, s2, s3, s4, s5, s_tmp, s_tmp2;
		public AddressDetail ad1, ad2, ad3, ad4, ad_tmp, ad_tmp2;
		public ContactDetail cd1, cd2, cd3, cd4, cd_tmp, cd_tmp2;
		public AccountHeader a1, a2, a3, a4, a_tmp, a_tmp2;		
		public InvestmentTransaction it1, it2, it3, it4, it5, it_tmp, it_tmp2;		
		public QuoteSource qs1;
		
		public QuoteSourceStore qss;
		public SecurityStore ss;
		public CurrencyStore cs;
		public ExchangeRateStore ers;
		public OnlineExchangeRates oer;
		public NumberOfSharesView nos_v;
		public AccountHeaderStore a_s;				
		public AddressDetailsStore ad_s;
		public ContactDetailsStore cd_s;		
		public InvestmentTransactionStore its;		

		[TestFixtureSetUp]
		public void OpenDatabaseConnection()
		{
			MPC.Database.SetTestDB();
		}

		[SetUp]
		public void MySetup()
		{
			cs = MPC.Database.GetDatabase.Currencies;
			cs.Del_All();

			ss = MPC.Database.GetDatabase.Securities;
			ss.Del_All();

			ers = MPC.Database.GetDatabase.ExchangeRates;
			ers.Del_All();

			qss = MPC.Database.GetDatabase.QuoteSources;
			qss.Del_All();			
			
			ad_s = MPC.Database.GetDatabase.AddressDetails;
			ad_s.Del_All();
			
			cd_s = MPC.Database.GetDatabase.ContactDetails;
			cd_s.Del_All();
				
			a_s = MPC.Database.GetDatabase.Accounts;
			a_s.Del_All();			

			its = MPC.Database.GetDatabase.InvestmentTransactions;
			its.Del_All();
			
			nos_v = MPC.Database.GetDatabase.GetNumberOfSharesView;
			
			oer = new OnlineExchangeRates();
				
			qs1 = new QuoteSource("Yahoo Global");
			qs1.Uri = "http://finance.yahoo.com/d/quotes.csv?s=%1&f=sl1d1";				
			qs1.Date_regexp = "[^,]*,[^,]*,\"([^\"]*)\"";
			qs1.Date_format = "M/d/yyyy";		
			qs1.Quote_regexp = "[^,]*,([^,]*),.*";
			qs1.Symbol_regexp = "\"([^,\"]*)\",.*";			
			qs1.Id = qss.Add_GetIndex (qs1);
			
			c1 = new Currency("SEK");
			c1.Country = "Swedish Krona";
			c1.BasePool = true;
			c2 = new Currency("USD");
			c2.Country = "US Dollar";			
			c3 = new Currency("AUD");
			c3.Country = "Australian Dollar";
			c3.BasePool = true;			
			c4 = new Currency("HKD");
			c4.Country = "HongKong Dollar";
			c5 = new Currency("CNY");
			c5.Country = "Chinese Reminbi";
			c5.BasePool = true;
			
			c1.Id = cs.Add_GetIndex (c1);
			c2.Id = cs.Add_GetIndex (c2);
			c3.Id = cs.Add_GetIndex (c3);
			c4.Id = cs.Add_GetIndex (c4);
			c5.Id = cs.Add_GetIndex (c5);

			ad1 = new AddressDetail();
			ad1.Swift = "swift";
			ad1.Phone = "Phone";
			ad1.Fax = "Fax";
			ad1.Name = "Name";
			ad1.Address1 = "Address1";
			ad1.Address2 = "Address2";
			ad1.Zip	= "zip";
			ad1.City = "city";
			ad1.Country = "country";
			ad1.Uri = "uri";
			ad1.Notes = "notes";
			ad1.Id = ad_s.Add_GetIndex(ad1);

			cd1 = new ContactDetail();
			cd1.Name = "cdBengt";
			cd1.Phone = "cdPhone";
			cd1.Mobile = "cdMobile";
			cd1.Other_phone = "cdOtherPhone";
			cd1.Fax = "Fax";
			cd1.Work_mail = "cdWorkMail";
			cd1.Private_mail = "cdPrivateMail";
			cd1.Other_mail = "cdOtherMail";
			cd1.Address_id	= ad1.Id;
			cd1.Uri = "cdUri";
			cd1.Notes = "cdNotes";
			cd1.Id = cd_s.Add_GetIndex (cd1);
			
			a1 = new AccountHeader();
			a1.Name = "Saving";
			a1.Address_id = ad1.Id;
			a1.Contact_id = cd1.Id;
			a1.Account_nr = "123456789";
			a1.Currency_id = c1.Id;
			a1.Id = a_s.Add_GetIndex (a1);
			
			s1 = new Security("ERIC");
			s1.Name = "Ericsson";
			s1.Quote_Source = qs1.Id;
			s1.Currency = c1.Id;

			s2 = new Security("MSFT");
			s2.Quote_Source = qs1.Id;			
			s2.Name = "Microsoft";
			s2.Currency = c2.Id;

			s3 = new Security("SEB Aktiesparfond");
			s3.Quote_Source = qs1.Id;			
			s3.Name = "SEB Europa Fond";
			s3.Currency = c3.Id;

			s4 = new Security("AZN");
			s4.Quote_Source = qs1.Id;			
			s4.Name = "Amazon";
			s4.Currency = c4.Id;

			s5 = new Security("IBM");
			s5.Quote_Source = qs1.Id;			
			s5.Name = "IBM";
			s5.Currency = c2.Id;
			
			
			s1.Id = ss.Add_GetIndex (s1);
			s2.Id = ss.Add_GetIndex (s2);
			s3.Id = ss.Add_GetIndex (s3);
			s4.Id = ss.Add_GetIndex (s4);
			s5.Id = ss.Add_GetIndex (s5);

			it1 = new InvestmentTransaction();
			it1.InvestmentAccount = a1.Id;
			it1.TimeStamp = new DateTime(2006, 10, 10, 0, 0, 0);
			it1.Memo = "it1";
			it1.Security_id = s1.Id;
			it1.Shares = 100;
			it1.Share_price = 0;
			it1.Reconsiled = false;	
			it1.Id = its.Add_GetIndex (it1);
			it2 = it1;
			it2.Security_id = s2.Id;
			it2.Id = its.Add_GetIndex (it2);
			it3 = it1;
			it3.Security_id = s3.Id;
			it3.Id = its.Add_GetIndex (it3);			
			it4 = it1;
			it4.Security_id = s4.Id;
			it4.Id = its.Add_GetIndex (it4);
			it5 = it1;
			it5.Security_id = s5.Id;
			it5.Id = its.Add_GetIndex (it5);						
		}

		[TestFixtureTearDown]
		public void CloseDatabaseConnection()
		{
		}

		[Test(), Category("Online")]
		public void Add_ExchangeRates()
		{
			Assert.AreEqual  ( 0, ers.Count() );						
			oer.GetExchangeRates();
			Assert.AreEqual  ( 10, ers.Count() );	
		}
			
	}
}
