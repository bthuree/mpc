// SecurityQuoteStoreTest.cs
//
// Copyright (q1) 2008 Bengt Thuree
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

//FIXME Need to clear all data in Setup, also need to tie in with data to SecurityStore

using System;
using System.IO;
using NUnit.Framework;

using MPC;
using MPC.Quotes;
using MPC.Utils;
using System.Data;
using Npgsql;

namespace QuotesTest
{


[TestFixture()]
public class SecurityQuoteStoreTest
{
	public SecurityQuote q1, q2, q3, q4, q_tmp, q_tmp2;
	public QuoteSource qs1, qs2, qs3, qs4, qs_tmp, qs_tmp2;		
	public Currency c1, c2, c3, c4, c_tmp, c_tmp2;		
	public Security s1, s2, s3, s4, s_tmp, s_tmp2;
	public SecurityQuoteStore sq_s;
	public SecurityStore ss;
	public CurrencyStore cs;
	public QuoteSourceStore qss;
	int first_index;

	[TestFixtureSetUp]
	public void OpenDatabaseConnection()
	{
			MPC.Database.SetTestDB();
	}

	[SetUp]
	public void MySetup()
	{
		cs = MPC.Database.GetDatabase.Currencies;

		qss = MPC.Database.GetDatabase.QuoteSources;
		qss.Del_All();			
			
		ss = MPC.Database.GetDatabase.Securities;
		ss.Del_All();

		sq_s = MPC.Database.GetDatabase.SecurityQuotes;
		sq_s.Del_All();

		c1 = cs.Get ("SEK");
		c2 = cs.Get ("USD");
		c3 = cs.Get ("AUD");

		qs1 = new QuoteSource("Yahoo global");
		qs1.Uri = "http://finance.yahoo.com/d/quotes.csv?s=%1&f=sl1d1";
		qs2 = new QuoteSource("Yahoo local");
		qs2.Uri = "http://finance.yahoo.se/d/quotes.csv?s=%1&f=sl1d1";
		qs3 = new QuoteSource("Yahoo Denmark");
		qs3.Uri = "http://finance.yahoo.dk/d/quotes.csv?s=%1&f=sl1d1";
			
		qss.Add (qs1);
		qs1 = qss.Get (qs1.Name);
		qss.Add (qs2);
		qs2 = qss.Get (qs2.Name);
		qss.Add (qs3);
		qs3 = qss.Get (qs3.Name);								
			
		s1 = new Security("ERIC");
		s1.Name = "Ericsson";
		s1.Currency = c1.Id;
		s1.Quote_Source = qs1.Id;

		s2 = new Security("MSFT");
		s2.Name = "Microsoft";
		s2.Currency = c2.Id;
		s2.Quote_Source = qs2.Id;

		s3 = new Security("SEB Europa Fond");
		s3.Name = "SEB Europa Fond";
		s3.Currency = c3.Id;
		s3.Quote_Source = qs3.Id;

		s4 = new Security("AZN");
		s4.Name = "Amazon";
		s4.Currency = c3.Id;
		s4.Quote_Source = qs3.Id;

		ss.Add (s1);
		s1 = ss.Get (s1.Symbol);
		ss.Add (s2);
		s2 = ss.Get (s2.Symbol);
		ss.Add (s3);
		s3 = ss.Get (s3.Symbol);
		ss.Add (s4);
		s4 = ss.Get (s4.Symbol);

		q1 = new SecurityQuote();
		q1.Symbol = s1.Id;
		q1.TimeStamp = new DateTime (2008, 12, 24, 13, 30, 0);
		q1.Quote_Value = 100;
		q1.Quote_Type = MPC.Quotes.UpdateTypeEnum.manual;

		q2 = new SecurityQuote();
		q2.Symbol = s1.Id;
		q2.TimeStamp = new DateTime (2008, 12, 24, 14, 30, 0);
		q2.Quote_Value = 110.00m;
		q2.Quote_Type = MPC.Quotes.UpdateTypeEnum.online;

		q3 = new SecurityQuote();
		q3.Symbol = s1.Id;
		q3.TimeStamp = new DateTime (2008, 12, 26, 18, 30, 0);
		q3.Quote_Value = 120.00m;
		q3.Quote_Type = MPC.Quotes.UpdateTypeEnum.online;

		q4 = new SecurityQuote();
		q4.Symbol = s2.Id;
		q4.TimeStamp = new DateTime (2008, 12, 25, 19, 30, 0);
		q4.Quote_Value = 12000.00m;
		q4.Quote_Type = MPC.Quotes.UpdateTypeEnum.buy;
	}

	private void AddSecurityQuote (decimal _quote, int _security, int year, int month, int day, MPC.Quotes.UpdateTypeEnum type )
	{
		SecurityQuote sq1 = new SecurityQuote();
		sq1.Symbol = _security;
		sq1.TimeStamp = new DateTime (year, month, day, 0, 0, 0);
		sq1.Quote_Value = _quote;
		sq1.Quote_Type = type;
		sq_s.Add (sq1);
	}
	
	[TestFixtureTearDown]
	public void CloseDatabaseConnection()
	{
	}

	[Test()]
	public void Add()
	{
		Assert.AreEqual  ( 0, sq_s.Count() );			
		sq_s.Add (q1);
		Assert.AreEqual  ( 1, sq_s.Count() );			
	}

	[Test()]
		//[ExpectedException("MPC.Utils.NotUniqueException")]
	[Ignore("WEIRD")]		
	public void Online_SameQuoteID()
	{
		sq_s.Add (q2);
		sq_s.Add (q2);
	}
		
	[Test()]
		//[ExpectedException("MPC.Utils.NotUniqueException")]
	[Ignore("WEIRD")]		
	public void Online_SameQuote()
	{
		sq_s.Add (q2);
		q_tmp = q2;
		q_tmp.Id=-1;
		sq_s.Add (q_tmp);
	}	
		
	[Test()]
	public void Online_SameDayDifferentQuote()
	{
		sq_s.Add (q2);
		q_tmp = q2;
		q_tmp.Id=-1;
		q_tmp.Quote_Value = q2.Quote_Value +10;
		sq_s.Add (q_tmp);
	}				

	[Test()]
	public void Del_Id()
	{
		q1.Id = sq_s.Add_GetIndex (q1);
		q2.Id = sq_s.Add_GetIndex (q2);
		q3.Id = sq_s.Add_GetIndex (q3);
		sq_s.Del (q2);
		Assert.AreEqual (2, sq_s.Count() );
		q_tmp = sq_s.GetLastDayQuote (q1);
		Assert.AreEqual (q1.Quote_Value, q_tmp.Quote_Value);
		q_tmp = sq_s.GetLastDayQuote (q3);
		Assert.AreEqual (q3.Quote_Value, q_tmp.Quote_Value);
	}

	[Test()]
	public void Del_All()
	{
		sq_s.Add (q1);
		sq_s.Add (q2);
		sq_s.Add (q3);
		sq_s.Del_All();
		Assert.AreEqual  ( 0, sq_s.Count() );
	}

	[Test()]
	public void Change_Quote()
	{
		q1.Id = sq_s.Add_GetIndex (q1);
		q_tmp = sq_s.Get (q1);
		q_tmp.TimeStamp = q3.TimeStamp;
		q_tmp.Quote_Value= q3.Quote_Value;
		q_tmp.Quote_Type = q3.Quote_Type;
		sq_s.Change (q_tmp);
		q_tmp2 = sq_s.GetLastDayQuote (q_tmp);
		Assert.AreEqual (q3.TimeStamp, q_tmp2.TimeStamp);
		Assert.AreEqual (q3.Quote_Value, q_tmp2.Quote_Value);
		Assert.AreEqual (q3.Quote_Type, q_tmp2.Quote_Type);
	}

	[Test()]
	public void Get_Id()
	{
		sq_s.Add (q1);
		q_tmp = sq_s.GetLastDayQuote (q1);
		first_index = q_tmp.Id;
		q_tmp2 = sq_s.Get (first_index);
		Assert.AreEqual (q1.Symbol, q_tmp2.Symbol);
		Assert.AreEqual (q1.Quote_Value, q_tmp2.Quote_Value);
		Assert.AreEqual (q1.Quote_Type, q_tmp2.Quote_Type);
	}

	[Test()]
	public void Get_GetLastDayQuote()
	{
		sq_s.Add (q2);
		sq_s.Add (q1);
		q_tmp = sq_s.GetLastDayQuote (q1);
		Assert.AreEqual (q1.Symbol, q_tmp.Symbol);
		Assert.AreEqual (q1.Quote_Value, q_tmp.Quote_Value);
		Assert.AreEqual (q1.Quote_Type, q_tmp.Quote_Type);
	}

	[Test()]
	public void Get_date()
	{
		AddSecurityQuote (100, s1.Id, 2006, 10, 2, MPC.Quotes.UpdateTypeEnum.online);				
		AddSecurityQuote (110, s1.Id, 2006, 10, 3, MPC.Quotes.UpdateTypeEnum.online);				
		AddSecurityQuote (120, s1.Id, 2006, 10, 4, MPC.Quotes.UpdateTypeEnum.online);				
		q_tmp = sq_s.GetLastDayQuote (s1.Id, new System.DateTime (2006, 10, 3, 0, 0, 0) );
		Assert.AreEqual (110, q_tmp.Quote_Value);
	}

	[Test()]
	public void Get_date_manual()
	{
		AddSecurityQuote (100, s1.Id, 2006, 10, 2, MPC.Quotes.UpdateTypeEnum.online);				
		AddSecurityQuote (110, s1.Id, 2006, 10, 3, MPC.Quotes.UpdateTypeEnum.manual);				
		AddSecurityQuote (120, s1.Id, 2006, 10, 4, MPC.Quotes.UpdateTypeEnum.online);				
		q_tmp2 = sq_s.GetLastDayQuote (s1.Id, new System.DateTime (2006, 10, 3, 0, 0, 0) );
		Assert.AreEqual (110, q_tmp2.Quote_Value);
	}
	[Test()]
	public void Get_date_manyquotes()
	{
		AddSecurityQuote (100, s1.Id, 2006, 10, 2, MPC.Quotes.UpdateTypeEnum.online);				
		AddSecurityQuote (110, s1.Id, 2006, 10, 3, MPC.Quotes.UpdateTypeEnum.sell);				
		AddSecurityQuote (112, s1.Id, 2006, 10, 3, MPC.Quotes.UpdateTypeEnum.buy);				
		AddSecurityQuote (114, s1.Id, 2006, 10, 3, MPC.Quotes.UpdateTypeEnum.online);				
		AddSecurityQuote (120, s1.Id, 2006, 10, 4, MPC.Quotes.UpdateTypeEnum.online);				
		q_tmp = sq_s.GetLastDayQuote (s1.Id, new System.DateTime (2006, 10, 3, 0, 0, 0) );
		Assert.AreEqual (114, q_tmp.Quote_Value);
	}
	[Test()]
	public void Get_date_noQuote()
	{
		AddSecurityQuote (100, s1.Id, 2006, 10, 2, MPC.Quotes.UpdateTypeEnum.online);				
		AddSecurityQuote (120, s1.Id, 2006, 10, 4, MPC.Quotes.UpdateTypeEnum.online);				
		q_tmp = sq_s.GetLastDayQuote (s1.Id, new System.DateTime (2006, 10, 3, 0, 0, 0) );
		Assert.AreEqual (0, 0);
	}
	[Test()]
	public void Get_Closestdate_NoDate()
	{
		AddSecurityQuote (100, s1.Id, 2006, 10, 2, MPC.Quotes.UpdateTypeEnum.online);				
		AddSecurityQuote (120, s1.Id, 2006, 10, 4, MPC.Quotes.UpdateTypeEnum.online);				
		q_tmp = sq_s.GetQuoteClosestTo (s1.Id, new System.DateTime (2006, 10, 3, 0, 0, 0) );
		Assert.AreEqual (100, q_tmp.Quote_Value);
	}
	[Test()]
	public void Get_Closestdate_NoDateManyQuotes()
	{
		AddSecurityQuote (100, s1.Id, 2006, 10, 2, MPC.Quotes.UpdateTypeEnum.online);				
		AddSecurityQuote (110, s1.Id, 2006, 10, 3, MPC.Quotes.UpdateTypeEnum.sell);				
		AddSecurityQuote (112, s1.Id, 2006, 10, 3, MPC.Quotes.UpdateTypeEnum.buy);				
		AddSecurityQuote (114, s1.Id, 2006, 10, 3, MPC.Quotes.UpdateTypeEnum.online);				
		AddSecurityQuote (120, s1.Id, 2006, 10, 5, MPC.Quotes.UpdateTypeEnum.online);				
		q_tmp = sq_s.GetQuoteClosestTo (s1.Id, new System.DateTime (2006, 10, 4, 0, 0, 0) );
		Assert.AreEqual (114, q_tmp.Quote_Value);
	}
	[Test()]
	public void Get_Closestdate_ManyQuotes()
	{
		AddSecurityQuote (100, s1.Id, 2006, 10, 2, MPC.Quotes.UpdateTypeEnum.online);				
		AddSecurityQuote (110, s1.Id, 2006, 10, 3, MPC.Quotes.UpdateTypeEnum.sell);				
		AddSecurityQuote (112, s1.Id, 2006, 10, 3, MPC.Quotes.UpdateTypeEnum.buy);				
		AddSecurityQuote (114, s1.Id, 2006, 10, 3, MPC.Quotes.UpdateTypeEnum.online);				
		AddSecurityQuote (120, s1.Id, 2006, 10, 5, MPC.Quotes.UpdateTypeEnum.online);				
		q_tmp = sq_s.GetQuoteClosestTo (s1.Id, new System.DateTime (2006, 10, 5, 0, 0, 0) );
		Assert.AreEqual (120, q_tmp.Quote_Value);
	}
	
	[Test()]
	public void CountTest()
	{
		Assert.AreEqual  ( 0, sq_s.Count() );
		q1.Id = sq_s.Add_GetIndex (q1) ;
		Assert.AreEqual  ( 1, sq_s.Count() );
		q2.Id = sq_s.Add_GetIndex (q2) ;
		Assert.AreEqual  ( 2, sq_s.Count() );
		q3.Id = sq_s.Add_GetIndex (q3) ;
		Assert.AreEqual  ( 3, sq_s.Count() );

		sq_s.Del (q1);	
		Assert.AreEqual  ( 2, sq_s.Count() );
		sq_s.Del (q2);
		Assert.AreEqual  ( 1, sq_s.Count() );
		sq_s.Del (q3);
		Assert.AreEqual  ( 0, sq_s.Count() );
	}

	[Test()]
	public void GetList_Counts()
	{
		Assert.AreEqual (0, (sq_s.GetList()).Count);
		q1.Id = sq_s.Add_GetIndex (q1) ;
		q2.Id = sq_s.Add_GetIndex (q2) ;
		q3.Id = sq_s.Add_GetIndex (q3) ;
		Assert.AreEqual (3, (sq_s.GetList()).Count);
		sq_s.Del (q1);	
		Assert.AreEqual (2, (sq_s.GetList()).Count);
		sq_s.Del (q2) ;	
		sq_s.Del (q3);	
		Assert.AreEqual (0, (sq_s.GetList()).Count);
	}

	[Test()]
	public void GetList_Contents()
	{
		System.Collections.ArrayList q_db;
		q1.Id = sq_s.Add_GetIndex (q1) ;
		q2.Id = sq_s.Add_GetIndex (q2) ;
		q3.Id = sq_s.Add_GetIndex (q3) ;
		q_db = sq_s.GetList();
		Assert.AreEqual ( (q_db[0] as SecurityQuote).Quote_Value, q1.Quote_Value);
		Assert.AreEqual ( (q_db[1] as SecurityQuote).Quote_Value, q2.Quote_Value);
		Assert.AreEqual ( (q_db[2] as SecurityQuote).Quote_Value, q3.Quote_Value);
		sq_s.Del (q1) ;	
		q_db = sq_s.GetList();
		Assert.AreEqual ( (q_db[0] as SecurityQuote).Quote_Value, q2.Quote_Value);
		Assert.AreEqual ( (q_db[1] as SecurityQuote).Quote_Value, q3.Quote_Value);
	}

	[Test()]
	public void GetList_Security()
	{
		q1.Id = sq_s.Add_GetIndex (q1) ;
		q2.Id = sq_s.Add_GetIndex (q2) ;
		q3.Id = sq_s.Add_GetIndex (q3) ;
		q4.Id = sq_s.Add_GetIndex (q4) ;			
		Assert.AreEqual (2, sq_s.GetList(s1, q4.TimeStamp).Count);
		sq_s.Del (q2);	
		Assert.AreEqual (1, (sq_s.GetList(s1, q4.TimeStamp)).Count);
	}		
		
	[Test()]
	public void UniqueIndex()
	{
		q1.Id = sq_s.Add_GetIndex (q1);
		first_index = q1.Id;
		q_tmp2= sq_s.Get(first_index);
		Assert.AreEqual (first_index, q_tmp2.Id);
		sq_s.Add (q2);
		q_tmp= sq_s.Get(first_index+1);
		Assert.AreEqual (first_index+1, q_tmp.Id);
		sq_s.Add (q3);
		q_tmp= sq_s.Get(first_index+2);
		Assert.AreEqual (first_index+2, q_tmp.Id);
		sq_s.Del (first_index+1) ;
		sq_s.Add (q4) ;
		q_tmp= sq_s.Get(first_index+3);
		Assert.AreEqual (first_index+3, q_tmp.Id);
	}

}
}
