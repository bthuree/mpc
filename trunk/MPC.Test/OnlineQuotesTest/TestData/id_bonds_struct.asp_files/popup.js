///////////////////////////////////////////////////////////////////////////
// Javascript f�r att bygga/visa en beskrivningsruta i eget lager
//
// Exempel p� kod i programmet som anropar detta script: 
// I head-taggen: <script language="JavaScript1.2" src="js/popup.js"></script>
//
// <!-- Lagret med beskrivningsrutan -->
// <DIV ID="lref" CLASS="popup" STYLE="position:absolute;z-index:1;visibility:inherit;"></DIV>
//
// <!-- Skicka med b�de den f�rdiga texten som ska finnas i beskrivningsrutan samt �nskad bredd p� rutan i pixlar -->
// <A class=popup onMouseOver="ShowInformation('Beskrivningstext','Storlek'); return true" onMouseOut="HideInformation(); return true" HREF="L�nk">L�nknamn</a>
// Om det inte finns n�gon l�nk utan det �r endast en beskrivningsruta som ska visas b�r HREF="L�nk" tas bort fr�n ovan
//
///////////////////////////////////////////////////////////////////////////

var over = '';


// Vilken browser anv�nds
function b_browser() {
	this.isNetscape4 = navigator.appName == "Netscape" ? true:false;
	this.DOM = document.getElementById ? true:false;
	this.isExplorer4 = document.all ? true:false;
	return this;
}

// H�mtar objektet
function trinity()
{
	if (browser.isNetscape4) {
		var over = document.lref;
	} 
	else {
		var over = lref.style;
	}
}

// Best�mmer var beskrivningsrutan ska ligga, snett ned till h�ger fr�n cursorn
function mouseMove(e) {
	if (browser.isNetscape4) {
		x=e.pageX+20;
		y=e.pageY+5;
	}
	if (browser.isExplorer4) {
		x=event.x+25; 
		y=event.y+10;
	}
	if (browser.DOM) {
		x=event.x+document.body.scrollLeft+25; 
		y=event.y+document.body.scrollTop+10;
	}
}

// Skapa och visa beskrivningsrutan
function ShowInformation(text, tablesize) {
//	var sHTML = "<TABLE WIDTH=" + tablesize + " BORDER=0 CELLPADDING=0 CELLSPACING=0 CLASS=popupborder>";
	var sHTML = "<TABLE WIDTH=" + tablesize + " BORDER=0 CELLPADDING=1 CELLSPACING=0 BGCOLOR=#000000>";
			sHTML = sHTML + "<TR>";
			sHTML = sHTML + "<TD>";
			sHTML = sHTML + "<TABLE WIDTH=100% BORDER=0 CELLPADDING=2 CELLSPACING=0 BGCOLOR=#FFFFCC>";
			sHTML = sHTML + "<TR>";
			sHTML = sHTML + "<TD ALIGN=left><FONT FACE=verdana,arial,helvetica SIZE=1 COLOR=#000000>" + text + "</FONT></TD>";
			sHTML = sHTML + "</TR>";
			sHTML = sHTML + "</TABLE>";
 			sHTML = sHTML + "</TD>";
			sHTML = sHTML + "</TR>";
			sHTML = sHTML + "</TABLE>";

	if(browser.isNetscape4) {

		document.lref.top = y;
		document.lref.left = x;
		document.lref.zIndex = 100;
		Lref = document.lref.document;
		Lref.write(sHTML);
		Lref.close();
		document.lref.visibility = "show";
		Lref = null;
		
	} 
	else {
		document.all["lref"].innerHTML = sHTML;
		document.all["lref"].style.top = y;
		document.all["lref"].style.left = x;
		document.all["lref"].style.visibility = "visible";
	}
}

// Tag bort beskrivningsrutan
function HideInformation() {
	if(browser.isNetscape4)	{
		var myover = document.lref;
		myover.visibility = "hide";
		myover = null;
	} 
	else {
		document.all["lref"].innerHTML = "";
		document.all["lref"].style.visibility = "hidden";
	}
}

var browser = new b_browser;

document.onmousemove = mouseMove;
if (browser.isNetscape4) document.captureEvents(Event.MOUSEMOVE);
