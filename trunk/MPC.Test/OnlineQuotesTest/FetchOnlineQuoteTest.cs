// FetchOnlineQuoteTest.cs
// 
// Copyright (C) 2008 Bengt Thuree
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//



using System;
using NUnit.Framework;
using MPC.Quotes;

namespace QuotesTest
{

public static class TestInit
{
	public static string TestPathBase = "file:///home/bengt/Development/mpc/";
	public static string TestPathTag = "trunk";
	public static string TestPath;
	public static void Init()
	{ 
		TestPath = String.Format("{0}/{1}", TestPathBase, TestPathTag);
	}
}
	

[TestFixture()]
public class FetchOnlineQuoteGeneric
{

	private QuoteSource qss;
	private FetchOnlineQuote foq;

	[SetUp]
	public void MySetup()
	{
		TestInit.Init();
		qss = new QuoteSource ("Yahoo Global");
		qss.Uri = "http://finance.yahoo.com/d/quotes.csv?s=%1&f=sl1d1";		
		qss.Date_regexp = "[^,]*,[^,]*,\"([^\"]*)\"";
		qss.Date_format = "M/d/yyyy";
		qss.Quote_regexp = "[^,]*,([^,]*),.*";
		qss.Symbol_regexp = "\"([^,\"]*)\",.*";
		foq = new FetchOnlineQuote (qss, "ERIC" );
	}

	[TearDown]
	public void MyTeardown()
	{
	}
	
	[Test()]
	public void ConvertQuoteStr_0()
	{
		Assert.AreEqual (0, foq.ConvertQuoteStr("0")); 	
		Assert.AreEqual (0, foq.ConvertQuoteStr("0.00")); 
		Assert.AreEqual (0, foq.ConvertQuoteStr("0,00")); 		 					
		
	}	

	[Test()]
	public void ConvertQuoteStr_Positive10()
	{
		Assert.AreEqual (10, foq.ConvertQuoteStr("10")); 		 		
	}	

	[Test()]
	public void ConvertQuoteStr_Negative10()
	{
		Assert.AreEqual (-10, foq.ConvertQuoteStr("-10")); 		 		
	}	

	[Test()]
	public void ConvertQuoteStr_ThousandSeparator1()
	{
		Assert.AreEqual (12345.6789, foq.ConvertQuoteStr("12,345.6789")); 		 		
	}	

	[Test()]
	public void ConvertQuoteStr_ThousandSeparator2()
	{
		Assert.AreEqual (12345.6789, foq.ConvertQuoteStr("12.345,6789")); 		 		
	}	

	[Test()]
	public void ConvertQuoteStr_ThousandSeparator3()
	{
		Assert.AreEqual (12345.6789, foq.ConvertQuoteStr("12 345,6789")); 		 		
	}	

	[Test()]
	public void ConvertQuoteStr_ThousandSeparator4()
	{
		Assert.AreEqual (12345.6789, foq.ConvertQuoteStr("12 345.6789")); 		 		
	}	

	[Test()]
	public void ConvertQuoteStr_Decimals1()
	{
		Assert.AreEqual (12345.6789, foq.ConvertQuoteStr("12345.6789")); 
	}	

	[Test()]
	public void ConvertQuoteStr_Decimals2()
	{
		Assert.AreEqual (12345.6789, foq.ConvertQuoteStr("12345,6789")); 	 		
	}	
	
	[Test()]
	public void ConvertDateStr_YYYY_MM_DD()
	{
		DateTime dat = foq.ConvertDateStr ("2007-11-12", "yyyy-MM-dd");
		Assert.AreEqual(11, dat.Month); 		
		Assert.AreEqual(12, dat.Day); 		
		Assert.AreEqual(2007, dat.Year); 		
	}

	[Test()]
	public void ConvertDateStr_null()
	{
		DateTime dat = foq.ConvertDateStr (null, "%y %m %d");
		Assert.AreEqual(DateTime.MinValue.Month, dat.Month); 		
		Assert.AreEqual(DateTime.MinValue.Day, dat.Day); 		
		Assert.AreEqual(DateTime.MinValue.Year, dat.Year); 		
	}

	[Test()]
	public void ConvertDateStr_empty1()
	{
		DateTime dat = foq.ConvertDateStr ("", "%y %m %d");
		Assert.AreEqual(DateTime.MinValue.Month, dat.Month); 		
		Assert.AreEqual(DateTime.MinValue.Day, dat.Day); 		
		Assert.AreEqual(DateTime.MinValue.Year, dat.Year); 		
	}

	[Test()]
	public void ConvertDateStr_empty2()
	{
		DateTime dat = foq.ConvertDateStr ("   	  	", "%y %m %d");
		Assert.AreEqual(DateTime.MinValue.Month, dat.Month); 		
		Assert.AreEqual(DateTime.MinValue.Day, dat.Day); 		
		Assert.AreEqual(DateTime.MinValue.Year, dat.Year); 		
	}

	[Test()]
	public void ConvertDateStr_na()
	{
		DateTime dat = foq.ConvertDateStr ("n/a", "%y %m %d");
		Assert.AreEqual(DateTime.MinValue.Month, dat.Month); 		
		Assert.AreEqual(DateTime.MinValue.Day, dat.Day); 		
		Assert.AreEqual(DateTime.MinValue.Year, dat.Year); 		
	}

	[Test()]
	public void ConvertDateStr_NA()
	{
		DateTime dat = foq.ConvertDateStr ("N/A", "%y %m %d");
		Assert.AreEqual(DateTime.MinValue.Month, dat.Month); 		
		Assert.AreEqual(DateTime.MinValue.Day, dat.Day); 		
		Assert.AreEqual(DateTime.MinValue.Year, dat.Year); 		
	}


	[Test()]
	public void ConvertDateStr_DD_MM_YYYY()
	{
		DateTime dat = foq.ConvertDateStr ("12-11-2007", "dd-MM-yyyy");
		Assert.AreEqual(11, dat.Month); 		
		Assert.AreEqual(12, dat.Day); 		
		Assert.AreEqual(2007, dat.Year); 		
	}

	[Test()]
	public void ConvertDateStr_D_M_YYYY()
	{
		DateTime dat = foq.ConvertDateStr ("2-1-2007", "d-M-yyyy");
		Assert.AreEqual(1, dat.Month); 		
		Assert.AreEqual(2, dat.Day); 		
		Assert.AreEqual(2007, dat.Year); 		
	}

	[Test()]
	public void ConvertDateStr_D_M_YYYY2()
	{
		DateTime dat = foq.ConvertDateStr ("02-01-2007", "dd-MM-yyyy");
		Assert.AreEqual(1, dat.Month); 		
		Assert.AreEqual(2, dat.Day); 		
		Assert.AreEqual(2007, dat.Year); 		
	}


	[Test()]
	public void ConvertDateStr_DD_MM_YYYY_hyphen()
	{
		DateTime dat = foq.ConvertDateStr ("12-11-2007", "dd-MM-yyyy");
		Assert.AreEqual(11, dat.Month); 		
		Assert.AreEqual(12, dat.Day); 		
		Assert.AreEqual(2007, dat.Year); 		
	}

	[Test()]
	public void ConvertDateStr_DD_MM_YYYY_dot()
	{
		DateTime dat = foq.ConvertDateStr ("12.11.2007", "dd.MM.yyyy");
		Assert.AreEqual(11, dat.Month); 		
		Assert.AreEqual(12, dat.Day); 		
		Assert.AreEqual(2007, dat.Year); 		
	}

	[Test()]
	public void ConvertDateStr_DD_MM_YYYY_comma()
	{
		DateTime dat = foq.ConvertDateStr ("12,11,2007", "dd,MM,yyyy");
		Assert.AreEqual(11, dat.Month); 		
		Assert.AreEqual(12, dat.Day); 		
		Assert.AreEqual(2007, dat.Year); 		
	}
	
	[Test()]
	public void ConvertDateStr_DD_MM_YYYY_division()
	{
		DateTime dat = foq.ConvertDateStr ("12/11/2007", "dd/MM/yyyy");
		Assert.AreEqual(11, dat.Month); 		
		Assert.AreEqual(12, dat.Day); 		
		Assert.AreEqual(2007, dat.Year); 		
	}

	[Test()]
	public void ConvertDateStr_DD_MM_YYYY_space()
	{
		DateTime dat = foq.ConvertDateStr ("12 11 2007", "dd MM yyyy");
		Assert.AreEqual(11, dat.Month); 		
		Assert.AreEqual(12, dat.Day); 		
		Assert.AreEqual(2007, dat.Year); 		
	}

	[Test()]
	public void ConvertDateStr_MMM_DD_YYYY()
	{
		DateTime dat = foq.ConvertDateStr ("Nov-12-2007", "MMM-dd-yyyy");
		Assert.AreEqual(11, dat.Month); 		
		Assert.AreEqual(12, dat.Day); 		
		Assert.AreEqual(2007, dat.Year); 		
	}

	[Test()]
	public void ConvertDateStr_MM_DD_YYYY1()
	{
		DateTime dat = foq.ConvertDateStr ("11/12/2007", "MM/dd/yyyy");
		Assert.AreEqual(11, dat.Month); 		
		Assert.AreEqual(12, dat.Day); 		
		Assert.AreEqual(2007, dat.Year); 		
	}

	[Test()]
	public void ConvertDateStr_MM_DD_YYYY2()
	{
		DateTime dat = foq.ConvertDateStr ("11 12, 2007", "MM dd, yyyy");
		Assert.AreEqual(11, dat.Month); 		
		Assert.AreEqual(12, dat.Day); 		
		Assert.AreEqual(2007, dat.Year); 		
	}

	[Test()]
	public void ConvertDateStr_MM_DD_YYYY3()
	{
		DateTime dat = foq.ConvertDateStr ("Nov 12, 2007", "MMM dd, yyyy");
		Assert.AreEqual(11, dat.Month); 		
		Assert.AreEqual(12, dat.Day); 		
		Assert.AreEqual(2007, dat.Year); 		
	}

	[Test()]
	public void ConvertDateStr_DD_MM()
	{
		DateTime dat = foq.ConvertDateStr ("12 Nov", "dd MMM");
		Assert.AreEqual(11, dat.Month); 		
		Assert.AreEqual(12, dat.Day); 		
		Assert.AreEqual(System.DateTime.Now.Year, dat.Year); 		
	}
	
	
	[Test()]
	public void ConvertDateStr_MM_DD_YY1()
	{
		DateTime dat = foq.ConvertDateStr ("11/12/07", "MM/dd/yy");
		Assert.AreEqual(11, dat.Month); 		
		Assert.AreEqual(12, dat.Day); 		
		Assert.AreEqual(2007, dat.Year); 		
	}

	[Test()]
	public void ConvertDateStr_MM_DD_YY2()
	{
		DateTime dat = foq.ConvertDateStr ("11/12/27", "MM/dd/yy");
		Assert.AreEqual(11, dat.Month); 		
		Assert.AreEqual(12, dat.Day); 		
		Assert.AreEqual(2027, dat.Year); 		
	}
	
	[Test()]
	public void ConvertDateStr_MM_DD_YY3()
	{
		DateTime dat = foq.ConvertDateStr ("11/12/97", "MM/dd/yy");
		Assert.AreEqual(11, dat.Month); 		
		Assert.AreEqual(12, dat.Day); 		
		Assert.AreEqual(1997, dat.Year); 		
	}

	[Test()]
	public void ConvertDateStr_MM_DD_YY4()
	{
		DateTime dat = foq.ConvertDateStr ("11/12/77", "MM/dd/yy");
		Assert.AreEqual(11, dat.Month); 		
		Assert.AreEqual(12, dat.Day); 		
		Assert.AreEqual(1977, dat.Year); 		
	}
	
	[Test()]
	public void BuildQuoteUri_empty()
	{
		Assert.AreEqual ("http://test.url.com", foq.BuildQuoteUri("http://test.url.com", "")); 
		Assert.AreEqual ("http://test.url.com", foq.BuildQuoteUri("http://test.url.com", null)); 
		Assert.AreEqual ("http://test.url.com", foq.BuildQuoteUri("http://test.url.com", "ERIC")); 
	}	

	[Test()]
	public void BuildQuoteUri_MiddleParameter()
	{
		Assert.AreEqual ("http://test.url.com?s=_f=abc", foq.BuildQuoteUri("http://test.url.com?s=%1_f=abc", "")); 
		Assert.AreEqual ("http://test.url.com?s=_f=abc", foq.BuildQuoteUri("http://test.url.com?s=%1_f=abc", null)); 
		Assert.AreEqual ("http://test.url.com?s=ERIC_f=abc", foq.BuildQuoteUri("http://test.url.com?s=%1_f=abc", "ERIC")); 
	}	
	
	[Test()]
	public void BuildQuoteUri_EndParameter()
	{
		Assert.AreEqual ("http://test.url.com?s=", foq.BuildQuoteUri("http://test.url.com?s=%1", "")); 
		Assert.AreEqual ("http://test.url.com?s=", foq.BuildQuoteUri("http://test.url.com?s=%1", null)); 
		Assert.AreEqual ("http://test.url.com?s=ERIC", foq.BuildQuoteUri("http://test.url.com?s=%1", "ERIC")); 
	}	

	[Test()]
	public void ValidateSymbolData()
	{
		Assert.IsTrue (foq.ValidateSymbolData("ERIC", "ERIC")); 
		Assert.IsTrue (foq.ValidateSymbolData("eric", "ERIC")); 
		Assert.IsFalse (foq.ValidateSymbolData("ERIC ", "ERIC")); 
	}	

	[Test()]
	public void IsItHtmlPage()
	{
		Assert.IsTrue  (foq.IsItHtmlPage("<html>")); 
		Assert.IsTrue  (foq.IsItHtmlPage("   <html>")); 
		Assert.IsTrue  (foq.IsItHtmlPage("	 	 		  <html>")); 
		Assert.IsTrue  (foq.IsItHtmlPage("	 	 \n\n		  <html>")); 
		Assert.IsFalse (foq.IsItHtmlPage(""));
		Assert.IsFalse (foq.IsItHtmlPage("This is not an <html> page"));
		Assert.IsFalse (foq.IsItHtmlPage(null));
	}

	[Test()]
	public void StoreInpageInTempFile()
	{
		qss.Encoding = "ISO-8859-1";		
		System.IO.Stream st = null;
		System.IO.StreamReader sr = null;
		System.Net.WebResponse resp = null;
		string test_file   = TestInit.TestPath + "/MPC.Test/OnlineQuotesTest/TestData/Senaste_fondkurserna.TXT";
		System.Net.WebRequest req = System.Net.WebRequest.Create( test_file );
		resp = req.GetResponse();
		st = resp.GetResponseStream();
		sr = new System.IO.StreamReader (st, System.Text.Encoding.GetEncoding("ISO-8859-1"));
		string test_string = sr.ReadToEnd();
		sr.Close();
		st.Close();
		resp.Close();
		string temp_file   = foq.StoreInpageInTempFile(test_string);
		req = System.Net.WebRequest.Create( temp_file );
		resp = req.GetResponse();
		st = resp.GetResponseStream();
		sr = new System.IO.StreamReader (st, System.Text.Encoding.GetEncoding("ISO-8859-1"));
		string temp_string = sr.ReadToEnd();
		sr.Close();
		st.Close();
		resp.Close();
		Assert.AreEqual( test_string, temp_string );
		System.IO.File.Delete (temp_file);
	}
	
	[Test()]
	public void BuildLynxCommand_UTF()
	{
		string in_file_test = "/tmp/INFILE.test";
		string real_lynx_cmd = "-nomargins -dump -nolist -force_html /tmp/INFILE.test";
		string lynx_cmd = foq.BuildLynxArguments (in_file_test);
		Assert.AreEqual( real_lynx_cmd, lynx_cmd );	
	}
	
	[Test()]
	public void BuildLynxCommand_ISO()
	{
		qss.Encoding = "ISO-8859-1";	
		string in_file_test = "/tmp/INFILE.test";
		string real_lynx_cmd = "-nomargins -dump -nolist -force_html -display_charset UTF8 -assume_charset ISO-8859-1 /tmp/INFILE.test";
		string lynx_cmd = foq.BuildLynxArguments (in_file_test);
		Assert.AreEqual( real_lynx_cmd, lynx_cmd );	
	}

	[Test()]
	public void RemoveHtmlTags_NoHTML()
	{
		string no_html_txt1 = "This web page do not have HTML tags";
		string no_html_txt2 = "This web <html> page do not have HTML tags";
		string no_html_txt3 = "This web page do not have HTML tags<html>";
		Assert.AreEqual (no_html_txt1, foq.RemoveHtmlTags (no_html_txt1));
		Assert.AreEqual (no_html_txt2, foq.RemoveHtmlTags (no_html_txt2));	
		Assert.AreEqual (no_html_txt3, foq.RemoveHtmlTags (no_html_txt3));
	}



	[Test()]
	[Ignore("skip LYNX for now")]		
	public void RemoveHtmlTags_HTML()
	{
		string html_txt1 = "<html><head><title>Title</title></head><body>This web page do have HTML tags</body></html>";
		string nohtml_txt1 = "This web page do have HTML tags\n\n";		
		Assert.AreEqual (nohtml_txt1, foq.RemoveHtmlTags (html_txt1));
	}

	[Test()]
	[Ignore("skip LYNX for now")]				
	public void RemoveHtmlTags_HTML_swe()
	{
		string html_txt1 = "<html><head><title>Title</title></head><body>This web page (åäö) do have HTML tags</body></html>";
		string nohtml_txt1 = "This web page (åäö) do have HTML tags\n\n";
		qss.Encoding = "ISO-8859-1";					
		Assert.AreEqual (nohtml_txt1, foq.RemoveHtmlTags (html_txt1));
	}

	[Test()]
	public void TodaysDate()
	{
		qss = new QuoteSource ("Test File");
		qss.Uri = TestInit.TestPath + "/Quotes/QuotesTest/TestData/quotes.csv";
		qss.Date_regexp = "<today>";
		qss.Quote_regexp = "[^,]*,([^,]*),.*";
		qss.Symbol_regexp = "\"([^,\"]*)\",.*";
		foq = new FetchOnlineQuote (qss, "ERIC" );
		foq.GetOnlineQuote();
		Assert.AreEqual(System.DateTime.Now.Month, foq.Date.Month); 		
		Assert.AreEqual(System.DateTime.Now.Day, foq.Date.Day); 		
		Assert.AreEqual(System.DateTime.Now.Year, foq.Date.Year); 		
	}



}

[TestFixture()]
public class FetchOnlineQuoteYahoo
{
	private QuoteSource qss;
	private FetchOnlineQuote foq;

	[SetUp]
	public void MySetup()
	{
		TestInit.Init();	
		qss = new QuoteSource ("Yahoo Global");
		qss.Uri = "http://finance.yahoo.com/d/quotes.csv?s=%1&f=sl1d1";				
		qss.Date_regexp = "[^,]*,[^,]*,\"([^\"]*)\"";
		qss.Date_format = "M/d/yyyy";		
		qss.Quote_regexp = "[^,]*,([^,]*),.*";
		qss.Symbol_regexp = "\"([^,\"]*)\",.*";
	}

	[TearDown]
	public void MyTeardown()
	{
	}
	
	[Test(), Category("Online")]
	public void FetchOnlineYahooEricQuote()
	{
		foq = new FetchOnlineQuote (qss, "ERIC" );	
		foq.GetOnlineQuote();
		Assert.IsTrue(foq.Success); 
		Assert.AreEqual("ERIC", foq.Symbol); 				
	}
	
	[Test()]
	public void FetchFileYahooEricQuote()
	{
		qss.Uri = TestInit.TestPath + "/MPC.Test/OnlineQuotesTest/TestData/quotes.csv";
		foq = new FetchOnlineQuote (qss, "ERIC" );
		foq.GetOnlineQuote();
		Assert.IsTrue(foq.Success); 
		Assert.AreEqual("ERIC", foq.Symbol); 		
		Assert.AreEqual(23.49, foq.Quote_value);
		Assert.AreEqual(12, foq.Date.Month); 		
		Assert.AreEqual(26, foq.Date.Day); 		
		Assert.AreEqual(2007, foq.Date.Year); 		
	}
}	

[TestFixture()]
public class FetchOnlineQuoteSEB
{
	private QuoteSource qss;
	private FetchOnlineQuote foq;

	[SetUp]
	public void MySetup()
	{
		TestInit.Init();	
	}

	[TearDown]
	public void MyTeardown()
	{
	}
	
	private void SetupSEBFond(string uri)
	{
		qss = new QuoteSource ("SEB Fonds");
		qss.Uri = uri;
		qss.Date_regexp = "^(.*);.*;.*;.*";
		qss.Date_format = "yyyy-MM-dd";
		qss.Quote_regexp = "^.*;.*;(.*);.*";
		qss.Symbol_regexp = "^.*;(.*);.*;.*";
		qss.Encoding = "ISO-8859-1";			
	}

	private void SetupSEBStruktObl(string uri)
	{
		qss = new QuoteSource ("SEB Strukt Obl");
		qss.Uri = uri;		
		qss.Date_regexp = @"Uppdaterad: ([0-9-]*).*";
		qss.Date_row_regexp = "Uppdaterad:";
		qss.Quote_regexp = ".*SEB Hel Retail[ ]*[^ ]*[ ]*[^ ]*[ ]*[^ ]*[ ]*[^ ]*[ ]*([^ ]*) .*";
		qss.Symbol_regexp = "[ ]*([.]*) SEB Hel Retail.*";
		qss.Encoding = "ISO-8859-1";
	}
	
	[Test(), Category("Online")]
	public void FetchOnlineSEBFonds()
	{
		SetupSEBFond("http://www.seb.se/pow/fmk/2100/Senaste_fondkurserna.TXT");
		foq = new FetchOnlineQuote (qss, "SEB Life - Optionsrätter Europa" );
		foq.GetOnlineQuote();
		Assert.IsTrue(foq.Success); 
		Assert.AreEqual("SEB Life - Optionsrätter Europa", foq.Symbol); 		
	}

	[Test(), Category("Online")]
	public void WrongSymbol()
	{
		SetupSEBFond("http://www.seb.se/pow/fmk/2100/Senaste_fondkurserna.TXT");
		foq = new FetchOnlineQuote (qss, "ERIC" );
		foq.GetOnlineQuote();
		Assert.IsFalse(foq.Success); 
	}
		
		
	[Test()]
	public void FetchFileSEBFonds_EnglishChar()
	{
		SetupSEBFond(TestInit.TestPath + "/MPC.Test/OnlineQuotesTest/TestData/Senaste_fondkurserna.TXT");
		foq = new FetchOnlineQuote (qss, "SEB Life - Nordenfond" );
		foq.GetOnlineQuote();
		Assert.IsTrue(foq.Success); 
		Assert.AreEqual("SEB Life - Nordenfond", foq.Symbol); 		
		Assert.AreEqual(12.682, foq.Quote_value);
		Assert.AreEqual(12, foq.Date.Month); 		
		Assert.AreEqual(27, foq.Date.Day); 		
		Assert.AreEqual(2007, foq.Date.Year); 		
	}

	[Test()]
	public void FetchFileSEBFonds_SwedishChar()
	{
		SetupSEBFond(TestInit.TestPath + "/MPC.Test/OnlineQuotesTest/TestData/Senaste_fondkurserna.TXT");
		foq = new FetchOnlineQuote (qss, "SEB Life - Optionsrätter Europa" );
		foq.GetOnlineQuote();
		Assert.IsTrue(foq.Success); 
		Assert.AreEqual("SEB Life - Optionsrätter Europa", foq.Symbol); 		
		Assert.AreEqual(1099.565, foq.Quote_value);
		Assert.AreEqual(12, foq.Date.Month); 		
		Assert.AreEqual(27, foq.Date.Day); 		
		Assert.AreEqual(2007, foq.Date.Year); 		
	}

	[Test(), Category("Online")]
	[Ignore("skip LYNX for now")]		
	public void FetchOnlineSEBStruktObl()
	{
		SetupSEBStruktObl("http://www.seb.se/pow/BorsFinans/listor/id_bonds_struct.asp?emittent=Visa_EMITTENTLISTA=_typ=Visa_EMITTENTLISTA=_ffd=Visa_EMITTENTLISTA=2012-12-19");
		foq = new FetchOnlineQuote (qss, "EMTN 30" );
		foq.GetOnlineQuote();
		Assert.IsTrue(foq.Success); 
		Assert.AreEqual("EMTN 30", foq.Symbol); 		
	}

	[Test(), Category("ignore")]
	[Ignore("skip LYNX for now")]			
	public void FetchFileSEBStruktObl_UriDate()
	{
		SetupSEBStruktObl(TestInit.TestPath + "/MPC.Test/OnlineQuotesTest/TestData/id_bonds_struct.asp.html");	
		foq = new FetchOnlineQuote (qss, "EMTN 32" );
		foq.GetOnlineQuote();
		Assert.IsTrue(foq.Success); 
		Assert.AreEqual("EMTN 32", foq.Symbol); 		
		Assert.AreEqual(99.0, foq.Quote_value);
		Assert.AreEqual(12, foq.Date.Month); 		
		Assert.AreEqual(31, foq.Date.Day); 		
		Assert.AreEqual(2007, foq.Date.Year); 		
	}
	
	[Test()]
	[Ignore("skip LYNX for now")]			
	public void FetchFileSEBStruktOblTodaysDate()
	{
		SetupSEBStruktObl(TestInit.TestPath + "/MPC.Test/OnlineQuotesTest/TestData/id_bonds_struct.asp.html");	
		qss.Date_regexp = "<today>";
		foq = new FetchOnlineQuote (qss, "EMTN 32" );
		foq.GetOnlineQuote();
		Assert.IsTrue(foq.Success); 
		Assert.AreEqual("EMTN 32", foq.Symbol); 		
		Assert.AreEqual(99.0, foq.Quote_value);
		Assert.AreEqual(System.DateTime.Now.Month, foq.Date.Month); 		
		Assert.AreEqual(System.DateTime.Now.Day, foq.Date.Day); 		
		Assert.AreEqual(System.DateTime.Now.Year, foq.Date.Year); 		
	}


}	
}

/*

//TODO All swedish funds
//TODO PPM Swedish funds
//TODO Currencies

// Currency
http://finance.yahoo.com/d/quotes.csv?s=%1%2=X_f=sl1d1


*/

