// CurrencyStoreTest.cs
// 
// Copyright (C) 2008 Bengt Thuree
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

//FIXME Need to clear all data in Setup, also need to tie in with data to SecurityStore

using System;
using System.IO;
using NUnit.Framework;

using MPC;
using MPC.Quotes;
using MPC.Utils;
using System.Data;
using Npgsql;

namespace CurrencyTest
{
	
	
[TestFixture()]
public class CurrencyStoreTest
{
	public Currency c, c2, c3, c4, c_tmp, c_tmp2;
	public CurrencyStore cs;
	int first_index;			
	public string TestPathBase = "/home/bengt/Development/mpc/";
	public string TestPathTag = "trunk";
	public string TestPath;
	

	[TestFixtureSetUp]
	public void OpenDatabaseConnection()
	{
		TestPath = String.Format("{0}/{1}", TestPathBase, TestPathTag);
	
		MPC.Database.SetTestDB();
	}

	[SetUp]
	public void MySetup()
	{
		c = new Currency("SEK");
		c.Country = "Swedish Krona";
		c.BasePool = true;
		c2 = new Currency("USD");
		c2.Country = "US Dollars";	
		c3 = new Currency("AUD");
		c3.Country = "Australian Dollar";
		c3.BasePool = true;
		cs = MPC.Database.GetDatabase.Currencies;
		cs.Del_All();
	}		
		
	[TestFixtureTearDown]
	public void CloseDatabaseConnection()
	{
	}
	
	[Test()]
	public void Add()
	{
		Assert.AreEqual (0, cs.Count() );					
		cs.Add (c);
		Assert.AreEqual (1, cs.Count() );		
	}
		
	[Test()]
	//[ExpectedException("MPC.Utils.NotUniqueException")]	
	[Ignore("WEIRD")]		
	public void Add_SameCurrency()
	{
		cs.Add (c);
		cs.Add (c);			
	}
		
	[Test()]
	public void Del_Id()
	{
		cs.Add (c);
		cs.Add (c2);	
		cs.Add (c3);			
		Assert.AreEqual (3, cs.Count() );			
		c_tmp = cs.Get (c2.Code);
		cs.Del (c_tmp.Id);
		Assert.AreEqual (2, cs.Count() );
		c_tmp = cs.Get (c.Code);
		Assert.AreEqual (c.Code, c_tmp.Code);			
		c_tmp = cs.Get (c3.Code);	
		Assert.AreEqual (c3.Code, c_tmp.Code);						
	}	
		
	[Test()]
	public void Del_Currency()
	{
		cs.Add (c);
		cs.Del (c.Code);
		Assert.AreEqual (0, cs.Count() );		
	}		

	[Test()]
	public void Del_Code()
	{
		cs.Add (c);
		c_tmp = cs.Get (c.Code);
		cs.Del (c_tmp.Code);
		Assert.AreEqual (0, cs.Count() );		
	}		
		
		
	[Test()]
	public void Del_All()
	{
		cs.Add (c);
		cs.Add (c2);
		cs.Add (c3);		
		cs.Del_All();			
		Assert.AreEqual  ( 0, cs.Count() );
	}		
		
	[Test()]
	public void Change_Currency()
	{
		cs.Add (c);
		c_tmp = cs.Get (c.Code);
		c_tmp.Country = c2.Country;
		c_tmp.Code = c2.Code;
		cs.Change (c_tmp);
		c_tmp2 = cs.Get (c_tmp.Code);
		Assert.AreEqual (c2.Code, c_tmp2.Code);	
		Assert.AreEqual (c2.Country, c_tmp2.Country);									
	}		

	[Test()]
	//[ExpectedException("MPC.Utils.NotUniqueException")]		
	[Ignore("WEIRD")]		
	public void Change_2ExistingCurrency()
	{
		cs.Add (c);
		cs.Add (c2);			
		c_tmp = cs.Get (c.Code);
		c_tmp.Country = c2.Country;
		c_tmp.Code = c2.Code;
		cs.Change (c_tmp);
	}								

	[Test()]
	public void Get_Id()
	{
		cs.Add (c);
		c_tmp = cs.Get (c.Code);
		first_index = c_tmp.Id;
		c_tmp = cs.Get (first_index);
		Assert.AreEqual (c.Code, c_tmp.Code);	
		Assert.AreEqual (c.Country, c_tmp.Country);									
	}				

	[Test()]
	public void Get_Code()
	{
		cs.Add (c);
		c_tmp = cs.Get (c.Code);
		Assert.AreEqual (c.Code, c_tmp.Code);	
		Assert.AreEqual (c.Country, c_tmp.Country);									
	}	

	[Test()]
	public void CountTest()
	{
		Assert.AreEqual  ( 0, cs.Count() );
		cs.Add (c);
		Assert.AreEqual  ( 1, cs.Count() );
		cs.Add (c2);	
		Assert.AreEqual  ( 2, cs.Count() );			
		cs.Add (c3);		
		Assert.AreEqual  ( 3, cs.Count() );	
			
		cs.Del (c.Code);			
		Assert.AreEqual  ( 2, cs.Count() );			
		cs.Del (c2.Code);
		Assert.AreEqual  ( 1, cs.Count() );
		cs.Del (c3.Code);
		Assert.AreEqual  ( 0, cs.Count() );									
	}		
	
	[Test()]
	public void GetList_Counts()
	{
		Assert.AreEqual (0, (cs.GetList()).Count);
		cs.Add (c) ;
		cs.Add (c2);	
		cs.Add (c3);		
		Assert.AreEqual (3, (cs.GetList()).Count);
		cs.Del (c.Code);
		Assert.AreEqual (2, (cs.GetList()).Count);			
		cs.Del (c3.Code);			
		cs.Del (c2.Code);						
		Assert.AreEqual (0, (cs.GetList()).Count);
	}	
	
	[Test()]
	public void GetList_Contents()
	{
		System.Collections.ArrayList c_db;
		cs.Add (c) ;
		cs.Add (c2);	
		cs.Add (c3);		
		c_db = cs.GetList();
		Assert.AreEqual ( c_db.Count, 3);
		Assert.AreEqual ( (c_db[0] as Currency).Code, c3.Code);
		Assert.AreEqual ( (c_db[1] as Currency).Code, c.Code);
		Assert.AreEqual ( (c_db[2] as Currency).Code, c2.Code);		
		cs.Del (c.Code);
		c_db = cs.GetList();
		Assert.AreEqual ( c_db.Count, 2);			
		Assert.AreEqual ( (c_db[0] as Currency).Code, c3.Code);
		Assert.AreEqual ( (c_db[1] as Currency).Code, c2.Code);		
	}	

	[Test()]
	public void UniqueIndex()
	{
		cs.Add (c) ;
		c_tmp = cs.Get (c.Code);
		first_index = c_tmp.Id;															
		c_tmp= cs.Get(first_index);
		Assert.AreEqual (first_index, c_tmp.Id);			
		cs.Add (c2);
		c_tmp= cs.Get(first_index+1);
		Assert.AreEqual (first_index+1, c_tmp.Id);	
		cs.Add (c3);
		c_tmp= cs.Get(first_index+2);
		Assert.AreEqual (first_index+2, c_tmp.Id);	
		cs.Del (first_index+1);
		c4 = new Currency ("HKD");
		cs.Add (c4);
		c_tmp= cs.Get(first_index+3);
		Assert.AreEqual (first_index+3, c_tmp.Id);				
	}		

	[Test()]
	public void Base_Currencies()
	{
		System.Collections.ArrayList c_db;
		cs.Add (c) ;
		cs.Add (c2);	
		cs.Add (c3);		
		c_db = cs.GetBaseCurrencies();
		Assert.AreEqual ( c_db.Count, 2);
		Assert.AreEqual ( (c_db[0] as Currency).Code, c3.Code);
		Assert.AreEqual ( (c_db[1] as Currency).Code, c.Code);		
	}	
	
	[Test()]
	public void Add_Defaults()
	{
		string test_csv_file   = TestPath + "/MPC.Test/OnlineQuotesTest/TestData/CurrencyList.csv";
		
		string wc_cmd = String.Format (" -l {0}", test_csv_file);

		System.Diagnostics.Process pr = new System.Diagnostics.Process();
		pr.StartInfo.FileName = "wc";
		pr.StartInfo.Arguments = wc_cmd;
		pr.StartInfo.UseShellExecute = false;
		pr.StartInfo.RedirectStandardOutput = true;
		pr.Start();   
		string tmp = pr.StandardOutput.ToString();
		string wc_page = pr.StandardOutput.ReadToEnd();
System.Console.WriteLine ("wc_page = >>{0}<<", wc_page);
System.Console.WriteLine ("tmp_page = >>{0}<<", tmp);
			System.Console.WriteLine ("file = >>{0}<<",test_csv_file );			
		pr.WaitForExit();
		pr.Close();
		
		cs.AddDefault(test_csv_file);
		int Expected_Rows = 169; // System.Convert.ToInt32 (wc_page);

System.Console.WriteLine ("Expected Rows = >>{0}<<", Expected_Rows);
		
		Assert.AreEqual  ( Expected_Rows, cs.Count() );			
	}	
	
}
}
