// OnlineQuotesTest.cs
// 
// Copyright (C) 2008 Bengt Thuree
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;
using System.IO;
using NUnit.Framework;

using MPC;
using MPC.Accounts;
using MPC.Quotes;
using MPC.Utils;
using System.Data;
using Npgsql;

namespace QuotesTest
{
	
	
	[TestFixture()]
	public class OnlineQuotesTest
	{
			



		public SecurityQuote q1, q2, q3, q4, q_tmp, q_tmp2;
		public QuoteSource qs1, qs2, qs3, qs4, qs_tmp, qs_tmp2;		
		public Currency c1, c2, c3, c4, c_tmp, c_tmp2;		
		public Security s1, s2, s3, s4, s_tmp, s_tmp2;
		public AddressDetail ad1, ad2, ad3, ad4, ad_tmp, ad_tmp2;
		public ContactDetail cd1, cd2, cd3, cd4, cd_tmp, cd_tmp2;
		public AccountHeader a1, a2, a3, a4, a_tmp, a_tmp2;		
		public InvestmentTransaction it1, it2, it3, it4, it5, it_tmp, it_tmp2;				
		
		public SecurityQuoteStore qs;
		public SecurityStore ss;
		public CurrencyStore cs;
		public QuoteSourceStore qss;
		public OnlineQuotes oq;
		public NumberOfSharesView nos_v;	
		public AccountHeaderStore a_s;				
		public AddressDetailsStore ad_s;
		public ContactDetailsStore cd_s;		
		public InvestmentTransactionStore its;				

		[TestFixtureSetUp]
		public void OpenDatabaseConnection()
		{
			MPC.Database.SetTestDB();
		}

		[SetUp]
		public void MySetup()
		{
			cs = MPC.Database.GetDatabase.Currencies;
			cs.Del_All();

			qss = MPC.Database.GetDatabase.QuoteSources;
			qss.Del_All();			
				
			ss = MPC.Database.GetDatabase.Securities;
			ss.Del_All();

			qs = MPC.Database.GetDatabase.SecurityQuotes;
			qs.Del_All();

			ad_s = MPC.Database.GetDatabase.AddressDetails;
			ad_s.Del_All();
			
			cd_s = MPC.Database.GetDatabase.ContactDetails;
			cd_s.Del_All();
				
			a_s = MPC.Database.GetDatabase.Accounts;
			a_s.Del_All();			

			its = MPC.Database.GetDatabase.InvestmentTransactions;
			its.Del_All();
			
			nos_v = MPC.Database.GetDatabase.GetNumberOfSharesView;
			
			oq = new OnlineQuotes();
				
			c1 = new Currency("SEK");
			c1.Country = "Swedish Krona";
			c2 = new Currency("USD");
			c2.Country = "US Dollar";
			c3 = new Currency("AUD");
			c3.Country = "Australian Dollar";

			cs.Add (c1);
			c1 = cs.Get (c1.Code);
			cs.Add (c2);
			c2 = cs.Get (c2.Code);
			cs.Add (c3);
			c3 = cs.Get (c3.Code);

			qs1 = new QuoteSource("Yahoo Global");
			qs1.Uri = "http://finance.yahoo.com/d/quotes.csv?s=%1&f=sl1d1";				
			qs1.Date_regexp = "[^,]*,[^,]*,\"([^\"]*)\"";
			qs1.Date_format = "M/d/yyyy";		
			qs1.Quote_regexp = "[^,]*,([^,]*),.*";
			qs1.Symbol_regexp = "\"([^,\"]*)\",.*";			
			
			qs2 = new QuoteSource ("SEB Fonds");
			qs2.Uri = "http://www.seb.se/pow/fmk/2100/Senaste_fondkurserna.TXT";
			qs2.Date_regexp = "^(.*);.*;.*;.*";
			qs2.Date_format = "yyyy-MM-dd";
			qs2.Quote_regexp = "^.*;.*;(.*);.*";
			qs2.Symbol_regexp = "^.*;(.*);.*;.*";
			qs2.Encoding = "ISO-8859-1";			
			
			qs3 = new QuoteSource ("SEB Strukt Obl");
			qs3.Uri = "http://www.seb.se/pow/BorsFinans/listor/id_bonds_struct.asp?emittent=Visa_EMITTENTLISTA=_typ=Visa_EMITTENTLISTA=_ffd=Visa_EMITTENTLISTA=2012-12-19";		
			qs3.Date_regexp = @"Uppdaterad: ([0-9-]*).*";
			qs3.Date_format = "yyyy-MM-dd";			
			qs3.Date_row_regexp = "Uppdaterad:";
			qs3.Quote_regexp = ".*SEB Hel Retail[ ]*[^ ]*[ ]*[^ ]*[ ]*[^ ]*[ ]*[^ ]*[ ]*([^ ]*) .*";
			qs3.Symbol_regexp = "[ ]*([.]*) SEB Hel Retail.*";
			qs3.Encoding = "ISO-8859-1";
			
			qs4 = new QuoteSource ("SEB Strukt Obl 2");
			qs4.Uri = "http://www.seb.se/pow/BorsFinans/listor/id_bonds_struct.asp?emittent=Visa&EMITTENTLISTA=SEB+Hel+Retail&typ=Visa&EMITTENTLISTA=&ffd=Visa&EMITTENTLISTA=";		
			qs4.Date_regexp = @"Uppdaterad: ([0-9-]*).*";
			qs4.Date_format = "yyyy-MM-dd";			
			qs4.Date_row_regexp = "Uppdaterad:";
			qs4.Quote_regexp = ".*SEB Hel Retail[ ]*[^ ]*[ ]*[^ ]*[ ]*[^ ]*[ ]*[^ ]*[ ]*([^ ]*) .*";
			qs4.Symbol_regexp = "[ ]*([.]*) SEB Hel Retail.*";
			qs4.Encoding = "ISO-8859-1";
				
			qss.Add (qs1);
			qs1 = qss.Get (qs1.Name);
			qss.Add (qs2);
			qs2 = qss.Get (qs2.Name);
			qss.Add (qs3);
			qs3 = qss.Get (qs3.Name);	
			qss.Add (qs4);
			qs4 = qss.Get (qs4.Name);											
				
			s1 = new Security("ERIC");
			s1.Name = "Ericsson";
			s1.Currency = c2.Id;
			s1.Quote_Source = qs1.Id;

			s2 = new Security("MSFT");
			s2.Name = "Microsoft";
			s2.Currency = c2.Id;
			s2.Quote_Source = qs1.Id;

			s3 = new Security("SEB Aktiesparfond");
			s3.Name = "SEB Europa Fond";
			s3.Currency = c1.Id;
			s3.Quote_Source = qs2.Id;

			s4 = new Security("AZN");
			s4.Name = "Amazon";
			s4.Currency = c2.Id;
			s4.Quote_Source = qs1.Id;

			s1.Id = ss.Add_GetIndex (s1);
			s2.Id = ss.Add_GetIndex (s2);
			s3.Id = ss.Add_GetIndex (s3);
			s4.Id = ss.Add_GetIndex (s4);
			
			ad1 = new AddressDetail();
			ad1.Swift = "swift";
			ad1.Phone = "Phone";
			ad1.Fax = "Fax";
			ad1.Name = "Name";
			ad1.Address1 = "Address1";
			ad1.Address2 = "Address2";
			ad1.Zip	= "zip";
			ad1.City = "city";
			ad1.Country = "country";
			ad1.Uri = "uri";
			ad1.Notes = "notes";
			ad1.Id = ad_s.Add_GetIndex(ad1);

			cd1 = new ContactDetail();
			cd1.Name = "cdBengt";
			cd1.Phone = "cdPhone";
			cd1.Mobile = "cdMobile";
			cd1.Other_phone = "cdOtherPhone";
			cd1.Fax = "Fax";
			cd1.Work_mail = "cdWorkMail";
			cd1.Private_mail = "cdPrivateMail";
			cd1.Other_mail = "cdOtherMail";
			cd1.Address_id	= ad1.Id;
			cd1.Uri = "cdUri";
			cd1.Notes = "cdNotes";
			cd1.Id = cd_s.Add_GetIndex (cd1);
			
			a1 = new AccountHeader();
			a1.Name = "Saving";
			a1.Address_id = ad1.Id;
			a1.Contact_id = cd1.Id;
			a1.Account_nr = "123456789";
			a1.Currency_id = c1.Id;
			a1.Id = a_s.Add_GetIndex (a1);

			it1 = new InvestmentTransaction();
			it1.InvestmentAccount = a1.Id;
			it1.TimeStamp = new DateTime(2006, 10, 10, 0, 0, 0);
			it1.Memo = "it1";
			it1.Security_id = s1.Id;
			it1.Shares = 100;
			it1.Share_price = 0;
			it1.Reconsiled = false;	
			it1.Id = its.Add_GetIndex (it1);
			it2 = it1;
			it2.Security_id = s2.Id;
			it2.Id = its.Add_GetIndex (it2);
			it3 = it1;
			it3.Security_id = s3.Id;
			it3.Id = its.Add_GetIndex (it3);			
			it4 = it1;
			it4.Security_id = s4.Id;
			it4.Id = its.Add_GetIndex (it4);
		}

		[TestFixtureTearDown]
		public void CloseDatabaseConnection()
		{
		}

		[Test(), Category("Online")]
		public void Add_Security_Quote()
		{
			Assert.AreEqual  ( 0, qs.Count() );						
			oq.GetQuote(s1);
			Assert.AreEqual  ( 1, qs.Count() );	
		}

		[Test(), Category("Online")]
		public void Add_QuoteSource_Quotes()
		{
			Assert.AreEqual  ( 0, qs.Count() );						
			oq.GetQuote(qs1);
			Assert.AreEqual  ( 3, qs.Count() );			
		}
		
		[Test(), Category("Online")]
		public void Add_All_Quotes()
		{
			Assert.AreEqual  ( 0, qs.Count() );						
			oq.GetQuote();
			Assert.AreEqual  ( 4, qs.Count() );			
		}		
			
	}
}
