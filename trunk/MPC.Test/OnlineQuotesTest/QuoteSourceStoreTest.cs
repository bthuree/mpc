// QuoteSourceStoreTest.cs
// 
// Copyright (C) 2008 Bengt Thuree
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

//FIXME Need to clear all data in Setup, also need to tie in with data to SecurityStore

using System;
using System.IO;
using NUnit.Framework;

using MPC;
using MPC.Quotes;
using MPC.Utils;
using System.Data;
using Npgsql;

namespace QuotesTest
{
	
	
[TestFixture()]
public class QuoteSourceStoreTest
{
	public QuoteSource qs, qs2, qs3, qs_tmp, qs_tmp2;
	public QuoteSourceStore qss;
	int first_index;			

	[TestFixtureSetUp]
	public void OpenDatabaseConnection()
	{
			MPC.Database.SetTestDB();
	}

	[SetUp]
	public void MySetup()
	{
		qs = new QuoteSource("Yahoo global");
		qs.Uri = "http://finance.yahoo.com/d/quotes.csv?s=%1&f=sl1d1";	
		qs2 = new QuoteSource("Yahoo local");
		qs2.Uri = "http://finance.yahoo.se/d/quotes.csv?s=%1&f=sl1d1";	
		qs3 = new QuoteSource("Yahoo Denmark");
		qs3.Uri = "http://finance.yahoo.dk/d/quotes.csv?s=%1&f=sl1d1";						
		qss = MPC.Database.GetDatabase.QuoteSources;
		qss.Del_All();
	}		
		
	[TestFixtureTearDown]
	public void CloseDatabaseConnection()
	{
	}
	
	[Test()]
	public void Add()
	{
		Assert.AreEqual (0, qss.Count() );					
		qss.Add (qs);
		Assert.AreEqual (1, qss.Count() );		
	}
	
	[Test()]
	//[ExpectedException("MPC.Utils.NotUniqueException")]
	[Ignore("WEIRD")]				
	public void Add_SameName()
	{
		qss.Add (qs);
		qss.Add (qs);
	}

	[Test()]
	public void Add_2qs()
	{
		qss.Add (qs);
		Assert.AreEqual (1, qss.Count() );								
		qss.Add (qs2);
		Assert.AreEqual (2, qss.Count() );					
	}
		
	[Test()]
	[Ignore("WEIRD2")]				
	public void Add_EmptyName()
	{
		qs.Name = "";
		qss.Add (qs);
		qs.Name = null;
		qss.Add (qs);	
		qs.Name = String.Empty;
		qss.Add (qs) ;						
	}		

	[Test()]
	public void Del_qs()
	{
		qss.Add (qs);
		qs_tmp = qss.Get (qs.Name);			
		qss.Del (qs_tmp);
		Assert.AreEqual (0, qss.Count() );								
	}

	[Test()]
	public void Del_id()
	{
		qss.Add (qs) ;
		qs_tmp = qss.Get (qs.Name);
		first_index = qs_tmp.Id;									
		qss.Del (first_index) ;
		Assert.AreEqual (0, qss.Count() );											
	}
	
	[Test()]
	public void Del_Name()
	{
		qss.Add (qs) ;
		qss.Del (qs.Name) ;	
		Assert.AreEqual (0, qss.Count() );											
	}
		
	[Test()]
	[Ignore("WEIRD2")]				
	public void Del_NonExistingName()
	{
		qss.Del (qs.Name);
		qss.Add (qs) ;
		qss.Del (qs.Name);
		qss.Del (qs.Name);
	}

	[Test()]
	[Ignore("WEIRD2")]				
	public void Del_NonExistingId()
	{
		qss.Add (qs);
		qs_tmp = qss.Get (qs.Name);
		first_index = qs_tmp.Id;	
		int NonExistingId = first_index + 10;
		qss.Del (NonExistingId);
		qss.Del (first_index);
		qss.Del (first_index);
		
	}

	[Test()]
	public void Del_Add2Del2()
	{
		qss.Add (qs);
		qss.Add (qs2);	
		Assert.AreEqual (2, qss.Count() );														
		qss.Del (qs2.Name);			
		qss.Del (qs.Name);	
		Assert.AreEqual (0, qss.Count() );														
	}

	[Test()]
	public void Del_All()
	{
		qss.Add (qs);
		qss.Add (qs2);
		qss.Add (qs3);		
		qss.Del_All();			
		Assert.AreEqual  ( 0, qss.Count() );
	}
			
	[Test()]
	public void UniqueIndex()
	{
		qss.Add (qs);
		qs_tmp = qss.Get (qs.Name);
		first_index = qs_tmp.Id;
		qss.Add (qs2);
		qss.Del (qs.Name);
		qss.Add (qs3);
		qs_tmp = qss.Get(qs3.Name);
		Assert.AreEqual (first_index + 2, qs_tmp.Id);				
	}		
		
	[Test()]
	public void ChangeNameUri()
	{
		qss.Add (qs);
		qs_tmp = qss.Get (qs.Name);
		Assert.AreEqual (qs_tmp.Name, qs.Name);
		Assert.AreEqual (qs_tmp.Uri, qs.Uri); 				
		qs_tmp.Name = "SEB";
		qs_tmp.Uri ="http://seb.se";
		qss.Change(qs_tmp);
		qs_tmp2 = qss.Get (qs_tmp.Id);
		Assert.AreEqual ("SEB", qs_tmp2.Name);
		Assert.AreEqual ("http://seb.se", qs_tmp2.Uri); 		
	}

	[Test()]
	public void ChangeAdd2Change1()
	{
		qss.Add (qs);
		qss.Add (qs2);					
		qs_tmp = qss.Get(qs2.Name);
		qs_tmp.Name = "SEB";
		qs_tmp.Uri ="http://seb.se";
		qss.Change(qs_tmp);
		qs_tmp2 = qss.Get (qs.Name);
		Assert.AreEqual (qs_tmp2.Name, qs.Name);
		Assert.AreEqual (qs_tmp2.Uri, qs.Uri); 		
		qs_tmp = qss.Get (qs.Name);
		Assert.AreEqual (qs_tmp.Name, qs.Name);
		Assert.AreEqual (qs_tmp.Uri, qs.Uri); 		
	}
		
	[Test()]
	//[ExpectedException("MPC.Utils.NotUniqueException")]
	[Ignore("WEIRD")]				
	public void Change_NotUnique()
	{
		qss.Add (qs);
		qss.Add (qs2);								
		qs_tmp = qss.Get(qs2.Name);
		qs_tmp.Name = qs.Name;
		qs_tmp.Uri =qs.Uri;
		qss.Change(qs_tmp);
	}
				
	[Test()]
	//[ExpectedException("MPC.Utils.NotFound")]
	[Ignore("WEIRD")]				
	public void Get_NotFoundName()
	{
		qs_tmp = qss.Get(qs.Name);
	}		
		
	[Test()]
	//[ExpectedException("MPC.Utils.NotFound")]
	[Ignore("WEIRD")]				
	public void Get_NotFoundId()
	{
		qs_tmp = qss.Get(0);
	}				
		
	[Test()]
	public void Get_Name()
	{
		qss.Add (qs);
		qs_tmp = qss.Get   ( qs.Name );
		Assert.AreEqual ( qs_tmp.Name, qs.Name);
		Assert.AreEqual ( qs_tmp.Uri, qs.Uri);	
	}

	[Test()]
	public void Get_Add2GetName1()
	{
		qss.Add (qs);
		qss.Add (qs2);											
		qs_tmp = qss.Get(qs.Name);
		Assert.AreEqual (qs_tmp.Name, qs.Name);
	}
		
	[Test()]
	public void Get_Add2GetName2()
	{
		qss.Add (qs);
		qss.Add (qs2);											
		qs_tmp = qss.Get(qs2.Name);
		Assert.AreEqual (qs_tmp.Name, qs2.Name);
	}
		
	[Test()]
	public void Get_Id()
	{
		qss.Add (qs);
		qs_tmp = qss.Get (qs.Name);
		first_index = qs_tmp.Id;		
		qs_tmp = qss.Get   ( first_index );
		Assert.AreEqual ( qs_tmp.Name, qs.Name);
		Assert.AreEqual ( qs_tmp.Uri, qs.Uri);	
	}

	[Test()]
	public void Get_Add2GetId1()
	{
		qss.Add (qs);
		qs_tmp = qss.Get (qs.Name);
		first_index = qs_tmp.Id;		
		qss.Add (qs2);											
		qs_tmp = qss.Get(first_index);
		Assert.AreEqual (qs_tmp.Name, qs.Name);
	}
		
	[Test()]
	public void Get_Add2GetId2()
	{
		qss.Add (qs);
		qs_tmp = qss.Get (qs.Name);
		first_index = qs_tmp.Id;				
		qss.Add (qs2);											
		qs_tmp = qss.Get(first_index+1);
		Assert.AreEqual (qs_tmp.Name, qs2.Name);
	}
		
		
	[Test()]
	public void CountTest()
	{
		Assert.AreEqual  ( 0, qss.Count() );
		qss.Add (qs);
		Assert.AreEqual  ( 1, qss.Count() );
		qss.Add (qs2);	
		Assert.AreEqual  ( 2, qss.Count() );			
		qss.Add (qs3);		
		Assert.AreEqual  ( 3, qss.Count() );	
			
		qss.Del (qs.Name);			
		Assert.AreEqual  ( 2, qss.Count() );			
		qss.Del (qs2.Name);
		Assert.AreEqual  ( 1, qss.Count() );
		qss.Del (qs3.Name);
		Assert.AreEqual  ( 0, qss.Count() );									
	}		
	
	[Test()]
	public void GetList_Count()
	{
		Assert.AreEqual (0, (qss.GetList()).Count);
		qss.Add (qs);
		qss.Add (qs2);	
		qss.Add (qs3);		
		Assert.AreEqual (3, (qss.GetList()).Count);
		qss.Del (qs.Name);
		Assert.AreEqual (2, (qss.GetList()).Count);			
		qss.Del (qs3.Name);			
		qss.Del (qs2.Name);						
		Assert.AreEqual (0, (qss.GetList()).Count);
	}
	
	[Test()]
	public void GetList_Contents()
	{
		System.Collections.ArrayList qs_db;
		qss.Add (qs);
		qss.Add (qs2);	
		qss.Add (qs3);		
		qs_db = qss.GetList();
		Assert.AreEqual ( qs_db.Count, 3);						
		Assert.AreEqual ( (qs_db[0] as QuoteSource).Name, qs3.Name);
		Assert.AreEqual ( (qs_db[1] as QuoteSource).Name, qs.Name);
		Assert.AreEqual ( (qs_db[2] as QuoteSource).Name, qs2.Name);		
		qss.Del (qs.Name);
		qs_db = qss.GetList();
		Assert.AreEqual ( qs_db.Count, 2);						
		Assert.AreEqual ( (qs_db[0] as QuoteSource).Name, qs3.Name);
		Assert.AreEqual ( (qs_db[1] as QuoteSource).Name, qs2.Name);		
	}	
	
}
}
